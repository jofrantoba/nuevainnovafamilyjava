package com.innova.nif.models.wrappers;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class MensajeFiltroTest {
    @Test
    public void testValoresPorDefectoNuevaInstancia() {
       MensajeFiltro mensajeFiltro = new MensajeFiltro();
        Assert.assertEquals(0, mensajeFiltro.getIdPersona());
        Assert.assertEquals(0, mensajeFiltro.getIdCorreo());
        Assert.assertEquals(0, mensajeFiltro.getIdMensaje());
        Assert.assertEquals(0, mensajeFiltro.getIdMensajeDestino());
        Assert.assertEquals(0, mensajeFiltro.getTotalPagina());
        Assert.assertEquals("", mensajeFiltro.getFiltro());
    }
}
