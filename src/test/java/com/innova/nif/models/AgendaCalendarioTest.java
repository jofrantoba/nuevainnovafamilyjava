package com.innova.nif.models;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class AgendaCalendarioTest {

    @Test
    public void testIdsArchivosEliminar() {
        AgendaCalendario agendaCalendario = new AgendaCalendario();
        agendaCalendario.setListaArchivosEliminar("1,2,3,,");
        List<Integer> respuesta = new ArrayList<>();
        respuesta.add(1);
        respuesta.add(2);
        respuesta.add(3);
        Assert.assertEquals(respuesta, agendaCalendario.idsArchivosEliminar());
        agendaCalendario.setListaArchivosEliminar(",,,");
        Assert.assertEquals(new ArrayList<Integer>(), agendaCalendario.idsArchivosEliminar());
    }
}