package com.innova.nif.services;

import com.innova.nif.models.DetalleTarea;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

public class AlertaAgendaServiceTest {
    @InjectMocks
    AlertaAgendaService service;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGenerarIdGrados() {
        List<DetalleTarea> detalleTareas = new ArrayList<>();
        DetalleTarea detalleTarea = new DetalleTarea();
        detalleTarea.setIdGrado(1);
        detalleTareas.add(detalleTarea);
        String ids = service.generarIdGrados(detalleTareas);
        Assert.assertEquals(",1,", ids);
    }
}