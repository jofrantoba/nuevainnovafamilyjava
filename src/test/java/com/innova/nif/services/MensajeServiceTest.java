package com.innova.nif.services;

import com.innova.nif.models.MensajeDestino;
import com.innova.nif.models.Persona;
import com.innova.nif.utils.Constantes;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;

public class MensajeServiceTest {
    @InjectMocks
    MensajeService service;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testEsGrupoDestino() {
        MensajeDestino mensajeDestino = new MensajeDestino();
        Persona destinatario = new Persona();
        destinatario.setGrupo("A");
        mensajeDestino.setDestinatario(destinatario);
        Assert.assertFalse(service.esGrupoDestino(mensajeDestino));
        destinatario.setGrupo(Constantes.GRUPO_DESTINO_ALUMNO_SEDE);
        Assert.assertTrue(service.esGrupoDestino(mensajeDestino));
    }
}