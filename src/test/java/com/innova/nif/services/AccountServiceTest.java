package com.innova.nif.services;

import com.innova.nif.models.Usuario;
import com.innova.nif.repositories.interfaces.IUserRepository;
import javassist.NotFoundException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;

@RunWith(SpringRunner.class)
public class AccountServiceTest {
    @Mock
    IUserRepository userRepository;
    @InjectMocks
    AccountService service;

    @Test
    public void testGetPassword() throws ParseException, NotFoundException {
        Usuario usuario = new Usuario();
        usuario.setIdPersona(1);
        usuario.setClave("123456");
        Mockito.when(userRepository.usuarioInformacionRecuperarContrasena(1, "PF")).thenReturn(usuario);

        Usuario response = service.obtenerUsuarioRecuperarContrasena(1, "PF");
        Assert.assertNotNull(response.getClave());
        Assert.assertEquals("123456", response.getClave());
    }


}
