package com.innova.nif.services;

import com.innova.nif.models.BackOfficeParametro;
import com.innova.nif.models.BackOfficeParametros;
import com.innova.nif.repositories.interfaces.IBackOfficeRepository;
import com.innova.nif.services.backofficebuilder.BaseBackOfficeQueryBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

public class BackOfficeServiceTest {
    @InjectMocks
    BackOfficeService service;
    @Mock
    IBackOfficeRepository repository;
    private BackOfficeParametros filtros;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(repository.obtenerConteoPorConsulta("total 3")).thenReturn(3);
        when(repository.obtenerConteoPorConsulta("total 4")).thenReturn(4);
    }

    @Before
    public void tearDown() {
        filtros = new BackOfficeParametros();
    }

    @Test
    public void testContieneFiltroSeleccionarTodosPrimerRol() {
        BackOfficeParametro rol = new BackOfficeParametro();
        rol.setParametroId(BackOfficeParametros.ALL_CODE);
        List<BackOfficeParametro> roles = new ArrayList<>();
        roles.add(rol);
        rol = new BackOfficeParametro();
        rol.setParametroId(BackOfficeParametros.ALUMNO_CODE);
        roles.add(rol);
        filtros.setRoles(roles);
        Assert.assertTrue(service.contieneFiltroSeleccionarTodosPrimerRol(filtros));
        roles = new ArrayList<>();
        rol = new BackOfficeParametro();
        rol.setParametroId(BackOfficeParametros.ALUMNO_CODE);
        roles.add(rol);
        filtros.setRoles(roles);
        Assert.assertFalse(service.contieneFiltroSeleccionarTodosPrimerRol(filtros));
    }

    @Test
    public void testContieneFiltroSeleccionarPPFF() {
        BackOfficeParametro rol = new BackOfficeParametro();
        rol.setParametroId(BackOfficeParametros.ALL_CODE);
        List<BackOfficeParametro> roles = new ArrayList<>();
        roles.add(rol);
        rol = new BackOfficeParametro();
        rol.setParametroId(BackOfficeParametros.PADRE_FAMILIA_CODE);
        roles.add(rol);
        filtros.setRoles(roles);
        Assert.assertTrue(service.contieneFiltroSeleccionarPPFF(filtros));
        roles = new ArrayList<>();
        rol = new BackOfficeParametro();
        rol.setParametroId(BackOfficeParametros.ALUMNO_CODE);
        roles.add(rol);
        filtros.setRoles(roles);
        Assert.assertFalse(service.contieneFiltroSeleccionarPPFF(filtros));
    }

    @Test
    public void testContieneFiltroSeleccionarDocente() {
        BackOfficeParametro rol = new BackOfficeParametro();
        rol.setParametroId(BackOfficeParametros.ALL_CODE);
        List<BackOfficeParametro> roles = new ArrayList<>();
        roles.add(rol);
        rol = new BackOfficeParametro();
        rol.setParametroId(BackOfficeParametros.DOCENTE_CODE);
        roles.add(rol);
        filtros.setRoles(roles);
        Assert.assertTrue(service.contieneFiltroSeleccionarDocente(filtros));
        roles = new ArrayList<>();
        rol = new BackOfficeParametro();
        rol.setParametroId(BackOfficeParametros.ALL_CODE);
        roles.add(rol);
        filtros.setRoles(roles);
        Assert.assertFalse(service.contieneFiltroSeleccionarDocente(filtros));
    }

    @Test
    public void testContieneFiltroSeleccionarAlumno() {
        BackOfficeParametro rol = new BackOfficeParametro();
        rol.setParametroId(BackOfficeParametros.ALL_CODE);
        List<BackOfficeParametro> roles = new ArrayList<>();
        roles.add(rol);
        rol = new BackOfficeParametro();
        rol.setParametroId(BackOfficeParametros.ALUMNO_CODE);
        roles.add(rol);
        filtros.setRoles(roles);
        Assert.assertTrue(service.contieneFiltroSeleccionarAlumno(filtros));
        roles = new ArrayList<>();
        rol = new BackOfficeParametro();
        rol.setParametroId(BackOfficeParametros.ALL_CODE);
        roles.add(rol);
        filtros.setRoles(roles);
        Assert.assertFalse(service.contieneFiltroSeleccionarAlumno(filtros));
    }

    @Test
    public void testContieneFiltroSeleccionarTodos() {
        BackOfficeParametro rol = new BackOfficeParametro();
        rol.setParametroId(BackOfficeParametros.ALUMNO_CODE);
        List<BackOfficeParametro> roles = new ArrayList<>();
        roles.add(rol);
        rol = new BackOfficeParametro();
        rol.setParametroId(BackOfficeParametros.ALL_CODE);
        roles.add(rol);
        filtros.setRoles(roles);
        Assert.assertTrue(service.contieneFiltroSeleccionarTodos(filtros));
        roles = new ArrayList<>();
        rol = new BackOfficeParametro();
        rol.setParametroId(BackOfficeParametros.ALUMNO_CODE);
        roles.add(rol);
        filtros.setRoles(roles);
        Assert.assertFalse(service.contieneFiltroSeleccionarTodos(filtros));
    }

    @Test
    public void testObtenerConteoPorConsultasSql() {
        List<String> consultasSql = new ArrayList<>();
        consultasSql.add("total 3");
        consultasSql.add("total 4");
        Assert.assertEquals(7, service.obtenerConteoPorConsultasSql(consultasSql));
    }

    @Test
    public void testObtenerConsultasConteoFiltros() {
        String consultaSql = "SELECT COUNT(ppff.id_persona_vinculada) AS 'count' FROM (SELECT distinct vinculo_familiar.id_persona_vinculada FROM datamanagement.aca_alumno alumno INNER JOIN datamanagement.gen_vinculo_familiar vinculo_familiar ON vinculo_familiar.id_persona = alumno.id_persona LEFT OUTER JOIN datamanagement.aca_seccion seccion on alumno.id_seccion = seccion.id INNER JOIN datamanagement.aca_grado grado on alumno.id_grado = grado.id INNER JOIN datamanagement.gen_persona persona on persona.id = vinculo_familiar.id_persona_vinculada WHERE vinculo_familiar.estado='ACT' AND alumno.estado in ('MAT','MDOC','PMR') and vinculo_familiar.id_tipo_vinculo in (1,2) AND LENGTH(COALESCE(persona.email,''))>0 AND alumno.id_periodo_lectivo= 20 AND grado.codigo_ps IN ('01') AND seccion.seccion IN ('A') AND alumno.id_sede in (17) ) AS ppff";
        List<String> consultasSql = new ArrayList<>();
        consultasSql.add(consultaSql);
        BackOfficeParametros parametros = new BackOfficeParametros();
        parametros.setIdPeriodo(20);
        parametros.setEstadoPeriodo("ACT");
        List<BackOfficeParametro> roles = new ArrayList<>();
        BackOfficeParametro rol = new BackOfficeParametro();
        rol.setParametroId("PF");
        rol.setNombre("Padre de familia");
        rol.setTipo(null);
        roles.add(rol);
        parametros.setRoles(roles);
        List<BackOfficeParametro> sedes = new ArrayList<>();
        BackOfficeParametro sede = new BackOfficeParametro();
        sede.setParametroId("17");
        sede.setNombre("Los Olivos 1 - Villa Sol");
        sede.setTipo(null);
        sedes.add(sede);
        parametros.setSedes(sedes);
        List<BackOfficeParametro> secciones = new ArrayList<>();
        BackOfficeParametro seccion = new BackOfficeParametro();
        seccion.setParametroId("A");
        seccion.setNombre("A");
        seccion.setTipo(null);
        secciones.add(seccion);
        parametros.setSecciones(secciones);
        List<BackOfficeParametro> grados = new ArrayList<>();
        BackOfficeParametro grado = new BackOfficeParametro();
        grado.setParametroId( "01");
        grado.setNombre("1° Primaria");
        grado.setTipo(null);
        grados.add(grado);
        parametros.setGrados(grados);
        Assert.assertEquals(consultasSql, service.obtenerConsultas(parametros, BaseBackOfficeQueryBuilder.COUNT_QUERY));
    }
}