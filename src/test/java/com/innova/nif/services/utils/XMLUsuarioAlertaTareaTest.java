package com.innova.nif.services.utils;

import com.innova.nif.models.AlertaUsuario;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.util.ArrayList;
import java.util.List;

public class XMLUsuarioAlertaTareaTest {
    private String xml;
    private List<AlertaUsuario> userAlertas;
    private XMLUsuarioAlertaTarea xmlUsuarioAlertaTarea;

    @Before
    public void setUp() throws ParserConfigurationException {
        xml = "<BEUserAlerta>" +
                "<Usuario>" +
                "<IdAlertaAgenda>10</IdAlertaAgenda>" +
                "<IdPersona>1</IdPersona>" +
                "<IdPersonaAlumno>2</IdPersonaAlumno>" +
                "<InicialesAlumno>CRLP</InicialesAlumno>" +
                "<Perfil>PF</Perfil>" +
                "</Usuario>" +
                "<Usuario>" +
                "<IdAlertaAgenda>10</IdAlertaAgenda>" +
                "<IdPersona>3</IdPersona>" +
                "<IdPersonaAlumno>4</IdPersonaAlumno>" +
                "<InicialesAlumno>JPLQ</InicialesAlumno>" +
                "<Perfil>AL</Perfil>" +
                "</Usuario>" +
                "</BEUserAlerta>";
        fakeData();
        xmlUsuarioAlertaTarea = new XMLUsuarioAlertaTarea(userAlertas, 10);
    }

    @Test
    public void testConvertirAString() throws TransformerException {
        String xmlString = xmlUsuarioAlertaTarea.convertirAString();
        Assert.assertEquals(xml, xmlString);
    }

    private void fakeData() {
        userAlertas = new ArrayList<>();
        AlertaUsuario userAlerta = new AlertaUsuario();
        userAlerta.setIdPersona(1);
        userAlerta.setPerfil("PF");
        userAlerta.setIdPersonaAlumno(2);
        userAlerta.setInicialesAlumno("CRLP");
        userAlertas.add(userAlerta);
        userAlerta = new AlertaUsuario();
        userAlerta.setIdPersona(3);
        userAlerta.setPerfil("AL");
        userAlerta.setIdPersonaAlumno(4);
        userAlerta.setInicialesAlumno("JPLQ");
        userAlertas.add(userAlerta);
    }
}