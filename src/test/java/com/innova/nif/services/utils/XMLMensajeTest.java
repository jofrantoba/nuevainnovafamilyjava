package com.innova.nif.services.utils;

import com.innova.nif.models.MensajeApp;
import com.innova.nif.models.MensajeDestino;
import com.innova.nif.models.Persona;
import org.junit.Assert;
import org.junit.Test;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class XMLMensajeTest {

    @Test
    public void convertirAString() throws ParserConfigurationException, TransformerException {
        MensajeApp mensajeApp = new MensajeApp();
        mensajeApp.setIdMensaje(1039319);
        ArrayList<MensajeDestino> mensajeDestinoList = new ArrayList<>();
        MensajeDestino mensajeDestino = new MensajeDestino();
        Persona destinatario = new Persona();
        destinatario.setCodigoPersona(28110);
        destinatario.setEmail("test_rocio.delgado@innovaschools.edu.pe");
        destinatario.setGrupo("DC");
        mensajeDestino.setIdMensaje(1039319);
        mensajeDestino.setIdSecuencia(1);
        mensajeDestino.setIdDestino(28110);
        mensajeDestino.setIdTipoDestino("PARA");
        mensajeDestino.setLeido(false);
        mensajeDestino.setEsFavorito(false);
        mensajeDestino.setEsEliminado(false);
        mensajeDestino.setGrupo("DC");
        mensajeDestino.setCorreo("test_rocio.delgado@innovaschools.edu.pe");
        mensajeDestino.setGradoSeccion("");
        mensajeDestino.setIdMensajeDestino(0);
        mensajeDestino.setDestinatario(destinatario);
        mensajeDestinoList.add(mensajeDestino);
        mensajeApp.setMensajeDestino(mensajeDestinoList);
        XMLMensaje xmlMensaje = new XMLMensaje(mensajeApp);
        String xml = "<BEMensajeDestino>" +
                "<Destinatario IdMensajeDestino=\"0\">" +
                "<AlertaRevisada>false</AlertaRevisada>" +
                "<Correo>test_rocio.delgado@innovaschools.edu.pe</Correo>" +
                "<EsEliminado>false</EsEliminado>" +
                "<EsFavorito>false</EsFavorito>" +
                "<GradoSeccion/>" +
                "<Grupo>DC</Grupo>" +
                "<IdDestino>28110</IdDestino>" +
                "<IdMensaje>1039319</IdMensaje>" +
                "<IdSecuencia>1</IdSecuencia>" +
                "<IdTipoDestino>PARA</IdTipoDestino>" +
                "<Leido>false</Leido>" +
                "</Destinatario>" +
                "</BEMensajeDestino>";
        Assert.assertEquals(xml, xmlMensaje.convertirAString());
    }
}