package com.innova.nif.services.utils;

import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.junit.Assert;
import org.junit.Test;

public class AgendaFechaHoraBuilderTest {

    private static final String FECHA_INICIO = "2019-03-01";

    @Test
    public void testConstruirFecha() {
        LocalDateTime localDateTime = LocalDateTime.parse(FECHA_INICIO);
        DateTime fechaInicio = new DateTime(localDateTime.toDateTime());
        DateTime fechaFin = new DateTime(localDateTime.toDateTime());
        AgendaFechaHoraBuilder builder = new AgendaFechaHoraBuilder(fechaInicio, fechaFin);
        Assert.assertEquals("viernes 1 marzo", builder.construirFecha());
        fechaInicio = new DateTime(localDateTime.toDateTime());
        localDateTime = LocalDateTime.parse("2019-03-02");
        fechaFin = new DateTime(localDateTime.toDateTime());
        builder = new AgendaFechaHoraBuilder(fechaInicio, fechaFin);
        Assert.assertEquals("Del 01/03 al 02/03", builder.construirFecha());
    }

    @Test
    public void testConstruirHora() {
        LocalDateTime startLocalDateTime = LocalDateTime.parse(FECHA_INICIO);
        LocalDateTime endLocalDateTime = LocalDateTime.parse("2019-03-02");
        DateTime fechaInicio = new DateTime(startLocalDateTime.toDateTime());
        DateTime fechaFin = new DateTime(endLocalDateTime.toDateTime());
        AgendaFechaHoraBuilder builder = new AgendaFechaHoraBuilder(fechaInicio, fechaFin);
        Assert.assertEquals("", builder.construirHora());
        startLocalDateTime = LocalDateTime.parse("2019-03-01T09:30");
        fechaInicio = new DateTime(startLocalDateTime.toDateTime());
        builder = new AgendaFechaHoraBuilder(fechaInicio, fechaFin);
        Assert.assertEquals(", a partir de la(s) 09:30", builder.construirHora());
        endLocalDateTime = LocalDateTime.parse("2019-03-02T09:30");
        fechaFin = new DateTime(endLocalDateTime.toDateTime());
        builder = new AgendaFechaHoraBuilder(fechaInicio, fechaFin);
        Assert.assertEquals(", a partir de la(s) 09:30", builder.construirHora());
        endLocalDateTime = LocalDateTime.parse("2019-03-02T17:30");
        fechaFin = new DateTime(endLocalDateTime.toDateTime());
        builder = new AgendaFechaHoraBuilder(fechaInicio, fechaFin);
        Assert.assertEquals(", de 09:30 a 17:30", builder.construirHora());
        startLocalDateTime = LocalDateTime.parse(FECHA_INICIO);
        fechaInicio = new DateTime(startLocalDateTime.toDateTime());
        builder = new AgendaFechaHoraBuilder(fechaInicio, fechaFin);
        Assert.assertEquals("", builder.construirHora());
    }
}