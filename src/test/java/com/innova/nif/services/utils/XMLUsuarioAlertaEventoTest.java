package com.innova.nif.services.utils;

import com.innova.nif.models.AlertaUsuario;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class XMLUsuarioAlertaEventoTest {
    private String xml;
    private List<AlertaUsuario> userAlertas;
    private XMLUsuarioAlertaEvento xmlUsuarioAlertaEvento;

    @Before
    public void setUp() throws ParserConfigurationException {
        xml = "<BEUserAlerta>" +
                "<Usuario>" +
                "<IdAlertaAgenda>10</IdAlertaAgenda>" +
                "<IdPersona>1</IdPersona>" +
                "<Perfil>PF</Perfil>" +
                "</Usuario>" +
                "<Usuario>" +
                "<IdAlertaAgenda>10</IdAlertaAgenda>" +
                "<IdPersona>3</IdPersona>" +
                "<Perfil>AL</Perfil>" +
                "</Usuario>" +
                "</BEUserAlerta>";
        fakeData();
        xmlUsuarioAlertaEvento = new XMLUsuarioAlertaEvento(userAlertas, 10);
    }

    @Test
    public void testConvertirAString() throws TransformerException {
        String xmlString = xmlUsuarioAlertaEvento.convertirAString();
        Assert.assertEquals(xml, xmlString);
    }

    private void fakeData() {
        userAlertas = new ArrayList<>();
        AlertaUsuario userAlerta = new AlertaUsuario();
        userAlerta.setIdPersona(1);
        userAlerta.setPerfil("PF");
        userAlerta.setIdPersonaAlumno(2);
        userAlerta.setInicialesAlumno("CRLP");
        userAlertas.add(userAlerta);
        userAlerta = new AlertaUsuario();
        userAlerta.setIdPersona(3);
        userAlerta.setPerfil("AL");
        userAlerta.setIdPersonaAlumno(4);
        userAlerta.setInicialesAlumno("JPLQ");
        userAlertas.add(userAlerta);
    }
}