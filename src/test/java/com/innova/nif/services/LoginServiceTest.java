package com.innova.nif.services;

import com.innova.nif.models.Sede;
import com.innova.nif.models.Usuario;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class LoginServiceTest {
    @InjectMocks
    LoginService service;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testObtenerSedesDeUsuario() {
        List<Usuario> sedesUsuarios = new ArrayList<>();
        Usuario usuarioSede = new Usuario();
        usuarioSede.setIdSede(1);
        final String sede1 = "Sede 1";
        usuarioSede.setNombreSede(sede1);
        sedesUsuarios.add(usuarioSede);
        usuarioSede = new Usuario();
        usuarioSede.setIdSede(2);
        usuarioSede.setNombreSede("Sede 2");
        sedesUsuarios.add(usuarioSede);
        usuarioSede = new Usuario();
        usuarioSede.setIdSede(3);
        final String sede3 = "Sede 3";
        usuarioSede.setNombreSede(sede3);
        sedesUsuarios.add(usuarioSede);
        Usuario usuario = new Usuario();
        usuario.setIdSede(1);
        usuario.setNombreSede(sede1);
        List<Sede> sedes = service.obtenerSedesDeUsuario(usuario, sedesUsuarios);
        Assert.assertEquals(3, sedes.size());
        Sede sedeSelected = sedes.stream().filter(Sede::isSelected).findFirst().orElse(null);
        assert sedeSelected != null;
        Assert.assertEquals(1, sedeSelected.getId());

        usuario = new Usuario();
        usuario.setIdSede(3);
        usuario.setNombreSede(sede3);
        sedesUsuarios = new ArrayList<>();
        usuarioSede = new Usuario();
        usuarioSede.setIdSede(1);
        usuarioSede.setNombreSede(sede1);
        sedesUsuarios.add(usuarioSede);
        usuarioSede = new Usuario();
        usuarioSede.setIdSede(1);
        usuarioSede.setNombreSede(sede1);
        sedesUsuarios.add(usuarioSede);
        usuarioSede = new Usuario();
        usuarioSede.setIdSede(3);
        usuarioSede.setNombreSede(sede3);
        sedesUsuarios.add(usuarioSede);
        sedes = service.obtenerSedesDeUsuario(usuario, sedesUsuarios);
        Assert.assertEquals(2, sedes.size());
        sedeSelected = sedes.stream().filter(Sede::isSelected).findFirst().orElse(null);
        assert sedeSelected != null;
        Assert.assertEquals(3, sedeSelected.getId());
    }

    @Test
    public void testGenerarCargosUsuarios() {
        List<Usuario> usuariosDM = new ArrayList<>();
        Usuario usuario = new Usuario();
        usuario.setCargo("Tutor 4A");
        usuariosDM.add(usuario);
        usuario = new Usuario();
        usuario.setCargo("Matemática");
        usuariosDM.add(usuario);
        Assert.assertEquals("Tutor 4A | Matemática", service.generarCargosUsuarios(usuariosDM));

        usuario = new Usuario();
        usuario.setCargo("Matemática");
        usuariosDM.add(usuario);
        usuario = new Usuario();
        usuario.setCargo("Tutor 4A");
        usuariosDM.add(usuario);
        Assert.assertEquals("Tutor 4A | Matemática", service.generarCargosUsuarios(usuariosDM));
    }
}