package com.innova.nif.repositories.utils;

import com.innova.nif.models.wrappers.FiltroWrapper;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class ParametrosDocenteTest {

    private static final String FLAG_ALERTA = "FLAG_ALERTA";
    private static final String NIVEL = "NIVEL";
    private static final String ID_PERIODO = "ID_PERIODO";
    private static final String ID_GRADO = "ID_GRADO";

    @Test
    public void testConsultarDCPorGrado() {
        FiltroWrapper filtros = new FiltroWrapper();
        Map<String, Object> filtrosValor = new HashMap<>();
        filtrosValor.put(FLAG_ALERTA, 2);
        filtrosValor.put(NIVEL, "Primaria");
        filtrosValor.put(ID_PERIODO, 20);
        filtrosValor.put("ID_SEDE", 5);
        filtrosValor.put("ID_CURSO", 10);
        filtrosValor.put(ID_GRADO, 15);

        filtros.setFlagAlerta((Integer) filtrosValor.get(FLAG_ALERTA));
        filtros.setNivel((String) filtrosValor.get(NIVEL));
        filtros.setIdPeriodo((Integer) filtrosValor.get(ID_PERIODO));
        filtros.setIdSede((Integer) filtrosValor.get("ID_SEDE"));
        filtros.setIdCurso((Integer) filtrosValor.get("ID_CURSO"));
        filtros.setIdGrado((Integer) filtrosValor.get(ID_GRADO));

        ParametrosDocente parametrosDocente = new ParametrosDocente(filtros);
        Assert.assertEquals("NIF_sp_ConsultarDCPorGrado", parametrosDocente.getNombreProcedimiento());
        Assert.assertFalse(parametrosDocente.isFiltrarCursoSolo());
        Assert.assertEquals(4, parametrosDocente.getParametros().getValues().size());
    }

    @Test
    public void testConsultarDC() {
        FiltroWrapper filtros = new FiltroWrapper();
        Map<String, Object> filtrosValor = new HashMap<>();
        filtrosValor.put(FLAG_ALERTA, 2);
        filtrosValor.put(NIVEL, "");
        filtrosValor.put("ID_SECCION", 15);

        filtros.setFlagAlerta((Integer) filtrosValor.get(FLAG_ALERTA));
        filtros.setNivel((String) filtrosValor.get(NIVEL));
        filtros.setIdSeccion((Integer) filtrosValor.get("ID_SECCION"));

        ParametrosDocente parametrosDocente = new ParametrosDocente(filtros);
        Assert.assertEquals("NIF_sp_ConsultarDC", parametrosDocente.getNombreProcedimiento());
        Assert.assertTrue(parametrosDocente.isFiltrarCursoSolo());
        Assert.assertEquals(1, parametrosDocente.getParametros().getValues().size());
    }

    @Test
    public void testConsultarDCAlerta() {
        FiltroWrapper filtros = new FiltroWrapper();
        Map<String, Object> filtrosValor = new HashMap<>();
        filtrosValor.put(FLAG_ALERTA, 1);
        filtrosValor.put(ID_PERIODO, 20);
        filtrosValor.put(ID_GRADO, 15);

        filtros.setFlagAlerta((Integer) filtrosValor.get(FLAG_ALERTA));
        filtros.setIdPeriodo((Integer) filtrosValor.get(ID_PERIODO));
        filtros.setIdGrado((Integer) filtrosValor.get(ID_GRADO));

        ParametrosDocente parametrosDocente = new ParametrosDocente(filtros);
        Assert.assertEquals("NIF_sp_ConsultarDCAlerta", parametrosDocente.getNombreProcedimiento());
        Assert.assertTrue(parametrosDocente.isFiltrarCursoSolo());
        Assert.assertEquals(3, parametrosDocente.getParametros().getValues().size());
    }
}