package com.innova.nif.repositories.factories.agenda;

import com.innova.nif.models.wrappers.AgendaFiltro;
import com.innova.nif.utils.Constantes;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import static org.junit.Assert.*;

public class ConsultaAgendaAlumnoTest {
    private static final String INTERMEDIO = "Intermedio";
    private static final String SECCION = "Seccion";
    private static final String NIVEL_INGLES = "NivelIngles";
    private ConsultaAgendaAlumno repositorio;

    @Before
    public void setUp() {
        repositorio = new ConsultaAgendaAlumno(null);
    }

    @Test
    public void testNombreProcedimientoAlmacenado() {
        AgendaFiltro filtros = new AgendaFiltro();
        filtros.setGrupo("A");
        filtros.setTipoAgenda(Constantes.TIPO_AGENDA_TAREA);
        Assert.assertEquals("SP_ListaTareaA", repositorio.nombreProcedimientoAlmacenado(filtros));
        filtros.setTipoAgenda(Constantes.TIPO_AGENDA_EVENTO);
        Assert.assertEquals("SP_ListaEventoA", repositorio.nombreProcedimientoAlmacenado(filtros));
        filtros.setTipoAgenda("");
        Assert.assertEquals("SP_ListaAgendaA", repositorio.nombreProcedimientoAlmacenado(filtros));
        filtros.setNivelIngles(INTERMEDIO);
        filtros.setTipoAgenda(Constantes.TIPO_AGENDA_TAREA);
        Assert.assertEquals("SP_ListaTareaInglesAL", repositorio.nombreProcedimientoAlmacenado(filtros));
        filtros.setTipoAgenda(Constantes.TIPO_AGENDA_EVENTO);
        Assert.assertEquals("SP_ListaAgendaInglesAL", repositorio.nombreProcedimientoAlmacenado(filtros));
    }

    @Test
    public void testAgregarParametrosPorTipo() {
        AgendaFiltro filtros = new AgendaFiltro();
        MapSqlParameterSource parametros = new MapSqlParameterSource();
        filtros.setTipoAgenda(Constantes.TIPO_AGENDA_TAREA);
        filtros.setNivelIngles(INTERMEDIO);
        filtros.setSeccion("1");
        repositorio.agregarParametrosPorTipo(parametros, filtros);
        Assert.assertTrue(parametros.hasValue(SECCION));
        Assert.assertEquals("1", parametros.getValue(SECCION));
        Assert.assertTrue(parametros.hasValue(NIVEL_INGLES));
        parametros = new MapSqlParameterSource();
        filtros.setTipoAgenda(Constantes.TIPO_AGENDA_EVENTO);
        filtros.setNivelIngles("");
        repositorio.agregarParametrosPorTipo(parametros, filtros);
        Assert.assertFalse(parametros.hasValue(SECCION));
        Assert.assertFalse(parametros.hasValue(NIVEL_INGLES));
        filtros.setNivelIngles(INTERMEDIO);
        parametros = new MapSqlParameterSource();
        repositorio.agregarParametrosPorTipo(parametros, filtros);
        Assert.assertTrue(parametros.hasValue(NIVEL_INGLES));
        Assert.assertEquals(INTERMEDIO, parametros.getValue(NIVEL_INGLES));
        Assert.assertFalse(parametros.hasValue(SECCION));
    }
}