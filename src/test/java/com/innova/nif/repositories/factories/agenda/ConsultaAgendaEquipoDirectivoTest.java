package com.innova.nif.repositories.factories.agenda;

import com.innova.nif.models.wrappers.AgendaFiltro;
import com.innova.nif.utils.Constantes;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class ConsultaAgendaEquipoDirectivoTest {

    @Test
    public void nombreProcedimientoAlmacenado() {
        ConsultaAgendaEquipoDirectivo repositorio = new ConsultaAgendaEquipoDirectivo(null);
        AgendaFiltro filtros = new AgendaFiltro();
        filtros.setTipoAgenda(Constantes.TIPO_AGENDA_TAREA);
        filtros.setGrupo("A");
        Assert.assertEquals("SP_ListaTareaA", repositorio.nombreProcedimientoAlmacenado(filtros));
        filtros.setTipoAgenda(Constantes.TIPO_AGENDA_EVENTO);
        Assert.assertEquals("SP_ListaEventoA", repositorio.nombreProcedimientoAlmacenado(filtros));
        filtros.setTipoAgenda("");
        Assert.assertEquals("SP_ListaAgendaA", repositorio.nombreProcedimientoAlmacenado(filtros));
    }
}