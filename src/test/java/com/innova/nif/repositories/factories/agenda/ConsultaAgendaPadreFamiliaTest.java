package com.innova.nif.repositories.factories.agenda;

import com.innova.nif.models.wrappers.AgendaFiltro;
import com.innova.nif.utils.Constantes;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import static org.junit.Assert.*;

public class ConsultaAgendaPadreFamiliaTest {
    private static final String SEDE_GRADO_SECCION = "sedeGradoSeccion";
    private static final String SEDE_GRADO = "sedeGrado";
    private ConsultaAgendaPadreFamilia repositorio;

    @Before
    public void setUp() {
        repositorio = new ConsultaAgendaPadreFamilia(null);
    }

    @Test
    public void testNombreProcedimientoAlmacenado() {
        AgendaFiltro filtros = new AgendaFiltro();
        filtros.setTipoAgenda(Constantes.TIPO_AGENDA_TAREA);
        filtros.setGrupo("A");
        Assert.assertEquals("SP_ListaTareaA", repositorio.nombreProcedimientoAlmacenado(filtros));
        filtros.setTipoAgenda(Constantes.TIPO_AGENDA_EVENTO);
        Assert.assertEquals("SP_ListaEventoA", repositorio.nombreProcedimientoAlmacenado(filtros));
        filtros.setTipoAgenda("");
        Assert.assertEquals("SP_ListaAgendaA", repositorio.nombreProcedimientoAlmacenado(filtros));
    }

    @Test
    public void testAgregarParametrosPorTipo() {
        AgendaFiltro filtros = new AgendaFiltro();
        filtros.setTipoAgenda(Constantes.TIPO_AGENDA_TAREA);
        final String idSecciones = "1,2,3";
        filtros.setListaSeccion(idSecciones);
        MapSqlParameterSource parametros = new MapSqlParameterSource();
        repositorio.agregarParametrosPorTipo(parametros, filtros);
        Assert.assertTrue(parametros.hasValue(SEDE_GRADO_SECCION));
        Assert.assertEquals(idSecciones, parametros.getValue(SEDE_GRADO_SECCION));
        Assert.assertFalse(parametros.hasValue(SEDE_GRADO));
        parametros = new MapSqlParameterSource();
        filtros.setTipoAgenda(Constantes.TIPO_AGENDA_EVENTO);
        filtros.setListaIdGrado(idSecciones);
        repositorio.agregarParametrosPorTipo(parametros, filtros);
        Assert.assertTrue(parametros.hasValue(SEDE_GRADO));
        Assert.assertEquals(idSecciones, parametros.getValue(SEDE_GRADO));
        Assert.assertFalse(parametros.hasValue(SEDE_GRADO_SECCION));
    }
}