package com.innova.nif.repositories.factories.agenda;

import com.innova.nif.models.wrappers.AgendaFiltro;
import com.innova.nif.utils.Constantes;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import static org.junit.Assert.*;

public class ConsultaAgendaDocenteTest {
    public static final String ID_PERSONA = "IdPersona";
    public static final String GRADOS = "grados";
    private ConsultaAgendaDocente repositorio;

    @Before
    public void setUp() {
        repositorio = new ConsultaAgendaDocente(null);
    }

    @Test
    public void testNombreProcedimientoAlmacenado() {
        AgendaFiltro filtros = new AgendaFiltro();
        filtros.setTipoAgenda(Constantes.TIPO_AGENDA_TAREA);
        filtros.setGrupo("A");
        Assert.assertEquals("SP_ListaTareaA", repositorio.nombreProcedimientoAlmacenado(filtros));
        filtros.setTipoAgenda(Constantes.TIPO_AGENDA_EVENTO);
        Assert.assertEquals("SP_ListaEventoA", repositorio.nombreProcedimientoAlmacenado(filtros));
        filtros.setTipoAgenda("");
        Assert.assertEquals("SP_ListaAgendaA", repositorio.nombreProcedimientoAlmacenado(filtros));
    }

    @Test
    public void testAgregarParametrosPorTipo() {
        AgendaFiltro filtros = new AgendaFiltro();
        filtros.setTipoAgenda(Constantes.TIPO_AGENDA_TAREA);
        filtros.setIdPersona(123);
        MapSqlParameterSource parametros = new MapSqlParameterSource();
        repositorio.agregarParametrosPorTipo(parametros, filtros);
        Assert.assertTrue(parametros.hasValue(ID_PERSONA));
        Assert.assertEquals(123, parametros.getValue(ID_PERSONA));
        Assert.assertFalse(parametros.hasValue(GRADOS));
        filtros.setTipoAgenda(Constantes.TIPO_AGENDA_EVENTO);
        filtros.setListaIdGrado("1,2,3");
        parametros = new MapSqlParameterSource();
        repositorio.agregarParametrosPorTipo(parametros, filtros);
        Assert.assertTrue(parametros.hasValue(GRADOS));
        Assert.assertEquals("1,2,3", parametros.getValue(GRADOS));
        Assert.assertFalse(parametros.hasValue(ID_PERSONA));
    }
}