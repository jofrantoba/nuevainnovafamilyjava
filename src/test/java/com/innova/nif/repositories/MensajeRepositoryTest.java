package com.innova.nif.repositories;

import com.innova.nif.repositories.factories.mensaje.MensajePaginadoFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class MensajeRepositoryTest {
    private MensajeRepository repository;

    @Before
    public void setUp() {
        repository = new MensajeRepository();
    }

    @Test
    public void testMensajesPaginadoProcedimiento() {
        String nombreProcedimiento = repository.nombreProcedimientoPorTipo("", "");
        Assert.assertEquals("SP_ConsultaBandejaPaginado", nombreProcedimiento);
    }

    @Test
    public void testMensajesPaginadoPorFiltroProcedimiento() {
        String nombreProcedimiento = repository
                .nombreProcedimientoPorTipo(MensajePaginadoFactory.MENSAJES_PAGINADOS, "true");
        Assert.assertEquals("SP_ConsultaBandejaPaginadoPorFiltro", nombreProcedimiento);
        nombreProcedimiento = repository
                .nombreProcedimientoPorTipo(MensajePaginadoFactory.MENSAJES_PAGINADOS, "");
        Assert.assertEquals("SP_ConsultaBandejaPaginado", nombreProcedimiento);
    }

    @Test
    public void testMensajesEnviadosPaginadoPorFiltroProcedimiento() {
        String nombreProcedimiento = repository
                .nombreProcedimientoPorTipo(MensajePaginadoFactory.MENSAJES_ENVIADOS_PAGINADOS, "true");
        Assert.assertEquals("SP_ConsultaBandejaPaginadoEnviadosPorFiltro", nombreProcedimiento);
        nombreProcedimiento = repository
                .nombreProcedimientoPorTipo(MensajePaginadoFactory.MENSAJES_ENVIADOS_PAGINADOS, "");
        Assert.assertEquals("SP_ConsultaBandejaPaginadoEnviados", nombreProcedimiento);
    }

    @Test
    public void testMensajesEliminadosPaginadoPorFiltroProcedimiento() {
        String nombreProcedimiento = repository
                .nombreProcedimientoPorTipo(MensajePaginadoFactory.MENSAJES_ELIMINADOS_PAGINADOS, "true");
        Assert.assertEquals("SP_ConsultaBandejaPaginadoEliminadosPorFiltro", nombreProcedimiento);
        nombreProcedimiento = repository
                .nombreProcedimientoPorTipo(MensajePaginadoFactory.MENSAJES_ELIMINADOS_PAGINADOS, "");
        Assert.assertEquals("SP_ConsultaBandejaPaginadoEliminados", nombreProcedimiento);
    }
}
