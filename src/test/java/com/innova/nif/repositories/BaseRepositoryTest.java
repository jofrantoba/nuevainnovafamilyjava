package com.innova.nif.repositories;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class BaseRepositoryTest {

    @Test
    public void generarLogProcedimiento() {
        var repoBase = new BaseRepository();
        String procedimiento = "SP_Prueba";
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue("id", 1)
                .addValue("nombre", "Juan Carlos")
                .addValue("apellidos", "Pérez López");
        var resultadoLog = "Error al ejecutar: " + procedimiento + " con parámetros: (id=" + 1 + "," + "nombre=Juan Carlos,apellidos=Pérez López)";
        Assert.assertEquals(resultadoLog, repoBase.generarLogProcedimiento(procedimiento, parametros));
    }
}