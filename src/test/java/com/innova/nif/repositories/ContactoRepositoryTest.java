package com.innova.nif.repositories;

import com.innova.nif.models.Contacto;
import com.innova.nif.models.Docente;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ContactoRepositoryTest {
    @InjectMocks
    ContactoRepository repository;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testObtenerDocentesUnicos() {
        List<Docente> docentes = new ArrayList<>();
        Docente contacto = new Docente();
        contacto.setIdPersona(2);
        docentes.add(contacto);
        contacto = new Docente();
        contacto.setIdPersona(2);
        docentes.add(contacto);
        contacto = new Docente();
        contacto.setIdPersona(3);
        docentes.add(contacto);
        contacto = new Docente();
        contacto.setIdPersona(1);
        docentes.add(contacto);
        List<Docente> contactosUnicos = repository.obtenerDocentesUnicos(docentes);
        Assert.assertEquals(3, contactosUnicos.size());
        int[] ordenIdPersonas = {2, 3, 1};
        for (int i = 0; i < contactosUnicos.size(); i++)
            Assert.assertEquals(ordenIdPersonas[i], contactosUnicos.get(i).getIdPersona());
    }
}