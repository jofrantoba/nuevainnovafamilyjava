package com.innova.nif.api.controllers;

import com.innova.nif.models.Usuario;
import com.innova.nif.services.interfaces.IAccountService;
import com.innova.nif.services.interfaces.IEmailService;
import com.innova.nif.services.interfaces.IPeriodoService;
import com.innova.nif.services.interfaces.IUsuarioService;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class AccountControllerTest {
    @Mock
    IAccountService serviceMock;
    @Mock
    IEmailService correoServiceMock;
    @Mock
    IPeriodoService periodoServiceMock;
    @Mock
    IUsuarioService usuarioServiceMock;

    @Test
    public void testUsuarioHaSolicitadoRecuperarRecientemente() {
        DateTime fechaHoraActual = DateTime.now();
        Usuario usuario = new Usuario();
        usuario.setRecoverPasswordDate(fechaHoraActual.plusMinutes(-6));
        Assert.assertFalse(
                new AccountController(serviceMock, correoServiceMock, periodoServiceMock, usuarioServiceMock).usuarioHaSolicitadoRecuperarRecientemente(usuario)
        );
        usuario = new Usuario();
        Assert.assertFalse(
                new AccountController(serviceMock, correoServiceMock, periodoServiceMock, usuarioServiceMock).usuarioHaSolicitadoRecuperarRecientemente(usuario)
        );
    }

    @Test
    public void testusUarioNoHaSolicitadoRecuperarRecientemente() {
        DateTime fechaHoraActual = DateTime.now();
        Usuario usuario = new Usuario();
        usuario.setRecoverPasswordDate(fechaHoraActual);
        Assert.assertTrue(
                new AccountController(serviceMock, correoServiceMock, periodoServiceMock, usuarioServiceMock).usuarioHaSolicitadoRecuperarRecientemente(usuario)
        );
    }
}
