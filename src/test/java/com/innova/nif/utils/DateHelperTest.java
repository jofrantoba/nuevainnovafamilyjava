package com.innova.nif.utils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.Calendar;
import java.sql.Date;

public class DateHelperTest {
    private Date fechaReferencial;

    @Before
    public void setUp() {
        Calendar calendarDate;
        calendarDate = Calendar.getInstance();
        calendarDate.set(Calendar.YEAR, 2019);
        calendarDate.set(Calendar.MONTH, Calendar.MARCH);
        calendarDate.set(Calendar.DATE, 15);
        calendarDate.set(Calendar.HOUR_OF_DAY, 14);
        calendarDate.set(Calendar.MINUTE, 30);
        calendarDate.set(Calendar.SECOND, 0);
        calendarDate.set(Calendar.MILLISECOND, 0);
        fechaReferencial = new Date(calendarDate.getTime().getTime());
    }

    @Test
    public void testFechaComoString() {
        Assert.assertEquals("03/15/2019", DateHelper.fechaComoString(fechaReferencial));
    }

    @Test
    public void testHoraComoString() {
        Timestamp timestamp = new Timestamp(fechaReferencial.getTime());
        Assert.assertEquals("02:30 PM", DateHelper.horaComoString(timestamp));
    }

    @Test
    public void testFormatoISO() {
        org.joda.time.DateTime jodaDateTime = new org.joda.time.DateTime(fechaReferencial);
        Assert.assertEquals("2019-03-15T14:30", DateHelper.formatoFullCalendar(jodaDateTime));
    }

    @Test
    public void testSqlDateTime() {
        Calendar calendarDate;
        calendarDate = Calendar.getInstance();
        calendarDate.set(Calendar.YEAR, 2019);
        calendarDate.set(Calendar.MONTH, Calendar.APRIL);
        calendarDate.set(Calendar.DATE, 1);
        calendarDate.set(Calendar.HOUR_OF_DAY, 14);
        calendarDate.set(Calendar.MINUTE, 30);
        calendarDate.set(Calendar.SECOND, 0);
        calendarDate.set(Calendar.MILLISECOND, 0);
        fechaReferencial = new Date(calendarDate.getTime().getTime());
        org.joda.time.DateTime jodaDateTime = new org.joda.time.DateTime(fechaReferencial);
        Assert.assertEquals("2019-04-01", DateHelper.convertirASQLDate(jodaDateTime));
    }
}