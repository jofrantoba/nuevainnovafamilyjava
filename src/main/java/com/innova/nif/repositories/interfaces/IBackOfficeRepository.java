package com.innova.nif.repositories.interfaces;

import java.util.List;

public interface IBackOfficeRepository {
    int obtenerConteoPorConsulta(String consultaSql);

    List<String> obtenerCodigoAlumnos(String consultaSql);

    int obtenerConteoPorConsultaDataManagement(String consultaSql);
}
