package com.innova.nif.repositories.interfaces;

import com.innova.nif.models.*;
import com.innova.nif.models.wrappers.FiltroWrapper;
import com.innova.nif.models.wrappers.MensajeFiltro;
import javassist.NotFoundException;

import java.util.List;

public interface IMensajeRepository {
    int consultarTotalMensajes(int idPersona, String grupo);

    List<MensajePaginado> consultarMensajesPaginado(MensajeFiltro mensajeFiltro);

    List detalleMensaje(int id);

    List obtenerAdjuntos(int idMensaje);

    List obtenerHistorial(int idCorreo, int idPersona);

    int actualizarMensaje(int id, String tipo, int idPersona);

    int eliminarMensaje(int idMensajeDestino, String tipoBandeja);

    MensajeDestino obtenerMensajeDestino(int idMensajeDestino) throws NotFoundException;

    List<Contacto> listaGruposAS(FiltroWrapper filtros);

    List<Contacto> listaGrupoDS(FiltroWrapper filtros);

    List<Contacto> listaGrupoPS(FiltroWrapper filtros);

    List<Contacto> listaGrupoAG(FiltroWrapper filtros);

    List<Contacto> listaGrupoDG(FiltroWrapper filtros);

    List<Contacto> listaGrupoPG(FiltroWrapper filtros);

    int insertarMensaje(MensajeApp mensajeApp);

    int insertarMensajeDestinatarioXML(String xmlMensajeDestino);

    int insertarAdjuntos(MensajeAdjunto m);
}
