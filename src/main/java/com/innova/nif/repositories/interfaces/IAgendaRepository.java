package com.innova.nif.repositories.interfaces;

import com.innova.nif.models.*;
import com.innova.nif.models.wrappers.AgendaFiltro;

import java.util.List;

public interface IAgendaRepository {
    EventoGeneral obtenerEventoEDDC(int idTareaEvento);

    List<AgendaAdjunto> adjuntosAgenda(AgendaFiltro filtros);

    TareaGeneral obtenerTarea(AgendaFiltro filtros);

    EventoGeneral obtenerEventoALPF(AgendaFiltro filtros);

    int registrarTarea(TareaCalendario agenda);

    DetalleTarea registrarDetalleTarea(DetalleTarea detalleTarea);

    int registrarEvento(EventoCalendario evento);

    void registrarDetalleEvento(DetalleEvento detalleEvento);

    void registrarAdjunto(AgendaAdjunto adjunto);

    List<String> obtenerGradosEvento(Integer idEvento);

    void editarEvento(EventoCalendario evento);

    void eliminarAdjuntos(List<Integer> idsArchivosEliminar);

    void registrarLogEvento(LogAgenda logEvento);

    void editarTarea(TareaCalendario tarea, DetalleTarea detalleTarea);

    void eliminarEvento(int idEvento);

    int eliminarTarea(int idTarea);

    Docente obtenerDocente(int idPersona);

    int registrarLogTarea(LogAgenda logTarea);

    int eliminarAdjunto(int idArchivo);
}