package com.innova.nif.repositories.interfaces;

import com.innova.nif.models.Persona;

public interface IAccountRepository extends IRepository {
    int updateUserPassword(Persona persona) ;
}
