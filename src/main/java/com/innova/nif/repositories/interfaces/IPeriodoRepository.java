package com.innova.nif.repositories.interfaces;

import com.innova.nif.models.PeriodoLectivo;

import java.util.List;

public interface IPeriodoRepository {
    PeriodoLectivo getPeriodoActivo();

    List<PeriodoLectivo> obtenerPeriodosPF();
}