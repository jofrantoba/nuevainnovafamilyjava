package com.innova.nif.repositories.interfaces;

import com.innova.nif.models.MensajeAdjunto;
import com.innova.nif.models.Persona;
import com.innova.nif.models.AlertaUsuario;
import com.innova.nif.models.Usuario;
import javassist.NotFoundException;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.List;

public interface IUserRepository {
    Usuario usuarioInformacionRecuperarContrasena(int idPersona, String perfil) throws ParseException, NotFoundException;

    MensajeAdjunto consultarFotoPerfil(int idPersona, String perfil);

    boolean actualizarContrasenaConHash(Persona persona);

    Usuario obtenerUsuarioPorContrasenaHash(String hash);

    int solicitarCambioDePassword(Usuario usuario) throws NoSuchAlgorithmException;

    Usuario obtenerUsuarioPF(String email, int idPeriodoActual) throws NotFoundException;

    Usuario obtenerUsuarioDm(String email, int idPeriodoActual, int idPeriodoPosterior) throws NotFoundException;

    boolean correoExiste(String email);

    boolean actualizarCorreo(Usuario usuario);

    Usuario obtenerUsuarioCambioContrasenaPorHash(String hash) throws NotFoundException;

    int insertarFoto(MensajeAdjunto fotoPerfil);

    boolean actualizarFoto(Persona persona);

    void actualizarEstadoSinNotificacion(Usuario usuario);

    void actualizarUsuarioNotificacion(Usuario usuario);

    List<AlertaUsuario> listarAlertasPorUsuarioTarea(int idSede, int idPeriodo, String idGrados);

    List<AlertaUsuario> listarAlertasPorUsuarioEvento(int idSede, int idPeriodo, String idGrados);

    int actualizarInicioSesion(Usuario usuario);
}