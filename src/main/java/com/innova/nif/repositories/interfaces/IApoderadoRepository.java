package com.innova.nif.repositories.interfaces;

import com.innova.nif.models.HijoApoderado;

import java.util.List;

public interface IApoderadoRepository {
    List<HijoApoderado> obtenerHijos(int idPersonaPadre, int idPeriodo);
}
