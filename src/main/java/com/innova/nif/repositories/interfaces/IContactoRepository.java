package com.innova.nif.repositories.interfaces;

import com.innova.nif.models.*;
import com.innova.nif.models.wrappers.FiltroWrapper;

import java.util.List;

public interface IContactoRepository {

    List<MensajeRemitente> listaCorreoRemitente(int idMensaje);

    List<Contacto> equipoDirectivoPorSede(int idSede);

    List<Docente> docentes(FiltroWrapper filtros);

    List<Grado> listaGrados();

    List<DocenteNivelIngles> obtenerNivelInglesDocente(FiltroWrapper filtros);

    List<Docente> docentePorDNI(String dni);

    List<Grado> listaGradosPorSedePeriodo(int idSede, int idPeriodo);

    List<SeccionAlumno> seccionesDM(FiltroWrapper filtros);

    List<Contacto> listarPadresFamilia(FiltroWrapper filtros);

    List<Contacto> listarAlumnos(FiltroWrapper filtros);
}
