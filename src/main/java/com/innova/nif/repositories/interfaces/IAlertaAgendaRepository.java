package com.innova.nif.repositories.interfaces;

import com.innova.nif.models.AlertaAgenda;

public interface IAlertaAgendaRepository {
    void registrarAlertaAgendaUsuarioTareaXML(String xml);

    int registrarAlertaAgenda(AlertaAgenda alerta);

    void registrarAlertaAgendaUsuarioEventoXML(String xml);
}
