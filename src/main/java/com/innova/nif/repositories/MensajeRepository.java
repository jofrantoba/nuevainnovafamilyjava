package com.innova.nif.repositories;

import com.innova.nif.models.*;
import com.innova.nif.models.wrappers.FiltroWrapper;
import com.innova.nif.models.wrappers.MensajeFiltro;
import com.innova.nif.repositories.factories.mensaje.*;
import com.innova.nif.repositories.interfaces.IMensajeRepository;
import com.innova.nif.utils.DateHelper;
import com.innova.nif.utils.StringHelper;
import javassist.NotFoundException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Repository
public class MensajeRepository extends BaseRepository implements IMensajeRepository {
    private static final String ID_MENSAJE = "IdMensaje";
    private static final String FECHA_REGISTRO = "FechaRegistro";
    private static final String GRUPO = "Grupo";
    private static final String MENSAJE = "Mensaje";
    private static final String ID_PERSONA = "IdPersona";
    private static final String RESPUESTA = "respuesta";
    private static final String ID_MENSAJE_DESTINO = "idMensajeDestino";
    private static final String ID_MENSAJE_DESTINO_CAPITALIZADO = "IdMensajeDestino";
    private static final String TOTAL_NO_LEIDOS = "TotalNoLeidos";
    private static final String ESTADO = "Estado";
    private static final String RESPUESTA_MAYUSCULA = "Respuesta";
    private static final String ID_CORREO = "IdCorreo";
    private static final String ASUNTO = "Asunto";
    private static final String ID_REMITENTE = "IdRemitente";
    private static final String FIRMA = "Firma";
    private static final String CON_COPIA = "ConCopia";
    private static final String REFERENCIA = "Referencia";
    private static final String CON_ADJUNTO = "ConAdjunto";
    private static final String P_ID_GRADO = "p_id_grado";
    private static final String P_ID_SEDE = "p_id_sede";
    private static final String P_ID_PERIODO = "p_id_periodo";
    private static final String CARGO = "Cargo";

    @Override
    public int consultarTotalMensajes(int idPersona, String grupo) {
        var parametros = new MapSqlParameterSource()
                .addValue(ID_PERSONA, idPersona)
                .addValue(GRUPO, grupo)
                .addValue(TOTAL_NO_LEIDOS, "");
        try {
            String[] rowValues = new String[]{TOTAL_NO_LEIDOS};
            var resultado = executeNifStoreProcedure("SP_ConsultarTotalCorreosNoLeidos", parametros,
                    rowValues);
            return (int) resultado.get(TOTAL_NO_LEIDOS);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_ConsultarTotalCorreosNoLeidos", parametros);
            throw ex;
        }
    }

    @Override
    public List<MensajePaginado> consultarMensajesPaginado(MensajeFiltro mensajeFiltro) {
        configurarFiltro(mensajeFiltro);
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue(ID_PERSONA, mensajeFiltro.getIdPersona())
                .addValue(GRUPO, mensajeFiltro.getGrupo())
                .addValue("Filtro", mensajeFiltro.getFiltro())
                .addValue("NumeroPagina", mensajeFiltro.getNumeroPagina())
                .addValue("TamanioPagina", mensajeFiltro.getTamanioPagina());
        final String nombreProcedimiento = nombreProcedimientoPorTipo(mensajeFiltro.getTipoBandeja(), mensajeFiltro.getFiltro());
        try {
            Map<String, Object> resultado = executeNifStoreProcedure(
                    nombreProcedimiento,
                    parametros, new MensajesPaginadoMapper(mensajeFiltro));
            return (List<MensajePaginado>) resultado.get(CUSTOM_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, nombreProcedimiento, parametros);
            throw ex;
        }
    }

    @Override
    public List detalleMensaje(int id) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue(ID_MENSAJE, id);
        try {
            Map<String, Object> resultado = executeNifStoreProcedure("SP_ConsultaDetalleMensaje",
                    parametros, new DetalleMensajePaginadoMapper());
            return (List<MensajePaginado>) resultado.get(CUSTOM_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_ConsultaDetalleMensaje", parametros);
            throw ex;
        }
    }

    @Override
    public List obtenerAdjuntos(int idMensaje) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue(ID_MENSAJE, idMensaje);
        try {
            Map<String, Object> resultado = executeNifStoreProcedure("SP_ConsultarAdjuntos", parametros,
                    new MensajeAdjuntosMapper());
            return (List<MensajeAdjunto>) resultado.get(CUSTOM_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_ConsultarAdjuntos", parametros);
            throw ex;
        }
    }

    @Override
    public List obtenerHistorial(int idCorreo, int idPersona) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue(ID_CORREO, idCorreo)
                .addValue(ID_PERSONA, idPersona);
        try {
            Map<String, Object> resultado = executeNifStoreProcedure("SP_ConsultarHistorial", parametros,
                    new MensajeHistorialMapper());
            return (List<MensajeAdjunto>) resultado.get(CUSTOM_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_ConsultarHistorial", parametros);
            throw ex;
        }
    }

    @Override
    public int actualizarMensaje(int id, String tipo, int idPersona) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue(ID_MENSAJE_DESTINO, id)
                .addValue("tipo", tipo)
                .addValue("idPersona", idPersona)
                .addValue(RESPUESTA, "");
        try {
            String[] rowValues = new String[]{RESPUESTA};
            Map<String, Object> resultado = executeNifStoreProcedure("SP_ActualizarMensajeDestino",
                    parametros, rowValues);
            return (int) resultado.get(RESPUESTA);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_ActualizarMensajeDestino", parametros);
            throw ex;
        }
    }

    @Override
    public int eliminarMensaje(int idMensajeDestino, String tipoBandeja) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue(ID_MENSAJE_DESTINO_CAPITALIZADO, idMensajeDestino)
                .addValue("TipoBandeja", tipoBandeja)
                .addValue(RESPUESTA_MAYUSCULA, "");
        try {
            String[] rowValues = new String[]{RESPUESTA_MAYUSCULA};
            Map<String, Object> resultado = executeNifStoreProcedure("SP_EliminarMensaje",
                    parametros, rowValues);
            return (int) resultado.get(RESPUESTA_MAYUSCULA);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_EliminarMensaje", parametros);
            throw ex;
        }
    }

    @Override
    public MensajeDestino obtenerMensajeDestino(int idMensajeDestino) throws NotFoundException {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue(ID_MENSAJE_DESTINO_CAPITALIZADO, idMensajeDestino);
        try {
            Map<String, Object> respuesta = executeNifStoreProcedure("SP_ObtenerMensajeDestino",
                    parametros, new MensajeDestinoMapper());
            List<MensajeDestino> resultado = (List<MensajeDestino>) respuesta.get(CUSTOM_RESULT_SET);
            if (!resultado.isEmpty())
                return resultado.get(0);
            throw new NotFoundException(String.format("El mensaje con id: %s no fue encontrado", idMensajeDestino));
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_ObtenerMensajeDestino", parametros);
            throw ex;
        }
    }

    @Override
    public List<Contacto> listaGruposAS(FiltroWrapper filtros) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue(P_ID_SEDE, filtros.getIdSede())
                .addValue(P_ID_PERIODO, filtros.getIdPeriodo());
        try {
            Map<String, Object> resultado = executeDMStoreProcedure("nif_sp_ListaGrupoAS", parametros,
                    new GrupoContactoMapper());
            return (List<Contacto>) resultado.get(CUSTOM_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, "nif_sp_ListaGrupoAS", parametros);
            throw ex;
        }
    }

    @Override
    public List<Contacto> listaGrupoDS(FiltroWrapper filtros) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue(P_ID_SEDE, filtros.getIdSede())
                .addValue(P_ID_PERIODO, filtros.getIdPeriodo());
        try {
            Map<String, Object> resultado = executeDMStoreProcedure("nif_sp_ListaGrupoDS", parametros,
                    new GrupoContactoMapper());
            return (List<Contacto>) resultado.get(CUSTOM_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, "nif_sp_ListaGrupoDS", parametros);
            throw ex;
        }
    }

    @Override
    public List<Contacto> listaGrupoPS(FiltroWrapper filtros) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue(P_ID_SEDE, filtros.getIdSede())
                .addValue(P_ID_PERIODO, filtros.getIdPeriodo());
        try {
            Map<String, Object> resultado = executeDMStoreProcedure("nif_sp_ListaGrupoPS", parametros,
                    new GrupoContactoPSMapper());
            return (List<Contacto>) resultado.get(CUSTOM_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, "nif_sp_ListaGrupoPS", parametros);
            throw ex;
        }
    }

    @Override
    public List<Contacto> listaGrupoAG(FiltroWrapper filtros) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue(P_ID_SEDE, filtros.getIdSede())
                .addValue(P_ID_PERIODO, filtros.getIdPeriodo())
                .addValue(P_ID_GRADO, filtros.getIdGrado());
        try {
            Map<String, Object> resultado = executeDMStoreProcedure("nif_sp_ListaGrupoAG", parametros,
                    new GrupoContactoMapper());
            return (List<Contacto>) resultado.get(CUSTOM_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, "nif_sp_ListaGrupoAG", parametros);
            throw ex;
        }
    }

    @Override
    public List<Contacto> listaGrupoDG(FiltroWrapper filtros) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue(P_ID_SEDE, filtros.getIdSede())
                .addValue(P_ID_PERIODO, filtros.getIdPeriodo())
                .addValue(P_ID_GRADO, filtros.getIdGrado());
        try {
            Map<String, Object> resultado = executeDMStoreProcedure("nif_sp_ListaGrupoDG", parametros,
                    new GrupoContactoMapper());
            return (List<Contacto>) resultado.get(CUSTOM_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, "nif_sp_ListaGrupoDG", parametros);
            throw ex;
        }
    }

    @Override
    public List<Contacto> listaGrupoPG(FiltroWrapper filtros) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue(P_ID_SEDE, filtros.getIdSede())
                .addValue(P_ID_PERIODO, filtros.getIdPeriodo())
                .addValue(P_ID_GRADO, filtros.getIdGrado());
        try {
            Map<String, Object> resultado = executeDMStoreProcedure("nif_sp_ListaGrupoPG", parametros,
                    new GrupoContactoMapper());
            return (List<Contacto>) resultado.get(CUSTOM_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, "nif_sp_ListaGrupoPG", parametros);
            throw ex;
        }
    }

    @Override
    public int insertarMensaje(MensajeApp mensajeApp) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue(ID_CORREO, mensajeApp.getIdCorreo())
                .addValue(ASUNTO, mensajeApp.getAsunto())
                .addValue(ID_REMITENTE, mensajeApp.getIdRemitente())
                .addValue("NombreRemitente", StringHelper.getValueOrEmpty(mensajeApp.getRemitente()))
                .addValue("CorreoRemitente", StringHelper.getValueOrEmpty(mensajeApp.getCorreoRemitente()))
                .addValue("GrupoRemitente", StringHelper.getValueOrEmpty(mensajeApp.getGrupo()))
                .addValue("Iniciales", StringHelper.getValueOrEmpty(mensajeApp.getNombreIniciales()))
                .addValue(CARGO, StringHelper.getValueOrEmpty(mensajeApp.getCargo()))
                .addValue(FIRMA, StringHelper.getValueOrEmpty(mensajeApp.getFirma()))
                .addValue("Para", StringHelper.getValueOrEmpty(mensajeApp.getPara()))
                .addValue("IdPara", StringHelper.getValueOrEmpty(mensajeApp.getIdPara()))
                .addValue("GrupoPara", StringHelper.getValueOrEmpty(mensajeApp.getGrupoPara()))
                .addValue(CON_COPIA, StringHelper.getValueOrEmpty(mensajeApp.getConCopia()))
                .addValue("IdConCopia", StringHelper.getValueOrEmpty(mensajeApp.getIdConCopia()))
                .addValue("GrupoCC", StringHelper.getValueOrEmpty(mensajeApp.getGrupoCc()))
                .addValue("ConCopiaOculta", StringHelper.getValueOrEmpty(mensajeApp.getConCopiaOculta()))
                .addValue(REFERENCIA, StringHelper.getValueOrEmpty(mensajeApp.getReferencia()))
                .addValue(MENSAJE, StringHelper.getValueOrEmpty(mensajeApp.getMensaje()))
                .addValue(CON_ADJUNTO, mensajeApp.getConAdjunto())
                .addValue("UsuarioRegistro", mensajeApp.getUsuarioRegistro())
                .addValue("IdMensajeRef", mensajeApp.getIdMensaje())
                .addValue("AceptarRespuesta", mensajeApp.getAceptarRespuesta())
                .addValue(RESPUESTA_MAYUSCULA, "");
        try {
            String[] rowValues = new String[]{RESPUESTA_MAYUSCULA};
            Map<String, Object> resultado = executeNifStoreProcedure("SP_InsertarMensaje", parametros,
                    rowValues);
            return (int) resultado.get(RESPUESTA_MAYUSCULA);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_InsertarMensaje", parametros);
            throw ex;
        }
    }

    @Override
    public int insertarMensajeDestinatarioXML(String xmlMensajeDestino) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue("xmlMensajeDestinoXML", xmlMensajeDestino)
                .addValue(RESPUESTA, "");
        try {
            String[] rowValues = new String[]{RESPUESTA};
            Map<String, Object> resultado = executeNifStoreProcedure("SP_InsertarDestinatarioXML", parametros,
                    rowValues);
            return (int) resultado.get(RESPUESTA);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_InsertarDestinatarioXML", parametros);
            throw ex;
        }
    }

    @Override
    public int insertarAdjuntos(MensajeAdjunto mensajeAdjunto) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue("id_mensaje", mensajeAdjunto.getIdMensaje())
                .addValue("url", StringHelper.getValueOrEmpty(mensajeAdjunto.getUrl()))
                .addValue("ruta", StringHelper.getValueOrEmpty(mensajeAdjunto.getUrl()))
                .addValue("id_tipo_archivo", mensajeAdjunto.getIdTipoArchivo())
                .addValue("nombre_original", mensajeAdjunto.getNombreOriginal())
                .addValue("nombre_alterado", mensajeAdjunto.getNombreAlterado())
                .addValue("peso_archivo", mensajeAdjunto.getTamanioArchivo())
                .addValue(RESPUESTA, "");
        try {
            String[] rowValues = new String[]{RESPUESTA};
            Map<String, Object> resultado = executeNifStoreProcedure("SP_InsertarMensajeAdjunto", parametros,
                    rowValues);
            return (int) resultado.get(RESPUESTA);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_InsertarMensajeAdjunto", parametros);
            throw ex;
        }
    }

    private void configurarFiltro(MensajeFiltro mensajeFiltro) {
        final String NO_APLICAR_FILTRO = "N";
        if (mensajeFiltro.getFiltro().equals(NO_APLICAR_FILTRO))
            mensajeFiltro.setFiltro("");
    }

    String nombreProcedimientoPorTipo(String tipo, String filtro) {
        MensajePaginadoFactory factory = new MensajesPaginadoProcedimiento(tipo);
        if (tipo.equals(MensajePaginadoFactory.MENSAJES_PAGINADOS))
            factory = new MensajesPaginadoPorFiltroProcedimiento(filtro);
        if (tipo.equals(MensajePaginadoFactory.MENSAJES_ENVIADOS_PAGINADOS))
            factory = new MensajesEnviadosPaginadoPorFiltroProcedimiento(filtro);
        if (tipo.equals(MensajePaginadoFactory.MENSAJES_ELIMINADOS_PAGINADOS))
            factory = new MensajesEliminadosPaginadoPorFiltroProcedimiento(filtro);
        return factory.nombreProcedimientoAlmacenado();
    }

    class MensajesPaginadoMapper implements RowMapper<MensajePaginado> {
        MensajeFiltro mensajeFiltro;

        MensajesPaginadoMapper(MensajeFiltro mensajeFiltro) {
            this.mensajeFiltro = mensajeFiltro;
        }

        @Override
        public MensajePaginado mapRow(ResultSet rs, int rowNum) throws SQLException {
            MensajePaginado model = new MensajePaginado();
            MensajeRepository.mapearMensajePaginado(rs, model);
            model.setNombreCompletoRemitente(rs.getString("NombreCompletoRemitente"));
            model.setIdFoto(rs.getInt("IdFoto"));
            model.setPara(rs.getString("Para"));
            model.setMensaje(StringHelper.obtenerTextoHtml(rs.getString(MENSAJE)));
            model.setEsFavorito(rs.getBoolean("EsFavorito"));
            model.setLeido(rs.getBoolean("Leido"));
            model.setGradoSeccion(mensajeFiltro.isShowData(), rs.getString("GradoSeccion"));
            return model;
        }
    }

    class DetalleMensajePaginadoMapper implements RowMapper<MensajePaginado> {

        @Override
        public MensajePaginado mapRow(ResultSet rs, int rowNum) throws SQLException {
            MensajePaginado model = new MensajePaginado();
            MensajeRepository.mapearMensajePaginado(rs, model);
            model.setNombreCompletoRemitente(rs.getString("NombreRemitente"));
            model.setIdPara(rs.getString("IdPara"));
            model.setPara(rs.getString("Para"));
            model.setGrupoPara(rs.getString("GrupoPara"));
            model.setIdConCopia(rs.getString("IdConCopia"));
            model.setConCopia(rs.getString(CON_COPIA));
            model.setGrupoCC(rs.getString("GrupoCC"));
            model.setMensaje(rs.getString(MENSAJE));
            model.setAceptarRespuesta(rs.getBoolean("AceptarRespuesta"));
            return model;
        }
    }

    class MensajeAdjuntosMapper implements RowMapper<MensajeAdjunto> {

        @Override
        public MensajeAdjunto mapRow(ResultSet rs, int rowNum) throws SQLException {
            MensajeAdjunto mensajeAdjunto = new MensajeAdjunto();
            mensajeAdjunto.setIdMensajeAdjunto(rs.getInt("IdMensajeAdjunto"));
            mensajeAdjunto.setIdMensaje(rs.getInt(ID_MENSAJE));
            mensajeAdjunto.setIdTipoArchivo(rs.getInt("IdTipoArchivo"));
            mensajeAdjunto.setNombreOriginal(rs.getString("NombreOriginal"));
            mensajeAdjunto.setNombreAlterado(rs.getString("NombreAlterado"));
            mensajeAdjunto.setTamanioArchivo(rs.getString("TamanioArchivo"));
            mensajeAdjunto.setFechaRegistro(rs.getString(FECHA_REGISTRO));
            return mensajeAdjunto;
        }
    }

    class MensajeHistorialMapper implements RowMapper<MensajeApp> {

        @Override
        public MensajeApp mapRow(ResultSet rs, int rowNum) throws SQLException {
            MensajeApp mensaje = new MensajeApp();
            mensaje.setIdMensaje(rs.getInt(ID_MENSAJE));
            mensaje.setIdRemitente(rs.getInt(ID_REMITENTE));
            mensaje.setRemitente(rs.getString("Remitente"));
            mensaje.setNombreIniciales(rs.getString("NombreIniciales"));
            mensaje.setPara(rs.getString("Para"));
            mensaje.setConCopia(rs.getString(CON_COPIA));
            mensaje.setReferencia(rs.getString(REFERENCIA));
            mensaje.setAsunto(rs.getString(ASUNTO));
            mensaje.setConAdjunto(rs.getBoolean(CON_ADJUNTO));
            mensaje.setMensaje(rs.getString(MENSAJE));
            mensaje.setGrupo(rs.getString(GRUPO));
            mensaje.setFirma(rs.getString(FIRMA));
            mensaje.setCargo(rs.getString(CARGO));
            mensaje.setUrl(rs.getString("URL"));
            mensaje.setFechaRegistro(DateHelper.fechaComoString(rs.getDate(FECHA_REGISTRO)));
            mensaje.setHoraRegistro(DateHelper.horaComoString(rs.getTimestamp(FECHA_REGISTRO)));
            return mensaje;
        }
    }

    class MensajeDestinoMapper implements RowMapper<MensajeDestino> {

        @Override
        public MensajeDestino mapRow(ResultSet rs, int rowNum) throws SQLException {
            MensajeDestino mensaje = new MensajeDestino();
            mensaje.setIdSecuencia(rs.getInt("IdSecuencia"));
            mensaje.setIdMensajeDestino(rs.getInt(ID_MENSAJE_DESTINO_CAPITALIZADO));
            mensaje.setIdMensaje(rs.getInt(ID_MENSAJE));
            mensaje.setIdDestino(rs.getInt("IdDestino"));
            mensaje.setIdTipoDestino(rs.getString("IdTipoDestino"));
            mensaje.setGrupo(rs.getString(GRUPO));
            mensaje.setCorreo(rs.getString("Correo"));
            mensaje.setLeido(rs.getBoolean("Leido"));
            mensaje.setEsFavorito(rs.getBoolean("EsFavorito"));
            mensaje.setEsEliminado(rs.getBoolean("EsEliminado"));
            mensaje.setEstado(rs.getBoolean(ESTADO));
            mensaje.setAlertaRevisada(rs.getBoolean(ESTADO));
            return mensaje;
        }
    }

    class GrupoContactoMapper implements RowMapper<Contacto> {

        @Override
        public Contacto mapRow(ResultSet rs, int rowNum) throws SQLException {
            Contacto contacto = new Contacto();
            contacto.setIdPersona(rs.getInt(ID_PERSONA));
            contacto.setNombres(StringHelper.getValueOrEmpty(rs.getString("Nombres")));
            contacto.setApellidoPaterno(StringHelper.getValueOrEmpty(rs.getString("ApellidoPaterno")));
            contacto.setApellidoMaterno(StringHelper.getValueOrEmpty(rs.getString("ApellidoMaterno")));
            contacto.setCorreo(StringHelper.getValueOrEmpty(rs.getString("Correo")));
            contacto.setGrupo(StringHelper.getValueOrEmpty(rs.getString(GRUPO)));
            contacto.setCargo(StringHelper.getValueOrEmpty(rs.getString(CARGO)));
            contacto.setEstado(rs.getInt(ESTADO));
            contacto.setOrden(rowNum);
            return contacto;
        }
    }

    class GrupoContactoPSMapper extends GrupoContactoMapper implements RowMapper<Contacto> {

        @Override
        public Contacto mapRow(ResultSet rs, int rowNum) throws SQLException {
            Contacto contacto = super.mapRow(rs, rowNum);
            assert contacto != null;
            contacto.setGradoSeccion(StringHelper.getValueOrEmpty(rs.getString("GradoSeccion")));
            return contacto;
        }
    }

    private static void mapearMensajePaginado(ResultSet rs, MensajePaginado model) throws SQLException {
        model.setIdCorreo(rs.getInt(ID_CORREO));
        model.setIdMensaje(rs.getInt(ID_MENSAJE));
        model.setIdMensajeDestino(rs.getInt(ID_MENSAJE_DESTINO_CAPITALIZADO));
        model.setIdRemitente(rs.getInt(ID_REMITENTE));
        model.setInicialesRemitente(rs.getString("InicialesRemitente"));
        model.setCargo(rs.getString(CARGO));
        model.setRol(rs.getString("Rol"));
        model.setUrl(StringHelper.getValueOrEmpty(rs.getString("URL")));
        model.setAsunto(rs.getString(ASUNTO));
        model.setConAdjunto(rs.getBoolean(CON_ADJUNTO));
        model.setFirma(rs.getString(FIRMA));
        model.setReferencia(rs.getString(REFERENCIA));
        model.setFechaRegistro(DateHelper.fechaComoString(rs.getDate(FECHA_REGISTRO)));
        model.setHoraRegistro(DateHelper.horaComoString(rs.getTimestamp(FECHA_REGISTRO)));
    }
}
