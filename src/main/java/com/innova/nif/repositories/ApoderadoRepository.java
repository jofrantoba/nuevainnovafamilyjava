package com.innova.nif.repositories;

import com.innova.nif.models.HijoApoderado;
import com.innova.nif.repositories.interfaces.IApoderadoRepository;
import com.innova.nif.utils.IntHelper;
import com.innova.nif.utils.StringHelper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class ApoderadoRepository extends BaseRepository implements IApoderadoRepository {
    @Override
    public List<HijoApoderado> obtenerHijos(int idPersonaPadre, int idPeriodo) {
        var parameters = new MapSqlParameterSource()
                .addValue("p_IdPersonaPadre", IntHelper.getValueOrDefault(idPersonaPadre))
                .addValue("p_PeriodoLectivo", IntHelper.getValueOrDefault(idPeriodo));
        try {
            var respuestaProcedimiento = executeDMStoreProcedure("NIF_sp_obtenerListaALPorPF", parameters,
                    new HijoApoderadoMapper());
            return (List<HijoApoderado>) respuestaProcedimiento.get(CUSTOM_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, "NIF_sp_obtenerListaALPorPF", parameters);
            throw ex;
        }
    }

    class HijoApoderadoMapper implements RowMapper<HijoApoderado> {

        @Override
        public HijoApoderado mapRow(ResultSet rs, int rowNum) throws SQLException {
            var modelo = new HijoApoderado();
            modelo.setIdPersona(rs.getInt("id_persona"));
            modelo.setNombres(rs.getString("nombres"));
            modelo.setNombreAlumno(rs.getString("paterno"), rs.getString("materno"));
            modelo.setNombresIniciales(modelo.getNombres(), rs.getString("paterno"));
            modelo.setDni(rs.getString("DNI"));
            modelo.setIdGrado(rs.getInt("id_grado"));
            modelo.setGrado(rs.getString("Grado"));
            modelo.setIdSeccion(rs.getInt("id_seccion"));
            modelo.setSeccionLetra(rs.getString("seccion_letra"));
            modelo.setNivel(StringHelper.getValueOrEmpty(rs.getString("nivel_ingles")));
            modelo.setIdSede(rs.getInt("id_sede"));
            modelo.setTitulo(rs.getString("titulo"));
            return modelo;
        }
    }
}
