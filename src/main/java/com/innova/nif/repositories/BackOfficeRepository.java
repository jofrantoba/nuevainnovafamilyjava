package com.innova.nif.repositories;

import com.innova.nif.repositories.interfaces.IBackOfficeRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class BackOfficeRepository extends BaseRepository implements IBackOfficeRepository {

    @Override
    public int obtenerConteoPorConsulta(String consultaSql) {
        try {
            List<Map<String, Object>> resultado = executePeaQuery(consultaSql);
            Map<String, Object> totalResultado = resultado.get(0);
            return ((Long) totalResultado.get("count")).intValue();
        } catch (Exception ex) {
            registrarExcepcion(ex, consultaSql);
            throw ex;
        }
    }

    @Override
    public List<String> obtenerCodigoAlumnos(String consultaSql) {
        try {
            List<String> codigos = new ArrayList<>();
            List<Map<String, Object>> informacionAlumnos = ejecutarConsulta(consultaSql);
            if (!informacionAlumnos.isEmpty())
                for (Map<String, Object> alumno : informacionAlumnos)
                    codigos.add((String) alumno.get("code"));
            return codigos;
        } catch (Exception ex) {
            registrarExcepcion(ex, consultaSql);
            throw ex;
        }
    }

    @Override
    public int obtenerConteoPorConsultaDataManagement(String consultaSql) {
        try {
            List<Map<String, Object>> resultado = executeDataManagementQuery(consultaSql);
            Map<String, Object> totalResultado = resultado.get(0);
            return ((Long) totalResultado.get("count")).intValue();
        } catch (Exception ex) {
            registrarExcepcion(ex, consultaSql);
            throw ex;
        }
    }

    private List<Map<String, Object>> ejecutarConsulta(String consultaSql) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(peaDataSource);
        return jdbcTemplate.queryForList(consultaSql);
    }
}
