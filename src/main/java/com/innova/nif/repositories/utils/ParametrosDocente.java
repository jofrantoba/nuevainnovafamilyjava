package com.innova.nif.repositories.utils;

import com.google.common.base.Strings;
import com.innova.nif.models.wrappers.FiltroWrapper;
import lombok.Getter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

public class ParametrosDocente {
    FiltroWrapper filtros;
    @Getter
    private MapSqlParameterSource parametros;
    @Getter
    private String nombreProcedimiento;
    @Getter
    private boolean filtrarCursoSolo;

    public ParametrosDocente(FiltroWrapper filtros) {
        this.filtros = filtros;
        this.parametros = new MapSqlParameterSource();
        procesar();
    }

    private void procesar() {
        if (noConsultarAlerta() && !Strings.isNullOrEmpty(filtros.getNivel())) {
            parametros.addValue("p_id_periodo", filtros.getIdPeriodo());
            parametros.addValue("p_id_sede", filtros.getIdSede());
            parametros.addValue("p_id_curso", filtros.getIdCurso());
            parametros.addValue("p_id_grado", filtros.getIdGrado());
            nombreProcedimiento = "NIF_sp_ConsultarDCPorGrado";
            filtrarCursoSolo = false;
        }
        else if (noConsultarAlerta() && Strings.isNullOrEmpty(filtros.getNivel())) {
            parametros.addValue("p_id_seccion", filtros.getIdSeccion());
            nombreProcedimiento = "NIF_sp_ConsultarDC";
            filtrarCursoSolo = true;
        } else {
            parametros.addValue("p_id_periodo", filtros.getIdPeriodo());
            parametros.addValue("p_id_sede", filtros.getIdSede());
            parametros.addValue("p_id_grado", filtros.getIdGrado());
            nombreProcedimiento = "NIF_sp_ConsultarDCAlerta";
            filtrarCursoSolo = true;
        }
    }

    private boolean noConsultarAlerta() {
        return filtros.getFlagAlerta() != 1;
    }
}
