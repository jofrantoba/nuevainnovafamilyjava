package com.innova.nif.repositories.utils;

import com.google.common.base.Strings;
import com.innova.nif.models.wrappers.FiltroWrapper;
import lombok.Getter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

public class ParametrosAlumno {
    private FiltroWrapper filtros;
    @Getter
    private MapSqlParameterSource parametros;
    @Getter
    private String nombreProcedimiento;

    public ParametrosAlumno(FiltroWrapper filtros) {
        this.filtros = filtros;
        this.parametros = new MapSqlParameterSource();
        procesar();
    }

    private void procesar() {
        boolean esUnaConsultaAlumno = filtros.getIdSeccion() != 0 || Strings.isNullOrEmpty(filtros.getNivel()) || filtros.getNivel().trim().length() > 0;
        if (esUnaConsultaAlumno) {
            parametros.addValue("p_id_sede", filtros.getIdSede());
            parametros.addValue("p_id_periodo", filtros.getIdPeriodo());
            parametros.addValue("p_id_grado", filtros.getIdGrado());
            parametros.addValue("p_id_seccion", filtros.getIdSeccion());
            parametros.addValue("p_nivel", filtros.getNivel());
            nombreProcedimiento = "NIF_sp_ConsultarAL";
        } else {
            parametros.addValue("p_id_sede", filtros.getIdSede());
            parametros.addValue("p_id_periodo", filtros.getIdPeriodo());
            parametros.addValue("p_id_grado", filtros.getIdGrado());
            nombreProcedimiento = "NIF_sp_ConsultarALAlerta";
        }
    }
}
