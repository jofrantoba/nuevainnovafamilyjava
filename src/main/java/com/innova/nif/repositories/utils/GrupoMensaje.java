package com.innova.nif.repositories.utils;

import com.innova.nif.models.Contacto;
import com.innova.nif.models.wrappers.FiltroWrapper;
import com.innova.nif.repositories.interfaces.IMensajeRepository;
import com.innova.nif.utils.Constantes;

import java.util.ArrayList;
import java.util.List;

public class GrupoMensaje {
    private IMensajeRepository repository;
    private FiltroWrapper filtros;
    private String grupo;

    public GrupoMensaje(IMensajeRepository repository, String grupo, FiltroWrapper filtros) {
        this.repository = repository;
        this.filtros = filtros;
        this.grupo = grupo;
    }

    public List<Contacto> listaGrupos() {
        if (grupo.equals(Constantes.GRUPO_DESTINO_ALUMNO_SEDE))
            return repository.listaGruposAS(filtros);
        if (grupo.equals(Constantes.GRUPO_DESTINO_DOCENTE_SEDE))
            return repository.listaGrupoDS(filtros);
        if (grupo.equals(Constantes.GRUPO_DESTINO_PPFF_SEDE))
            return repository.listaGrupoPS(filtros);
        if (grupo.equals(Constantes.GRUPO_DESTINO_ALUMNO_GRADO))
            return repository.listaGrupoAG(filtros);
        if (grupo.equals(Constantes.GRUPO_DESTINO_DOCENTE_GRADO))
            return repository.listaGrupoDG(filtros);
        if (grupo.equals(Constantes.GRUPO_DESTINO_PPFF_GRADO))
            return repository.listaGrupoPG(filtros);
        return new ArrayList<>();
    }
}
