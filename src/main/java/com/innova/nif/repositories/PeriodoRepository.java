package com.innova.nif.repositories;

import com.innova.nif.models.PeriodoLectivo;
import com.innova.nif.repositories.interfaces.IPeriodoRepository;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class PeriodoRepository extends BaseRepository implements IPeriodoRepository {

    @Override
    public PeriodoLectivo getPeriodoActivo() {
        var parameters = new MapSqlParameterSource();

        var response = executeNifStoreProcedure("SP_PeriodoActivo", parameters, new PeriodoLectivoRowMapper());
        return (PeriodoLectivo) ((ArrayList) response.get(CUSTOM_RESULT_SET)).get(0);
    }

    @Override
    public List<PeriodoLectivo> obtenerPeriodosPF() {
        MapSqlParameterSource parametros = new MapSqlParameterSource();
        try {
            Map<String, Object> resultado = executeNifStoreProcedure("SP_ListaPeriodoPPFF", parametros,
                    new PeriodoLectivoRowMapper());
            return (List<PeriodoLectivo>) resultado.get(CUSTOM_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_ListaPeriodoPPFF", parametros);
            throw ex;
        }
    }

    class PeriodoLectivoRowMapper implements RowMapper<PeriodoLectivo> {
        @Override
        public PeriodoLectivo mapRow(ResultSet rs, int rowNum) throws SQLException {
            PeriodoLectivo usuario = new PeriodoLectivo();
            usuario.setAnio(rs.getInt("Descripcion"));
            usuario.setIdPeriodo(rs.getInt("Valor"));
            usuario.setEstado(rs.getString("Estado"));
            return usuario;
        }
    }
}
