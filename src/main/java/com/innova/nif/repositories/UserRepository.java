package com.innova.nif.repositories;

import com.innova.nif.models.MensajeAdjunto;
import com.innova.nif.models.Persona;
import com.innova.nif.models.AlertaUsuario;
import com.innova.nif.models.Usuario;
import com.innova.nif.repositories.interfaces.IUserRepository;
import com.innova.nif.utils.*;
import com.google.common.base.Strings;
import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;
import javassist.NotFoundException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Repository
public class UserRepository extends BaseRepository implements IUserRepository {

    private static final String PERFIL = "Perfil";
    private static final String FECHA_RECUPERACION_CONTRASENA = "RecoverPasswordDate";
    private static final String RESPUESTA = "respuesta";
    private static final String CODIGO_PERSONA = "codigo_persona";
    private static final String NUEVO_CORREO = "NuevoCorreo";
    private static final String PERSON_ID = "PersonId";
    private static final String ID_PERSONA = "IdPersona";

    @Override
    public Usuario usuarioInformacionRecuperarContrasena(int idPersona, String perfil) throws NotFoundException {
        var parameters = new MapSqlParameterSource()
                .addValue(ID_PERSONA, IntHelper.getValueOrDefault(idPersona))
                .addValue(PERFIL, StringHelper.getValueOrEmpty(perfil));
        try {
            var rowValues = new String[]{"Clave", "RecoverPasswordHash", FECHA_RECUPERACION_CONTRASENA};
            var response = executeNifStoreProcedure("SP_ObtenerContrasenia", parameters, rowValues);
            var items = getDefaultResultSet(response);
            return informacionUsuario(items);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_ObtenerContrasenia", parameters);
            throw ex;
        }
    }

    @Override
    public MensajeAdjunto consultarFotoPerfil(int idPersona, String perfil) {
        var parameters = new MapSqlParameterSource()
                .addValue(ID_PERSONA, IntHelper.getValueOrDefault(idPersona))
                .addValue("IdPerfil", StringHelper.getValueOrEmpty(perfil));
        try {
            MensajeAdjunto mensajeAdjunto = new MensajeAdjunto();
            var rowValues = new String[]{"url", "NuevoEmailNoVerificado"};
            var response = executeNifStoreProcedure("SP_ConsultarFotoPerfil", parameters, rowValues);
            var items = getDefaultResultSet(response);
            if (!items.isEmpty()) {
                var item = items.get(0);
                mensajeAdjunto.setUrl(StringHelper.getValueOrEmpty(item.get("url")));
                mensajeAdjunto.setNuevoEmail(StringHelper.getValueOrEmpty(item.get("NuevoEmailNoVerificado")));
            }
            return mensajeAdjunto;
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_ConsultarFotoPerfil", parameters);
            throw ex;
        }
    }

    @Override
    public int solicitarCambioDePassword(Usuario usuario) {
        var respuesta = Constantes.CODIGO_ERROR;
        var parameters = new MapSqlParameterSource()
                .addValue(CODIGO_PERSONA, IntHelper.getValueOrDefault(usuario.getIdPersona()))
                .addValue(PERFIL, StringHelper.getValueOrEmpty(usuario.getGrupo()))
                .addValue("RecoverPassowordHash", StringHelper.getValueOrEmpty(usuario.getRecoverPasswordHash()))
                .addValue("Dni", StringHelper.getValueOrEmpty(usuario.getDni()));
        try {
            var rowValues = new String[]{RESPUESTA};
            var response = executeNifStoreProcedure("SP_SolicitarCambioContrasena", parameters, rowValues);
            if (!response.isEmpty())
                respuesta = (int) response.get(RESPUESTA);
            return respuesta;
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_SolicitarCambioContrasena", parameters);
            throw ex;
        }
    }

    @Override
    public Usuario obtenerUsuarioPF(String email, int idPeriodoActual) throws NotFoundException {
        var parameters = new MapSqlParameterSource()
                .addValue("p_Correo", StringHelper.getValueOrEmpty(email))
                .addValue("p_PeriodoLectivo", IntHelper.getValueOrDefault(idPeriodoActual));
        try {
            var response = executeDMStoreProcedure("NIF_sp_obtenerUsuarioPF", parameters, new UsuarioRowMapper());
            ArrayList resultado = (ArrayList) response.get(CUSTOM_RESULT_SET);
            if (resultado.isEmpty())
                throw new NotFoundException("El correo es inválido.");
            return (Usuario) resultado.get(0);
        } catch (Exception ex) {
            registrarExepcion(ex, "NIF_sp_obtenerUsuarioPF", parameters);
            throw ex;
        }
    }

    @Override
    public Usuario obtenerUsuarioDm(String email, int idPeriodoActual, int idPeriodoPosterior) throws NotFoundException {
        var parameters = new MapSqlParameterSource()
                .addValue("p_Correo", email)
                .addValue("p_PeriodoLectivo", idPeriodoActual)
                .addValue("p_PeriodoLectivoPost", idPeriodoPosterior);
        try {
            var response = executeDMStoreProcedure("NIF_sp_obtenerUsuarioPFPost", parameters, new UsuarioRowMapperPeriodo());
            ArrayList resultado = (ArrayList) response.get(CUSTOM_RESULT_SET);
            if (resultado.isEmpty())
                throw new NotFoundException("El correo es inválido.");
            return (Usuario) resultado.get(0);
        } catch (Exception ex) {
            registrarExepcion(ex, "NIF_sp_obtenerUsuarioPFPost", parameters);
            throw ex;
        }
    }

    @Override
    public boolean actualizarContrasenaConHash(Persona persona) {
        var parameters = new MapSqlParameterSource()
                .addValue(CODIGO_PERSONA, IntHelper.getValueOrDefault(persona.getCodigoPersona()))
                .addValue("clave_nueva", StringHelper.getValueOrEmpty(persona.getClaveNueva()))
                .addValue(PERFIL, StringHelper.getValueOrEmpty(persona.getCargo()))
                .addValue("HashRequest", StringHelper.getValueOrEmpty(persona.getRecoverPasswordHash()));
        try {
            var resultKeys = new String[]{"resultado"};
            var response = executeNifStoreProcedure("SP_ActualizarContrasenaConHash", parameters, resultKeys);
            return (int) response.get(RESPUESTA) == Constantes.CODIGO_EXITO;
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_ActualizarContrasenaConHash", parameters);
            throw ex;
        }
    }

    @Override
    public Usuario obtenerUsuarioPorContrasenaHash(String hash) {
        var parameters = new MapSqlParameterSource()
                .addValue("RequestHash", StringHelper.getValueOrEmpty(hash));
        try {
            var response = executeNifStoreProcedure("SP_GetUsuarioRecuperarContrasenaHash", parameters,
                    new UsuarioContrasenaHashRowMapper());
            var resultado = ((ArrayList) response.get(CUSTOM_RESULT_SET));
            if (resultado.isEmpty())
                return new Usuario();
            return (Usuario) resultado.get(0);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_GetUsuarioRecuperarContrasenaHash", parameters);
            throw ex;
        }
    }

    @Override
    public boolean correoExiste(String email) {
        String claveRespuesta = "COUNT(id)";
        var parameters = new MapSqlParameterSource()
                .addValue("nuevoEmail", StringHelper.getValueOrEmpty(email));
        try {
            var rowValues = new String[]{claveRespuesta};
            var respuesta = executeDMStoreProcedure("NIF_sp_ConsultarCorreoExistente", parameters, rowValues);
            var resultado = (getDefaultResultSet(respuesta)).get(0);
            return (long) resultado.get(claveRespuesta) > 0;
        } catch (Exception ex) {
            registrarExepcion(ex, "NIF_sp_ConsultarCorreoExistente", parameters);
            throw ex;
        }
    }

    @Override
    public boolean actualizarCorreo(Usuario usuario) {
        var parameters = new MapSqlParameterSource()
                .addValue(CODIGO_PERSONA, IntHelper.getValueOrDefault(usuario.getIdPersona()))
                .addValue(PERFIL, StringHelper.getValueOrEmpty(usuario.getGrupo()))
                .addValue("NuevoEmail", StringHelper.getValueOrEmpty(usuario.getNuevoEmail()))
                .addValue("NuevoEmailNoVerificadoHash", StringHelper.getValueOrEmpty(usuario.getRecoverPasswordHash()))
                .addValue("Dni", StringHelper.getValueOrEmpty(usuario.getDni()));
        try {
            var rowValues = new String[]{RESPUESTA};
            var respuesta = executeNifStoreProcedure("SP_ActualizarCorreoElectronico", parameters, rowValues);
            return (int) respuesta.get(RESPUESTA) == Constantes.CODIGO_EXITO;
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_ActualizarCorreoElectronico", parameters);
            throw ex;
        }
    }

    @Override
    public Usuario obtenerUsuarioCambioContrasenaPorHash(String hash) throws NotFoundException {
        var parameters = new MapSqlParameterSource()
                .addValue("RequestHash", StringHelper.getValueOrEmpty(hash));
        try {
            var rowValues = new String[]{PERSON_ID, NUEVO_CORREO};
            var respuesta = executeNifStoreProcedure("SP_GetUsuarioCambiarCorreoHash", parameters, rowValues);
            if (respuesta.get(PERSON_ID).toString().isEmpty() || respuesta.get(NUEVO_CORREO).toString().isEmpty())
                throw new NotFoundException("El hash es inválido.");
            var usuario = new Usuario();
            usuario.setIdPersona(Integer.parseInt(respuesta.get(PERSON_ID).toString()));
            usuario.setNuevoEmail(respuesta.get(NUEVO_CORREO).toString());
            usuario.setCorreo(respuesta.get(NUEVO_CORREO).toString());
            return usuario;
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_GetUsuarioCambiarCorreoHash", parameters);
            throw ex;
        }
    }

    @Override
    public int insertarFoto(MensajeAdjunto adjunto) {
        var parameters = new MapSqlParameterSource()
                .addValue("nombre_archivo", StringHelper.getValueOrEmpty(adjunto.getNombreOriginal()))
                .addValue("nombre_modificado", StringHelper.getValueOrEmpty(adjunto.getNombreAlterado()))
                .addValue("url", StringHelper.getValueOrEmpty(adjunto.getUrl()));
        try {
            var rowValues = new String[]{RESPUESTA};
            var respuesta = executeNifStoreProcedure("SP_InsertarFoto", parameters, rowValues);
            return (int) respuesta.get(RESPUESTA);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_InsertarFoto", parameters);
            throw ex;
        }
    }

    @Override
    public boolean actualizarFoto(Persona persona) {
        var parameters = new MapSqlParameterSource()
                .addValue("id_foto", IntHelper.getValueOrDefault(persona.getIdFoto()))
                .addValue("usuario_modificacion", StringHelper.getValueOrEmpty(persona.getUsuarioModificacion()))
                .addValue(CODIGO_PERSONA, persona.getCodigoPersona())
                .addValue("DNI", StringHelper.getValueOrEmpty(persona.getClaveNueva()))
                .addValue("grupo", StringHelper.getValueOrEmpty(persona.getGrupo()));
        try {
            var rowValues = new String[]{RESPUESTA};
            var respuesta = executeNifStoreProcedure("SP_ActualizarFotoPerfil", parameters, rowValues);
            return (int) respuesta.get(RESPUESTA) == Constantes.CODIGO_EXITO;
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_ActualizarFotoPerfil", parameters);
            throw ex;
        }
    }

    @Override
    public void actualizarEstadoSinNotificacion(Usuario usuario) {
        var parameters = new MapSqlParameterSource()
                .addValue("CodigoPersona", IntHelper.getValueOrDefault(usuario.getIdPersona()))
                .addValue(PERFIL, StringHelper.getValueOrEmpty(usuario.getGrupo()));
        try {
            executeNifStoreProcedure("SP_ActualizarEstadoCorreoSinNotificacion", parameters);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_ActualizarEstadoCorreoSinNotificacion", parameters);
            throw ex;
        }
    }

    @Override
    public void actualizarUsuarioNotificacion(Usuario usuario) {
        var parameters = new MapSqlParameterSource()
                .addValue("codigoPersona", usuario.getIdPersona())
                .addValue("perfil", usuario.getGrupo())
                .addValue("permitirEnvio", usuario.isAllowsEmailNotifications());
        try {
            executeNifStoreProcedure("SP_ActualizarUsuarioNotificacion", parameters);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_ActualizarUsuarioNotificacion", parameters);
            throw ex;
        }
    }

    @Override
    public List<AlertaUsuario> listarAlertasPorUsuarioTarea(int idSede, int idPeriodo, String idGrados) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue("p_id_sede", idSede)
                .addValue("p_id_periodo", idPeriodo)
                .addValue("p_id_grados", idGrados);
        try {
            Map<String, Object> respuesta = executeDMStoreProcedure("NIF_sp_listUserTareaByGrados",
                    parametros, new UsuarioAlertaMapper());
            return (List<AlertaUsuario>) respuesta.get(CUSTOM_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, "NIF_sp_listUserTareaByGrados", parametros);
            throw ex;
        }
    }

    @Override
    public List<AlertaUsuario> listarAlertasPorUsuarioEvento(int idSede, int idPeriodo, String idGrados) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue("p_id_sede", idSede)
                .addValue("p_id_periodo", idPeriodo);
        String nombreProcedimiento = "NIF_sp_listUserEvento";
        if (!Strings.isNullOrEmpty(idGrados)) {
            nombreProcedimiento = "NIF_sp_listUserEventoByGrados";
            parametros.addValue("p_id_grados", idGrados);
        }
        try {
            Map<String, Object> respuesta = executeDMStoreProcedure(nombreProcedimiento, parametros,
                    new UserAlertaEventoMapper());
            return (List<AlertaUsuario>) respuesta.get(CUSTOM_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, nombreProcedimiento, parametros);
            throw ex;
        }
    }

    @Override
    public int actualizarInicioSesion(Usuario usuario) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue(CODIGO_PERSONA, usuario.getIdPersona())
                .addValue("DNI", StringHelper.getValueOrEmpty(usuario.getDni()))
                .addValue("grupo", StringHelper.getValueOrEmpty(usuario.getGrupo()))
                .addValue(RESPUESTA, "");
        try {
            String[] rowValues = {RESPUESTA};
            Map<String, Object> respuesta = executeNifStoreProcedure("SP_ActualizarUsuarioInicioSesion", parametros, rowValues);
            return (int) respuesta.get(RESPUESTA);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_ActualizarUsuarioInicioSesion", parametros);
            throw ex;
        }
    }

    private Usuario informacionUsuario(ArrayList<HashMap<String, Object>> items) {
        boolean existeUsuario = !items.isEmpty();
        Usuario usuario = new Usuario();
        if (existeUsuario) {
            var informacionUsuario = items.get(0);
            usuario.setClave(StringHelper.getValueOrEmpty(informacionUsuario.get("Clave")));
            usuario.setRecoverPasswordHash(StringHelper.getValueOrEmpty(informacionUsuario.get("RecoverPasswordHash")));
            usuario.setRecoverPasswordDate(DateHelper.getDateOrDefault(informacionUsuario.get(FECHA_RECUPERACION_CONTRASENA)));
        }
        return usuario;
    }

    class UsuarioRowMapper implements RowMapper<Usuario> {

        @Override
        public Usuario mapRow(ResultSet rs, int rowNum) throws SQLException {
            Usuario usuario = new Usuario();
            usuario.setCorreo(rs.getString("Correo"));
            usuario.setIdPersona(rs.getInt("idPersona"));
            usuario.setDni(rs.getString("DNI"));
            usuario.setCargo(rs.getString("Cargo"));
            usuario.setNombres(rs.getString("Nombres"));
            usuario.setGrupo(rs.getString("Grupo"));
            usuario.setIdSede(rs.getInt("IdSede"));
            usuario.setApellidoPaterno(rs.getString("ApellidoPaterno"));
            usuario.setApellidoMaterno(rs.getString("ApellidoMaterno"));

            return usuario;
        }
    }

    class UsuarioRowMapperPeriodo extends UsuarioRowMapper implements RowMapper<Usuario> {

        @Override
        public Usuario mapRow(ResultSet rs, int rowNum) throws SQLException {
            Usuario usuario = super.mapRow(rs, rowNum);
            assert usuario != null;
            usuario.setIdPeriodoLectivo(rs.getInt("IdPeriodoLectivo"));
            return usuario;
        }
    }

    class UsuarioContrasenaHashRowMapper implements RowMapper<Usuario> {

        @Override
        public Usuario mapRow(ResultSet rs, int rowNum) throws SQLException {
            Usuario usuario = new Usuario();
            usuario.setIdPersona(rs.getInt("CodigoPersona"));
            usuario.setRecoverPasswordDate(new DateTime(rs.getDate(FECHA_RECUPERACION_CONTRASENA).getTime()));
            usuario.setMinutosTranscurridos(rs.getInt("MinutosTranscurridos"));
            return usuario;
        }
    }

    class UsuarioAlertaMapper implements RowMapper<AlertaUsuario> {

        @Override
        public AlertaUsuario mapRow(ResultSet rs, int rowNum) throws SQLException {
            AlertaUsuario alerta = new AlertaUsuario();
            alerta.setIdPersona(rs.getInt(ID_PERSONA));
            alerta.setPerfil(StringHelper.getValueOrEmpty(rs.getString(PERFIL)));
            alerta.setIdPersonaAlumno(rs.getInt("IdPersonaAlumno"));
            alerta.setIdGrado(rs.getInt("IdGrado"));
            alerta.setNivelIngles(StringHelper.getValueOrEmpty(rs.getString("NivelIngles")));
            alerta.setSeccion(StringHelper.getValueOrEmpty(rs.getString("Seccion")));
            alerta.setInicialesAlumno(Util.obtenerInicialesNombre(rs.getString("Nombres"),
                    rs.getString("Paterno")));
            return alerta;
        }
    }

    class UserAlertaEventoMapper implements RowMapper<AlertaUsuario> {

        @Override
        public AlertaUsuario mapRow(ResultSet rs, int rowNum) throws SQLException {
            AlertaUsuario alerta = new AlertaUsuario();
            alerta.setIdPersona(rs.getInt(ID_PERSONA));
            alerta.setPerfil(StringHelper.getValueOrEmpty(rs.getString(PERFIL)));
            return alerta;
        }
    }
}
