package com.innova.nif.repositories.factories.mensaje;

public class MensajesEnviadosPaginadoPorFiltroProcedimiento extends MensajePaginadoFactory {
    public MensajesEnviadosPaginadoPorFiltroProcedimiento(String filtro) {
        super(filtro);
    }

    @Override
    public String nombreProcedimientoAlmacenado() {
        return esFiltro ? "SP_ConsultaBandejaPaginadoEnviadosPorFiltro" : "SP_ConsultaBandejaPaginadoEnviados";
    }
}
