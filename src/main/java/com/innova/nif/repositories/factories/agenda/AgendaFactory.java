package com.innova.nif.repositories.factories.agenda;

import com.innova.nif.repositories.BaseRepository;
import com.innova.nif.utils.Constantes;
import org.springframework.stereotype.Repository;

@Repository
public class AgendaFactory extends BaseRepository implements IAgendaFactory {

    @Override
    public IConsultaAgendaRepository getRepository(String grupo) {
        IConsultaAgendaRepository repository;
        repository = new ConsultaAgendaEquipoDirectivo(nifDataSource);
        if (grupo.equals(Constantes.CONSULTA_PADRE_FAMILIA))
            repository = new ConsultaAgendaPadreFamilia(nifDataSource);
        if (grupo.equals(Constantes.CONSULTA_DOCENTE))
            repository = new ConsultaAgendaDocente(nifDataSource);
        if (grupo.equals(Constantes.CONSULTA_ALUMNO))
            repository = new ConsultaAgendaAlumno(nifDataSource);
        return repository;
    }
}
