package com.innova.nif.repositories.factories.usuario;

public interface ILoginFactory {
    IConsultaUsuarioRepository getRepository(String tipoUsuario);
}
