package com.innova.nif.repositories.factories.agenda;

import com.innova.nif.models.Calendario;
import com.innova.nif.models.wrappers.AgendaFiltro;
import com.innova.nif.repositories.BaseRepository;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ConsultaAgendaPadreFamilia extends BaseRepository implements IConsultaAgendaRepository {
    public ConsultaAgendaPadreFamilia(DataSource nifDataSource) {
        this.nifDataSource = nifDataSource;
    }

    @Override
    public List obtenerDatos(AgendaFiltro filtros) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue("FechaInicio", filtros.getFechaInicio())
                .addValue("FechaFin", filtros.getFechaFin())
                .addValue("IdPeriodo", filtros.getIdPeriodo());
        agregarParametrosPorTipo(parametros, filtros);
        String procedimientoAlmacenado = nombreProcedimientoAlmacenado(filtros);
        try {
            Map<String, Object> respuesta = executeBaseStoreProcedure(this.nifDataSource, procedimientoAlmacenado,
                    parametros, new String[]{}, new CalendarioMapper());
            return (ArrayList<Calendario>) respuesta.get(CUSTOM_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, procedimientoAlmacenado, parametros);
            throw ex;
        }
    }

    @Override
    public String nombreProcedimientoAlmacenado(AgendaFiltro filtro) {
        return ConsultaAgendaMapper.nombreProcedimientoPorTipo(filtro);
    }

    public void agregarParametrosPorTipo(MapSqlParameterSource parametros, AgendaFiltro filtro) {
        if (!filtro.esEvento())
            parametros.addValue("sedeGradoSeccion", filtro.getListaSeccion());
        if (!filtro.esTarea())
            parametros.addValue("sedeGrado", filtro.getListaIdGrado());
    }

    class CalendarioMapper implements RowMapper<Calendario> {

        @Override
        public Calendario mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new ConsultaAgendaMapper(rs).generarModelo();
        }
    }
}
