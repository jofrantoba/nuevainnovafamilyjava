package com.innova.nif.repositories.factories.usuario;

import com.innova.nif.models.Usuario;

import java.util.List;

public interface IConsultaUsuarioRepository {
    List<Usuario> obtenerDatos(String correo, int periodo);
}
