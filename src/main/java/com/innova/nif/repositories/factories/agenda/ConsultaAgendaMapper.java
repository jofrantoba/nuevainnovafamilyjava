package com.innova.nif.repositories.factories.agenda;

import com.innova.nif.models.Calendario;
import com.innova.nif.models.wrappers.AgendaFiltro;
import com.innova.nif.utils.Constantes;
import com.innova.nif.utils.DateHelper;
import com.innova.nif.utils.StringHelper;
import org.apache.commons.lang.StringEscapeUtils;
import org.joda.time.DateTime;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ConsultaAgendaMapper {
    ResultSet resultSet;

    public ConsultaAgendaMapper(ResultSet resultSet) {
        this.resultSet = resultSet;
    }

    public Calendario generarModelo() throws SQLException {
        String tipoAgenda = StringHelper.getValueOrEmpty(resultSet.getString("TipoAgenda"));
        DateTime fechaEntrega = new DateTime(resultSet.getTimestamp("FechaEntrega"));
        DateTime fechaEnvio = new DateTime(resultSet.getTimestamp("fechaEnvio"));
        Calendario calendario = new Calendario();
        calendario.setIdAgenda(determinarIdAgenda(resultSet.getInt("IdAgenda"),
                resultSet.getInt("IdDetalleAgenda"), tipoAgenda));
        calendario.setTitulo(determinarTitulo(tipoAgenda, resultSet));
        calendario.setColor(StringHelper.getValueOrEmpty(resultSet.getString("Color")));
        calendario.setFechaInicio(determinarFechaInicio(fechaEntrega, fechaEnvio, tipoAgenda));
        calendario.setFechaFinal(determinarFechaFinal(fechaEntrega, fechaEnvio, tipoAgenda));
        return calendario;
    }

    private String determinarTitulo(String tipoAgenda, ResultSet rs) throws SQLException {
        String titulo = StringHelper.getValueOrEmpty(rs.getString("Titulo"));
        String grado = StringHelper.getValueOrEmpty(rs.getString("CodigoGrado"));
        String seccion = StringHelper.getValueOrEmpty(rs.getString("Seccion"));
        String tituloFinal = esEvento(tipoAgenda) ?
                titulo : new StringBuilder().append("Tarea - ").append(titulo).append(" (")
                .append(grado.trim()).append("° ")
                .append(seccion).append(")").toString();
        return StringEscapeUtils.unescapeHtml(tituloFinal);
    }

    private String determinarIdAgenda(int idAgenda, int idDetalleAgenda, String tipoAgenda) {
        return new StringBuilder()
                .append(idAgenda).append("-").append(idDetalleAgenda).append("-").append(tipoAgenda).toString();
    }

    private String determinarFechaInicio(DateTime fechaEntrega, DateTime fechaEnvio, String tipoAgenda) {
        if (esEvento(tipoAgenda))
            return DateHelper.formatoFullCalendar(fechaEnvio);
        if (esTarea(tipoAgenda))
            return DateHelper.formatoFullCalendar(fechaEntrega);
        throw new UnsupportedOperationException(
                String.format("No se puede determinar la fecha de inicio para el tipo de agenda=%s.", tipoAgenda));
    }

    private String determinarFechaFinal(DateTime fechaEntrega, DateTime fechaEnvio, String tipoAgenda) {
        if (esEvento(tipoAgenda) && fechaEnvioEsDiferenteDeFechaEntrega(fechaEntrega, fechaEnvio) &&
                sonLasCeroHoras(fechaEntrega) && sonLasCeroHoras(fechaEnvio)) {
            DateTime fechaFinal = new DateTime(
                    fechaEntrega.getYear(), fechaEntrega.getMonthOfYear(), fechaEntrega.getDayOfMonth(),
                    fechaEnvio.getHourOfDay(), fechaEnvio.getMinuteOfHour());
            return DateHelper.formatoFullCalendar(fechaFinal);
        }
        if (esEvento(tipoAgenda) && !fechaEnvioEsDiferenteDeFechaEntrega(fechaEntrega, fechaEnvio))
            return DateHelper.formatoFullCalendar(fechaEntrega);
        return "";
    }
    private boolean fechaEnvioEsDiferenteDeFechaEntrega(DateTime fechaEntrega, DateTime fechaEnvio) {
        return fechaEntrega.equals(fechaEnvio);
    }

    private static boolean esEvento(String tipoAgenda) {
        return tipoAgenda.equals(Constantes.TIPO_AGENDA_EVENTO);
    }

    private static boolean esTarea(String tipoAgenda) {
        return tipoAgenda.equals(Constantes.TIPO_AGENDA_TAREA);
    }

    static String nombreProcedimientoPorTipo(AgendaFiltro filtro) {
        if (filtro.esEvento())
            return String.format("SP_ListaEvento%s", filtro.getGrupo());
        if (filtro.esTarea())
            return String.format("SP_ListaTarea%s", filtro.getGrupo());
        return String.format("SP_ListaAgenda%s", filtro.getGrupo());
    }

    private boolean sonLasCeroHoras(DateTime fecha) {
        return fecha.getHourOfDay() == 0 && fecha.getMinuteOfDay() == 0 && fecha.getSecondOfDay() == 0;
    }
}
