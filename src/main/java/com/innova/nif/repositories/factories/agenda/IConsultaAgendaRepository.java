package com.innova.nif.repositories.factories.agenda;

import com.innova.nif.models.wrappers.AgendaFiltro;

import java.util.List;

public interface IConsultaAgendaRepository {
    List obtenerDatos(AgendaFiltro filtros);
    String nombreProcedimientoAlmacenado(AgendaFiltro filtros);
}
