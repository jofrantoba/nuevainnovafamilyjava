package com.innova.nif.repositories.factories.usuario;

import com.innova.nif.models.Usuario;
import com.innova.nif.repositories.BaseRepository;
import com.innova.nif.utils.StringHelper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ConsultaAlumnoRepository extends BaseRepository implements IConsultaUsuarioRepository {
    public ConsultaAlumnoRepository(DataSource nifDataSource, DataSource dmDataSource) {
        this.nifDataSource = nifDataSource;
        this.dmDataSource = dmDataSource;
    }

    @Override
    public List<Usuario> obtenerDatos(String correo, int periodo) {
        var parameters = new MapSqlParameterSource()
                .addValue("p_dni", obtenerDNI(correo))
                .addValue("p_PeriodoLectivo", periodo);
        try {
            var response = executeBaseStoreProcedure(this.dmDataSource, "NIF_sp_obtenerUsuarioAL",
                    parameters, new String[]{}, new UsuarioAlumnoRowMapper());
            return (ArrayList<Usuario>) response.get(CUSTOM_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, "NIF_sp_obtenerUsuarioAL", parameters);
            throw ex;
        }
    }

    private String obtenerDNI(String correo) {
        return correo.split("@")[0];
    }

    class UsuarioAlumnoRowMapper implements RowMapper<Usuario> {

        @Override
        public Usuario mapRow(ResultSet rs, int rowNum) throws SQLException {
            Usuario usuario = new Usuario();
            mapearCamposUsuarioBasico(usuario, rs);
            usuario.setIdGrado(rs.getInt("IdGrado"));
            usuario.setSeccion(StringHelper.getValueOrEmpty(rs.getString("Seccion")));
            usuario.setIdSeccion(rs.getInt("IdSeccion"));
            usuario.setCargo("Alumno");
            usuario.setNivel(StringHelper.getValueOrEmpty(rs.getString("Nivel")));
            usuario.setGrado(StringHelper.getValueOrEmpty(rs.getString("Grado")));
            usuario.setNombreSede(StringHelper.getValueOrEmpty(rs.getString("NombreSede")));
            usuario.setRegion(StringHelper.getValueOrEmpty(rs.getString("NombreRegion")));
            return usuario;
        }

        private void mapearCamposUsuarioBasico(Usuario usuario, ResultSet rs) throws SQLException {
            usuario.setCorreo(StringHelper.getValueOrEmpty(rs.getString("Correo")));
            usuario.setApellidoPaterno(StringHelper.getValueOrEmpty(rs.getString("ApellidoPaterno")));
            usuario.setIdPersona(rs.getInt("idPersona"));
            usuario.setApellidoMaterno(StringHelper.getValueOrEmpty(rs.getString("ApellidoMaterno")));
            usuario.setGrupo(StringHelper.getValueOrEmpty(rs.getString("Grupo")));
            usuario.setDni(StringHelper.getValueOrEmpty(rs.getString("DNI")));
            usuario.setNombres(StringHelper.getValueOrEmpty(rs.getString("nombres").trim()));
            usuario.setIdSede(rs.getInt("IdSede"));
        }
    }
}
