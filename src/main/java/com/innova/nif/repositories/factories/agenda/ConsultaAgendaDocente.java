package com.innova.nif.repositories.factories.agenda;

import com.innova.nif.models.Calendario;
import com.innova.nif.models.wrappers.AgendaFiltro;
import com.innova.nif.repositories.BaseRepository;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ConsultaAgendaDocente extends BaseRepository implements IConsultaAgendaRepository {
    public ConsultaAgendaDocente(DataSource nifDataSource) {
        this.nifDataSource = nifDataSource;
    }

    @Override
    public List obtenerDatos(AgendaFiltro filtros) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue("FechaInicio", filtros.getFechaInicio())
                .addValue("FechaFin", filtros.getFechaFin())
                .addValue("IdSede", filtros.getIdSede())
                .addValue("IdPeriodo", filtros.getIdPeriodo());
        String nombreProcedimientoAlmacenado = nombreProcedimientoAlmacenado(filtros);
        agregarParametrosPorTipo(parametros, filtros);
        try {
            Map<String, Object> resultado = executeBaseStoreProcedure(this.nifDataSource, nombreProcedimientoAlmacenado,
                    parametros, new String[]{}, new ConsultaAgendaDocenteMapper());
            return (ArrayList<Calendario>) resultado.get(CUSTOM_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, nombreProcedimientoAlmacenado, parametros);
            throw ex;
        }
    }

    @Override
    public String nombreProcedimientoAlmacenado(AgendaFiltro filtros) {
        return ConsultaAgendaMapper.nombreProcedimientoPorTipo(filtros);
    }

    public void agregarParametrosPorTipo(MapSqlParameterSource parametros, AgendaFiltro filtros) {
        if (!filtros.esEvento())
            parametros.addValue("IdPersona", filtros.getIdPersona());
        if (!filtros.esTarea())
            parametros.addValue("grados", filtros.getListaIdGrado());
    }

    class ConsultaAgendaDocenteMapper implements RowMapper<Calendario> {

        @Override
        public Calendario mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new ConsultaAgendaMapper(rs).generarModelo();
        }
    }
}
