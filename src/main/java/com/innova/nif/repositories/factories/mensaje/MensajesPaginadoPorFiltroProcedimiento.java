package com.innova.nif.repositories.factories.mensaje;

public class MensajesPaginadoPorFiltroProcedimiento extends MensajePaginadoFactory {
    public MensajesPaginadoPorFiltroProcedimiento(String filtro) {
        super(filtro);
    }

    @Override
    public String nombreProcedimientoAlmacenado() {
        return esFiltro ? "SP_ConsultaBandejaPaginadoPorFiltro" : "SP_ConsultaBandejaPaginado";
    }
}
