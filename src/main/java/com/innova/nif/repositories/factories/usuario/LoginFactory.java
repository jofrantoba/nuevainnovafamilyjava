package com.innova.nif.repositories.factories.usuario;

import com.innova.nif.repositories.BaseRepository;
import com.innova.nif.utils.Constantes;
import org.springframework.stereotype.Repository;

@Repository
public class LoginFactory extends BaseRepository implements ILoginFactory {
    private IConsultaUsuarioRepository consultaUsuarioRepository;

    public IConsultaUsuarioRepository getRepository(String tipoUsuario) {
        if (tipoUsuario.equals(Constantes.CONSULTA_PADRE_FAMILIA))
            consultaUsuarioRepository = new ConsultaPadreFamiliaRepository(nifDataSource, dmDataSource);
        if (tipoUsuario.equals(Constantes.CONSULTA_ALUMNO))
            consultaUsuarioRepository = new ConsultaAlumnoRepository(nifDataSource, dmDataSource);
        if (tipoUsuario.equals(Constantes.CONSULTA_INNOVA))
            consultaUsuarioRepository = new ConsultaUsuarioInnovaRepository(nifDataSource, dmDataSource);
        if (consultaUsuarioRepository == null)
            throw new NullPointerException(String.format("No existe un usuario en DM con tipo=%s", tipoUsuario));
        return consultaUsuarioRepository;
    }
}
