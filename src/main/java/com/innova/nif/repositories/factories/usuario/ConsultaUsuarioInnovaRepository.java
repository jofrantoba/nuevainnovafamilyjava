package com.innova.nif.repositories.factories.usuario;

import com.innova.nif.models.Usuario;
import com.innova.nif.repositories.BaseRepository;
import com.innova.nif.utils.StringHelper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ConsultaUsuarioInnovaRepository extends BaseRepository implements IConsultaUsuarioRepository {
    public ConsultaUsuarioInnovaRepository(DataSource nifDataSource, DataSource dmDataSource) {
        this.nifDataSource = nifDataSource;
        this.dmDataSource = dmDataSource;
    }

    @Override
    public List<Usuario> obtenerDatos(String correo, int periodo) {
        var parameters = new MapSqlParameterSource()
                .addValue("p_Correo", correo)
                .addValue("p_PeriodoLectivo", periodo);
        try {
            var response = executeBaseStoreProcedure(this.dmDataSource, "NIF_sp_obtenerUsuarioIn",
                    parameters, new String[]{}, new UsuarioInnovaRowMapper());
            return (ArrayList<Usuario>) response.get(CUSTOM_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, "NIF_sp_obtenerUsuarioIn", parameters);
            throw ex;
        }
    }

    class UsuarioInnovaRowMapper implements RowMapper<Usuario> {

        @Override
        public Usuario mapRow(ResultSet rs, int rowNum) throws SQLException {
            Usuario usuario = new Usuario();
            usuario.setCorreo(StringHelper.getValueOrEmpty(rs.getString("Correo")));
            usuario.setIdPersona(rs.getInt("idPersona"));
            usuario.setApellidoPaterno(StringHelper.getValueOrEmpty(rs.getString("ApellidoPaterno")));
            usuario.setApellidoMaterno(StringHelper.getValueOrEmpty(rs.getString("ApellidoMaterno")));
            usuario.setDni(StringHelper.getValueOrEmpty(rs.getString("DNI")));
            usuario.setIdSede(rs.getInt("IdSede"));
            usuario.setGrupo(StringHelper.getValueOrEmpty(rs.getString("Grupo")));
            usuario.setCargo(StringHelper.getValueOrEmpty(rs.getString("Cargo")));
            usuario.setNombres(StringHelper.getValueOrEmpty(rs.getString("nombres")).trim());
            usuario.setGradoDesarrollo(StringHelper.getValueOrEmpty(rs.getString("GradoDesarrollo")));
            usuario.setNombreSede(StringHelper.getValueOrEmpty(rs.getString("NombreSede")));
            usuario.setRegion(StringHelper.getValueOrEmpty(rs.getString("NombreRegion")));
            return usuario;
        }
    }
}
