package com.innova.nif.repositories.factories.mensaje;

public class MensajesPaginadoProcedimiento extends MensajePaginadoFactory {
    public MensajesPaginadoProcedimiento(String filtro) {
        super(filtro);
    }

    @Override
    public String nombreProcedimientoAlmacenado() {
        return "SP_ConsultaBandejaPaginado";
    }
}
