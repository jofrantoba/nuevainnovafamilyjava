package com.innova.nif.repositories.factories.agenda;

public interface IAgendaFactory {
    IConsultaAgendaRepository getRepository(String grupo);
}
