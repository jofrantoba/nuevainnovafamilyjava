package com.innova.nif.repositories.factories.agenda;

import com.innova.nif.models.Calendario;
import com.innova.nif.models.wrappers.AgendaFiltro;
import com.innova.nif.repositories.BaseRepository;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ConsultaAgendaEquipoDirectivo extends BaseRepository implements IConsultaAgendaRepository {
    public ConsultaAgendaEquipoDirectivo(DataSource nifDataSource) {
        this.nifDataSource = nifDataSource;
    }

    @Override
    public List obtenerDatos(AgendaFiltro filtros) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue("FechaFin", filtros.getFechaFin())
                .addValue("FechaInicio", filtros.getFechaInicio())
                .addValue("IdSede", filtros.getIdSede())
                .addValue("IdPeriodo", filtros.getIdPeriodo());
        String procedimientoAlmacenado = nombreProcedimientoAlmacenado(filtros);
        try {
            Map<String, Object> respuesta = executeBaseStoreProcedure(this.nifDataSource, procedimientoAlmacenado,
                    parametros, new String[]{}, new ConsultaAgendaEquipoDirectivoMapper());
            return (ArrayList<Calendario>) respuesta.get(CUSTOM_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, procedimientoAlmacenado, parametros);
            throw ex;
        }
    }

    @Override
    public String nombreProcedimientoAlmacenado(AgendaFiltro filtros) {
        return ConsultaAgendaMapper.nombreProcedimientoPorTipo(filtros);
    }

    class ConsultaAgendaEquipoDirectivoMapper implements RowMapper<Calendario> {

        @Override
        public Calendario mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new ConsultaAgendaMapper(rs).generarModelo();
        }
    }
}
