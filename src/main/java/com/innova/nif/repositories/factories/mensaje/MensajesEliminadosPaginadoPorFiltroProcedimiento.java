package com.innova.nif.repositories.factories.mensaje;

public class MensajesEliminadosPaginadoPorFiltroProcedimiento extends MensajePaginadoFactory {
    public MensajesEliminadosPaginadoPorFiltroProcedimiento(String filtro) {
        super(filtro);
    }

    @Override
    public String nombreProcedimientoAlmacenado() {
        return esFiltro ? "SP_ConsultaBandejaPaginadoEliminadosPorFiltro" : "SP_ConsultaBandejaPaginadoEliminados";
    }
}
