package com.innova.nif.repositories.factories.usuario;

import com.innova.nif.models.Usuario;
import com.innova.nif.repositories.BaseRepository;
import com.innova.nif.utils.StringHelper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ConsultaPadreFamiliaRepository extends BaseRepository implements IConsultaUsuarioRepository {
    public ConsultaPadreFamiliaRepository(DataSource nifDataSource, DataSource dmDataSource) {
        this.nifDataSource = nifDataSource;
        this.dmDataSource = dmDataSource;
    }

    @Override
    public List<Usuario> obtenerDatos(String correo, int periodo) {
        var parameters = new MapSqlParameterSource()
                .addValue("p_Correo", correo)
                .addValue("p_PeriodoLectivo", periodo);
        try {
            var response = executeBaseStoreProcedure(this.dmDataSource, "NIF_sp_obtenerUsuarioPF",
                    parameters, new String[]{}, new UsuarioPadreFamiliaRowMapper());
            return (ArrayList<Usuario>) response.get(CUSTOM_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, "NIF_sp_obtenerUsuarioPF", parameters);
            throw ex;
        }
    }

    class UsuarioPadreFamiliaRowMapper implements RowMapper<Usuario> {

        @Override
        public Usuario mapRow(ResultSet rs, int rowNum) throws SQLException {
            Usuario usuario = new Usuario();
            usuario.setIdPersona(rs.getInt("idPersona"));
            usuario.setCorreo(StringHelper.getValueOrEmpty(rs.getString("Correo")));
            usuario.setApellidoMaterno(StringHelper.getValueOrEmpty(rs.getString("ApellidoMaterno")));
            usuario.setApellidoPaterno(StringHelper.getValueOrEmpty(rs.getString("ApellidoPaterno")));
            usuario.setGrupo(StringHelper.getValueOrEmpty(rs.getString("Grupo")));
            usuario.setDni(StringHelper.getValueOrEmpty(rs.getString("DNI")));
            usuario.setIdSede(rs.getInt("IdSede"));
            usuario.setNombres(StringHelper.getValueOrEmpty(rs.getString("nombres").trim()));
            usuario.setCargo(StringHelper.getValueOrEmpty(rs.getString("Cargo")));
            return usuario;
        }
    }
}
