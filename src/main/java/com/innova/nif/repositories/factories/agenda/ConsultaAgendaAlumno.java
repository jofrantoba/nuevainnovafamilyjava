package com.innova.nif.repositories.factories.agenda;

import com.google.common.base.Strings;
import com.innova.nif.models.Calendario;
import com.innova.nif.models.wrappers.AgendaFiltro;
import com.innova.nif.repositories.BaseRepository;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ConsultaAgendaAlumno extends BaseRepository implements IConsultaAgendaRepository {
    public ConsultaAgendaAlumno(DataSource nifDataSource) {
        this.nifDataSource = nifDataSource;
    }

    @Override
    public List obtenerDatos(AgendaFiltro filtros) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue("FechaInicio", filtros.getFechaInicio())
                .addValue("FechaFin", filtros.getFechaFin())
                .addValue("IdSede", filtros.getIdSede())
                .addValue("IdPeriodo", filtros.getIdPeriodo())
                .addValue("IdGrado", filtros.getIdGrado());
        agregarParametrosPorTipo(parametros, filtros);
        String procedimientoAlmacenado = nombreProcedimientoAlmacenado(filtros);
        try {
            Map<String, Object> respuesta = executeBaseStoreProcedure(this.nifDataSource, procedimientoAlmacenado,
                    parametros, new String[]{}, new ConsultaAgendaAlumnoMapper());
            return (ArrayList<Calendario>) respuesta.get(CUSTOM_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, procedimientoAlmacenado, parametros);
            throw ex;
        }
    }

    @Override
    public String nombreProcedimientoAlmacenado(AgendaFiltro filtros) {
        if (Strings.isNullOrEmpty(filtros.getNivelIngles()))
            return ConsultaAgendaMapper.nombreProcedimientoPorTipo(filtros);
        if (filtros.esTarea())
            return "SP_ListaTareaInglesAL";
        return "SP_ListaAgendaInglesAL";
    }

    public void agregarParametrosPorTipo(MapSqlParameterSource parametros, AgendaFiltro filtros) {
        if (!filtros.esEvento())
            parametros.addValue("Seccion", filtros.getSeccion());
        if (!Strings.isNullOrEmpty(filtros.getNivelIngles()))
            parametros.addValue("NivelIngles", filtros.getNivelIngles());
    }

    class ConsultaAgendaAlumnoMapper implements RowMapper<Calendario> {

        @Override
        public Calendario mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new ConsultaAgendaMapper(rs).generarModelo();
        }
    }
}
