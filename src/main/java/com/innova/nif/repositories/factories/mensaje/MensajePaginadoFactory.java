package com.innova.nif.repositories.factories.mensaje;

import lombok.Getter;
import lombok.Setter;

public abstract class MensajePaginadoFactory {
    public static final String MENSAJES_PAGINADOS = "1";
    public static final String MENSAJES_ENVIADOS_PAGINADOS = "2";
    public static final String MENSAJES_ELIMINADOS_PAGINADOS = "3";

    @Getter
    @Setter
    String filtro;

    @Getter
    boolean esFiltro;

    public MensajePaginadoFactory(String filtro) {
        this.filtro = filtro;
        this.esFiltro = !this.filtro.isEmpty() || !this.filtro.isBlank();
    }

    public abstract String nombreProcedimientoAlmacenado();
}
