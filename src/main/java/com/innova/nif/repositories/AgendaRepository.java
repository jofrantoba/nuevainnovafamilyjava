package com.innova.nif.repositories;

import com.innova.nif.models.*;
import com.innova.nif.models.wrappers.AgendaFiltro;
import com.innova.nif.repositories.interfaces.IAgendaRepository;
import com.innova.nif.services.utils.AgendaFechaHoraBuilder;
import com.innova.nif.utils.DateHelper;
import com.innova.nif.utils.StringHelper;
import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class AgendaRepository extends BaseRepository implements IAgendaRepository {
    private static final String ID_PERIODO = "IdPeriodo";
    private static final String FECHA_ENTREGA = "FechaEntrega";
    private static final String FECHA_ENVIO = "FechaEnvio";
    private static final String DESCRIPCION = "Descripcion";
    private static final String TITULO = "Titulo";
    private static final String ID_LOG = "IdLog";
    private static final String TIPO_AGENDA = "TipoAgenda";
    private static final String COLOR = "Color";
    private static final String ID_GRADO = "IdGrado";
    private static final String ID_SEDE = "IdSede";
    private static final String ID_EVENTO = "IdEvento";
    private static final String UBICACION = "Ubicacion";
    private static final String ID_DETALLE_TAREA_OUT = "IdDetalleTareaOut";
    private static final String ID_TAREA = "IdTarea";
    private static final String ID_PERSONA = "IdPersona";
    private static final String CODIGO_GRADO = "CodigoGrado";
    private static final String NOMBRE_CURSO = "NombreCurso";
    public static final String SP_REGISTRO_LOG_TAREA = "SP_RegistroLogTarea";

    @Override
    public EventoGeneral obtenerEventoEDDC(int idTareaEvento) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue(ID_EVENTO, idTareaEvento);
        try {
            Map<String, Object> resultado = executeNifStoreProcedure("SP_ObtenerEventoDCED", parametros,
                    new EventoGeneralMapper(idTareaEvento));
            List<EventoGeneral> eventos = new ArrayList<>();
            if (resultado.get(CUSTOM_RESULT_SET) instanceof List)
                eventos = (List<EventoGeneral>) resultado.get(CUSTOM_RESULT_SET);
            return eventos.isEmpty() ? new EventoGeneral() : eventos.get(0);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_ObtenerEventoDCED", parametros);
            throw ex;
        }
    }

    @Override
    public List<AgendaAdjunto> adjuntosAgenda(AgendaFiltro filtros) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue("IdAgenda", filtros.getIdTareaEvento())
                .addValue(TIPO_AGENDA, filtros.getTipoAgenda());
        try {
            Map<String, Object> resultado = executeNifStoreProcedure("SP_ListaAgendaArchivoAdjunto",
                    parametros, new AgendaAdjuntosMapper());
            return (List<AgendaAdjunto>) resultado.get(CUSTOM_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_ListaAgendaArchivoAdjunto", parametros);
            throw ex;
        }
    }

    @Override
    public TareaGeneral obtenerTarea(AgendaFiltro filtros) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue(ID_TAREA, filtros.getIdTareaEvento())
                .addValue("IdDetalleTarea", filtros.getIdAula());
        try {
            Map<String, Object> resultado = executeNifStoreProcedure("SP_ObtenerTarea", parametros,
                    new TareaGeneralMapper(filtros.getIdTareaEvento()));
            List<TareaGeneral> tareas = new ArrayList<>();
            if (resultado.get(CUSTOM_RESULT_SET) instanceof List)
                tareas = (List<TareaGeneral>) resultado.get(CUSTOM_RESULT_SET);
            return tareas.isEmpty() ? new TareaGeneral() : tareas.get(0);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_ObtenerTarea", parametros);
            throw ex;
        }
    }

    @Override
    public EventoGeneral obtenerEventoALPF(AgendaFiltro filtros) {
        MapSqlParameterSource parametros = new MapSqlParameterSource().addValue(ID_EVENTO, filtros.getIdTareaEvento());
        try {
            Map<String, Object> resultado = executeNifStoreProcedure("SP_ObtenerEventoALPF", parametros,
                    new EventoGeneralPFMapper());
            List<EventoGeneral> eventos = new ArrayList<>();
            if (resultado.get(CUSTOM_RESULT_SET) instanceof List)
                eventos = (List<EventoGeneral>) resultado.get(CUSTOM_RESULT_SET);
            return !eventos.isEmpty() ? eventos.get(0) : new EventoGeneral();
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_ObtenerEventoALPF", parametros);
            throw ex;
        }
    }

    @Override
    public int registrarTarea(TareaCalendario tarea) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue(ID_PERSONA, tarea.getIdPersona())
                .addValue(TITULO, tarea.getTitulo())
                .addValue(DESCRIPCION, tarea.getDescripcion())
                .addValue(FECHA_ENTREGA, DateHelper.convertirASQLDate(tarea.getFechaEntrega()))
                .addValue(COLOR, tarea.getColor())
                .addValue(ID_PERIODO, tarea.getIdPeriodo())
                .addValue(ID_TAREA, "");
        try {
            String[] rowValues = {ID_TAREA};
            Map<String, Object> resultado = executeNifStoreProcedure("SP_RegistroTarea", parametros, rowValues);
            return (int) resultado.get(ID_TAREA);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_RegistroTarea", parametros);
            throw ex;
        }
    }

    @Override
    public DetalleTarea registrarDetalleTarea(DetalleTarea detalleTarea) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue(ID_TAREA, detalleTarea.getIdTarea())
                .addValue(ID_SEDE, detalleTarea.getIdSede())
                .addValue(ID_GRADO, detalleTarea.getIdGrado())
                .addValue(CODIGO_GRADO, detalleTarea.getCodigoGrado())
                .addValue("Seccion", detalleTarea.getSeccion())
                .addValue("IdCurso", detalleTarea.getIdCurso())
                .addValue(NOMBRE_CURSO, detalleTarea.getNombreCurso())
                .addValue(ID_DETALLE_TAREA_OUT, "");
        try {
            String[] rowValues = {ID_DETALLE_TAREA_OUT};
            Map<String, Object> respuesta = executeNifStoreProcedure("SP_RegistroDetalleTarea", parametros, rowValues);
            detalleTarea.setIdDetalleTarea((int) respuesta.get(ID_DETALLE_TAREA_OUT));
            return detalleTarea;
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_RegistroDetalleTarea", parametros);
            throw ex;
        }
    }

    @Override
    public int registrarEvento(EventoCalendario evento) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue(ID_PERSONA, evento.getIdPersona())
                .addValue(TITULO, evento.getTitulo())
                .addValue(DESCRIPCION, evento.getDescripcion())
                .addValue("FechaInicio", DateHelper.convertirASQLDateTime(evento.getFechaInicio()))
                .addValue("FechaFin", DateHelper.convertirASQLDateTime(evento.getFechaFin()))
                .addValue(UBICACION, evento.getUbicacion())
                .addValue(COLOR, evento.getColor())
                .addValue("ValorRegistro", evento.getValorRegistro())
                .addValue("CadenaParticipante", evento.getCadenaParticipante())
                .addValue(ID_PERIODO, evento.getIdPeriodo())
                .addValue(ID_EVENTO, "");
        try {
            String[] rowValues = {ID_EVENTO};
            Map<String, Object> resultado = executeNifStoreProcedure("SP_RegistroEvento", parametros,
                    rowValues);
            return (int) resultado.get(ID_EVENTO);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_RegistroEvento", parametros);
            throw ex;
        }
    }

    @Override
    public void registrarDetalleEvento(DetalleEvento detalleEvento) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue(ID_EVENTO, detalleEvento.getIdEvento())
                .addValue(ID_SEDE, detalleEvento.getIdSede())
                .addValue(ID_GRADO, detalleEvento.getIdGrado())
                .addValue(CODIGO_GRADO, detalleEvento.getCodigoGrado())
                .addValue("Nivel", detalleEvento.getNivel())
                .addValue("Grado", detalleEvento.getGrado());
        try {
            executeNifStoreProcedure("SP_RegistroDetalleEvento", parametros);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_RegistroDetalleEvento", parametros);
            throw ex;
        }
    }

    @Override
    public void registrarAdjunto(AgendaAdjunto adjunto) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue("IdTareaEvento", adjunto.getIdTareaEvento())
                .addValue(TIPO_AGENDA, adjunto.getTipoAgenda())
                .addValue("Nombre", adjunto.getNombreArchivo())
                .addValue("NombreAlterado", adjunto.getNombreAlterado())
                .addValue("TamanioArchivo", adjunto.getTamanioArchivo())
                .addValue("IdArchivo", "");
        try {
            executeNifStoreProcedure("SP_RegistroArchivo", parametros);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_RegistroArchivo", parametros);
            throw ex;
        }
    }

    @Override
    public List<String> obtenerGradosEvento(Integer idEvento) {
        String query = "select IdGrado, IdSede from DetalleEvento where idevento=?";
        Object[] params = {idEvento};
        try {
            return (List<String>) executeNifQuery(query, params, new GradosEventoMapper());
        } catch (Exception ex) {
            registrarExepcion(ex, query, params);
            throw ex;
        }
    }

    @Override
    public void editarEvento(EventoCalendario evento) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue(ID_PERSONA, evento.getIdPersona())
                .addValue(TITULO, evento.getTitulo())
                .addValue(DESCRIPCION, evento.getDescripcion())
                .addValue("FechaFin", DateHelper.convertirASQLDateTime(evento.getFechaFin()))
                .addValue("FechaInicio", DateHelper.convertirASQLDateTime(evento.getFechaInicio()))
                .addValue(UBICACION, evento.getUbicacion())
                .addValue(COLOR, evento.getColor())
                .addValue("ValorRegistro", evento.getValorRegistro())
                .addValue("CadenaParticipante", evento.getCadenaParticipante())
                .addValue(ID_PERIODO, evento.getIdPeriodo())
                .addValue(ID_EVENTO, evento.getIdEvento());
        try {
            executeNifStoreProcedure("SP_EdicionEvento", parametros);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_EdicionEvento", parametros);
            throw ex;
        }
    }

    @Override
    public void eliminarAdjuntos(List<Integer> idsArchivosEliminar) {
        String ids = idsArchivosEliminar.stream().map(String::valueOf).collect(Collectors.joining(","));
        String query = "delete [dbo].[Archivo] where idArchivo in (?)";
        Object[] parametros = {ids};
        try {
            executeNifDeleteQuery(query, parametros);
        } catch (Exception ex) {
            registrarExepcion(ex, query, parametros);
            throw ex;
        }
    }

    @Override
    public void registrarLogEvento(LogAgenda logEvento) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue(ID_TAREA, logEvento.getIdTarea())
                .addValue("IdDocente", logEvento.getIdDocente())
                .addValue(TIPO_AGENDA, logEvento.getTipo())
                .addValue("FechaLog", DateHelper.convertirASQLDateTime(logEvento.getFechaLog()))
                .addValue("Original", logEvento.getOriginal())
                .addValue("Modificado", logEvento.getModificado())
                .addValue(ID_LOG, "");
        try {
            String[] rowValues = {ID_LOG};
            Map<String, Object> resultado = executeNifStoreProcedure(SP_REGISTRO_LOG_TAREA, parametros,
                    rowValues);
            logEvento.setIdLog((int) resultado.get(ID_LOG));
        } catch (Exception ex) {
            registrarExepcion(ex, SP_REGISTRO_LOG_TAREA, parametros);
            throw ex;
        }
    }

    @Override
    public void editarTarea(TareaCalendario tarea, DetalleTarea detalleTarea) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue(TITULO, tarea.getTitulo())
                .addValue(DESCRIPCION, tarea.getDescripcion())
                .addValue(ID_TAREA, tarea.getIdTarea())
                .addValue(FECHA_ENTREGA, DateHelper.convertirASQLDate(tarea.getFechaEntrega()))
                .addValue("IdDetalleTarea", detalleTarea.getIdDetalleTarea())
                .addValue("IdCurso", detalleTarea.getIdCurso())
                .addValue(NOMBRE_CURSO, detalleTarea.getNombreCurso())
                .addValue(ID_SEDE, detalleTarea.getIdSede())
                .addValue(ID_GRADO, detalleTarea.getIdGrado())
                .addValue(CODIGO_GRADO, detalleTarea.getCodigoGrado())
                .addValue("Seccion", detalleTarea.getSeccion());
        try {
            executeNifStoreProcedure("SP_ActualizarTarea", parametros);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_ActualizarTarea", parametros);
            throw ex;
        }
    }

    @Override
    public void eliminarEvento(int idEvento) {
        MapSqlParameterSource parametros = new MapSqlParameterSource().addValue(ID_EVENTO, idEvento);
        try {
            executeNifStoreProcedure("SP_EliminarEvento", parametros);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_EliminarEvento", parametros);
            throw ex;
        }
    }

    @Override
    public int eliminarTarea(int idTarea) {
        MapSqlParameterSource parametros = new MapSqlParameterSource().addValue(ID_TAREA, idTarea);
        try {
            Map<String, Object> resultado = executeNifStoreProcedure("SP_EliminarTarea", parametros);
            return (int) resultado.get(DEFAULT_EXECUTE_NON_QUERY_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_EliminarTarea", parametros);
            throw ex;
        }
    }

    @Override
    public Docente obtenerDocente(int idPersona) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue("p_idPersona", idPersona);
        try {
            Map<String, Object> resultado = executeDMStoreProcedure("NIF_sp_obtenerNombreDC", parametros,
                    new DocenteMapper());
            List<Docente> respuesta = (List<Docente>) resultado.get(CUSTOM_RESULT_SET);
            if (!respuesta.isEmpty())
                return respuesta.get(0);
            return new Docente();
        } catch (Exception ex) {
            registrarExepcion(ex, "NIF_sp_obtenerNombreDC", parametros);
            throw ex;
        }
    }

    @Override
    public int registrarLogTarea(LogAgenda logTarea) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue(ID_TAREA, logTarea.getIdTarea())
                .addValue("IdDocente", logTarea.getIdDocente())
                .addValue(TIPO_AGENDA, logTarea.getTipo())
                .addValue("FechaLog", DateHelper.convertirASQLDateTime(logTarea.getFechaLog()))
                .addValue("Original", logTarea.getOriginal())
                .addValue("Modificado", logTarea.getModificado())
                .addValue(ID_LOG, "");
        try {
            String[] rowValues = {ID_LOG};
            Map<String, Object> resultado = executeNifStoreProcedure(SP_REGISTRO_LOG_TAREA, parametros, rowValues);
            return (int) resultado.get(ID_LOG);
        } catch (Exception ex) {
            registrarExepcion(ex, SP_REGISTRO_LOG_TAREA, parametros);
            throw ex;
        }
    }

    @Override
    public int eliminarAdjunto(int idArchivo) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue("IdAdjunto", idArchivo);
        try {
            String[] rowValues = {"IdAdjunto"};
            Map<String, Object> resultado = executeNifStoreProcedure("SP_EliminarAdjunto", parametros,
                    rowValues);
            return (int) resultado.get(DEFAULT_EXECUTE_NON_QUERY_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_EliminarAdjunto", parametros);
            throw ex;
        }
    }

    class EventoGeneralMapper implements RowMapper<EventoGeneral> {
        int idEvento;

        EventoGeneralMapper(int idEvento) {
            this.idEvento = idEvento;
        }

        @Override
        public EventoGeneral mapRow(ResultSet rs, int rowNum) throws SQLException {
            EventoGeneral evento = new EventoGeneral();
            EventoCalendario eventoCalendario = new EventoCalendario();
            eventoCalendario.setIdEvento(idEvento);
            eventoCalendario.setTitulo(StringHelper.getValueOrEmpty(rs.getString(TITULO)));
            eventoCalendario.setDescripcion(StringHelper.getValueOrEmpty(rs.getString(DESCRIPCION)));
            eventoCalendario.setFechaInicio(new DateTime(rs.getTimestamp(FECHA_ENVIO)));
            eventoCalendario.setFechaFin(new DateTime(rs.getTimestamp(FECHA_ENTREGA)));
            eventoCalendario.setUbicacion(StringHelper.getValueOrEmpty(rs.getString(UBICACION)));
            eventoCalendario.setIdPersona(rs.getInt(ID_PERSONA));
            evento.setEvento(eventoCalendario);
            evento.setParticipante(StringHelper.getValueOrEmpty(rs.getString("Participante")));
            evento.setEsEditado(rs.getBoolean("EsEditado"));
            evento.setTodaLaSede(rs.getBoolean("TodaLaSede"));
            return evento;
        }
    }

    class AgendaAdjuntosMapper implements RowMapper<AgendaAdjunto> {

        @Override
        public AgendaAdjunto mapRow(ResultSet rs, int rowNum) throws SQLException {
            AgendaAdjunto adjunto = new AgendaAdjunto();
            adjunto.setIdArchivo(rs.getInt("IdArchivo"));
            adjunto.setNombreArchivo(StringHelper.getValueOrEmpty(rs.getString("NombreArchivo")));
            adjunto.setNombreAlterado(StringHelper.getValueOrEmpty(rs.getString("NombreModificado")));
            return adjunto;
        }
    }

    class TareaGeneralMapper implements RowMapper<TareaGeneral> {
        int idTareaEvento;

        public TareaGeneralMapper(int idTareaEvento) {
            this.idTareaEvento = idTareaEvento;
        }

        @Override
        public TareaGeneral mapRow(ResultSet rs, int rowNum) throws SQLException {
            TareaGeneral tarea = new TareaGeneral();
            TareaCalendario tareaCalendario = new TareaCalendario();
            tareaCalendario.setTitulo(StringHelper.getValueOrEmpty(rs.getString(TITULO)));
            tareaCalendario.setDescripcion(StringHelper.getValueOrEmpty(rs.getString(DESCRIPCION)));
            tareaCalendario.setFechaEntrega(new DateTime(rs.getTimestamp(FECHA_ENTREGA)));
            tareaCalendario.setFechaEnvio(new DateTime(rs.getTimestamp(FECHA_ENVIO)));
            tareaCalendario.setFechaRegistro(new DateTime(rs.getTimestamp("FechaRegistro")));
            tareaCalendario.setIdPersona(rs.getInt(ID_PERSONA));
            tareaCalendario.setIdTarea(idTareaEvento);
            tarea.setTarea(tareaCalendario);
            tarea.setCabecera(StringHelper.getValueOrEmpty(rs.getString("Cabecera")));
            tarea.setGradoSeccion(StringHelper.getValueOrEmpty(rs.getString("GradoSeccion")));
            tarea.setIdFoto(rs.getInt("IdFoto"));
            tarea.setUrlFoto(StringHelper.getValueOrEmpty(rs.getString("RutaFoto")));
            tarea.setNombreCurso(StringHelper.getValueOrEmpty(rs.getString(NOMBRE_CURSO)));
            return tarea;
        }
    }

    class EventoGeneralPFMapper implements RowMapper<EventoGeneral> {

        @Override
        public EventoGeneral mapRow(ResultSet rs, int rowNum) throws SQLException {
            EventoGeneral evento = new EventoGeneral();
            EventoCalendario eventoCalendario = new EventoCalendario();
            eventoCalendario.setIdSede(rs.getInt(ID_SEDE));
            eventoCalendario.setTitulo(rs.getString(TITULO));
            eventoCalendario.setDescripcion(rs.getString(DESCRIPCION));
            eventoCalendario.setFechaInicio(new DateTime(rs.getTimestamp(FECHA_ENVIO)));
            eventoCalendario.setFechaFin(new DateTime(rs.getTimestamp(FECHA_ENTREGA)));
            eventoCalendario.setUbicacion(rs.getString(UBICACION));
            evento.setEvento(eventoCalendario);
            evento.setParticipante(StringHelper.getValueOrEmpty(rs.getString("Participante")));
            evento.setEsEditado(rs.getBoolean("EsEditado"));
            evento.setFechaHora(new AgendaFechaHoraBuilder(
                    eventoCalendario.getFechaInicio(), eventoCalendario.getFechaFin()).construirFechaHora()
            );
            return evento;
        }
    }

    class GradosEventoMapper implements RowMapper<String> {

        @Override
        public String mapRow(ResultSet rs, int rowNum) throws SQLException {
            String grado = StringHelper.getValueOrEmpty(rs.getString(ID_GRADO));
            String sede = StringHelper.getValueOrEmpty(rs.getString(ID_SEDE));
            return String.format("%s_%s", sede, grado);
        }
    }

    class DocenteMapper implements RowMapper<Docente> {

        @Override
        public Docente mapRow(ResultSet rs, int rowNum) throws SQLException {
            Docente docente = new Docente();
            docente.setNombres(StringHelper.getValueOrEmpty(rs.getString("nombres")));
            docente.setApellidoPaterno(StringHelper.getValueOrEmpty(rs.getString("paterno")));
            docente.setApellidoMaterno(StringHelper.getValueOrEmpty(rs.getString("materno")));
            docente.setNombresIniciales(docente.getPrimerNombre(), docente.getApellidoPaterno());
            return docente;
        }
    }
}