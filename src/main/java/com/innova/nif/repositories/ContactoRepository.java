package com.innova.nif.repositories;

import com.innova.nif.models.*;
import com.innova.nif.models.wrappers.FiltroWrapper;
import com.innova.nif.repositories.interfaces.IContactoRepository;
import com.innova.nif.repositories.utils.ParametrosAlumno;
import com.innova.nif.repositories.utils.ParametrosDocente;
import com.innova.nif.utils.StringHelper;
import org.apache.commons.lang.WordUtils;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ContactoRepository extends BaseRepository implements IContactoRepository {
    private static final String ID_GRADO = "IdGrado";
    private static final String CARGO = "Cargo";
    private static final String ID_PERSONA = "IdPersona";
    private static final String NOMBRES = "Nombres";
    private static final String APELLIDO_PATERNO = "ApellidoPaterno";
    private static final String APELLIDO_MATERNO = "ApellidoMaterno";
    private static final String CORREO = "Correo";
    private static final String GRUPO = "Grupo";
    private static final String ESTADO = "Estado";
    private static final String DESCRIPCION = "Descripcion";
    private static final String P_ID_SEDE = "p_id_sede";
    private static final String P_ID_PERIODO = "p_id_periodo";

    @Override
    public List<MensajeRemitente> listaCorreoRemitente(int idMensaje) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue("IdMensaje", idMensaje);
        try {
            Map<String, Object> resultado = executeNifStoreProcedure("SP_ConsultarMensajeRemitente",
                    parametros, new CorreoRemitenteMapper());
            return (List<MensajeRemitente>) resultado.get(CUSTOM_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_ConsultarMensajeRemitente", parametros);
            throw ex;
        }
    }

    @Override
    public List<Contacto> equipoDirectivoPorSede(int idSede) {
        MapSqlParameterSource parametros = new MapSqlParameterSource().addValue(P_ID_SEDE, idSede);
        try {
            Map<String, Object> resultado = executeDMStoreProcedure("NIF_sp_ConsultarED", parametros,
                    new EquipoDirectivoSedeMapper());
            return (List<Contacto>) resultado.get(CUSTOM_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, "NIF_sp_ConsultarED", parametros);
            throw ex;
        }
    }

    @Override
    public List<Docente> docentes(FiltroWrapper filtros) {
        ParametrosDocente parametrosDocente = new ParametrosDocente(filtros);
        try {
            Map<String, Object> resultado = executeDMStoreProcedure(parametrosDocente.getNombreProcedimiento(),
                    parametrosDocente.getParametros(), new DocenteContactoMapper());
            List<Docente> docentes = (List<Docente>) resultado.get(CUSTOM_RESULT_SET);
            return obtenerDocentesUnicos(docentes);
        } catch (Exception ex) {
            registrarExepcion(ex, parametrosDocente.getNombreProcedimiento(), parametrosDocente.getParametros());
            throw ex;
        }
    }

    @Override
    public List<Grado> listaGrados() {
        MapSqlParameterSource parametros = new MapSqlParameterSource();
        try {
            Map<String, Object> resultado = executeDMStoreProcedure("NIF_sp_listaGrado", parametros,
                    new ListaGradosMapper());
            return (List<Grado>) resultado.get(CUSTOM_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, "NIF_sp_listaGrado", parametros);
            throw ex;
        }
    }

    @Override
    public List<DocenteNivelIngles> obtenerNivelInglesDocente(FiltroWrapper filtros) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue("idsede", filtros.getIdSede())
                .addValue("anio", filtros.getAnio())
                .addValue("idgrado", filtros.getGrado())
                .addValue("dni", StringHelper.getValueOrEmpty(filtros.getDni()));
        try {
            Map<String, Object> resultado = executePeaStoreProcedure("NIF_sp_listarNivelInglesDC", parametros,
                    new DocenteNivelInglesMapper());
            return (List<DocenteNivelIngles>) resultado.get(CUSTOM_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, "NIF_sp_listarNivelInglesDC", parametros);
            throw ex;
        }
    }

    @Override
    public List<Docente> docentePorDNI(String dni) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue("p_numero_doc", dni);
        try {
            Map<String, Object> respuesta = executeDMStoreProcedure("NIF_sp_ConsultarDCPorDni", parametros,
                    new DocenteContactoDNIMapper());
            return (List<Docente>) respuesta.get(CUSTOM_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, "NIF_sp_ConsultarDCPorDni", parametros);
            throw ex;
        }
    }

    @Override
    public List<Grado> listaGradosPorSedePeriodo(int idSede, int idPeriodo) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue(P_ID_SEDE, idSede, Types.BIGINT)
                .addValue(P_ID_PERIODO, idPeriodo, Types.BIGINT);
        try {
            Map<String, Object> respuesta = executeDMStoreProcedure("NIF_sp_listaGradoBySedePeriodo", parametros,
                    new GradosPorSedePeriodoMapper());
            return (List<Grado>) respuesta.get(CUSTOM_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, "NIF_sp_listaGradoBySedePeriodo", parametros);
            throw ex;
        }
    }

    @Override
    public List<SeccionAlumno> seccionesDM(FiltroWrapper filtros) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue(P_ID_PERIODO, filtros.getIdPeriodo())
                .addValue(P_ID_SEDE, filtros.getIdSede())
                .addValue("p_id_grado", filtros.getIdGrado());
        try {
            Map<String, Object> respuesta = executeDMStoreProcedure("NIF_sp_listarSeccion", parametros,
                    new SeccionesDMMapper());
            return (List<SeccionAlumno>) respuesta.get(CUSTOM_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, "NIF_sp_listarSeccion", parametros);
            throw ex;
        }
    }

    @Override
    public List<Contacto> listarPadresFamilia(FiltroWrapper filtros) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue(P_ID_SEDE, filtros.getIdSede())
                .addValue(P_ID_PERIODO, filtros.getIdPeriodo())
                .addValue("p_id_grado", filtros.getIdGrado())
                .addValue("p_id_seccion", filtros.getIdSeccion())
                .addValue("p_nivel", filtros.getNivel());
        try {
            Map<String, Object> respuesta = executeDMStoreProcedure("NIF_sp_ConsultarPF", parametros,
                    new PadreFamiliaMapper());
            return (List<Contacto>) respuesta.get(CUSTOM_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, "NIF_sp_ConsultarPF", parametros);
            throw ex;
        }
    }

    @Override
    public List<Contacto> listarAlumnos(FiltroWrapper filtros) {
        ParametrosAlumno parametrosAlumno = new ParametrosAlumno(filtros);
        try {
            Map<String, Object> respuesta = executeDMStoreProcedure(parametrosAlumno.getNombreProcedimiento(),
                    parametrosAlumno.getParametros(), new AlumnoMapper());
            return (List<Contacto>) respuesta.get(CUSTOM_RESULT_SET);
        } catch (Exception ex) {
            registrarExepcion(ex, parametrosAlumno.getNombreProcedimiento(), parametrosAlumno.getParametros());
            throw ex;
        }
    }

    class CorreoRemitenteMapper implements RowMapper<MensajeRemitente> {

        @Override
        public MensajeRemitente mapRow(ResultSet rs, int rowNum) throws SQLException {
            MensajeRemitente mensajeRemitente = new MensajeRemitente();
            mensajeRemitente.setIdMensaje(rs.getInt("IdMensaje"));
            mensajeRemitente.setIdRemitente(rs.getInt("IdRemitente"));
            mensajeRemitente.setNombreRemitente(StringHelper.getValueOrEmpty(rs.getString("NombreRemitente")));
            mensajeRemitente.setCorreoRemitente(StringHelper.getValueOrEmpty(rs.getString("CorreoRemitente")));
            mensajeRemitente.setGrupoRemitente(StringHelper.getValueOrEmpty(rs.getString("GrupoRemitente")));
            mensajeRemitente.setIniciales(StringHelper.getValueOrEmpty(rs.getString("Iniciales")));
            mensajeRemitente.setCargo(StringHelper.getValueOrEmpty(rs.getString(CARGO)));
            mensajeRemitente.setFirma(StringHelper.getValueOrEmpty(rs.getString("Firma")));
            mensajeRemitente.setEsEliminado(rs.getBoolean("EsEliminado"));
            return mensajeRemitente;
        }
    }

    class EquipoDirectivoSedeMapper implements RowMapper<Contacto> {

        @Override
        public Contacto mapRow(ResultSet rs, int rowNum) throws SQLException {
            return mapearContactoBase(rs, rowNum);
        }

        private Contacto mapearContactoBase(ResultSet rs, int rowNum) throws SQLException {
            Contacto contacto = new Contacto();
            contacto.setIdPersona(rs.getInt(ID_PERSONA));
            contacto.setNombres(WordUtils.capitalizeFully(rs.getString(NOMBRES)));
            contacto.setApellidoPaterno(WordUtils.capitalizeFully(rs.getString(APELLIDO_PATERNO)));
            contacto.setApellidoMaterno(WordUtils.capitalizeFully(rs.getString(APELLIDO_MATERNO)));
            contacto.setCorreo(StringHelper.getValueOrEmpty(rs.getString(CORREO)));
            contacto.setGrupo(StringHelper.getValueOrEmpty(rs.getString(GRUPO)));
            contacto.setCargo(StringHelper.getValueOrEmpty(rs.getString(CARGO)));
            contacto.setEstado(rs.getInt(ESTADO));
            contacto.setOrden(rowNum);
            return contacto;
        }
    }

    class DocenteContactoMapper implements RowMapper<Contacto> {

        @Override
        public Docente mapRow(ResultSet rs, int rowNum) throws SQLException {
            Docente docente = new Docente();
            docente.setIdPersona(rs.getInt(ID_PERSONA));
            docente.setNombres(WordUtils.capitalizeFully(rs.getString(NOMBRES)));
            docente.setApellidoPaterno(WordUtils.capitalizeFully(rs.getString(APELLIDO_PATERNO)));
            docente.setCorreo(StringHelper.getValueOrEmpty(rs.getString(CORREO)));
            docente.setApellidoMaterno(WordUtils.capitalizeFully(rs.getString(APELLIDO_MATERNO)));
            docente.setCargo(StringHelper.getValueOrEmpty(rs.getString(CARGO)));
            docente.setGrupo(StringHelper.getValueOrEmpty(rs.getString(GRUPO)));
            docente.setEstado(rs.getInt(ESTADO));
            docente.setOrden(rowNum);
            docente.setIdGrado(rs.getInt(ID_GRADO));
            docente.setDni(StringHelper.getValueOrEmpty(rs.getString("Dni")));
            docente.setIdCurso(rs.getInt("IdCurso"));
            docente.setIdCursoPadre(rs.getInt("IdCursoPadre"));
            return docente;
        }
    }

    class ListaGradosMapper implements RowMapper<Grado> {

        @Override
        public Grado mapRow(ResultSet rs, int rowNum) throws SQLException {
            Grado grado = new Grado();
            grado.setIdGrado(rs.getInt(ID_GRADO));
            grado.setDescripcion(StringHelper.getValueOrEmpty(rs.getString(DESCRIPCION)));
            grado.setCodigoGrado(StringHelper.getValueOrEmpty(rs.getString("Codigo")));
            grado.setIdNivel(rs.getInt("Nivel"));
            grado.setDetalleNivel(StringHelper.getValueOrEmpty(rs.getString("DetalleNivel")));
            return grado;
        }
    }

    class DocenteNivelInglesMapper implements RowMapper<DocenteNivelIngles> {

        @Override
        public DocenteNivelIngles mapRow(ResultSet rs, int rowNum) throws SQLException {
            DocenteNivelIngles docenteNivelIngles = new DocenteNivelIngles();
            docenteNivelIngles.setClassRoomLevelCode(StringHelper.getValueOrEmpty(rs.getString("ClassRoomLevelCode")));
            docenteNivelIngles.setEnglishLevelId(StringHelper.getValueOrEmpty(rs.getString("NivelIngles")));
            docenteNivelIngles.setDni(StringHelper.getValueOrEmpty(rs.getString("DNI")));
            docenteNivelIngles.setGrado(StringHelper.getValueOrEmpty(rs.getString("Grado")));
            docenteNivelIngles.setIdGrado(rs.getInt(ID_GRADO));
            return docenteNivelIngles;
        }
    }

    class DocenteContactoDNIMapper implements RowMapper<Contacto> {

        @Override
        public Docente mapRow(ResultSet rs, int rowNum) throws SQLException {
            Docente docente = new Docente();
            docente.setIdPersona(rs.getInt(ID_PERSONA));
            docente.setNombres(WordUtils.capitalizeFully(rs.getString(NOMBRES)));
            docente.setApellidoPaterno(WordUtils.capitalizeFully(rs.getString(APELLIDO_PATERNO)));
            docente.setApellidoMaterno(WordUtils.capitalizeFully(rs.getString(APELLIDO_MATERNO)));
            docente.setGrupo(StringHelper.getValueOrEmpty(rs.getString(GRUPO)));
            docente.setCorreo(StringHelper.getValueOrEmpty(rs.getString(CORREO)));
            docente.setCargo(StringHelper.getValueOrEmpty(rs.getString(CARGO)));
            docente.setEstado(rs.getInt(ESTADO));
            docente.setOrden(rowNum);
            docente.setDni(StringHelper.getValueOrEmpty(rs.getString("Dni")));
            return docente;
        }
    }

    class GradosPorSedePeriodoMapper implements RowMapper<Grado> {

        @Override
        public Grado mapRow(ResultSet rs, int rowNum) throws SQLException {
            Grado grado = new Grado();
            grado.setIdGrado(rs.getInt(ID_GRADO));
            grado.setDescripcion(StringHelper.getValueOrEmpty(rs.getString(DESCRIPCION)));
            grado.setIdNivel(rs.getInt("Nivel"));
            grado.setCodigoGrado(StringHelper.getValueOrEmpty(rs.getString("Codigo")));
            grado.setDetalleNivel(StringHelper.getValueOrEmpty(rs.getString("DetalleNivel")));
            return grado;
        }
    }

    class SeccionesDMMapper implements RowMapper<SeccionAlumno> {

        @Override
        public SeccionAlumno mapRow(ResultSet rs, int rowNum) throws SQLException {
            SeccionAlumno seccion = new SeccionAlumno();
            seccion.setIdSeccion(rs.getInt("IdSeccion"));
            seccion.setDescripcion(StringHelper.getValueOrEmpty(rs.getString(DESCRIPCION)));
            seccion.setIdPeriodoSede(rs.getInt("IdPeriodoSede"));
            return seccion;
        }
    }

    class PadreFamiliaMapper implements RowMapper<Contacto> {

        @Override
        public Contacto mapRow(ResultSet rs, int rowNum) throws SQLException {
            Contacto contacto = new Contacto();
            contacto.setIdPersona(rs.getInt(ID_PERSONA));
            contacto.setIdReferente(rs.getInt("IdReferente"));
            contacto.setNombres(StringHelper.getValueOrEmpty(rs.getString(NOMBRES)));
            contacto.setApellidoPaterno(StringHelper.getValueOrEmpty(rs.getString(APELLIDO_PATERNO)));
            contacto.setApellidoMaterno(StringHelper.getValueOrEmpty(rs.getString(APELLIDO_MATERNO)));
            contacto.setCorreo(StringHelper.getValueOrEmpty(rs.getString(CORREO)));
            contacto.setGrupo(StringHelper.getValueOrEmpty(rs.getString(GRUPO)));
            contacto.setCargo(StringHelper.getValueOrEmpty(rs.getString(CARGO)));
            contacto.setEstado(rs.getInt(ESTADO));
            contacto.setGradoSeccion(StringHelper.getValueOrEmpty(rs.getString("GradoSeccion")));
            contacto.setOrden(rowNum);
            return contacto;
        }
    }

    class AlumnoMapper implements RowMapper<Contacto> {

        @Override
        public Contacto mapRow(ResultSet rs, int rowNum) throws SQLException {
            Contacto alumno = new Contacto();
            alumno.setIdPersona(rs.getInt(ID_PERSONA));
            alumno.setApellidoPaterno(StringHelper.getValueOrEmpty(rs.getString(APELLIDO_PATERNO)));
            alumno.setNombres(StringHelper.getValueOrEmpty(rs.getString(NOMBRES)));
            alumno.setCorreo(StringHelper.getValueOrEmpty(rs.getString(CORREO)));
            alumno.setGrupo(StringHelper.getValueOrEmpty(rs.getString(GRUPO)));
            alumno.setApellidoMaterno(StringHelper.getValueOrEmpty(rs.getString(APELLIDO_MATERNO)));
            alumno.setEstado(rs.getInt(ESTADO));
            alumno.setCargo(StringHelper.getValueOrEmpty(rs.getString(CARGO)));
            alumno.setOrden(rowNum);
            return alumno;
        }
    }

    public List<Docente> obtenerDocentesUnicos(List<Docente> docentes) {
        LinkedHashMap<Integer, Docente> diccionarioDocentes = new LinkedHashMap<>();
        docentes.forEach(d -> {
            if (!diccionarioDocentes.containsKey(d.getIdPersona()))
                diccionarioDocentes.put(d.getIdPersona(), d);
        });
        return new ArrayList<>(diccionarioDocentes.values());
    }
}
