package com.innova.nif.repositories;

import com.innova.nif.repositories.interfaces.IRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;


public class BaseRepository implements IRepository {
    protected final Logger logger;

    public static final String DEFAULT_RESULT_SET = "#result-set-1";
    public static final String CUSTOM_RESULT_SET = "custom-result-set";
    public static final String DEFAULT_EXECUTE_NON_QUERY_RESULT_SET = "#update-count-1";

    public BaseRepository() {
        logger = LoggerFactory.getLogger(this.getClass());
    }

    @Autowired
    @Qualifier("nifDb")
    protected DataSource nifDataSource;

    @Autowired
    @Qualifier("dmDb")
    protected DataSource dmDataSource;

    @Autowired
    @Qualifier("peaDb")
    protected DataSource peaDataSource;

    ArrayList<HashMap<String, Object>> getDefaultResultSet(Map<String, Object> response) {
        return (ArrayList<HashMap<String, Object>>) response.get(DEFAULT_RESULT_SET);
    }

    protected Map<String, Object> executeDMStoreProcedure(String procedureName, MapSqlParameterSource inputParameters, RowMapper rowMapper) {
        return executeDMStoreProcedure(procedureName, inputParameters, new String[]{}, rowMapper);
    }

    Map<String, Object> executeDMStoreProcedure(String procedureName, SqlParameterSource inputParameters, String[] resultSetKeys) {
        return executeDMStoreProcedure(procedureName, inputParameters, resultSetKeys, null);
    }

    Map<String, Object> executeDMStoreProcedure(String procedureName, SqlParameterSource inputParameters) {
        return executeDMStoreProcedure(procedureName, inputParameters, new String[]{}, null);
    }

    private Map<String, Object> executeDMStoreProcedure(String procedureName, SqlParameterSource inputParameters, String[] resultSetKeys, RowMapper rowMapper) {
        return executeBaseStoreProcedure(dmDataSource, procedureName, inputParameters, resultSetKeys, rowMapper);
    }

    Map<String, Object> executeNifStoreProcedure(String procedureName, SqlParameterSource inputParameters, RowMapper rowMapper) {
        return executeNifStoreProcedure(procedureName, inputParameters, new String[]{}, rowMapper);
    }

    Map<String, Object> executeNifStoreProcedure(String procedureName, SqlParameterSource inputParameters, String[] resultSetKeys) {
        return executeBaseStoreProcedure(nifDataSource, procedureName, inputParameters, resultSetKeys, null);
    }

    Map<String, Object> executePeaStoreProcedure(String procedureName, SqlParameterSource inputParameters, RowMapper rowMapper) {
        return executeBaseStoreProcedure(peaDataSource, procedureName, inputParameters, new String[]{}, rowMapper);
    }

    protected void registrarExepcion(Exception ex, String procedimiento, MapSqlParameterSource parametros) {
        String mensajeExepcion = generarLogProcedimiento(procedimiento, parametros);
        logger.error(mensajeExepcion, ex);
    }

    protected void registrarExepcion(Exception ex, String query, Object[] params) {
        String mensajeError = new StringBuilder().append("Error al ejecutar la consulta: '").append(query)
                .append("' con los parámetros: ").append(Arrays.toString(params)).append(")").toString();
        logger.error(mensajeError, ex);
    }

    protected void registrarExcepcion(Exception ex, String consulta) {
        String mensajeError = new StringBuilder().append("Error al ejecutar la consulta: ").append(consulta).toString();
        logger.error(mensajeError, ex);
    }

    Map<String, Object> executeNifStoreProcedure(String procedureName, SqlParameterSource inputParameters) {
        return executeNifStoreProcedure(procedureName, inputParameters, new String[]{}, null);
    }

    protected Object executeNifQuery(String query, Object[] params, RowMapper rowMapper) {
        return executeBaseQuery(nifDataSource, query, params, rowMapper);
    }

    protected List<Map<String, Object>> executePeaQuery(String query) {
        return new JdbcTemplate(peaDataSource).queryForList(query);
    }

    protected List<Map<String, Object>> executeDataManagementQuery(String query) {
        return new JdbcTemplate(dmDataSource).queryForList(query);
    }

    protected int executeNifDeleteQuery(String deleteQuery, Object[] params) {
        return executeBaseDeleteQuery(nifDataSource, deleteQuery, params);
    }

    private int executeBaseDeleteQuery(DataSource nifDataSource, String deleteQuery, Object[] params) {
        return new JdbcTemplate(nifDataSource).update(deleteQuery, params);
    }

    private Map<String, Object> executeNifStoreProcedure(String procedureName, SqlParameterSource inputParameters, String[] resultSetKeys, RowMapper rowMapper) {
        return executeBaseStoreProcedure(nifDataSource, procedureName, inputParameters, resultSetKeys, rowMapper);
    }

    protected Map<String, Object> executeBaseStoreProcedure(DataSource dataSource, String procedureName,
                                                            SqlParameterSource inputParameters, String[] resultSetKeys, RowMapper rowMapper) {
        var jdbcTemplate = new JdbcTemplate(dataSource);

        MapSqlParameterSource parametros = (MapSqlParameterSource) inputParameters;

        StringBuilder stringBuilder = new StringBuilder();
        ArrayList<String> listaParametros = parametrosComoLista(parametros);
        stringBuilder.append("(");
        stringBuilder.append(String.join(",", listaParametros));
        stringBuilder.append(")");

        logger.info("parametros executeDMStoreProcedure: (" + stringBuilder.toString() + ")");
        var funcGetActorName = new SimpleJdbcCall(jdbcTemplate).withProcedureName(procedureName);
        if (rowMapper != null) {
            funcGetActorName.returningResultSet(CUSTOM_RESULT_SET, rowMapper);
        } else if (resultSetKeys.length > 0) {
            funcGetActorName.returningResultSet(DEFAULT_RESULT_SET, new HashMapRowMapper(resultSetKeys));
        }
        Map<String, Object> p = new HashMap<>();
        Arrays.stream(Objects.requireNonNull(parametros.getParameterNames())).forEach(key -> p.put(key ,inputParameters.getTypeName(key)) );
        if ( inputParameters.getParameterNames()!=null)
            for(var a: inputParameters.getParameterNames())
                p.put(a, inputParameters.getValue(a));
        return funcGetActorName.execute(inputParameters);
    }

    private Object executeBaseQuery(DataSource dataSource, String query, Object[] params, RowMapper rowMapper) {
        return new JdbcTemplate(dataSource).query(query, params, rowMapper);
    }

    public String generarLogProcedimiento(String procedimiento, MapSqlParameterSource parametros) {
        var stringBuilder = new StringBuilder().append("Error al ejecutar: ").append(procedimiento)
                .append(" con parámetros: ");
        ArrayList<String> listaParametros = parametrosComoLista(parametros);
        stringBuilder.append("(");
        stringBuilder.append(String.join(",", listaParametros));
        stringBuilder.append(")");
        return stringBuilder.toString();
    }

    private ArrayList<String> parametrosComoLista(MapSqlParameterSource parametros) {
        var nombresParametros = parametros.getParameterNames();
        nombresParametros = nombresParametros.length > 0 ? nombresParametros : new String[]{};
        var listaParametros = new ArrayList<String>();
        for (var nombreParametro : nombresParametros) {
            listaParametros.add(nombreParametro + "=" + parametros.getValue(nombreParametro));
        }
        return listaParametros;
    }

    class HashMapRowMapper implements RowMapper<Map<String, Object>> {
        String[] keys;

        HashMapRowMapper(String[] keys) {
            this.keys = keys;
        }

        @Override
        public Map<String, Object> mapRow(ResultSet rs, int rowNum) throws SQLException {
            Map<String, Object> myclass = new HashMap<>();
            for (var key : keys) {
                myclass.put(key, rs.getObject(key));
            }
            return myclass;
        }
    }
}
