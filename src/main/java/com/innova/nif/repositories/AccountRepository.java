package com.innova.nif.repositories;

import com.innova.nif.models.Persona;
import com.innova.nif.repositories.interfaces.IAccountRepository;
import com.innova.nif.utils.IntHelper;
import com.innova.nif.utils.StringHelper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class AccountRepository extends BaseRepository implements IAccountRepository {

    @Override
    public int updateUserPassword(Persona persona) {
        var parameters = new MapSqlParameterSource()
                .addValue("codigo_persona", IntHelper.getValueOrDefault(persona.getCodigoPersona()))
                .addValue("clave_anterior", StringHelper.getValueOrEmpty(persona.getClaveAnterior()))
                .addValue("clave_nueva", StringHelper.getValueOrEmpty(persona.getClaveNueva()))
                .addValue("usuario_modificacion", IntHelper.getValueOrDefault(persona.getUsuarioModificacion()))
                .addValue("Perfil", StringHelper.getValueOrEmpty(persona.getCargo()));

        var response = executeNifStoreProcedure("SP_ActualizarContrasena", parameters);
        return (int) response.get("respuesta");
    }
}