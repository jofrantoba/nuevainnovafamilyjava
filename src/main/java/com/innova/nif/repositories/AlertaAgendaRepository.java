package com.innova.nif.repositories;

import com.innova.nif.models.AlertaAgenda;
import com.innova.nif.repositories.interfaces.IAlertaAgendaRepository;
import com.innova.nif.utils.DateHelper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class AlertaAgendaRepository extends BaseRepository implements IAlertaAgendaRepository {

    private static final String ID_ALERTA_AGENDA_OUT = "IdAlertaAgendaOut";

    @Override
    public void registrarAlertaAgendaUsuarioTareaXML(String xml) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("xmlAlertarUsuarioXML", xml);
        try {
            executeNifStoreProcedure("SP_InsertarAlertaUsuarioTareaXML", parameterSource);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_InsertarAlertaUsuarioTareaXML", parameterSource);
            throw ex;
        }
    }

    @Override
    public int registrarAlertaAgenda(AlertaAgenda alerta) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue("IdTareaEvento", alerta.getIdTareaEvento())
                .addValue("IdDetalle", alerta.getIdDetalle())
                .addValue("Tipo", alerta.getTipo())
                .addValue("Titulo", alerta.getTitulo())
                .addValue("IdPeriodo", alerta.getIdPeriodo())
                .addValue("IdSede", alerta.getIdSede())
                .addValue("FechaEntrega", DateHelper.convertirASQLDateTime(alerta.getFechaEntrega()))
                .addValue("Color", alerta.getColor())
                .addValue("Ubicacion", alerta.getUbicacion())
                .addValue("CodigoGrado", alerta.getCodigoGrado())
                .addValue("Seccion", alerta.getSeccion())
                .addValue("NombreCurso", alerta.getNombreCurso())
                .addValue(ID_ALERTA_AGENDA_OUT, "");
        try {
            String[] resultSet = {ID_ALERTA_AGENDA_OUT};
            Map<String, Object> resultado = executeNifStoreProcedure("SP_RegistroAlertaAgenda", parametros, resultSet);
            return (int) resultado.get(ID_ALERTA_AGENDA_OUT);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_RegistroAlertaAgenda", parametros);
            throw ex;
        }
    }

    @Override
    public void registrarAlertaAgendaUsuarioEventoXML(String xml) {
        MapSqlParameterSource parametros = new MapSqlParameterSource()
                .addValue("xmlAlertarUsuarioXML", xml);
        try {
            executeNifStoreProcedure("SP_InsertarAlertaUsuarioEventoXML", parametros);
        } catch (Exception ex) {
            registrarExepcion(ex, "SP_InsertarAlertaUsuarioEventoXML", parametros);
            throw ex;
        }
    }
}
