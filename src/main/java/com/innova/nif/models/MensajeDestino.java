package com.innova.nif.models;

import lombok.Getter;
import lombok.Setter;

public class MensajeDestino {
    @Getter
    @Setter
    int idMensajeDestino;

    @Getter
    @Setter
    Integer idCorreo;

    @Getter
    @Setter
    Integer idMensaje;

    @Getter
    @Setter
    Integer idSecuencia;

    @Getter
    @Setter
    Integer idDestino;

    @Getter
    @Setter
    String idTipoDestino;

    @Getter
    @Setter
    String grupo;

    @Getter
    @Setter
    boolean leido;

    @Getter
    @Setter
    boolean esFavorito;

    @Getter
    @Setter
    boolean esEliminado;

    @Getter
    @Setter
    boolean estado;

    @Getter
    @Setter
    boolean alertaRevisada;

    @Getter
    @Setter
    MensajeApp mensaje;

    @Getter
    @Setter
    Persona destinatario;

    @Getter
    @Setter
    String correo;

    @Getter
    @Setter
    String gradoSeccion;
}

