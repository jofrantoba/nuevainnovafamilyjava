package com.innova.nif.models;

import lombok.Getter;
import lombok.Setter;

public class SeccionAlumno {
    @Getter
    @Setter
    int idSeccion;

    @Getter
    @Setter
    int idPeriodoSede;

    @Getter
    @Setter
    String descripcion;

    @Getter
    @Setter
    int idGrado;

    @Getter
    @Setter
    String seccion;

    @Getter
    @Setter
    String nivel;

    @Getter
    @Setter
    String nivelIngles;
}
