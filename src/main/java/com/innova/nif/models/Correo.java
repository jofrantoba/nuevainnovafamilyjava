package com.innova.nif.models;

import lombok.Getter;
import lombok.Setter;
import org.springframework.lang.Nullable;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

public class Correo {

    public Correo() {
        mensaje = new HashSet<>();
    }

    @Getter
    @Setter
    private int idCorreo;

    @Getter
    @Setter
    private String asunto;

    @Getter
    @Setter
    @Nullable
    private Date fechaRegistro;

    @Getter
    @Setter
    private Collection<MensajeApp> mensaje;
}
