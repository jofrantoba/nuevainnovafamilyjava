package com.innova.nif.models;

import com.innova.nif.utils.StringHelper;
import lombok.Getter;
import lombok.Setter;

public class MensajePaginado {
    @Getter
    @Setter
    int idMensajeDestino;

    @Getter
    @Setter
    int idRemitente;

    @Getter
    @Setter
    int idCorreo;

    @Getter
    @Setter
    String nombreCompletoRemitente;

    @Getter
    @Setter
    int idMensaje;

    @Getter
    @Setter
    String inicialesRemitente;

    @Getter
    @Setter
    String para;

    @Getter
    @Setter
    String grupoPara;

    @Getter
    @Setter
    String idPara;

    @Getter
    @Setter
    int idFoto;

    @Getter
    @Setter
    String conCopia;

    @Getter
    @Setter
    String grupoCC;

    @Getter
    @Setter
    String idConCopia;

    @Getter
    @Setter
    String asunto;

    @Getter
    @Setter
    String mensaje;

    @Getter
    @Setter
    boolean esFavorito;

    @Getter
    @Setter
    String fechaRegistro;

    @Getter
    @Setter
    String horaRegistro;

    @Getter
    @Setter
    boolean leido;

    @Getter
    @Setter
    boolean conAdjunto;

    @Getter
    @Setter
    String rol;

    @Getter
    @Setter
    String cargo;

    @Getter
    @Setter
    String firma;

    @Getter
    @Setter
    String referencia;

    @Getter
    @Setter
    String url;

    @Getter
    @Setter
    boolean aceptarRespuesta;

    @Getter
    String gradoSeccion;

    public void setGradoSeccion(boolean showData, String gradoSeccion) {
        this.gradoSeccion = !showData || gradoSeccion == null || gradoSeccion.isBlank() || gradoSeccion.isEmpty() ? StringHelper.EMPTY_STRING : gradoSeccion;
    }
}
