package com.innova.nif.models;

import com.google.common.base.Strings;
import lombok.Getter;
import lombok.Setter;

public class Docente extends Contacto {
    @Getter
    @Setter
    String nivelIngles;

    public String getNombreCompleto() {
        return String.format("%s %s %s", Strings.isNullOrEmpty(primerNombre) ? "" : primerNombre,
                Strings.isNullOrEmpty(apellidoPaterno) ? "" : apellidoMaterno,
                Strings.isNullOrEmpty(apellidoMaterno) ? "" : apellidoMaterno);
    }
}
