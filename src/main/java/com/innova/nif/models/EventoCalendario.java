package com.innova.nif.models;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.innova.nif.utils.DateTimeSerializer;
import com.innova.nif.utils.StringHelper;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

public class EventoCalendario {
    public EventoCalendario() {
        titulo = StringHelper.EMPTY_STRING;
        descripcion = StringHelper.EMPTY_STRING;
        color = StringHelper.EMPTY_STRING;
        ubicacion = StringHelper.EMPTY_STRING;
        valorRegistro = StringHelper.EMPTY_STRING;
        cadenaParticipante = StringHelper.EMPTY_STRING;
    }

    @Getter
    @Setter
    int idEvento;

    @Getter
    @Setter
    String titulo;

    @Getter
    @Setter
    String descripcion;

    @Getter
    @Setter
    String ubicacion;

    @Getter
    @Setter
    @JsonSerialize(using = DateTimeSerializer.class)
    DateTime fechaInicio;

    @Getter
    @Setter
    @JsonSerialize(using = DateTimeSerializer.class)
    DateTime fechaFin;

    @Getter
    @Setter
    int idPersona;

    @Getter
    @Setter
    String color;

    @Getter
    @Setter
    int idPeriodo;

    @Getter
    @Setter
    String valorRegistro;

    @Getter
    @Setter
    String cadenaParticipante;

    @Getter
    @Setter
    int idSede;

    @Getter
    @Setter
    String participante;

    @Override
    public String toString() {
        return new StringBuilder()
                .append("(")
                .append("idEvento=").append(idEvento).append(",titulo=").append(titulo).append(",descripcion=")
                .append(descripcion).append(",ubicacion=").append(ubicacion).append(",fechaInicio=").append(fechaInicio)
                .append(",fechaFin=").append(fechaFin).append(",idPersona=").append(idPersona).append(",color=")
                .append(color).append(",idPeriodo=").append(idPeriodo).append(",valorRegistro=").append(valorRegistro)
                .append(",cadenaParticipante=").append(cadenaParticipante).append(",idSede=").append(idSede)
                .append(",participante=").append(participante).append(")").toString();

    }
}