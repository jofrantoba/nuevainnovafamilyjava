package com.innova.nif.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.innova.nif.utils.DateTimeSerializer;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

public class Usuario extends AbstractPersona {

    public Usuario() {
        primerNombre = "";
        correo = "";
        apellidoPaterno = "";
        segundoNombre = "";
        apellidoMaterno = "";
        dni = "";
        cargo = "";
        grupo = "";
        curso = "";
        firma = "";
        seccion = "";
        grado = "";
        nivel = "";
        nivelIngles = "";
        bimestre = "";
        gradoDesarrollo = "";
        region = "";
        nombreSede = "";
        ciudad = "";
        area = "";
        clave = "";
        sedes = new ArrayList<>();
    }

    @Getter
    @Setter
    private String correo;

    @Getter
    @Setter
    private int idPersona;

    @Getter
    @Setter
    @JsonProperty("IdAlumno")
    private int idAlumno;

    @Getter
    @Setter
    @JsonProperty("Dni")
    private String dni;

    @Getter
    @Setter
    @JsonProperty("ApellidoPaterno")
    private String apellidoPaterno;

    @Getter
    @Setter
    @JsonProperty("ApellidoMaterno")
    private String apellidoMaterno;

    @Getter
    @Setter
    @JsonProperty("Grupo")
    private String grupo;

    @Getter
    @Setter
    @JsonProperty("Cargo")
    private String cargo;

    @Getter
    @Setter
    @JsonProperty("Curso")
    private String curso;

    @Getter
    @Setter
    @JsonProperty("IdSede")
    private int idSede;

    @Getter
    @Setter
    @JsonProperty("Seccion")
    private String seccion;

    @Getter
    @Setter
    private int idSeccion;

    @Getter
    @Setter
    @JsonProperty("IdGrado")
    private int idGrado;

    @Getter
    @Setter
    private int estado;

    @Getter
    @Setter
    @JsonProperty("Firma")
    private String firma;

    @Getter
    @Setter
    @JsonProperty("Nivel")
    private String nivel;

    @Getter
    @Setter
    @JsonProperty("Grado")
    private String grado;

    @Getter
    @Setter
    @JsonProperty("NivelIngles")
    private String nivelIngles;

    @Getter
    @Setter
    @JsonProperty("GradoDesarrollo")
    private String gradoDesarrollo;

    @Getter
    @Setter
    @JsonProperty("Bimestre")
    private String bimestre;

    @Getter
    @Setter
    @JsonProperty("Region")
    private String region;

    @Getter
    @Setter
    @JsonProperty("Ciudad")
    private String ciudad;

    @Getter
    @Setter
    @JsonProperty("NombreSede")
    private String nombreSede;

    @Getter
    @Setter
    @JsonProperty("Area")
    private String area;

    @Getter
    @Setter
    @JsonProperty("Clave")
    private String clave;

    @Getter
    @Setter
    private String recoverPasswordHash;

    @Getter
    @Setter
    @JsonSerialize(using = DateTimeSerializer.class)
    private DateTime recoverPasswordDate;

    @Getter
    @Setter
    private Integer minutosTranscurridos;

    @Getter
    @Setter
    @JsonProperty("NuevoEmail")
    private String nuevoEmail;

    @Getter
    @Setter
    private boolean allowsEmailNotifications;

    @Getter
    @Setter
    @JsonProperty("IdPeriodoLectivo")
    private int idPeriodoLectivo;

    @Getter
    @Setter
    private PeriodoLectivo periodoLectivo;

    @Getter
    @Setter
    private List<Sede> sedes;

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder()
                .append("(")
                .append("primerNombre=").append(primerNombre).append(",segundoNombre=").append(segundoNombre)
                .append(",nombresIniciales=").append(nombresIniciales).append(",idPersona=").append(idPersona)
                .append(",correo=").append(correo).append(",nombre=").append(nombres)
                .append(",ApellidoPaterno=").append(apellidoPaterno).append(",ApellidoMaterno=").append(apellidoMaterno)
                .append(",IdAlumno=").append(idAlumno).append(",Dni=").append(dni).append(",Grupo=").append(grupo)
                .append(",Cargo=").append(cargo).append(",Curso=").append(curso).append(",IdSede=").append(idSede)
                .append(",Seccion=").append(seccion).append(",idSeccion=").append(idSeccion)
                .append(",IdGrado=").append(idGrado).append(",estado=").append(estado).append(",Firma=").append(firma)
                .append(",Nivel=").append(nivel).append(",Grado=").append(grado)
                .append(",NivelIngles=").append(nivelIngles).append(",GradoDesarrollo=").append(gradoDesarrollo)
                .append(",Bimestre=").append(bimestre).append(",Region=").append(region)
                .append(",Ciudad=").append(ciudad).append(",NombreSede=").append(nombreSede)
                .append(",Area=").append(area).append(",recoverPasswordHash=").append(recoverPasswordHash)
                .append(",recoverPasswordDate=").append(recoverPasswordDate)
                .append(",minutosTranscurridos=").append(minutosTranscurridos)
                .append(",NuevoEmail=").append(nuevoEmail)
                .append(",allowsEmailNotifications=").append(allowsEmailNotifications)
                .append(",IdPeriodoLectivo=").append(idPeriodoLectivo).append(")");
        return builder.toString();
    }
}
