package com.innova.nif.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.lang.Nullable;


public class MensajeAdjunto {

    public MensajeAdjunto() {
        url = "";
        ruta = "";
        nombreOriginal = "";
        nombreAlterado = "";
        tamanioArchivo = "";
        mensaje = new MensajeApp();
    }

    @Getter
    @Setter
    private int idMensajeAdjunto;

    @Nullable
    @Getter
    @Setter
    private int idMensaje;

    @Getter
    @Setter
    @JsonProperty("URL")
    private String url;

    @Getter
    @Setter
    private String urlView;

    @Getter
    @Setter
    private String ruta;

    @Nullable
    @Getter
    @Setter
    private int idTipoArchivo;

    @Getter
    @Setter
    private String nombreOriginal;

    @Getter
    @Setter
    private String nombreAlterado;

    @Getter
    @Setter
    private String tamanioArchivo;

    @Getter
    @Setter
    private String fechaRegistro;

    @Getter
    @Setter
    private MensajeApp mensaje;

    @Getter
    @Setter
    private String nuevoEmail;
}
