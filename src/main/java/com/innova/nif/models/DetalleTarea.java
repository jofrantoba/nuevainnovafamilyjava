package com.innova.nif.models;

import lombok.Getter;
import lombok.Setter;

public class DetalleTarea {
    @Getter
    @Setter
    int idDetalleTarea;

    @Getter
    @Setter
    int idTarea;

    @Getter
    @Setter
    int idSede;

    @Getter
    @Setter
    int idGrado;

    @Getter
    @Setter
    String seccion;

    @Getter
    @Setter
    String codigoGrado;

    @Getter
    @Setter
    int idCurso;

    @Getter
    @Setter
    String nombreCurso;

    @Override
    public String toString() {
        return new StringBuilder()
                .append("(")
                .append("idDetalleTarea=").append(idDetalleTarea).append(",idTarea=").append(idTarea)
                .append(",idSede=").append(idSede).append(",idGrado=").append(idGrado)
                .append(",seccion=").append(seccion).append(",codigoGrado=").append(codigoGrado)
                .append(",idCurso=").append(idCurso).append(",nombreCurso=").append(nombreCurso)
                .append(")").toString();
    }
}