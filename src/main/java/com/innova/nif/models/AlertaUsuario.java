package com.innova.nif.models;

import lombok.Getter;
import lombok.Setter;

public class AlertaUsuario {
    @Getter
    @Setter
    int idAlertaAgenda;

    @Getter
    @Setter
    int idPersona;

    @Getter
    @Setter
    String perfil;

    @Getter
    @Setter
    int idPersonaAlumno;

    @Getter
    @Setter
    String inicialesAlumno;

    @Getter
    @Setter
    int idGrado;

    @Getter
    @Setter
    String seccion;

    @Getter
    @Setter
    String nivelIngles;
}
