package com.innova.nif.models;

import lombok.Getter;
import lombok.Setter;

public class PeriodoLectivo {
    @Getter
    @Setter
    int idPeriodo;
    @Getter
    @Setter
    int anio;
    @Getter
    @Setter
    String estado;
}
