package com.innova.nif.models;

import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

public class LogAgenda {
    @Getter
    @Setter
    int idLog;

    @Getter
    @Setter
    int idTarea;

    @Getter
    @Setter
    int idDocente;

    @Getter
    @Setter
    DateTime fechaLog;

    @Getter
    @Setter
    String original;

    @Getter
    @Setter
    String modificado;

    @Getter
    @Setter
    String tipo;
}
