package com.innova.nif.models;

import lombok.Getter;
import lombok.Setter;
import org.springframework.lang.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

public class MensajeApp {

    public MensajeApp() {
        remitente = "";
        nombreIniciales = "";
        correoRemitente = "";
        para = "";
        idPara = "";
        grupoPara = "";
        conCopia = "";
        idConCopia = "";
        grupoCc = "";
        conCopiaOculta = "";
        referencia = "";
        asunto = "";
        conAdjunto = false;
        mensaje = "";
        correo = new Correo();
        fechaRegistro = "";
        horaRegistro = "";
        mensajeAdjunto = new ArrayList<>();
        mensajeDestino = new ArrayList<>();
        grupo = "";
        foto = "";
        cargo = "";
        tipoRespuesta = "";
    }

    @Getter
    @Setter
    private int idPeriodo;

    @Getter
    @Setter
    private int idMensaje;

    @Nullable
    @Getter
    @Setter
    private int idCorreo;

    @Nullable
    @Getter
    @Setter
    private int idRemitente;

    @Getter
    @Setter
    private String remitente;

    @Getter
    @Setter
    private String nombreIniciales;

    @Getter
    @Setter
    private String correoRemitente;

    @Getter
    @Setter
    private String para;

    @Getter
    @Setter
    private String idPara;

    @Getter
    @Setter
    private String grupoPara;

    @Getter
    @Setter
    private String conCopia;

    @Getter
    @Setter
    private String idConCopia;

    @Getter
    @Setter
    private String grupoCc;

    @Getter
    @Setter
    private String conCopiaOculta;

    @Getter
    @Setter
    private String referencia;

    @Getter
    @Setter
    private String asunto;

    @Getter
    @Setter
    private Boolean conAdjunto;

    @Getter
    @Setter
    private String tipoRespuesta;

    @Getter
    @Setter
    private String mensaje;

    @Getter
    @Setter
    private String fechaRegistro;

    @Getter
    @Setter
    private String horaRegistro;

    @Nullable
    @Getter
    @Setter
    private int usuarioRegistro;

    @Nullable
    @Getter
    @Setter
    private Date fechaActualizacion;

    @Nullable
    @Getter
    @Setter
    private int usuarioModificacion;

    @Getter
    @Setter
    private Correo correo;

    @Getter
    @Setter
    private String adjunto;

    @Getter
    @Setter
    private ArrayList<MensajeAdjunto> mensajeAdjunto;

    @Getter
    @Setter
    private ArrayList<MensajeDestino> mensajeDestino;

    @Getter
    @Setter
    private String grupo;

    @Getter
    @Setter
    private String firma;

    @Getter
    @Setter
    private int idFoto;

    @Getter
    @Setter
    private String foto;

    @Getter
    @Setter
    private String cargo;

    @Getter
    @Setter
    private String url;

    @Getter
    @Setter
    private Collection<BackOfficeParametros> backOfficeDestinarios;

    @Nullable
    @Getter
    @Setter
    private Boolean aceptarRespuesta;

    @Override
    public String toString() {
        return new StringBuilder().append("(").append("idPeriodo=").append(idPeriodo)
                .append(",idMensaje=").append(idMensaje).append(",idCorreo=").append(idCorreo)
                .append(",idRemitente=").append(idRemitente).append(",remitente=").append(remitente)
                .append(",nombreIniciales=").append(nombreIniciales).append(",correoRemitente=").append(correoRemitente)
                .append(",para=").append(para).append(",idPara=").append(idPara).append(",grupoPara=").append(grupoPara)
                .append(",conCopia=").append(conCopia).append(",idConCopia=").append(idConCopia)
                .append(",grupoCc=").append(grupoCc).append(",conCopiaOculta=").append(conCopiaOculta)
                .append(",referencia=").append(referencia).append(",asunto=").append(asunto)
                .append(",conAdjunto=").append(conAdjunto).append(",tipoRespuesta=").append(tipoRespuesta)
                .append(",mensaje=").append(mensaje).append(",fechaRegistro=").append(fechaRegistro)
                .append(",horaRegistro=").append(horaRegistro).append(",usuarioRegistro=").append(usuarioRegistro)
                .append(",fechaActualizacion=").append(fechaActualizacion)
                .append(",usuarioModificacion=").append(usuarioModificacion).append(",correo=").append(correo)
                .append(",adjunto=").append(adjunto).append(",mensajeAdjunto=").append(mensajeAdjunto)
                .append(",mensajeDestino=").append(mensajeDestino).append(",grupo=").append(grupo)
                .append(",firma=").append(firma).append(",idFoto=").append(idFoto)
                .append(",foto=").append(foto).append(",cargo=").append(cargo).append(",url=").append(url)
                .append(",backOfficeDestinarios=").append(backOfficeDestinarios)
                .append(",aceptarRespuesta=").append(aceptarRespuesta).append(")").toString();
    }
}
