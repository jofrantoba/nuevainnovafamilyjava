package com.innova.nif.models;

import lombok.Getter;
import lombok.Setter;

public class BackOfficeParametro {
    @Getter
    @Setter
    private String parametroId;

    @Getter
    @Setter
    private String nombre;

    @Getter
    @Setter
    private String tipo;
}
