package com.innova.nif.models;

import com.innova.nif.utils.StringHelper;
import lombok.Getter;
import lombok.Setter;

public class Sede {
    public Sede() {
        nombre = StringHelper.EMPTY_STRING;
        isSelected = false;
    }

    public Sede(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
        this.isSelected = false;
    }

    @Getter
    @Setter
    private int id;

    @Getter
    @Setter
    private String nombre;

    @Getter
    @Setter
    private boolean isSelected;
}
