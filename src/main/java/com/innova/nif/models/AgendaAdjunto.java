package com.innova.nif.models;

import lombok.Getter;
import lombok.Setter;

public class AgendaAdjunto {
    @Getter
    @Setter
    int idArchivo;

    @Getter
    @Setter
    int idTareaEvento;

    @Getter
    @Setter
    String tipoAgenda;

    @Getter
    @Setter
    String nombreArchivo;

    @Getter
    @Setter
    String nombreAlterado;

    @Getter
    @Setter
    String tamanioArchivo;

    @Getter
    @Setter
    String url;

    @Getter
    @Setter
    String urlView;

    @Override
    public String toString() {
        return new StringBuilder()
                .append(")")
                .append("idArchivo=").append(idArchivo).append("idTareaEvento=").append(idTareaEvento)
                .append(",tipoAgenda=").append(tipoAgenda).append(",nombreArchivo=").append(nombreArchivo)
                .append(",nombreAlterado=").append(nombreAlterado).append(",tamanioArchivo=").append(tamanioArchivo)
                .append(",url=").append(url).append(",urlView=").append(urlView).append(")").toString();
    }
}