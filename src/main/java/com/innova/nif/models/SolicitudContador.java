package com.innova.nif.models;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

public class SolicitudContador {
    public SolicitudContador() {
        codigosAlumnos = new ArrayList<>();
    }

    @Getter
    @Setter
    private BackOfficeParametros parametros;

    @Getter
    @Setter
    private List<String> codigosAlumnos;
}
