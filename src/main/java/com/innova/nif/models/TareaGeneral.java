package com.innova.nif.models;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class TareaGeneral {
    @Getter
    @Setter
    TareaCalendario tarea;

    @Getter
    @Setter
    int idAula;

    @Getter
    @Setter
    String cabecera;

    @Getter
    @Setter
    String nombreDocente;

    @Getter
    @Setter
    String inicialesDocente;

    @Getter
    @Setter
    String gradoSeccion;

    @Getter
    @Setter
    List<String> listaAulas;

    @Getter
    @Setter
    List<AgendaAdjunto> lstAdjunto;

    @Getter
    @Setter
    int idFoto;

    @Getter
    @Setter
    String urlFoto;

    @Getter
    @Setter
    String nombreCurso;
}
