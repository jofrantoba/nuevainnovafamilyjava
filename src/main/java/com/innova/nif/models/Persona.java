package com.innova.nif.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.innova.nif.utils.StringHelper;
import lombok.Getter;
import lombok.Setter;
import org.springframework.lang.Nullable;

import java.util.Date;

public class Persona {
    public Persona() {
        segundoNombre = StringHelper.EMPTY_STRING;
        primerNombre = StringHelper.EMPTY_STRING;
        apellidoMaterno = StringHelper.EMPTY_STRING;
        apellidoPaterno = StringHelper.EMPTY_STRING;
        email = StringHelper.EMPTY_STRING;
        nombreCompleto = StringHelper.EMPTY_STRING;
        cargo = StringHelper.EMPTY_STRING;
        iniciales = StringHelper.EMPTY_STRING;
        claveNueva = StringHelper.EMPTY_STRING;
        claveAnterior = StringHelper.EMPTY_STRING;
        grupo = StringHelper.EMPTY_STRING;
    }

    @Getter
    @Setter
    @JsonProperty("IdPersona")
    private int idPersona;

    @Nullable
    @Getter
    @Setter
    @JsonProperty("IdRol")
    private int idRol;

    @Getter
    @Setter
    @JsonProperty("PrimerNombre")
    private String primerNombre;

    @Getter
    @Setter
    @JsonProperty("SegundoNombre")
    private String segundoNombre;

    @Getter
    @Setter
    @JsonProperty("ApellidoMaterno")
    private String apellidoMaterno;

    @Getter
    @Setter
    @JsonProperty("ApellidoPaterno")
    private String apellidoPaterno;

    @Getter
    @Setter
    @JsonProperty("Email")
    private String email;

    @Getter
    @Setter
    @JsonProperty("NombreCompleto")
    private String nombreCompleto;

    @Getter
    @Setter
    @JsonProperty("Grupo")
    private String grupo;

    @Getter
    @Setter
    @JsonProperty("Iniciales")
    private String iniciales;

    @Nullable
    @Getter
    @Setter
    @JsonProperty("IdFoto")
    private int idFoto;

    @Getter
    @Setter
    @JsonProperty("Cargo")
    private String cargo;

    @Getter
    @Setter
    @Nullable
    @JsonProperty("FechaRegistro")
    private Date fechaRegistro;

    @Getter
    @Setter
    @Nullable
    @JsonProperty("UsuarioRegistro")
    private int usuarioRegistro;

    @Getter
    @Setter
    @Nullable
    @JsonProperty("FechaActualizacion")
    private Date fechaActualizacion;

    @Getter
    @Setter
    @Nullable
    @JsonProperty("UsuarioModificacion")
    private int usuarioModificacion;

    @Getter
    @Setter
    @JsonProperty("CodigoPersona")
    private int codigoPersona;

    @Getter
    @Setter
    @JsonProperty("ClaveNueva")
    private String claveNueva;

    @Getter
    @Setter
    @JsonProperty("ClaveAnterior")
    private String claveAnterior;

    @Getter
    @Setter
    @JsonProperty("IdSede")
    private int idSede;

    @Getter
    @Setter
    @JsonProperty("IdGrado")
    private int idGrado;

    @Getter
    @Setter
    @JsonProperty("IdSeccion")
    private int idSeccion;

    @Getter
    @Setter
    @JsonProperty("RecoverPasswordHash")
    private String recoverPasswordHash;

    @Getter
    @Setter
    @JsonProperty("FotoPerfil")
    private MensajeAdjunto fotoPerfil;

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder()
                .append("(").append("IdPersona=").append(idPersona).append(",IdRol=").append(idRol)
                .append(",PrimerNombre=").append(primerNombre).append(",SegundoNombre=").append(segundoNombre)
                .append(",ApellidoPaterno=").append(apellidoPaterno).append(",ApellidoMaterno=").append(apellidoMaterno)
                .append(",NombreCompleto=").append(nombreCompleto).append(",Email=").append(email)
                .append(",Grupo=").append(grupo).append(",Iniciales=").append(iniciales)
                .append(",IdFoto=").append(idFoto).append(",Cargo=").append(cargo)
                .append(",FechaRegistro=").append(fechaRegistro).append(",UsuarioRegistro=").append(usuarioRegistro)
                .append(",FechaActualizacion=").append(fechaActualizacion)
                .append(",UsuarioModificacion=").append(usuarioModificacion)
                .append(",CodigoPersona=").append(codigoPersona).append(",IdSede=").append(idSede)
                .append(",IdGrado=").append(idGrado).append(",IdSeccion=").append(idSeccion)
                .append(",RecoverPasswordHash=").append(recoverPasswordHash).append(",FotoPerfil=").append(fotoPerfil)
                .append(")");
        return builder.toString();
    }
}
