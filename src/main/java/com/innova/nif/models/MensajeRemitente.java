package com.innova.nif.models;

import lombok.Getter;
import lombok.Setter;

public class MensajeRemitente {

    public MensajeRemitente() {
        nombreRemitente = "";
        correoRemitente = "";
        grupoRemitente = "";
        iniciales = "";
        cargo = "";
        firma = "";
        esEliminado = false;
    }

    @Getter
    @Setter
    boolean esEliminado;

    @Getter
    @Setter
    String firma;

    @Getter
    @Setter
    String cargo;

    @Getter
    @Setter
    String iniciales;

    @Getter
    @Setter
    String grupoRemitente;

    @Getter
    @Setter
    String correoRemitente;

    @Getter
    @Setter
    String nombreRemitente;

    @Getter
    @Setter
    int idRemitente;

    @Getter
    @Setter
    int idMensaje;
}
