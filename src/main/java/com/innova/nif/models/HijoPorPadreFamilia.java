package com.innova.nif.models;

import lombok.Getter;
import lombok.Setter;

public class HijoPorPadreFamilia extends Hijo {
    @Getter
    @Setter
    String referenteHijo;

    @Getter
    @Setter
    int numeroHijoEdad;
}