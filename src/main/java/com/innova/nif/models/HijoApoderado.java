package com.innova.nif.models;

import lombok.Getter;
import lombok.Setter;

public class HijoApoderado extends Hijo {
    @Getter
    @Setter
    String referenteHijo;

    @Getter
    @Setter
    int numeroHijoEdad;

    @Getter
    String nombreAlumno;

    public void setNombreAlumno(String apellidoPaterno, String apellidoMaterno) {
        nombreAlumno = new StringBuilder()
                .append(primerNombre).append(" ")
                .append(apellidoPaterno).append(" ")
                .append(apellidoMaterno).toString();
    }
}
