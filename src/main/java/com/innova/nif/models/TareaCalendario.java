package com.innova.nif.models;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.innova.nif.utils.DateTimeSerializer;
import com.innova.nif.utils.StringHelper;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

public class TareaCalendario {
    public TareaCalendario() {
        titulo = StringHelper.EMPTY_STRING;
        descripcion = StringHelper.EMPTY_STRING;
        color = StringHelper.EMPTY_STRING;
    }

    @Getter
    @Setter
    int idTarea;

    @Getter
    @Setter
    String titulo;

    @Getter
    @Setter
    String descripcion;

    @Getter
    @Setter
    @JsonSerialize(using = DateTimeSerializer.class)
    DateTime fechaEntrega;

    @Getter
    @Setter
    @JsonSerialize(using = DateTimeSerializer.class)
    DateTime fechaEnvio;

    @Getter
    @Setter
    @JsonSerialize(using = DateTimeSerializer.class)
    DateTime fechaRegistro;

    @Getter
    @Setter
    int idPersona;

    @Getter
    @Setter
    String color;

    @Getter
    @Setter
    int idPeriodo;

    @Setter
    String fechaEntregaConFormato;

    public String getFechaEntregaConFormato() {
        return DateTimeFormat.forPattern("dd-MM-yyyy").print(fechaEntrega);
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("(")
                .append("idTarea=").append(idTarea).append(",titulo=").append(titulo).append(",descripcion=")
                .append(descripcion).append(",fechaEntrega=").append(fechaEntrega).append(",fechaEnvio=")
                .append(fechaEnvio).append(",fechaRegistro=").append(fechaRegistro).append(",idPersona=")
                .append(idPersona).append(",color=").append(color).append(",idPeriodo=").append(idPeriodo)
                .append(",fechaEntregaConFormato=").append(fechaEntregaConFormato).append(",getFechaEntregaConFormato=")
                .append(getFechaEntregaConFormato()).append(")").toString();
    }
}