package com.innova.nif.models;

import lombok.Getter;
import lombok.Setter;

public class Grado {
    @Getter
    @Setter
    int idGrado;

    @Getter
    @Setter
    String descripcion;

    @Getter
    @Setter
    int idNivel;

    @Getter
    @Setter
    String codigoGrado;

    @Getter
    @Setter
    String detalleNivel;

    @Getter
    @Setter
    int idSede;
}
