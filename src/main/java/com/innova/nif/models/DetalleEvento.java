package com.innova.nif.models;

import lombok.Getter;
import lombok.Setter;

public class DetalleEvento {
    @Getter
    @Setter
    int idEvento;

    @Getter
    @Setter
    int idSede;

    @Getter
    @Setter
    int idGrado;

    @Getter
    @Setter
    String codigoGrado;

    @Getter
    @Setter
    int nivel;

    @Getter
    @Setter
    String grado;

    @Override
    public String toString() {
        return new StringBuilder()
                .append(")")
                .append("idEvento=").append(idEvento).append(",idSede=").append(idSede).append(",idGrado=")
                .append(idGrado).append(",codigoGrado=").append(codigoGrado).append(",nivel=").append(nivel)
                .append(",grado=").append(grado).append(")").toString();
    }
}