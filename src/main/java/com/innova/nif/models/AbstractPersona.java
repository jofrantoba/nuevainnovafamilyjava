package com.innova.nif.models;

import lombok.Getter;

public abstract class AbstractPersona {
    @Getter
    String primerNombre;

    @Getter
    String segundoNombre;

    @Getter
    String nombres;

    public void setNombres(String nombre) {
        this.nombres = nombre;
        var indexSeparator = nombre.indexOf(' ');
        if (nombre.isEmpty() || indexSeparator <= 0) {
            primerNombre = nombre;
            segundoNombre = "";
        } else {
            primerNombre = nombre.substring(0, indexSeparator);
            segundoNombre = nombre.substring(indexSeparator).trim();
        }
    }

    @Getter
    String nombresIniciales;

    public void setNombresIniciales(String primerNombre, String apellidoPaterno) {
        if ((primerNombre.isEmpty() || primerNombre.isBlank()) ||
                (apellidoPaterno.isEmpty() || apellidoPaterno.isBlank()))
            nombresIniciales = "";
        else
            nombresIniciales = primerNombre.substring(0, 1).toUpperCase() +
                    apellidoPaterno.substring(0, 1).toUpperCase();
    }
}
