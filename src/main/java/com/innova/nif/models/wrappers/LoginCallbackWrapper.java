package com.innova.nif.models.wrappers;

import com.innova.nif.utils.StringHelper;
import lombok.Getter;
import lombok.Setter;

public class LoginCallbackWrapper {
    public LoginCallbackWrapper() {
        isAuthenticated = false;
        loginSuccessful = false;
        oneTimeToken = StringHelper.EMPTY_STRING;
        messageExit = StringHelper.EMPTY_STRING;
        authWithGoogleFailed = true;
    }

    @Getter
    @Setter
    boolean isAuthenticated;

    @Getter
    @Setter
    boolean loginSuccessful;

    @Getter
    @Setter
    String oneTimeToken;

    @Getter
    @Setter
    String messageExit;

    @Getter
    @Setter
    boolean authWithGoogleFailed;

    @Getter
    @Setter
    String correo;

    @Override
    public String toString() {
        return new StringBuilder().append("(")
                .append("isAuthenticated=").append(isAuthenticated).append(",loginSuccessful=").append(loginSuccessful)
                .append(",oneTimeToken=").append(oneTimeToken).append(",messageExit=").append(messageExit)
                .append(",authWithGoogleFailed=").append(authWithGoogleFailed).append(",correo=").append(correo)
                .append(")").toString();
    }
}
