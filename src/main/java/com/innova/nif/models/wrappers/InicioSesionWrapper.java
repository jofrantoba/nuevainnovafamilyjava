package com.innova.nif.models.wrappers;

import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.util.Strings;


public class InicioSesionWrapper {
    public InicioSesionWrapper() {
        correo = Strings.EMPTY;
        password = Strings.EMPTY;
        nombreUsuario = Strings.EMPTY;
        idPerfil = 0;
        perfil = Strings.EMPTY;
        tipo = Strings.EMPTY;
        returnUrl = Strings.EMPTY;
        dni = Strings.EMPTY;
        anio = Strings.EMPTY;
    }

    @Getter
    @Setter
    String correo;

    @Getter
    @Setter
    String password;

    @Getter
    @Setter
    String nombreUsuario;

    @Getter
    @Setter
    int idPerfil;

    @Getter
    @Setter
    String perfil;

    @Getter
    @Setter
    String tipo;

    @Getter
    @Setter
    String dni;

    @Getter
    @Setter
    String anio;

    @Getter
    @Setter
    String returnUrl;

    @Getter
    @Setter
    int tipoConsulta;

    @Override
    public String toString() {
        return new StringBuilder().append("(")
                .append("correo=").append(correo).append(",password=").append(password).append(",nombreUsuario=")
                .append(nombreUsuario).append(",idPerfil=").append(idPerfil).append(",perfil=").append(perfil)
                .append(",tipo=").append(tipo).append(",dni=").append(dni).append(",anio=").append(anio)
                .append(",returnUrl=").append(returnUrl).append(",tipoConsulta=").append(tipoConsulta)
                .append(")").toString();
    }
}
