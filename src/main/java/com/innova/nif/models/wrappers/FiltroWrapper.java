package com.innova.nif.models.wrappers;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

public class FiltroWrapper {
    @Getter
    @Setter
    int idHijo;

    @Getter
    @Setter
    int idPersona;

    @Getter
    @Setter
    int idSede;

    @Getter
    @Setter
    int idPeriodo;

    @Getter
    @Setter
    int idPeriodoSede;

    @Getter
    @Setter
    int anio;

    @Getter
    @Setter
    int idGrado;

    @Getter
    @Setter
    String grado;

    @Getter
    @Setter
    String seccion;

    @Getter
    @Setter
    int idSeccion;

    @Getter
    @Setter
    int idCurso;

    @Getter
    @Setter
    String nivel;

    @Getter
    @Setter
    String tipoAlerta;

    @Getter
    @Setter
    String listaAlerta;

    @Getter
    @Setter
    int flagAlerta;

    @Getter
    @Setter
    String recursos;

    @Getter
    @Setter
    String dni;

    @Getter
    @Setter
    Map<String, Object> informacionSesion;

    @Override
    public String toString() {
        return new StringBuilder().append("(")
                .append("idHijo=").append(idHijo).append(",idPersona=").append(idPersona).append(",idSede=")
                .append(idSede).append(",idPeriodo=").append(idPeriodo).append(",idPeriodoSede=").append(idPeriodoSede)
                .append(",anio=").append(anio).append(",idGrado=").append(idGrado).append(",grado=").append(grado)
                .append(",seccion=").append(seccion).append(",idSeccion=").append(idSeccion).append(",idCurso=")
                .append(idCurso).append(",nivel=").append(nivel).append(",tipoAlerta=").append(tipoAlerta)
                .append(",listaAlerta=").append(listaAlerta).append(",flagAlerta=").append(flagAlerta)
                .append(",recursos=").append(recursos).append(",dni=").append(dni).append(",informacionSesion=")
                .append(informacionSesion)
                .append(")").toString();
    }
}