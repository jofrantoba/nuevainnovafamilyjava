package com.innova.nif.models.wrappers;

import lombok.Getter;
import lombok.Setter;

public class UserNifParametros {
    public UserNifParametros() {
    }

    public UserNifParametros(String correo, int idPeriodo, String tipoConsulta) {
        this.correo = correo;
        this.idPeriodo = idPeriodo;
        this.tipoConsulta = tipoConsulta;
    }

    @Getter
    @Setter
    private String correo;
    @Getter
    @Setter
    private String dni;

    @Getter
    @Setter
    private int anio;

    @Getter
    @Setter
    private int idSede;

    @Getter
    @Setter
    private String tipoConsulta;

    @Getter
    @Setter
    private int idPeriodo;
}
