package com.innova.nif.models.wrappers;

import com.innova.nif.utils.Constantes;
import lombok.Getter;
import lombok.Setter;

public class AgendaFiltro {
    @Getter
    @Setter
    String idAgenda;

    @Getter
    @Setter
    int idTareaEvento;

    @Getter
    @Setter
    int idAula;

    @Getter
    @Setter
    int idGrado;

    @Getter
    @Setter
    String tipoAgenda;

    @Getter
    @Setter
    String fechaInicio;

    @Getter
    @Setter
    String fechaFin;

    @Getter
    @Setter
    String fechayyyymmddInicio;

    @Getter
    @Setter
    String fechayyyymmddFin;

    @Getter
    @Setter
    int idPersona;

    @Getter
    @Setter
    int idSede;

    @Getter
    @Setter
    String grupo;

    @Getter
    @Setter
    String listaIdGrado;

    @Getter
    @Setter
    String listaSeccion;

    @Getter
    @Setter
    String listaAlumno;

    @Getter
    @Setter
    int codigoPersona;

    @Getter
    @Setter
    int idPeriodo;

    @Getter
    @Setter
    String seccion;

    @Getter
    @Setter
    String nivelIngles;

    public boolean esTarea() {
        return tipoAgenda.equals(Constantes.TIPO_AGENDA_TAREA);
    }

    public boolean esEvento() {
        return tipoAgenda.equals(Constantes.TIPO_AGENDA_EVENTO);
    }
}