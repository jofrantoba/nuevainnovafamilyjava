package com.innova.nif.models.wrappers;

import com.innova.nif.models.Usuario;
import lombok.Getter;
import lombok.Setter;

public class UsuarioSesionWrapper {
    @Getter
    @Setter
    Usuario usuarioDM;

    @Getter
    @Setter
    Usuario usuarioNIF;
}
