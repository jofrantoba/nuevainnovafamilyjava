package com.innova.nif.models.wrappers;

import com.innova.nif.utils.StringHelper;
import lombok.Getter;
import lombok.Setter;

public class MensajeFiltro {
    public MensajeFiltro() {
        filtro = StringHelper.EMPTY_STRING;
        tipoBandeja = StringHelper.EMPTY_STRING;
        numeroPagina = 1;
        tamanioPagina = 10;
        grupo = StringHelper.EMPTY_STRING;
        showData = false;
    }

    @Getter
    @Setter
    int idCorreo;

    @Getter
    @Setter
    int idPersona;

    @Getter
    @Setter
    int idMensaje;

    @Getter
    @Setter
    String filtro;

    @Getter
    @Setter
    int idMensajeDestino;

    @Getter
    @Setter
    String tipoBandeja;

    @Getter
    @Setter
    int numeroPagina;

    @Getter
    @Setter
    int tamanioPagina;

    @Getter
    @Setter
    int totalPagina;

    @Getter
    @Setter
    String grupo;

    @Getter
    @Setter
    boolean showData;

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder()
                .append("(")
                .append("idPersona=").append(idPersona).append(",idCorreo=").append(idCorreo)
                .append(",idMensaje=").append(idMensaje).append(",idMensajeDestino=").append(idMensajeDestino)
                .append(",filtro=").append(filtro).append(",tipoBandeja=").append(tipoBandeja)
                .append(",numeroPagina=").append(numeroPagina).append(",tamanioPagina=").append(tamanioPagina)
                .append(",totalPagina=").append(totalPagina).append(",grupo=").append(grupo)
                .append(",showData=").append(showData)
                .append(")");
        return builder.toString();
    }
}
