package com.innova.nif.models.wrappers;

import lombok.Getter;
import lombok.Setter;

public class MensajeWrapper {
    @Getter
    @Setter
    int id;

    @Getter
    @Setter
    String tipo;

    @Getter
    @Setter
    int idPersona;

    @Getter
    @Setter
    int idUsuarioActual;

    @Getter
    @Setter
    String grupoUsuarioActual;

    @Override
    public String toString() {
        return new StringBuilder().append("(")
                .append("id=").append(id).append(",tipo=").append(tipo).append(",idPersona=").append(idPersona)
                .append(",idUsuarioActual=").append(idUsuarioActual).append(",grupoUsuarioActual=")
                .append(grupoUsuarioActual)
                .append(")").toString();
    }
}
