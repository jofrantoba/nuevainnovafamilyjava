package com.innova.nif.models.wrappers;

import com.innova.nif.models.DocenteNivelIngles;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import java.util.List;

public class FiltroIngles {
    @Getter
    @Setter
    String dni;

    @Getter
    @Setter
    int idSede;

    @Getter
    @Setter
    int anio;

    @Getter
    @Setter
    int idGrado;

    @Getter
    @Setter
    DateTime fecha;

    @Getter
    @Setter
    List<DocenteNivelIngles> listaDocentes;
}
