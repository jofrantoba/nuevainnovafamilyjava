package com.innova.nif.models.wrappers;

import com.innova.nif.models.Contacto;
import com.innova.nif.models.Grado;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

public class DocentesWrapper {
    public DocentesWrapper() {
        grados = new ArrayList<>();
    }

    @Getter
    @Setter
    List<Contacto> docentes;

    @Getter
    @Setter
    List<Grado> grados;
}
