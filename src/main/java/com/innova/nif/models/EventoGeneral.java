package com.innova.nif.models;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EventoGeneral {
    public EventoGeneral() {
        idGrados = new ArrayList<>();
        lstAdjunto = new ArrayList<>();
    }

    @Getter
    @Setter
    EventoCalendario evento;

    @Getter
    @Setter
    int idGrado;

    @Getter
    @Setter
    String fechaHora;

    @Getter
    @Setter
    boolean todaLaSede;

    @Getter
    @Setter
    boolean esEditado;

    @Getter
    @Setter
    String participante;

    @Getter
    @Setter
    List<String> idGrados;

    @Setter
    List<String> listaGradosSeleccionados;

    public List<String> getListaGradosSeleccionados() {
        return this.todaLaSede ? new ArrayList<>(Collections.singletonList("T")) : this.idGrados;
    }

    @Getter
    @Setter
    List<AgendaAdjunto> lstAdjunto;
}
