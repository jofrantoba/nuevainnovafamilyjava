package com.innova.nif.models;

import lombok.Getter;
import lombok.Setter;

public class Hijo extends AbstractPersona {
    @Getter
    @Setter
    int id;

    @Getter
    @Setter
    int idPersona;

    @Getter
    @Setter
    String sede;

    @Getter
    @Setter
    int idSede;

    @Getter
    @Setter
    int idGrado;

    @Getter
    @Setter
    String grado;

    @Getter
    @Setter
    String gradoNombre;

    @Getter
    @Setter
    String seccionLetra;


    @Getter
    @Setter
    int codigoAlumno;

    @Getter
    @Setter
    int codigoPadre;

    @Getter
    @Setter
    String titulo;

    @Getter
    @Setter
    String esApoderado;

    @Getter
    @Setter
    String esResponsablePago;

    @Getter
    @Setter
    String dni;

    @Getter
    @Setter
    int idSeccion;

    @Getter
    @Setter
    String nivel;
}
