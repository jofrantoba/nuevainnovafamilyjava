package com.innova.nif.models;

import lombok.Getter;
import lombok.Setter;

public class Calendario {
    @Getter
    @Setter
    String idAgenda;

    @Getter
    @Setter
    String titulo;

    @Getter
    @Setter
    String fechaInicio;

    @Getter
    @Setter
    String fechaFinal;

    @Getter
    @Setter
    String color;
}