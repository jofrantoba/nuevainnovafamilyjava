package com.innova.nif.models;

import lombok.Getter;
import lombok.Setter;

public class DocenteNivelIngles {
    @Getter
    @Setter
    String classRoomLevelCode;

    @Getter
    @Setter
    String englishLevelId;

    @Getter
    @Setter
    String dni;

    @Getter
    @Setter
    int idGrado;

    @Getter
    @Setter
    String grado;
}
