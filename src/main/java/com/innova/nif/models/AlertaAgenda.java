package com.innova.nif.models;

import com.innova.nif.utils.StringHelper;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

public class AlertaAgenda {

    public AlertaAgenda() {
        tipo = StringHelper.EMPTY_STRING;
        titulo = StringHelper.EMPTY_STRING;
        color = StringHelper.EMPTY_STRING;
        ubicacion = StringHelper.EMPTY_STRING;
        codigoGrado = StringHelper.EMPTY_STRING;
        seccion = StringHelper.EMPTY_STRING;
        nombreCurso = StringHelper.EMPTY_STRING;
        listaUserAlerta = new ArrayList<>();
    }

    @Getter
    @Setter
    int idAlertaAgenda;

    @Getter
    @Setter
    int idTareaEvento;

    @Getter
    @Setter
    int idDetalle;

    @Getter
    @Setter
    String tipo;

    @Getter
    @Setter
    String titulo;

    @Getter
    @Setter
    int idPeriodo;

    @Getter
    @Setter
    int idSede;

    @Getter
    @Setter
    DateTime fechaEntrega;

    @Getter
    @Setter
    String color;

    @Getter
    @Setter
    String ubicacion;

    @Getter
    @Setter
    int idGrado;

    @Getter
    @Setter
    String codigoGrado;

    @Getter
    @Setter
    String seccion;

    @Getter
    @Setter
    String nombreCurso;

    @Getter
    @Setter
    List<AlertaUsuario> listaUserAlerta;
}
