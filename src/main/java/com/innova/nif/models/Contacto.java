package com.innova.nif.models;

import lombok.Getter;
import lombok.Setter;

public class Contacto extends AbstractPersona {
    @Getter
    @Setter
    int orden;

    @Getter
    @Setter
    int idReferente;

    @Getter
    @Setter
    int idPersona;

    @Getter
    @Setter
    String apellidoPaterno;

    @Getter
    @Setter
    String apellidoMaterno;

    @Getter
    @Setter
    String correo;

    @Getter
    @Setter
    String dni;

    @Getter
    @Setter
    String grupo;

    @Getter
    @Setter
    String cargo;

    @Getter
    @Setter
    int idGrado;

    @Getter
    @Setter
    int estado;

    @Getter
    @Setter
    int idCurso;

    @Getter
    @Setter
    int idCursoPadre;

    @Getter
    @Setter
    String gradoSeccion;
}