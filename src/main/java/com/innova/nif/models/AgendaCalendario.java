package com.innova.nif.models;

import com.innova.nif.utils.Constantes;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class AgendaCalendario {
    AgendaCalendario() {
        listaDetalleTarea = new ArrayList<>();
        listaDetalleEvento = new ArrayList<>();
        listaAdjunto = new ArrayList<>();
    }

    @Getter
    @Setter
    String tipoAgenda;

    @Getter
    @Setter
    TareaCalendario tarea;

    @Getter
    @Setter
    String fechaddmmyyyyInicio;

    @Getter
    @Setter
    String fechaddmmyyyyFin;

    @Getter
    @Setter
    String listaGrado;

    @Getter
    @Setter
    String listaArchivo;

    @Getter
    @Setter
    String listaArchivosEliminar;

    @Getter
    @Setter
    List<DetalleTarea> listaDetalleTarea;

    @Getter
    @Setter
    EventoCalendario evento;

    @Getter
    @Setter
    List<DetalleEvento> listaDetalleEvento;

    @Getter
    @Setter
    List<AgendaAdjunto> listaAdjunto;

    @Getter
    @Setter
    int idAula;

    @Getter
    @Setter
    int idTarea;

    @Getter
    @Setter
    Integer idEvento;

    public List<Integer> idsArchivosEliminar() {
        if (listaArchivosEliminar == null || listaArchivosEliminar.isBlank() || listaArchivosEliminar.isEmpty())
            return new ArrayList<>();
        return Arrays.stream(listaArchivosEliminar
                .split(","))
                .map(String::trim)
                .mapToInt(Integer::parseInt).boxed().collect(Collectors.toList());
    }

    public boolean esTarea() {
        return tipoAgenda.equals(Constantes.TIPO_AGENDA_TAREA);
    }

    public boolean esEvento() {
        return tipoAgenda.equals(Constantes.TIPO_AGENDA_EVENTO);
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("(")
                .append("tipoAgenda=").append(tipoAgenda).append(",tarea=").append(tarea)
                .append(",fechaddmmyyyyInicio=").append(fechaddmmyyyyInicio).append(",fechaddmmyyyyFin=")
                .append(fechaddmmyyyyFin).append(",listaGrado=").append(listaGrado).append(",listaArchivo=")
                .append(listaArchivo).append(",listaArchivosEliminar=").append(listaArchivosEliminar)
                .append(",listaDetalleTarea=").append(listaDetalleTarea).append(",evento=").append(evento)
                .append(",listaDetalleEvento=").append(listaDetalleEvento).append(",listaAdjunto=").append(listaAdjunto)
                .append(",idAula=").append(idAula).append(",idTarea=").append(idTarea).append(",idEvento=")
                .append(idEvento).append(",idsArchivosEliminar=").append(idsArchivosEliminar()).append(")").toString();
    }
}