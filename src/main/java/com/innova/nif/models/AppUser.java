package com.innova.nif.models;

import lombok.Getter;
import lombok.Setter;

public class AppUser {
    @Getter
    @Setter
    int idPersona;

    @Getter
    @Setter
    int idGrado;

    @Getter
    @Setter
    int idSeccion;

    @Getter
    @Setter
    int idSede;

    @Getter
    @Setter
    int idPeriodo;

    @Getter
    @Setter
    String nombre;

    @Getter
    @Setter
    String nombreCorto;

    @Getter
    @Setter
    String iniciales;

    @Getter
    @Setter
    String grupo;

    @Getter
    @Setter
    String dni;

    @Getter
    @Setter
    String correo;

    @Getter
    @Setter
    String firma;

    @Getter
    @Setter
    String seccion;

    @Getter
    @Setter
    String cargo;

    @Getter
    @Setter
    String grado;

    @Getter
    @Setter
    String anio;

    @Getter
    @Setter
    String apellidos;

    @Getter
    @Setter
    String nivel;

    @Getter
    @Setter
    String sede;

    @Getter
    @Setter
    String regionSede;

    @Getter
    @Setter
    boolean isEmailValidating;
}
