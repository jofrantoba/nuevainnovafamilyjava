package com.innova.nif.models;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

public class BackOfficeParametros {
    public static final String ALL_CODE = "ALL";
    public static final String ALUMNO_CODE = "AL";
    public static final String PADRE_FAMILIA_CODE = "PF";
    public static final String DOCENTE_CODE = "DOC";

    public static List<String> rolesPea() {
        List<String> rolesPea = new ArrayList<>();
        rolesPea.add(ALUMNO_CODE);
        rolesPea.add(PADRE_FAMILIA_CODE);
        rolesPea.add(DOCENTE_CODE);
        return rolesPea;
    }

    public BackOfficeParametros() {
        roles = new ArrayList<>();
        sedes = new ArrayList<>();
        secciones = new ArrayList<>();
        grados = new ArrayList<>();
        niveles = new ArrayList<>();
    }

    @Getter
    @Setter
    private List<BackOfficeParametro> roles;

    @Getter
    @Setter
    private List<BackOfficeParametro> sedes;

    @Getter
    @Setter
    private List<BackOfficeParametro> secciones;

    @Getter
    @Setter
    private List<BackOfficeParametro> grados;

    @Getter
    @Setter
    private List<BackOfficeParametro> niveles;

    @Getter
    @Setter
    private int idPeriodo;

    @Getter
    @Setter
    private String estadoPeriodo;
}
