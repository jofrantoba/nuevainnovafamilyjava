package com.innova.nif.utils;

import static com.innova.nif.utils.ObjectHelper.notNull;

public class StringHelper {

    private StringHelper() {
    }

    public static final String EMPTY_STRING = "";
    private static final String DEFAULT_STRING = EMPTY_STRING;

    public static String getValueOrEmpty(String value) {
        var text = DEFAULT_STRING;
        if (notNull(value) && !value.isBlank())
            text = value;
        return text;
    }

    public static String getValueOrEmpty(Object value) {
        var text = DEFAULT_STRING;
        if (value != null) text = value.toString();
        return text;
    }

    public static String obtenerTextoHtml(String texto) {
        if (texto == null) return EMPTY_STRING;
        texto = eliminarStyleTags(texto);
        texto = eliminaTagsXML(texto);
        if (texto == null) return EMPTY_STRING;
        texto = texto.replaceAll("&nbsp;", " ");
        return texto;
    }

    private static String eliminarStyleTags(String texto) {
        String styleTagsPattern = "<\\s*style[^>]*>(.*?)<\\s*/\\s*style>";
        return texto.replaceAll(styleTagsPattern, EMPTY_STRING);
    }

    private static String eliminaTagsXML(String texto) {
        if (texto == null || texto.isBlank() || texto.isEmpty()) return null;
        return texto.replaceAll("<[^>]+>", "");
    }
}
