package com.innova.nif.utils;

import lombok.Getter;
import lombok.Setter;

public class RespuestaApi {
    @Getter
    @Setter
    private String dataJson;

    @Getter
    @Setter
    private String stackTrace;

    @Getter
    @Setter
    private String mensaje;

    @Getter
    @Setter
    private int mensajeId;

    @Getter
    @Setter
    private String source;

    @Getter
    @Setter
    private String status;

    @Getter
    @Setter
    private Object data;

    @Getter
    @Setter
    private int statusCode;

    @Getter
    @Setter
    private String viewResult;

    @Getter
    @Setter
    private String correosEnviado;

    @Getter
    @Setter
    private int pageCount;

    @Getter
    @Setter
    private String idPersona;

    @Getter
    @Setter
    private int idPeriodoLectivo;

    @Getter
    @Setter
    private int idSede;

    @Getter
    @Setter
    private String idWsPersona;

    @Getter
    @Setter
    private int referenciaId;


    public RespuestaApi()
    {
        mensaje = "";
        status = "OK";
    }
}
