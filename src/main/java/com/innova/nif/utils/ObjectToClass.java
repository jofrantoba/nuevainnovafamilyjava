package com.innova.nif.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class ObjectToClass {
    private ObjectMapper mapper;

    public ObjectToClass() {
        mapper = new ObjectMapper();
        mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public <T> T obtenerObjeto(Object objetoFuente, Class<T> claseObjetivo) {
        return mapper.convertValue(objetoFuente, claseObjetivo);
    }

    public <T> T obtenerListaObjetos(Object objetoFuente) throws IOException {
        return mapper.readValue(objetoFuente.toString(), new TypeReference<java.util.List<T>>(){});
    }
}
