package com.innova.nif.utils;

public class Constantes {

    private Constantes() {
    }

    public static final String PERIODO_LECTIVO_POSTERIOR = "POS";
    public static final String PERIODO_LECTIVO_ACTUAL = "ACT";
    public static final String DATETIME_FORMAT = "dd-MM-yyyy hh:mm:ss";
    public static final String DATE_FORMAT_MMDDYYYY = "MM/dd/yyyy";
    public static final String FULL_CALENDAR_FORMAT = "yyyy-MM-dd'T'HH:mm";
    public static final String DATE_SQL_FORMAT = "yyyy-MM-dd";
    public static final String DATE_TIME_SQL_FORMAT = "yyyy-MM-dd HH:mm";
    public static final String TIME_FORMAT = "hh:mm a";
    public static final String ERROR = "ERROR";
    public static final String OK = "OK";
    public static final int CODIGO_EXITO = 1;
    public static final int CODIGO_ERROR = 0;
    public static final String CONSULTA_PADRE_FAMILIA = "PF";
    public static final String CONSULTA_ALUMNO = "AL";
    public static final String CONSULTA_INNOVA = "IN";
    public static final String CONSULTA_DOCENTE = "DC";
    public static final String TIPO_AGENDA_TAREA = "T";
    public static final String TIPO_AGENDA_EVENTO = "E";
    public static final String PERFIL_EQUIPO_DIRECTIVO = "ED";
    public static final String PERFIL_DOCENTE = "DC";
    public static final String PERFIL_PP_FF = "PF";
    public static final String PERFIL_ALUMNO = "AL";
    public static final String SESION_LISTA_HIJOS = "ListaHijos";
    public static final String SESION_LISTA_GRADOS = "ListaGrados";
    public static final int GRADO_SECUNDARIA = 10;
    public static final String GRUPO_DESTINO_ALUMNO_SEDE = "AS";
    public static final String GRUPO_DESTINO_DOCENTE_SEDE = "DS";
    public static final String GRUPO_DESTINO_PPFF_SEDE = "PS";
    public static final String GRUPO_DESTINO_ALUMNO_GRADO = "AG";
    public static final String GRUPO_DESTINO_DOCENTE_GRADO = "DG";
    public static final String GRUPO_DESTINO_PPFF_GRADO = "PG";
}
