package com.innova.nif.utils;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.sql.Date;
import java.sql.Timestamp;

public class DateHelper {
    static DateTimeFormatter dateFormatter = DateTimeFormat.forPattern(Constantes.DATE_FORMAT_MMDDYYYY);
    static DateTimeFormatter timeFormatter = DateTimeFormat.forPattern(Constantes.TIME_FORMAT);
    static DateTimeFormatter datetimeFormatterISO = DateTimeFormat.forPattern(Constantes.FULL_CALENDAR_FORMAT);
    static DateTimeFormatter dateFormatterSQL = DateTimeFormat.forPattern(Constantes.DATE_SQL_FORMAT);
    static DateTimeFormatter dateTimeFormatterSQL = DateTimeFormat.forPattern(Constantes.DATE_TIME_SQL_FORMAT);

    private DateHelper() {

    }

    public static DateTime getDateOrDefault(Object value) {
        if (value != null)
            return new DateTime(value);
        return null;
    }

    public static String fechaComoString(Date fecha) {
        return dateFormatter.print(fecha.getTime());
    }

    public static String horaComoString(Timestamp fecha) {
        return timeFormatter.print(fecha.getTime());
    }

    public static String formatoFullCalendar(DateTime datetime) {
        return datetimeFormatterISO.print(datetime);
    }

    public static String convertirASQLDate(DateTime dateTime) {
        return dateFormatterSQL.print(dateTime);
    }

    public static String convertirASQLDateTime(DateTime dateTime) {
        return dateTimeFormatterSQL.print(dateTime);
    }
}
