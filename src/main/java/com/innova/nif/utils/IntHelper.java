package com.innova.nif.utils;

import static com.innova.nif.utils.ObjectHelper.notNull;

public class IntHelper {

    private IntHelper() {
    }

    private static final int DEFAULT_INT = 0;

    public static int getValueOrDefault(int value) {
        var number = DEFAULT_INT;
        if (notNull(value)) {
            number = value;
        }

        return number;
    }
}
