package com.innova.nif.utils;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ErrorManager {
    private ErrorManager() {
    }

    public static String getStringStackTrace(Exception ex) {
        StringWriter sw = new StringWriter();
        ex.printStackTrace(new PrintWriter(sw));
        return sw.toString();
    }
}
