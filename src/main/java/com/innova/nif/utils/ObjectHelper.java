package com.innova.nif.utils;

public class ObjectHelper {
    private ObjectHelper() {
    }

    public static Boolean isNull(Object object) {
        return object == null;
    }

    public static Boolean notNull(Object object) {
        return object != null;
    }
}
