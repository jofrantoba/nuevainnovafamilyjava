package com.innova.nif.utils;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

public class Util {
    private Util() {

    }

    public static String generateUUID() throws NoSuchAlgorithmException {
        MessageDigest salt = MessageDigest.getInstance("SHA-256");
        salt.update(UUID.randomUUID().toString().getBytes(StandardCharsets.UTF_8));
        return bytesToHex(salt.digest());
    }

    public static String obtenerInicialesNombre(String nombres, String paterno) {
        final int START_INDEX = 0;
        final int END_INDEX = 1;
        if (nombres != null && !nombres.isEmpty() && !nombres.isBlank() && paterno != null && !paterno.isEmpty()
                && !paterno.isBlank())
            return String.format("%s%s", nombres.trim().substring(START_INDEX, END_INDEX).toUpperCase(),
                    paterno.trim().substring(START_INDEX, END_INDEX).toUpperCase());
        return "";
    }

    private static String bytesToHex(byte[] hashInBytes) {
        var hashInBytesHex = new StringBuilder();
        for (byte byteItem : hashInBytes) {
            hashInBytesHex.append(String.format("%02x", byteItem));
        }
        return hashInBytesHex.toString();
    }

    public static String encodeUrl(String url) {
        return URLEncoder.encode(url, java.nio.charset.StandardCharsets.UTF_8);
    }
}
