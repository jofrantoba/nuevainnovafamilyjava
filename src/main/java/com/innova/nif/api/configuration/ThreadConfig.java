package com.innova.nif.api.configuration;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
public class ThreadConfig {
    @Value("${thread.CorePoolSize}")
    private String corePoolSize;
    @Value("${thread.MaxPoolSize}")
    private String maxPoolSize;
    @Value("${thread.QueueCapacity}")
    private String queueCapacity;
    @Value("${thread.KeepAliveSeconds}")
    private String keepAliveSeconds;

    @Bean
    public TaskExecutor threadPoolTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(Integer.parseInt(corePoolSize));
        executor.setMaxPoolSize(Integer.parseInt(maxPoolSize));
        executor.setQueueCapacity(Integer.parseInt(queueCapacity));
        executor.setAllowCoreThreadTimeOut(true);
        executor.setKeepAliveSeconds(Integer.parseInt(keepAliveSeconds));
        executor.setThreadNamePrefix("default_task_executor_thread");
        executor.initialize();
        return executor;
    }
}
