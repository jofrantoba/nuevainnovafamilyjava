package com.innova.nif.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.innova.nif"})
public class Application extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean(name = "nifDb")
    @Primary
    @ConfigurationProperties(prefix = "nif.datasource")
    public DataSource primaryDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "dmDb")
    @ConfigurationProperties(prefix = "dm.datasource")
    public DataSource secondaryDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "peaDb")
    @ConfigurationProperties(prefix = "pea.datasource")
    public DataSource peaDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "transacNif")
    @Autowired
    @Primary
    DataSourceTransactionManager tm1(@Qualifier("nifDb") DataSource datasource) {
        return new DataSourceTransactionManager(datasource);
    }

    @Bean(name = "transacDM")
    @Autowired
    DataSourceTransactionManager tm2(@Qualifier("dmDb") DataSource datasource) {
        return new DataSourceTransactionManager(datasource);
    }

    @Bean(name="transacPEA")
    @Autowired
    DataSourceTransactionManager tm3(@Qualifier("peaDb") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }
}