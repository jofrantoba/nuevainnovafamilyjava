package com.innova.nif.api.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.innova.nif.api.ses.factories.EmailGeneratorFactory;
import com.innova.nif.models.MensajeAdjunto;
import com.innova.nif.models.PeriodoLectivo;
import com.innova.nif.models.Persona;
import com.innova.nif.models.Usuario;
import com.innova.nif.services.interfaces.IAccountService;
import com.innova.nif.services.interfaces.IUsuarioService;
import com.innova.nif.services.interfaces.IEmailService;
import com.innova.nif.services.interfaces.IPeriodoService;
import com.innova.nif.utils.*;
import javassist.NotFoundException;
import org.joda.time.DateTime;
import org.springframework.web.bind.annotation.*;

import java.io.InvalidObjectException;
import java.text.ParseException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/Account")
public class AccountController extends BaseApiController {

    private final IAccountService service;
    private final IUsuarioService usuarioService;
    private final IEmailService correoService;
    private final IPeriodoService periodoService;

    public AccountController(IAccountService service, IEmailService correoService, IPeriodoService periodoService, IUsuarioService usuarioService) {
        super();
        this.service = service;
        this.usuarioService = usuarioService;
        this.correoService = correoService;
        this.periodoService = periodoService;
    }

    @PostMapping(value = "/ActualizarContrasena")
    public RespuestaApi actualizarContrasena(@RequestBody Persona persona) {
        logInicioMetodo("actualizarContrasena", persona);
        RespuestaApi respuesta = new RespuestaApi();
        try {
            verificarClave(persona);
            var resultado = service.updateUserPassword(persona);
            ObjectMapper mapper = new ObjectMapper();
            respuesta.setStatus(resultado ? Constantes.OK : Constantes.ERROR);
            respuesta.setDataJson(mapper.writeValueAsString(resultado));
        } catch (Exception ex) {
            asignarExcepciones(ex, respuesta);
        }
        logFinalMetodo("actualizarContrasena");
        return respuesta;
    }

    @GetMapping(value = "/ObtenerContasenia")
    public RespuestaApi obtenerContasenia(int idPersona, String perfilUsuario) {
        logInicioMetodo("obtenerContasenia",
                String.format("(idPersona=%s, perfilUsuario=%s)", idPersona, perfilUsuario));
        RespuestaApi respuesta = new RespuestaApi();
        try {
            ObjectMapper mapper = new ObjectMapper();
            respuesta.setDataJson(mapper.writeValueAsString(service.obtenerUsuarioRecuperarContrasena(idPersona, perfilUsuario)));
        } catch (Exception ex) {
            asignarExcepciones(ex, respuesta);
        }
        logFinalMetodo("obtenerContasenia");
        return respuesta;
    }

    @GetMapping(value = "/ConsultarFotoPerfil")
    public RespuestaApi consultarFotoPerfil(int idPersona, String idPerfil) {
        RespuestaApi respuesta = new RespuestaApi();
        logInicioMetodo("consultarFotoPerfil",
                String.format("(idPersona=%s, idPerfil=%s)", idPersona, idPerfil));
        try {
            ObjectMapper mapper = new ObjectMapper();
            MensajeAdjunto adjunto = service.consultarFotoPerfil(idPersona, idPerfil);
            respuesta.setDataJson(mapper.writeValueAsString(adjunto));
        } catch (Exception ex) {
            asignarExcepciones(ex, respuesta);
        }
        logFinalMetodo("consultarFotoPerfil");
        return respuesta;
    }

    @PostMapping(value = "/ActualizarContrasenaConHash")
    public RespuestaApi actualizarContrasenaConHash(@RequestBody Persona persona) {
        logInicioMetodo("actualizarContrasenaConHash", persona);
        var respuesta = new RespuestaApi();
        try {
            var resultado = usuarioService.actualizarContrasenaConHash(persona);
            respuesta.setStatus(resultado ? Constantes.OK : Constantes.ERROR);
            ObjectMapper mapper = new ObjectMapper();
            respuesta.setDataJson(mapper.writeValueAsString(resultado));
        } catch (Exception ex) {
            asignarExcepciones(ex, respuesta);
        }
        logFinalMetodo("actualizarContrasenaConHash");
        return respuesta;
    }

    @GetMapping(value = "/GetUsuarioRecuperarContrasenaHash")
    public RespuestaApi getUsuarioRecuperarContrasenaHash(String hashCode) {
        logInicioMetodo("getUsuarioRecuperarContrasenaHash", String.format("(hashCode=%s)", hashCode));
        var respuesta = new RespuestaApi();
        try {
            ObjectMapper mapper = new ObjectMapper();
            respuesta.setDataJson(mapper.writeValueAsString(usuarioService.obtenerUsuarioPorContrasenaHash(hashCode)));
            respuesta.setStatus(Constantes.OK);
        } catch (Exception ex) {
            asignarExcepciones(ex, respuesta);
        }
        logFinalMetodo("getUsuarioRecuperarContrasenaHash");
        return respuesta;
    }

    @PostMapping(value = "/SolicitarCambioPassword")
    public RespuestaApi solicitarCambioPassword(@RequestBody String email) {
        logInicioMetodo("solicitarCambioPassword", String.format("(email=%s)", email));
        var respuesta = new RespuestaApi();
        try {
            validarEmail(email);
            Usuario usuarioDm = obtenerUsuarioDmPorPeriodo(email);
            Usuario usuarioNif = usuarioService.obtenerContrasenia(usuarioDm.getIdPersona(), usuarioDm.getGrupo());
            if (usuarioHaSolicitadoRecuperarRecientemente(usuarioNif))
                throw new InvalidObjectException("Intente nuevamente en unos minutos.");
            usuarioDm.setRecoverPasswordHash(Util.generateUUID());
            var result = service.procesarSolicitudDeCambioPassword(usuarioDm);
            if (result)
                correoService.enviarNotificacion(usuarioDm, "Innova Family - Cambio de contraseña", EmailGeneratorFactory.RESTAURAR_CONTRASENA);
        } catch (Exception ex) {
            asignarExcepciones(ex, respuesta);
        }
        logFinalMetodo("solicitarCambioPassword");
        return respuesta;
    }

    @GetMapping(value = "/VerificarEmailExiste")
    public RespuestaApi verificarEmailExiste(String correo) {
        logInicioMetodo("verificarEmailExiste", String.format("(correo=%s)", correo));
        RespuestaApi respuesta = new RespuestaApi();
        try {
            var resultado = usuarioService.correoExiste(correo);
            respuesta.setStatus(resultado ? Constantes.OK : Constantes.ERROR);
        } catch (Exception ex) {
            asignarExcepciones(ex, respuesta);
        }
        logFinalMetodo("verificarEmailExiste");
        return respuesta;
    }

    @PostMapping(value = "/ActualizarCorreoElectronico")
    public RespuestaApi actualizarCorreoElectronico(@RequestBody Usuario usuario) {
        logInicioMetodo("actualizarCorreoElectronico", usuario);
        RespuestaApi respuesta = new RespuestaApi();
        try {
            usuario.setRecoverPasswordHash(Util.generateUUID());
            var resultado = usuarioService.procesarActualizarCorreo(usuario);
            if (resultado) {
                usuario.setCorreo(usuario.getNuevoEmail());
                correoService.enviarNotificacion(usuario, "Innova Family - Cambio de correo",
                        EmailGeneratorFactory.CONFIRMAR_CAMBIO_CONTRASENA);
                respuesta.setStatus(Constantes.OK);
            }
        } catch (Exception ex) {
            asignarExcepciones(ex, respuesta);
        }
        logFinalMetodo("actualizarCorreoElectronico");
        return respuesta;
    }

    @GetMapping(value = "/GetUsuarioIdActualizarCorreoHash")
    public RespuestaApi getUsuarioIdActualizarCorreoHash(String hashCode) {
        logInicioMetodo("getUsuarioIdActualizarCorreoHash", String.format("(hashCode=%s)", hashCode));
        RespuestaApi respuesta = new RespuestaApi();
        try {
            ObjectMapper mapper = new ObjectMapper();
            respuesta.setDataJson(mapper.writeValueAsString(usuarioService.obtenerUsuarioCambioContrasenaPorHash(hashCode)));
            respuesta.setStatus(Constantes.OK);
        } catch (Exception ex) {
            asignarExcepciones(ex, respuesta);
        }
        logFinalMetodo("getUsuarioIdActualizarCorreoHash");
        return respuesta;
    }

    @PostMapping(value = "/InsertarFotoPerfil")
    public RespuestaApi insertarFotoPerfil(@RequestBody Persona persona) {
        logInicioMetodo("insertarFotoPerfil", persona);
        var respuesta = new RespuestaApi();
        try {
            boolean resultado = procesarInsertarFoto(persona);
            respuesta.setStatus(resultado ? Constantes.OK : Constantes.ERROR);
            ObjectMapper mapper = new ObjectMapper();
            respuesta.setDataJson(mapper.writeValueAsString(resultado));
        } catch (Exception ex) {
            asignarExcepciones(ex, respuesta);
        }
        logFinalMetodo("insertarFotoPerfil");
        return respuesta;
    }

    @PostMapping(value = "/ActualizarUsuarioNotificacion")
    public RespuestaApi actualizarUsuarioNotificacion(@RequestBody Usuario usuario) {
        logInicioMetodo("actualizarUsuarioNotificacion", usuario);
        var respuesta = new RespuestaApi();
        try {
            usuarioService.actualizarEstadoSinNotificacion(usuario);
            usuarioService.actualizarUsuarioNotificacion(usuario);
            ObjectMapper mapper = new ObjectMapper();
            respuesta.setDataJson(mapper.writeValueAsString(true));
            respuesta.setStatus(Constantes.OK);
        } catch (Exception ex) {
            asignarExcepciones(ex, respuesta);
        }
        logFinalMetodo("actualizarUsuarioNotificacion");
        return respuesta;
    }

    private Usuario obtenerUsuarioDmPorPeriodo(String email) throws NotFoundException, ParseException {
        var periodoActual = periodoService.getPeriodoActivo();
        var periodoPosterior = obtenerPeriodoActual(periodoService.obtenerPeriodosPF());
        return service.obtenerUsuarioDmPeriodo(email, periodoActual, periodoPosterior);
    }

    boolean usuarioHaSolicitadoRecuperarRecientemente(Usuario usuario) {
        return usuario.getRecoverPasswordDate() != null && usuario.getRecoverPasswordDate().plusMinutes(5).isAfter(DateTime.now());
    }

    private void validarEmail(String email) {
        if (email == null || email.isEmpty() || email.isBlank())
            throw new NullPointerException("Debe ingresar correo electrónico");
    }

    private boolean procesarInsertarFoto(Persona persona) {
        boolean resultado;
        int idFoto = usuarioService.insertarAdjunto(persona.getFotoPerfil());
        persona.setIdFoto(idFoto);
        resultado = usuarioService.actualizarFoto(persona);
        return resultado;
    }

    private void verificarClave(Persona persona) {
        if (persona.getClaveNueva() == null || persona.getClaveNueva().isBlank() || persona.getClaveNueva().isEmpty())
            throw new NullPointerException("Ingrese la nueva contraseña.");
        if (persona.getClaveNueva().equals(persona.getClaveAnterior()))
            throw new NullPointerException("Ingrese una contraseña diferente a la anterior.");
    }

    private PeriodoLectivo obtenerPeriodoActual(List<PeriodoLectivo> periodos) {
        Optional<PeriodoLectivo> periodo = periodos.stream().filter(f -> f.getIdPeriodo() > 0 &&
                f.getEstado().equals(Constantes.PERIODO_LECTIVO_ACTUAL)).findFirst();
        return periodo.orElse(null);
    }
}
