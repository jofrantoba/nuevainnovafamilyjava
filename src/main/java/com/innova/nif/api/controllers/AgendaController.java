package com.innova.nif.api.controllers;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.innova.nif.models.*;
import com.innova.nif.models.wrappers.AgendaFiltro;
import com.innova.nif.services.async.RegistroAlertaAgendaAsync;
import com.innova.nif.services.interfaces.IAgendaService;
import com.innova.nif.utils.Constantes;
import com.innova.nif.utils.RespuestaApi;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/Agenda")
public class AgendaController extends BaseApiController {
    private final IAgendaService service;
    private final RegistroAlertaAgendaAsync registroAlertaAgenda;

    public AgendaController(IAgendaService service, RegistroAlertaAgendaAsync registroAlertaAgenda) {
        this.service = service;
        this.registroAlertaAgenda = registroAlertaAgenda;
    }

    @GetMapping("/ListaAgenda")
    public RespuestaApi listaAgenda(String jsonParametro) {
        logInicioMetodo("listaAgenda", jsonParametro);
        RespuestaApi respuesta = new RespuestaApi();
        try {
            ObjectMapper mapper = jsonStringMapper();
            AgendaFiltro filtros = cargarFiltros(mapper, jsonParametro);
            List<Calendario> calendario = service.listaAgenda(filtros);
            respuesta.setDataJson(mapper.writeValueAsString(calendario));
            respuesta.setStatus(Constantes.OK);
        } catch (Exception ex) {
            asignarExcepciones(ex, respuesta);
        }
        logFinalMetodo("listaAgenda");
        return respuesta;
    }

    @GetMapping("/ObtenerEventoEDDC")
    public RespuestaApi obtenerEventoEDDC(String jsonParametro) {
        logInicioMetodo("obtenerEventoEDDC", jsonParametro);
        RespuestaApi respuesta = new RespuestaApi();
        try {
            ObjectMapper mapper = jsonStringMapper();
            AgendaFiltro filtros = cargarFiltros(mapper, jsonParametro);
            EventoGeneral calendario = service.obtenerEventoEDDC(filtros);
            respuesta.setDataJson(mapper.writeValueAsString(calendario));
            respuesta.setStatus(Constantes.OK);
        } catch (Exception ex) {
            asignarExcepciones(ex, respuesta);
        }
        logFinalMetodo("obtenerEventoEDDC");
        return respuesta;
    }

    @GetMapping("/ObtenerTarea")
    public RespuestaApi obtenerTarea(String jsonParametro) {
        logInicioMetodo("obtenerTarea", jsonParametro);
        RespuestaApi respuesta = new RespuestaApi();
        try {
            ObjectMapper mapper = jsonStringMapper();
            AgendaFiltro filtros = cargarFiltros(mapper, jsonParametro);
            TareaGeneral calendario = service.obtenerTarea(filtros);
            respuesta.setDataJson(mapper.writeValueAsString(calendario));
            respuesta.setStatus(Constantes.OK);
        } catch (Exception ex) {
            asignarExcepciones(ex, respuesta);
        }
        logFinalMetodo("obtenerTarea");
        return respuesta;
    }

    @GetMapping("/ObtenerEventoALPF")
    public RespuestaApi obtenerEventoALPF(String jsonParametro) {
        logInicioMetodo("obtenerEventoALPF", jsonParametro);
        RespuestaApi respuesta = new RespuestaApi();
        try {
            ObjectMapper mapper = jsonStringMapper();
            AgendaFiltro filtros = cargarFiltros(mapper, jsonParametro);
            EventoGeneral calendario = service.obtenerEventoALPF(filtros);
            respuesta.setDataJson(mapper.writeValueAsString(calendario));
            respuesta.setStatus(Constantes.OK);
        } catch (Exception ex) {
            asignarExcepciones(ex, respuesta);
        }
        logFinalMetodo("obtenerEventoALPF");
        return respuesta;
    }

    @PostMapping("/RegistrarAgenda")
    public RespuestaApi registrarAgenda(@RequestBody AgendaCalendario agenda) {
        logInicioMetodo("registrarAgenda", agenda);
        RespuestaApi respuestaApi = new RespuestaApi();
        try {
            int resultado = service.registrarAgenda(agenda);
            registroAlertaAgenda.ejecutar(agenda);
            ObjectMapper mapper = new ObjectMapper();
            respuestaApi.setDataJson(mapper.writeValueAsString(resultado));
            respuestaApi.setStatus(Constantes.OK);
        } catch (Exception ex) {
            asignarExcepciones(ex, respuestaApi);
        }
        logFinalMetodo("registrarAgenda");
        return respuestaApi;
    }

    @PostMapping("/ActualizarEvento")
    public RespuestaApi actualizarEvento(@RequestBody AgendaCalendario agenda) {
        logInicioMetodo("actualizarEvento", agenda);
        RespuestaApi respuestaApi = new RespuestaApi();
        try {
            LogAgenda resultado = service.editarEvento(agenda);
            registroAlertaAgenda.ejecutar(agenda);
            ObjectMapper mapper = new ObjectMapper();
            respuestaApi.setDataJson(mapper.writeValueAsString(resultado));
            respuestaApi.setStatus(Constantes.OK);
        } catch (Exception ex) {
            asignarExcepciones(ex, respuestaApi);
        }
        logFinalMetodo("actualizarEvento");
        return respuestaApi;
    }

    @PostMapping("/ActualizarAgenda")
    public RespuestaApi actualizarAgenda(@RequestBody AgendaCalendario agenda) {
        logInicioMetodo("actualizarAgenda", agenda);
        RespuestaApi respuestaApi = new RespuestaApi();
        try {
            int resultado = service.editarTarea(agenda);
            registroAlertaAgenda.ejecutar(agenda);
            ObjectMapper mapper = new ObjectMapper();
            respuestaApi.setDataJson(mapper.writeValueAsString(resultado));
            respuestaApi.setStatus(Constantes.OK);
        } catch (Exception ex) {
            asignarExcepciones(ex, respuestaApi);
        }
        logFinalMetodo("actualizarAgenda");
        return respuestaApi;
    }

    @GetMapping("/EliminarEvento")
    public RespuestaApi eliminarEvento(int eventId, int personaId) {
        logInicioMetodo("eliminarEvento", String.format("(eventId=%s,personaId=%s)", eventId, personaId));
        RespuestaApi respuestaApi = new RespuestaApi();
        try {
            LogAgenda logEvento = service.eliminarEvento(eventId, personaId);
            ObjectMapper mapper = new ObjectMapper();
            respuestaApi.setDataJson(mapper.writeValueAsString(logEvento));
        } catch (Exception ex) {
            asignarExcepciones(ex, respuestaApi);
        }
        logFinalMetodo("eliminarEvento");
        return respuestaApi;
    }

    @PostMapping("/EliminarAgenda")
    public RespuestaApi eliminarAgenda(@RequestBody AgendaCalendario agenda) {
        logInicioMetodo("eliminarAgenda", agenda);
        RespuestaApi respuestaApi = new RespuestaApi();
        try {
            int resultado = service.eliminarAgenda(agenda.getTarea().getIdTarea());
            ObjectMapper mapper = new ObjectMapper();
            respuestaApi.setDataJson(mapper.writeValueAsString(resultado));
        } catch (Exception ex) {
            asignarExcepciones(ex, respuestaApi);
        }
        logFinalMetodo("eliminarAgenda");
        return respuestaApi;
    }

    @PostMapping("/RegistrarLogTarea")
    public RespuestaApi registrarLogTarea(@RequestBody LogAgenda logTarea) {
        logInicioMetodo("registrarLogTarea", logTarea);
        RespuestaApi respuestaApi = new RespuestaApi();
        try {
            logTarea.setTipo(Constantes.TIPO_AGENDA_TAREA);
            LogAgenda resultado = service.registrarLogTarea(logTarea);
            ObjectMapper mapper = new ObjectMapper();
            respuestaApi.setDataJson(mapper.writeValueAsString(resultado));
        } catch (Exception ex) {
            asignarExcepciones(ex, respuestaApi);
        }
        logFinalMetodo("registrarLogTarea");
        return respuestaApi;
    }

    @PostMapping("/EliminarArchivo")
    public RespuestaApi eliminarArchivo(@RequestBody AgendaAdjunto adjunto) {
        logInicioMetodo("eliminarArchivo", adjunto);
        RespuestaApi respuestaApi = new RespuestaApi();
        try {
            int resultado = service.eliminarAdjunto(adjunto);
            ObjectMapper mapper = new ObjectMapper();
            respuestaApi.setDataJson(mapper.writeValueAsString(resultado));
        } catch (Exception ex) {
            asignarExcepciones(ex, respuestaApi);
        }
        logFinalMetodo("eliminarArchivo");
        return respuestaApi;
    }

    private ObjectMapper jsonStringMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        return mapper;
    }

    private AgendaFiltro cargarFiltros(ObjectMapper mapper, String jsonParametro) throws IOException {
        return mapper.readValue(jsonParametro, AgendaFiltro.class);
    }
}