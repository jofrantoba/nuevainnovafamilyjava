package com.innova.nif.api.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.innova.nif.services.interfaces.IApoderadoService;
import com.innova.nif.utils.RespuestaApi;
import com.innova.nif.utils.Constantes;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/Apoderado")
public class ApoderadoController extends BaseApiController {
    private final IApoderadoService service;

    public ApoderadoController(IApoderadoService service) {
        this.service = service;
    }

    @GetMapping("/ListaAlumnoPadre")
    public RespuestaApi listaAlumnoPadre(int idPersonaPadre, int periodo) {
        var respuesta = new RespuestaApi();
        try {
            var resultado = service.obtenerHijos(idPersonaPadre, periodo);
            respuesta.setStatus(Constantes.OK);
            ObjectMapper mapper = new ObjectMapper();
            respuesta.setDataJson(mapper.writeValueAsString(resultado));
        } catch (Exception ex) {
            asignarExcepciones(ex, respuesta);
        }
        return respuesta;
    }
}
