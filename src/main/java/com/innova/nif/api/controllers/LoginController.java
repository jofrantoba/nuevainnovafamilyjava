package com.innova.nif.api.controllers;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.innova.nif.models.Usuario;
import com.innova.nif.models.wrappers.InicioSesionWrapper;
import com.innova.nif.models.wrappers.UsuarioSesionWrapper;
import com.innova.nif.services.async.ActualizarInicioSesionAsync;
import com.innova.nif.services.interfaces.ILoginService;
import com.innova.nif.utils.RespuestaApi;
import com.innova.nif.utils.Constantes;
import com.innova.nif.utils.ErrorManager;
import javassist.NotFoundException;
import org.apache.logging.log4j.util.Strings;
import org.springframework.web.bind.annotation.*;

import java.security.InvalidParameterException;

@RestController
@RequestMapping("/Login")
public class LoginController extends BaseApiController {
    private final ILoginService service;
    private final ActualizarInicioSesionAsync actualizarInicioSesion;

    public LoginController(ILoginService service, ActualizarInicioSesionAsync actualizarInicioSesion) {
        this.service = service;
        this.actualizarInicioSesion = actualizarInicioSesion;
    }

    @GetMapping(value = "/ObtenerUsuario")
    public RespuestaApi obtenerUsuario(String correo, int periodo, String tipo) {
        logInicioMetodo("consultarFotoPerfil",
                String.format("(correo=%s,periodo=%s,tipo=%s)", correo, periodo, tipo));
        RespuestaApi respuesta = new RespuestaApi();
        try {
            var result = service.obtenerUsuarioDm(correo, periodo, tipo);
            ObjectMapper mapper = new ObjectMapper();
            respuesta.setDataJson(mapper.writeValueAsString(result));
        } catch (Exception ex) {
            respuesta.setMensaje(ex.getMessage());
            respuesta.setStackTrace(ErrorManager.getStringStackTrace(ex));
            respuesta.setStatus(Constantes.ERROR);
        }
        logFinalMetodo("consultarFotoPerfil");
        return respuesta;
    }

    @PostMapping(value = "/LoginPF")
    public RespuestaApi loginPF(@RequestBody InicioSesionWrapper wrapper) {
        logInicioMetodo("loginPF", wrapper);
        RespuestaApi respuestaApi = new RespuestaApi();
        try {
            Usuario usuarioDM = service.obtenerUsuarioPFDM(wrapper.getCorreo());
            if (usuarioDM == null)
                throw new NotFoundException("No encontramos ninguna cuenta asociada a este correo. Por favor comunícate con tu sede.");
            Usuario usuarioNIF = service.obtenerUsuarioPFNIF(usuarioDM);
            if (usuarioNIF == null)
                throw new NotFoundException("No se encontraron datos en el servicio NIF.");
            if (!claveEsCorrecta(wrapper.getPassword(), usuarioDM, usuarioNIF))
                throw new InvalidParameterException("Su cuenta o contraseña es incorrecta.");
            actualizarInicioSesion.ejecutar(usuarioDM);
            ObjectMapper mapper = new ObjectMapper();
            UsuarioSesionWrapper usuarioSesionWrapper = new UsuarioSesionWrapper();
            usuarioSesionWrapper.setUsuarioDM(usuarioDM);
            usuarioSesionWrapper.setUsuarioNIF(usuarioNIF);
            respuestaApi.setDataJson(mapper.writeValueAsString(usuarioSesionWrapper));
        } catch (Exception ex) {
            asignarExcepciones(ex, respuestaApi);
        }
        logFinalMetodo("loginPF");
        return respuestaApi;
    }

    @PostMapping("/LoginGeneral")
    public RespuestaApi loginGeneral(@RequestBody InicioSesionWrapper wrapper) {
        logInicioMetodo("loginGeneral", wrapper);
        RespuestaApi respuestaApi = new RespuestaApi();
        try {
            Usuario usuarioDM = service.obtenerUsuarioDm(wrapper.getCorreo());
            actualizarInicioSesion.ejecutar(usuarioDM);
            ObjectMapper mapper = new ObjectMapper();
            respuestaApi.setDataJson(mapper.writeValueAsString(usuarioDM));
        } catch (Exception ex) {
            asignarExcepciones(ex, respuestaApi);
        }
        logFinalMetodo("loginGeneral");
        return respuestaApi;
    }

    private boolean claveEsCorrecta(String clave, Usuario usuarioDM, Usuario usuarioNIF) {
        boolean claveEsDNI = usuarioNIF.getClave().equals(Strings.EMPTY);
        return claveEsDNI ? usuarioDM.getDni().equals(clave) : usuarioNIF.getClave().equals(clave);
    }
}
