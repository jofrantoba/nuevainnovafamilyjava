package com.innova.nif.api.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.innova.nif.models.MensajeApp;
import com.innova.nif.models.MensajeDestino;
import com.innova.nif.models.wrappers.MensajeFiltro;
import com.innova.nif.models.wrappers.MensajeWrapper;
import com.innova.nif.services.interfaces.IMensajeService;
import com.innova.nif.utils.Constantes;
import com.innova.nif.utils.RespuestaApi;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Mensaje")
public class MensajeController extends BaseApiController {
    private final IMensajeService service;

    public MensajeController(IMensajeService service) {
        this.service = service;
    }

    @GetMapping("/ConsultarTotalMensajes")
    public RespuestaApi consultarTotalMensajes(int idPersona, String grupo) {
        logInicioMetodo("consultarTotalMensajes", String.format("(idPersona=%s,grupo=%s)", idPersona, grupo));
        RespuestaApi respuesta = new RespuestaApi();
        try {
            ObjectMapper mapper = new ObjectMapper();
            var resultado = service.consultarTotalMensajes(idPersona, grupo);
            respuesta.setDataJson(mapper.writeValueAsString(resultado));
            respuesta.setStatus(Constantes.OK);
        } catch (Exception ex) {
            asignarExcepciones(ex, respuesta);
        }
        logFinalMetodo("consultarTotalMensajes");
        return respuesta;
    }

    @PostMapping("/ConsultarMensajePorPagina")
    public RespuestaApi consultarMensajePorPagina(@RequestBody MensajeFiltro mensajeFiltro) {
        logInicioMetodo("consultarMensajePorPagina", mensajeFiltro);
        RespuestaApi respuesta = new RespuestaApi();
        try {
            var resultado = service.consultarMensajesPaginado(mensajeFiltro);
            ObjectMapper mapper = new ObjectMapper();
            respuesta.setDataJson(mapper.writeValueAsString(resultado));
            respuesta.setStatus(Constantes.OK);
        } catch (Exception ex) {
            asignarExcepciones(ex, respuesta);
        }
        logFinalMetodo("consultarMensajePorPagina");
        return respuesta;
    }

    @PostMapping("/ConsultarDetalleMensaje")
    public RespuestaApi consultarDetalleMensaje(@RequestBody MensajeFiltro mensajeFiltro) {
        logInicioMetodo("consultarDetalleMensaje", mensajeFiltro);
        RespuestaApi respuesta = new RespuestaApi();
        try {
            List resultado = service.detalleMensaje(mensajeFiltro.getIdMensaje());
            ObjectMapper mapper = new ObjectMapper();
            respuesta.setDataJson(mapper.writeValueAsString(resultado));
            respuesta.setStatus(Constantes.OK);
        } catch (Exception ex) {
            asignarExcepciones(ex, respuesta);
        }
        logFinalMetodo("consultarDetalleMensaje");
        return respuesta;
    }

    @GetMapping("/ConsultarAdjuntos/{idCorreo}")
    public RespuestaApi consultarAdjuntos(@PathVariable int idCorreo) {
        logInicioMetodo("consultarAdjuntos", String.format("(idMensaje=%s)", idCorreo));
        RespuestaApi respuesta = new RespuestaApi();
        try {
            List resultado = service.consultarAdjuntos(idCorreo);
            ObjectMapper mapper = new ObjectMapper();
            respuesta.setDataJson(mapper.writeValueAsString(resultado));
            respuesta.setStatus(Constantes.OK);
        } catch (Exception ex) {
            asignarExcepciones(ex, respuesta);
        }
        logFinalMetodo("consultarAdjuntos");
        return respuesta;
    }

    @GetMapping("/ConsultarHistorial")
    public RespuestaApi consultarHistorial(int idCorreo, int idPersona) {
        logInicioMetodo("consultarHistorial", String.format("(idCorreo=%s,idPersona=%s)", idCorreo, idPersona));
        RespuestaApi respuesta = new RespuestaApi();
        try {
            List resultado = service.consultarHistorial(idCorreo, idPersona);
            ObjectMapper mapper = new ObjectMapper();
            respuesta.setDataJson(mapper.writeValueAsString(resultado));
            respuesta.setStatus(Constantes.OK);
        } catch (Exception ex) {
            asignarExcepciones(ex, respuesta);
        }
        logFinalMetodo("consultarHistorial");
        return respuesta;
    }

    @GetMapping("/ActualizarMensaje")
    public RespuestaApi actualizarMensaje(MensajeWrapper mensajeWrapper) {
        logInicioMetodo("actualizarMensaje", mensajeWrapper);
        RespuestaApi respuesta = new RespuestaApi();
        try {
            int resultado = service.actualizarMensaje(mensajeWrapper);
            ObjectMapper mapper = new ObjectMapper();
            respuesta.setDataJson(mapper.writeValueAsString(resultado));
            respuesta.setStatus(Constantes.OK);
        } catch (Exception ex) {
            asignarExcepciones(ex, respuesta);
        }
        logFinalMetodo("actualizarMensaje");
        return respuesta;
    }

    @GetMapping("/EliminarMensaje")
    public RespuestaApi eliminarMensaje(MensajeWrapper mensajeWrapper) {
        logInicioMetodo("eliminarMensaje", mensajeWrapper);
        RespuestaApi respuesta = new RespuestaApi();
        try {
            int resultado = service.eliminarMensaje(mensajeWrapper);
            ObjectMapper mapper = new ObjectMapper();
            respuesta.setDataJson(mapper.writeValueAsString(resultado));
            respuesta.setStatus(Constantes.OK);
        } catch (Exception ex) {
            asignarExcepciones(ex, respuesta);
        }
        logFinalMetodo("idMensajeDestino");
        return respuesta;
    }

    @GetMapping("/ObtenerMensajeDestino")
    public RespuestaApi obtenerMensajeDestino(int idMensajeDestino) {
        logInicioMetodo("obtenerMensajeDestino", String.format("(idMensajeDestino=%s)", idMensajeDestino));
        RespuestaApi respuesta = new RespuestaApi();
        try {
            MensajeDestino mensajeDestino = service.obtenerMensajeDestino(idMensajeDestino);
            ObjectMapper mapper = new ObjectMapper();
            respuesta.setDataJson(mapper.writeValueAsString(mensajeDestino));
            respuesta.setStatus(Constantes.OK);
        } catch (Exception ex) {
            asignarExcepciones(ex, respuesta);
        }
        logFinalMetodo("obtenerMensajeDestino");
        return respuesta;
    }

    @PostMapping("/InsertarMensaje")
    public RespuestaApi insertarMensaje(@RequestBody MensajeApp mensajeApp) {
        logInicioMetodo("insertarMensaje", mensajeApp);
        RespuestaApi respuestaApi = new RespuestaApi();
        try {
            int respuesta = service.insertarMensaje(mensajeApp);
            ObjectMapper mapper = new ObjectMapper();
            respuestaApi.setDataJson(mapper.writeValueAsString(respuesta));
            respuestaApi.setData(mapper.writeValueAsString(respuesta));
        } catch (Exception ex) {
            asignarExcepciones(ex, respuestaApi);
        }
        logFinalMetodo("insertarMensaje");
        return respuestaApi;
    }
}
