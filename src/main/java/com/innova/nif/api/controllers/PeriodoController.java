package com.innova.nif.api.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.innova.nif.services.interfaces.IPeriodoService;
import com.innova.nif.utils.RespuestaApi;
import com.innova.nif.utils.ErrorManager;
import com.innova.nif.utils.Constantes;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/Periodo")
public class PeriodoController {
    private IPeriodoService periodoService;

    public PeriodoController(IPeriodoService periodoService) {
        this.periodoService = periodoService;
    }

    @GetMapping(value = "/GetPeriodoActivo")
    public RespuestaApi getPeriodoActivo() {
        RespuestaApi respuesta = new RespuestaApi();
        ObjectMapper mapper = new ObjectMapper();

        try {
            var result = periodoService.getPeriodoActivo();
            respuesta.setDataJson(mapper.writeValueAsString(result));
        } catch (Exception ex) {
            respuesta.setMensaje(ex.getMessage());
            respuesta.setStackTrace(ErrorManager.getStringStackTrace(ex));
            respuesta.setStatus(Constantes.ERROR);
        }

        return respuesta;
    }
}
