package com.innova.nif.api.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.innova.nif.models.BackOfficeParametros;
import com.innova.nif.services.interfaces.IBackOfficeService;
import com.innova.nif.utils.RespuestaApi;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/BackOffice")
public class BackOfficeController extends BaseApiController {
    private final IBackOfficeService service;

    public BackOfficeController(IBackOfficeService service) {
        this.service = service;
    }

    @PostMapping("/ObtenerContador")
    public RespuestaApi obtenerContador(@RequestBody BackOfficeParametros filtros) {
        logInicioMetodo("obtenerContador", filtros);
        RespuestaApi respuestaApi = new RespuestaApi();
        try {
            int contador = service.obtenerContador(filtros);
            ObjectMapper mapper = new ObjectMapper();
            respuestaApi.setMensaje(mapper.writeValueAsString(contador));
        } catch (Exception ex) {
            asignarExcepciones(ex, respuestaApi);
        }
        logFinalMetodo("obtenerContador");
        return respuestaApi;
    }

    @PostMapping("/ObtenerConteoFiltros")
    public RespuestaApi obtenerConteoFiltros(@RequestBody BackOfficeParametros filtros) {
        logInicioMetodo("obtenerConteoFiltros", filtros);
        RespuestaApi respuestaApi = new RespuestaApi();
        try {
            int contador = service.obtenerConteoFiltros(filtros);
            ObjectMapper mapper = new ObjectMapper();
            respuestaApi.setMensaje(mapper.writeValueAsString(contador));
        } catch (Exception ex) {
            asignarExcepciones(ex, respuestaApi);
        }
        logFinalMetodo("obtenerConteoFiltros");
        return respuestaApi;
    }
}
