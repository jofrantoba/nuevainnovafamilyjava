package com.innova.nif.api.controllers;

import com.innova.nif.utils.RespuestaApi;
import com.innova.nif.utils.ErrorManager;
import com.innova.nif.utils.Constantes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BaseApiController {
    protected final Logger logger;

    public BaseApiController() {
        logger = LoggerFactory.getLogger(this.getClass());
    }

    void asignarExcepciones(Exception ex, RespuestaApi respuesta) {
        respuesta.setMensaje(ex.getMessage());
        respuesta.setStackTrace(ErrorManager.getStringStackTrace(ex));
        respuesta.setStatus(Constantes.ERROR);
        registrarExepcion(ex, respuesta);
    }

    private void registrarExepcion(Exception ex, RespuestaApi respuestaApi) {
        logger.error(respuestaApi.getDataJson(), ex);
    }

    void logInicioMetodo(String metodo, Object object) {
        String mensaje = String.format("Iniciando el método: %s.%s con los parámetros %s",
                this.getClass(), metodo, object);
        logger.info(mensaje);
    }

    void logFinalMetodo(String metodo) {
        String mensaje = String.format("Fin del método: %s.%s", this.getClass(), metodo);
        logger.info(mensaje);
    }
}
