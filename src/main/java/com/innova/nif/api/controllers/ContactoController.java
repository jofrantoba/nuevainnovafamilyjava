package com.innova.nif.api.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.innova.nif.models.*;
import com.innova.nif.models.wrappers.FiltroWrapper;
import com.innova.nif.services.interfaces.IContactoService;
import com.innova.nif.utils.Constantes;
import com.innova.nif.utils.RespuestaApi;
import com.innova.nif.utils.StringHelper;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Contacto")
public class ContactoController extends BaseApiController {
    private final IContactoService service;

    public ContactoController(IContactoService service) {
        this.service = service;
    }

    @GetMapping("/ListaCorreoRemitente")
    public RespuestaApi listaCorreoRemitente(int idMensaje) {
        logInicioMetodo("listaCorreoRemitente", String.format("(idMensaje=%s)", idMensaje));
        RespuestaApi respuesta = new RespuestaApi();
        try {
            List<MensajeRemitente> resultado = service.listaCorreoRemitente(idMensaje);
            ObjectMapper mapper = new ObjectMapper();
            respuesta.setDataJson(mapper.writeValueAsString(resultado));
            respuesta.setStatus(Constantes.OK);
        } catch (Exception ex) {
            asignarExcepciones(ex, respuesta);
        }
        logFinalMetodo("listaCorreoRemitente");
        return respuesta;
    }

    @PostMapping("/EquipoDirectivo")
    public RespuestaApi equipoDirectivo(@RequestBody FiltroWrapper filtros) {
        logInicioMetodo("equipoDirectivo", filtros);
        RespuestaApi respuestaApi = new RespuestaApi();
        try {
            List<Contacto> contactos = service.equipoDirectivo(filtros);
            ObjectMapper mapper = new ObjectMapper();
            respuestaApi.setDataJson(mapper.writeValueAsString(contactos));
        } catch (Exception ex) {
            asignarExcepciones(ex, respuestaApi);
        }
        logFinalMetodo("equipoDirectivo");
        return respuestaApi;
    }

    @PostMapping("/Docentes")
    public RespuestaApi docentes(@RequestBody FiltroWrapper filtros) {
        logInicioMetodo("docentes", filtros);
        RespuestaApi respuestaApi = new RespuestaApi();
        try {
            List<Docente> docentes = service.docentes(filtros);
            ObjectMapper mapper = new ObjectMapper();
            respuestaApi.setDataJson(mapper.writeValueAsString(docentes));
        } catch (Exception ex) {
            asignarExcepciones(ex, respuestaApi);
        }
        logFinalMetodo("docentes");
        return respuestaApi;
    }

    @GetMapping("/ListaGrado")
    public RespuestaApi listaGrado() {
        logInicioMetodo("listaGrado", "");
        RespuestaApi respuestaApi = new RespuestaApi();
        try {
            List<Grado> grados = service.listaGrados();
            ObjectMapper mapper = new ObjectMapper();
            respuestaApi.setDataJson(mapper.writeValueAsString(grados));
        } catch (Exception ex) {
            asignarExcepciones(ex, respuestaApi);
        }
        logFinalMetodo("listaGrado");
        return respuestaApi;
    }

    @GetMapping("/GetNivelInglesDocente")
    public RespuestaApi getNivelInglesDocente(int idSede, int anio, int idGrado, String dniDocente) {
        logInicioMetodo("getNivelInglesDocente", String.format("(idSede=%s, anio=%s, idGrado=%s, dniDocente=%s)",
                idSede, anio, idGrado, dniDocente));
        RespuestaApi respuestaApi = new RespuestaApi();
        try {
            FiltroWrapper filtros = filtroWrapper(idSede, anio, idGrado, dniDocente);
            List<DocenteNivelIngles> docentesNivelIngles = service.obtenerNivelIngles(filtros);
            ObjectMapper mapper = new ObjectMapper();
            respuestaApi.setDataJson(mapper.writeValueAsString(docentesNivelIngles));
        } catch (Exception ex) {
            asignarExcepciones(ex, respuestaApi);
        }
        logFinalMetodo("getNivelInglesDocente");
        return respuestaApi;
    }

    @GetMapping("/ListaDatosProfesorPorDni")
    public RespuestaApi listaDatosProfesorPorDni(String json) {
        logInicioMetodo("listaDatosProfesorPorDni", json);
        RespuestaApi respuestaApi = new RespuestaApi();
        try {
            List<Docente> docentes = service.docentesPorDNI(json);
            ObjectMapper mapper = new ObjectMapper();
            respuestaApi.setDataJson(mapper.writeValueAsString(docentes));
        } catch (Exception ex) {
            asignarExcepciones(ex, respuestaApi);
        }
        logFinalMetodo("listaDatosProfesorPorDni");
        return respuestaApi;
    }

    @PostMapping("/ObtenerDocentesNivelIngles")
    public RespuestaApi obtenerDocentesNivelIngles(@RequestBody FiltroWrapper filtros) {
        logInicioMetodo("obtenerDocentesNivelIngles", filtros);
        RespuestaApi respuestaApi = new RespuestaApi();
        try {
            List<Docente> docentes = service.obtenerDocentesNivelIngles(filtros);
            ObjectMapper mapper = new ObjectMapper();
            respuestaApi.setDataJson(mapper.writeValueAsString(docentes));
        } catch (Exception ex) {
            asignarExcepciones(ex, respuestaApi);
        }
        logFinalMetodo("obtenerDocentesNivelIngles");
        return respuestaApi;
    }

    @GetMapping("/ListaGradoBySedePeriodo")
    public RespuestaApi listaGradoBySedePeriodo(int idSede, int idPeriodo) {
        logInicioMetodo("listaGradoBySedePeriodo", String.format("(idSede=%s, idPeriodo=%s)", idSede, idPeriodo));
        RespuestaApi respuestaApi = new RespuestaApi();
        try {
            List<Grado> gradosPorSedePeriodo = service.listaGradosPorSedePeriodo(idSede, idPeriodo);
            ObjectMapper mapper = new ObjectMapper();
            respuestaApi.setDataJson(mapper.writeValueAsString(gradosPorSedePeriodo));
        } catch (Exception ex) {
            asignarExcepciones(ex, respuestaApi);
        }
        logFinalMetodo("listaGradoBySedePeriodo");
        return respuestaApi;
    }

    @PostMapping("/ListaSeccion")
    public RespuestaApi listaSeccion(@RequestBody FiltroWrapper filtros) {
        logInicioMetodo("listaSeccion", filtros);
        RespuestaApi respuestaApi = new RespuestaApi();
        try {
            List<SeccionAlumno> secciones = service.listarSecciones(filtros);
            ObjectMapper mapper = new ObjectMapper();
            respuestaApi.setDataJson(mapper.writeValueAsString(secciones));
        } catch (Exception ex) {
            asignarExcepciones(ex, respuestaApi);
        }
        logFinalMetodo("listaSeccion");
        return respuestaApi;
    }

    @PostMapping("/PadresFamilia")
    public RespuestaApi padresFamilia(@RequestBody FiltroWrapper filtros) {
        logInicioMetodo("padresFamilia", filtros);
        RespuestaApi respuestaApi = new RespuestaApi();
        try {
            List<Contacto> padresFamilia = service.listarPadresFamilia(filtros);
            ObjectMapper mapper = new ObjectMapper();
            respuestaApi.setDataJson(mapper.writeValueAsString(padresFamilia));
        } catch (Exception ex) {
            asignarExcepciones(ex, respuestaApi);
        }
        logFinalMetodo("padresFamilia");
        return respuestaApi;
    }

    @PostMapping("/Alumnos")
    public RespuestaApi alumnos(@RequestBody FiltroWrapper filtros) {
        logInicioMetodo("alumnos", filtros);
        RespuestaApi respuestaApi = new RespuestaApi();
        try {
            List<Contacto> alumnos = service.listarAlumnos(filtros);
            ObjectMapper mapper = new ObjectMapper();
            respuestaApi.setDataJson(mapper.writeValueAsString(alumnos));
        } catch (Exception ex) {
            asignarExcepciones(ex, respuestaApi);
        }
        logFinalMetodo("alumnos");
        return respuestaApi;
    }

    private FiltroWrapper filtroWrapper(int idSede, int anio, int idGrado, String dniDocente) {
        FiltroWrapper filtros = new FiltroWrapper();
        filtros.setIdSede(idSede);
        filtros.setAnio(anio);
        filtros.setIdGrado(idGrado);
        filtros.setDni(StringHelper.getValueOrEmpty(dniDocente));
        return filtros;
    }
}
