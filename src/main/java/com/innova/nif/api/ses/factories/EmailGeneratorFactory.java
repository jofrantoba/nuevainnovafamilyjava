package com.innova.nif.api.ses.factories;

import com.innova.nif.models.Usuario;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import java.io.StringWriter;

public class EmailGeneratorFactory {

    public static final String RESTAURAR_CONTRASENA = "RestaurarContrasena";
    public static final String CONFIRMAR_CAMBIO_CONTRASENA = "ConfirmarCambioEmail";

    public String generateEmail(String emailType, Usuario model, String urlInnova, String urlImagenCorreo) {
        VelocityContext context = new VelocityContext();

        var query = stringTemplate(emailType);
        context.put("model", model);
        context.put("baseUrl", urlInnova);
        context.put("imagenCorreo", urlImagenCorreo);

        StringWriter writer = new StringWriter();
        query.merge(context, writer);
        return writer.toString();
    }

    private static Template stringTemplate(String template) {
        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.init();

        return velocityEngine.getTemplate("src/main/resources/templates/" + template + ".vm", "UTF-8");
    }

}
