package com.innova.nif.services;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.innova.nif.models.*;
import com.innova.nif.models.wrappers.FiltroWrapper;
import com.innova.nif.models.wrappers.MensajeFiltro;
import com.innova.nif.models.wrappers.MensajeWrapper;
import com.innova.nif.repositories.interfaces.IMensajeRepository;
import com.innova.nif.repositories.utils.GrupoMensaje;
import com.innova.nif.services.interfaces.IAmazonS3Service;
import com.innova.nif.services.interfaces.IMensajeService;
import com.innova.nif.services.utils.RequestSender;
import com.innova.nif.services.utils.XMLMensaje;
import com.innova.nif.utils.Constantes;
import com.innova.nif.utils.RespuestaApi;
import javassist.NotFoundException;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class MensajeService implements IMensajeService {
    private static final String ENVIAR_MENSAJE = "1";
    private static final String RESPONDER_MENSAJE = "2";
    private final IMensajeRepository repository;
    private final IAmazonS3Service s3Service;
    @Value("${urlServiceApp}")
    private String urlServiceApp;
    @Value("${resourceMensajeApp}")
    private String resourceMensajeApp;
    private final Logger logger;

    public MensajeService(IMensajeRepository repository, IAmazonS3Service s3Service) {
        this.repository = repository;
        this.s3Service = s3Service;
        logger = LoggerFactory.getLogger(this.getClass());
    }

    @Override
    public int consultarTotalMensajes(int idPersona, String grupo) {
        return repository.consultarTotalMensajes(idPersona, grupo);
    }

    @Override
    public List<MensajePaginado> consultarMensajesPaginado(MensajeFiltro mensajeFiltro) {
        return repository.consultarMensajesPaginado(mensajeFiltro);
    }

    @Override
    public List detalleMensaje(int id) {
        return repository.detalleMensaje(id);
    }

    @Override
    public List consultarAdjuntos(int idMensaje) {
        List<MensajeAdjunto> adjuntos = (List<MensajeAdjunto>) repository.obtenerAdjuntos(idMensaje);
        adjuntos.forEach(f -> {
                    f.setUrl(s3Service.generarUrlArchivo(f.getNombreAlterado()));
                    f.setUrlView(s3Service.obtenerUrlArchivoPorTipo(f.getNombreAlterado()));
                }
        );
        return adjuntos;
    }

    @Override
    public List consultarHistorial(int idCorreo, int idPersona) {
        return repository.obtenerHistorial(idCorreo, idPersona);
    }

    @Override
    public int actualizarMensaje(MensajeWrapper mensajeWrapper) throws IOException, NotFoundException, HttpException {
        int resultado = repository.actualizarMensaje(mensajeWrapper.getId(), mensajeWrapper.getTipo(), mensajeWrapper.getIdPersona());
        enviarNotificacionesMensajePut(mensajeWrapper);
        return resultado;
    }

    @Override
    public int eliminarMensaje(MensajeWrapper mensajeWrapper) throws IOException, NotFoundException, HttpException {
        int resultado = repository.eliminarMensaje(mensajeWrapper.getId(), mensajeWrapper.getTipo());
        if (mensajeWrapper.getTipo().equals(ENVIAR_MENSAJE))
            enviarNotificacionesMensajePut(mensajeWrapper);
        else if (mensajeWrapper.getTipo().equals(RESPONDER_MENSAJE))
            enviarNotificacionesMensaje(mensajeWrapper, mensajeWrapper.getId());
        return resultado;
    }

    @Override
    public MensajeDestino obtenerMensajeDestino(int idMensajeDestino) throws NotFoundException {
        return repository.obtenerMensajeDestino(idMensajeDestino);
    }

    @Override
    public int insertarMensaje(MensajeApp mensajeApp) throws TransformerException, ParserConfigurationException, IOException, HttpException {
        List<MensajeDestino> mensajesDestinoPorGrupo = new ArrayList<>();
        mensajeApp.getMensajeDestino().stream().filter(this::esGrupoDestino).forEach(m -> {
            Persona destinatario = m.getDestinatario();
            GrupoMensaje grupoMensaje = obtenerGruposDeMensaje(mensajeApp, destinatario);
            List<Contacto> grupos = grupoMensaje.listaGrupos();
            if (grupos.isEmpty())
                throw new NullPointerException(String.format("Grupo %s no tiene contactos asociados",
                        destinatario.getPrimerNombre()));
            String tipoMensaje = m.getIdTipoDestino();
            mensajesDestinoPorGrupo.addAll(generarMensajeDestinoGrupo(tipoMensaje, grupos));
        });
        mensajeApp.getMensajeDestino().addAll(mensajesDestinoPorGrupo);
        mensajeApp.setAceptarRespuesta(true);
        List<MensajeDestino> mensajesDestino = mensajeApp.getMensajeDestino().stream()
                .filter(this::personaNoTieneCodigo).collect(Collectors.toList());
        mensajeApp.setMensajeDestino((ArrayList<MensajeDestino>) obtenerMensajeDestinoUnicos(mensajesDestino));
        return procesarInsertarMensaje(mensajeApp);
    }

    private GrupoMensaje obtenerGruposDeMensaje(MensajeApp mensajeApp, Persona destinatario) {
        FiltroWrapper filtros = new FiltroWrapper();
        filtros.setIdSede(destinatario.getIdSede());
        filtros.setIdPeriodo(mensajeApp.getIdPeriodo());
        filtros.setIdGrado(destinatario.getIdGrado());
        return new GrupoMensaje(repository, destinatario.getGrupo(), filtros);
    }

    private boolean personaNoTieneCodigo(MensajeDestino m) {
        return m.getDestinatario().getCodigoPersona() != -1;
    }

    private int procesarInsertarMensaje(MensajeApp mensajeApp) throws ParserConfigurationException, TransformerException, IOException, HttpException {
        mensajeApp.setIdMensaje(repository.insertarMensaje(mensajeApp));
        if (!mensajeApp.getMensajeDestino().isEmpty()) {
            String xmlMensajeDestino = new XMLMensaje(mensajeApp).convertirAString();
            repository.insertarMensajeDestinatarioXML(xmlMensajeDestino);
        }
        if (mensajeApp.getConAdjunto())
            procesarInsertarAdjuntos(mensajeApp);
        enviarNotificacionesMensajePost(mensajeApp);
        return mensajeApp.getIdMensaje();
    }

    private void enviarNotificacionesMensajePost(MensajeApp mensajeApp) throws IOException, HttpException {
        List<NameValuePair> urlParameters = new ArrayList<>();
        urlParameters.add(new BasicNameValuePair("idMensaje", String.valueOf(mensajeApp.getIdMensaje())));
        HttpResponse respuesta = new RequestSender(String.format("%s%s", urlServiceApp, resourceMensajeApp))
                .makePost(urlParameters);
        procesarRequest(respuesta);
    }

    private void procesarInsertarAdjuntos(MensajeApp mensajeApp) {
        mensajeApp.getMensajeAdjunto().forEach(m -> {
            m.setIdMensaje(mensajeApp.getIdMensaje());
            repository.insertarAdjuntos(m);
        });
    }

    private List<MensajeDestino> generarMensajeDestinoGrupo(String tipoMensaje, List<Contacto> grupos) {
        return grupos.stream().map(g -> {
            MensajeDestino mensajeDestino = new MensajeDestino();
            Persona destinatario = new Persona();
            destinatario.setCodigoPersona(g.getIdPersona());
            destinatario.setGrupo(g.getGrupo());
            destinatario.setEmail(g.getCorreo());
            mensajeDestino.setIdTipoDestino(tipoMensaje);
            mensajeDestino.setLeido(false);
            mensajeDestino.setEsFavorito(false);
            mensajeDestino.setEsEliminado(false);
            mensajeDestino.setAlertaRevisada(false);
            mensajeDestino.setDestinatario(destinatario);
            mensajeDestino.setGradoSeccion(g.getGradoSeccion());
            return mensajeDestino;
        }).collect(Collectors.toList());
    }

    public boolean esGrupoDestino(MensajeDestino mensaje) {
        String[] gruposValidos = {Constantes.GRUPO_DESTINO_ALUMNO_SEDE, Constantes.GRUPO_DESTINO_DOCENTE_SEDE,
                Constantes.GRUPO_DESTINO_PPFF_SEDE, Constantes.GRUPO_DESTINO_ALUMNO_GRADO,
                Constantes.GRUPO_DESTINO_DOCENTE_GRADO, Constantes.GRUPO_DESTINO_PPFF_GRADO
        };
        return Arrays.stream(gruposValidos).anyMatch(g -> mensaje.getDestinatario().getGrupo().equals(g));
    }

    private List<MensajeDestino> obtenerMensajeDestinoUnicos(List<MensajeDestino> mensajes) {
        Map<String, MensajeDestino> mapMensajesDestino = new HashMap<>();
        mensajes.forEach(m -> {
            String key = String.format("%s-%s", m.getDestinatario().getCodigoPersona(), m.getDestinatario().getGrupo());
            mapMensajesDestino.putIfAbsent(key, m);
        });
        return new ArrayList<>(mapMensajesDestino.values());
    }

    private void enviarNotificacionesMensajePut(MensajeWrapper mensajeWrapper) throws IOException, NotFoundException, HttpException {
        MensajeDestino mensajeDestino = repository.obtenerMensajeDestino(mensajeWrapper.getId());
        enviarNotificacionesMensaje(mensajeWrapper, mensajeDestino.getIdMensaje());
    }

    private void enviarNotificacionesMensaje(MensajeWrapper mensajeWrapper, int idMensaje) throws IOException, HttpException {
        List<NameValuePair> urlParameters = new ArrayList<>();
        urlParameters.add(new BasicNameValuePair("idMensaje", String.valueOf(idMensaje)));
        urlParameters.add(new BasicNameValuePair("idPersona", String.valueOf(mensajeWrapper.getIdUsuarioActual())));
        urlParameters.add(new BasicNameValuePair("grupo", mensajeWrapper.getGrupoUsuarioActual()));
        HttpResponse respuesta = new RequestSender(String.format("%s%s", urlServiceApp, resourceMensajeApp))
                .makePut(urlParameters);
        procesarRequest(respuesta);
    }

    private void procesarRequest(HttpResponse respuesta) throws IOException, HttpException {
        final boolean respuestaEsExitosa = respuesta.getStatusLine().getStatusCode() == HttpStatus.OK.value();
        if (respuestaEsExitosa) {
            ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            RespuestaApi respuestaApi = objectMapper.readValue(respuesta.getEntity().getContent(), RespuestaApi.class);
            String informacion = respuestaApi.getData().toString();
            logger.debug(informacion);
        } else {
            throw new HttpException(String.format("Ocurrió un error en: %s.%s, con código: %s", this.getClass(),
                    "enviarNotificacionesMensajePut", respuesta.getStatusLine().getStatusCode()));
        }
    }
}
