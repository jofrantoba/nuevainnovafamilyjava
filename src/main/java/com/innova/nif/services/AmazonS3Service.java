package com.innova.nif.services;

import com.amazonaws.HttpMethod;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.ResponseHeaderOverrides;
import com.innova.nif.services.interfaces.IAmazonS3Service;
import com.innova.nif.utils.Util;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.net.URL;
import java.util.Arrays;
import java.util.Date;

@Service
public class AmazonS3Service implements IAmazonS3Service {
    @Value("${cloud.aws.credentials.accessKey}")
    private String accessKey;
    @Value("${cloud.aws.credentials.secretKey}")
    private String secretKey;
    @Value("${cloud.aws.region.static}")
    private String region;
    @Value("${cloud.aws.credentials.awsBucketS3}")
    private String bucketName;
    @Value("${cloud.aws.credentials.aWSCarpetaS3}")
    private String awsCarpetas3;
    @Value("${cloud.aws.expiracionMinutos}")
    private String expiracionMinutos;

    private AmazonS3 s3Client;

    @PostConstruct
    private void initializeAmazon() {
        BasicAWSCredentials awsCredentials = new BasicAWSCredentials(accessKey, secretKey);
        this.s3Client = AmazonS3ClientBuilder.standard()
                .withRegion(region)
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                .build();
    }

    public String obtenerUrlArchivoPorTipo(String nombreArchivo) {
        String[] archivosNoEncodeables = new String[]{"jpg", "jpeg", "png", "mp4"};
        String urlArchivo = generarUrlArchivo(nombreArchivo);
        String extension = FilenameUtils.getExtension(nombreArchivo);
        if (Arrays.stream(archivosNoEncodeables).anyMatch(f -> f.equalsIgnoreCase(extension)))
            return urlArchivo;
        return Util.encodeUrl(urlArchivo);
    }

    public String generarUrlArchivo(String nombreArchivo) {
        String objectKey = String.format("%s/%s", awsCarpetas3, nombreArchivo);
        GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, objectKey, HttpMethod.GET)
                .withMethod(HttpMethod.GET)
                .withExpiration(tiempoExpiracion())
                .withResponseHeaders(new ResponseHeaderOverrides().withContentType("application/octet-stream").withContentDisposition("inline"));
        URL url = s3Client.generatePresignedUrl(generatePresignedUrlRequest);
        return url.toString();
    }

    private Date tiempoExpiracion() {
        final int tiempoEnMinutos = Integer.parseInt(expiracionMinutos);
        Date expiration = new Date();
        long expTimeMillis = expiration.getTime();
        expTimeMillis += 1000 * tiempoEnMinutos * tiempoEnMinutos;
        expiration.setTime(expTimeMillis);
        return expiration;
    }
}
