package com.innova.nif.services;

import com.google.common.base.Strings;
import com.innova.nif.models.*;
import com.innova.nif.models.wrappers.FiltroWrapper;
import com.innova.nif.repositories.interfaces.IContactoRepository;
import com.innova.nif.services.interfaces.IContactoService;
import com.innova.nif.utils.Constantes;
import com.innova.nif.utils.ObjectToClass;
import com.innova.nif.utils.StringHelper;
import javassist.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ContactoService implements IContactoService {
    private final IContactoRepository repository;

    public ContactoService(IContactoRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<MensajeRemitente> listaCorreoRemitente(int idMensaje) {
        return repository.listaCorreoRemitente(idMensaje);
    }

    @Override
    public List<Contacto> equipoDirectivo(FiltroWrapper filtros) {
        return repository.equipoDirectivoPorSede(filtros.getIdSede());
    }

    @Override
    public List<Docente> docentes(FiltroWrapper filtros) {
        return repository.docentes(filtros);
    }

    @Override
    public List<Grado> listaGrados() {
        return repository.listaGrados();
    }

    @Override
    public List<DocenteNivelIngles> obtenerNivelIngles(FiltroWrapper filtros) {
        return repository.obtenerNivelInglesDocente(filtros);
    }

    @Override
    public List<Docente> docentesPorDNI(String dniSeparadoPorComa) {
        List<String> listaDni = Arrays.asList(dniSeparadoPorComa.split(","));
        List<Docente> docentes = new ArrayList<>();
        listaDni.forEach(dni -> docentes.addAll(repository.docentePorDNI(dni)));
        return docentes;
    }

    @Override
    public List<Docente> obtenerDocentesNivelIngles(FiltroWrapper filtros) throws NotFoundException {
        List<Docente> docentes = new ArrayList<>();
        Optional<Grado> grado = obtenerGradoHijo(filtros);
        grado.ifPresent(value -> filtros.setGrado(value.getCodigoGrado()));
        List<DocenteNivelIngles> docentesNivelIngles = obtenerNivelIngles(filtros);
        filtrarDocentesPorTipoUsuario(docentesNivelIngles, filtros);
        if (docentesNivelIngles != null && !docentesNivelIngles.isEmpty())
            docentes = configurarCargoNivelIngles(docentesNivelIngles);
        return docentes;
    }

    @Override
    public List<Grado> listaGradosPorSedePeriodo(int idSede, int idPeriodo) {
        return repository.listaGradosPorSedePeriodo(idSede, idPeriodo);
    }

    @Override
    public List<SeccionAlumno> listarSecciones(FiltroWrapper filtros) throws NotFoundException {
        List<SeccionAlumno> seccionesDM = repository.seccionesDM(filtros);
        boolean esGradoSecundaria = filtros.getIdGrado() >= Constantes.GRADO_SECUNDARIA;
        if (esGradoSecundaria) {
            Optional<Grado> grado = obtenerGradoHijo(filtros);
            grado.ifPresent(value -> filtros.setGrado(value.getCodigoGrado()));
            agregarDocentesConNivelIngles(filtros, seccionesDM);
        }
        return seccionesDM;
    }

    @Override
    public List<Contacto> listarPadresFamilia(FiltroWrapper filtros) {
        return repository.listarPadresFamilia(filtros);
    }

    @Override
    public List<Contacto> listarAlumnos(FiltroWrapper filtros) {
        return repository.listarAlumnos(filtros);
    }

    private void agregarDocentesConNivelIngles(FiltroWrapper filtros, List<SeccionAlumno> seccionesDM) {
        List<DocenteNivelIngles> docentesNivelIngles = repository.obtenerNivelInglesDocente(filtros);
        docentesNivelIngles.forEach(d -> {
            SeccionAlumno seccion = new SeccionAlumno();
            seccion.setNivel(d.getClassRoomLevelCode());
            seccion.setNivelIngles(d.getEnglishLevelId());
            seccion.setIdGrado(d.getIdGrado());
            seccionesDM.add(seccion);
        });
    }

    private Optional<Grado> obtenerGradoHijo(FiltroWrapper filtros) throws NotFoundException {
        Optional<Grado> grado;
        List gradosSession = (List) filtros.getInformacionSesion().get(Constantes.SESION_LISTA_GRADOS);
        if (gradosSession == null || gradosSession.isEmpty())
            grado = obtenerGradoHijoDesdeRepositorio(filtros);
        else
            grado = obtenerGradoHijoDesdeSesion(gradosSession, filtros);
        if (grado.isEmpty())
            throw new NotFoundException(String.format("No se encontró un obtenerGradoHijo con id=%s", filtros.getIdGrado()));
        return grado;
    }

    private Optional<Grado> obtenerGradoHijoDesdeRepositorio(FiltroWrapper filtros) {
        return listaGrados().stream().filter(f -> f.getIdGrado() == filtros.getIdGrado()).findFirst();
    }

    private Optional<Grado> obtenerGradoHijoDesdeSesion(List gradosSession, FiltroWrapper filtros) {
        List<Grado> grados = new ArrayList<>();
        gradosSession.forEach(g -> grados.add(new ObjectToClass().obtenerObjeto(g, Grado.class)));
        return grados.stream().filter(f -> f.getIdGrado() == filtros.getIdGrado()).findFirst();
    }

    private void filtrarDocentesPorTipoUsuario(List<DocenteNivelIngles> docentesNivelIngles, FiltroWrapper filtros) throws NotFoundException {
        AppUser usuario = new ObjectToClass().obtenerObjeto(filtros.getInformacionSesion().get("CurrentUser"), AppUser.class);
        if (!Strings.isNullOrEmpty(filtros.getNivel()))
            eliminarDocenteIngles(docentesNivelIngles, filtros.getNivel());
        if (usuario.getGrupo().equals(Constantes.PERFIL_PP_FF))
            eliminarDocenteInglesPorPPFF(docentesNivelIngles, filtros);
        if (usuario.getGrupo().equals(Constantes.PERFIL_ALUMNO))
            eliminarDocenteIngles(docentesNivelIngles, usuario.getNivel());
    }

    private void eliminarDocenteIngles(List<DocenteNivelIngles> docentesNivelIngles, String nivel) {
        docentesNivelIngles.removeIf(f -> !f.getClassRoomLevelCode().equals(nivel));
    }

    private void eliminarDocenteInglesPorPPFF(List<DocenteNivelIngles> docentesNivelIngles, FiltroWrapper filtros) throws NotFoundException {
        List listaHijos = (List) filtros.getInformacionSesion().get(Constantes.SESION_LISTA_HIJOS);
        List<HijoPorPadreFamilia> hijos = new ArrayList<>();
        listaHijos.forEach(x -> hijos.add(new ObjectToClass().obtenerObjeto(x, HijoPorPadreFamilia.class)));
        if (hijos.isEmpty())
            throw new NotFoundException("El padre de familia no tiene hijos.");
        Optional<HijoPorPadreFamilia> hijoPorPadreFamilia = hijos.stream().filter(h -> h.getIdPersona() == filtros.getIdHijo()).findFirst();
        if (hijoPorPadreFamilia.isPresent()) {
            String nivelIngles = StringHelper.getValueOrEmpty(hijoPorPadreFamilia.get().getNivel());
            eliminarDocenteIngles(docentesNivelIngles, nivelIngles);
        }
    }

    private List<Docente> configurarCargoNivelIngles(List<DocenteNivelIngles> docentesNivelIngles) {
        List<Docente> docentes;
        List<String> dnis = docentesNivelIngles.stream().map(DocenteNivelIngles::getDni).collect(Collectors.toList());
        docentes = docentesPorDNI(String.join(",", dnis));
        docentes.forEach(d -> {
            Optional<DocenteNivelIngles> docenteNivelIngles = docentesNivelIngles.stream().filter(f -> f.getDni().equals(d.getDni())).findFirst();
            if (docenteNivelIngles.isPresent()) {
                d.setCargo(String.format("%s - %s", d.getCargo(), docenteNivelIngles.get().getClassRoomLevelCode()));
                d.setNivelIngles(docenteNivelIngles.get().getClassRoomLevelCode());
            }
        });
        return docentes;
    }
}
