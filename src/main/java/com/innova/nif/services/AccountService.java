package com.innova.nif.services;

import com.innova.nif.models.MensajeAdjunto;
import com.innova.nif.models.PeriodoLectivo;
import com.innova.nif.models.Persona;
import com.innova.nif.models.Usuario;
import com.innova.nif.repositories.interfaces.IAccountRepository;
import com.innova.nif.repositories.interfaces.IUserRepository;
import com.innova.nif.services.interfaces.IAccountService;
import com.innova.nif.utils.Constantes;
import javassist.NotFoundException;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

@Service
public class AccountService implements IAccountService {

    private final IAccountRepository accountRepository;
    private final IUserRepository userRepository;

    public AccountService(IAccountRepository accountRepository, IUserRepository userRepository) {
        this.accountRepository = accountRepository;
        this.userRepository = userRepository;
    }

    @Override
    public boolean updateUserPassword(Persona persona) {
        return accountRepository.updateUserPassword(persona) == Constantes.CODIGO_EXITO;
    }

    @Override
    public Usuario obtenerUsuarioRecuperarContrasena(int idPerson, String profile) throws ParseException, NotFoundException {
        return userRepository.usuarioInformacionRecuperarContrasena(idPerson, profile);
    }

    @Override
    public MensajeAdjunto consultarFotoPerfil(int idPersona, String idPefil) {
        return userRepository.consultarFotoPerfil(idPersona, idPefil);
    }

    @Override
    public boolean procesarSolicitudDeCambioPassword(Usuario usuario) throws NoSuchAlgorithmException {
        var respuesta = userRepository.solicitarCambioDePassword(usuario);
        return respuesta == Constantes.CODIGO_EXITO;
    }

    @Override
    public Usuario obtenerUsuarioDmPeriodo(String email, PeriodoLectivo periodoActual, PeriodoLectivo periodoPosterior) throws NotFoundException {
        Usuario usuarioDm;
        if (periodoPosterior  == null)
            usuarioDm = userRepository.obtenerUsuarioPF(email, periodoActual.getIdPeriodo());
        else
            usuarioDm = userRepository.obtenerUsuarioDm(email, periodoActual.getIdPeriodo(), periodoPosterior.getIdPeriodo());
        if (usuarioDm == null)
            throw new NotFoundException("No encontramos ninguna cuenta asociada a este correo. Por favor comunícate con tu sede.");
        return usuarioDm;
    }
}
