package com.innova.nif.services;

import com.amazonaws.services.simpleemail.model.*;
import com.innova.nif.api.ses.SimpleMailConfig;
import com.innova.nif.api.ses.factories.EmailGeneratorFactory;
import com.innova.nif.models.Usuario;
import com.innova.nif.services.interfaces.IEmailService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class EmailService implements IEmailService {
    @Value("${urlInnova}")
    private String urlInnova;
    @Value("${urlImagenCorreo}")
    private String urlImagenCorreo;
    @Value("${cloud.aws.credentials.accessKey}")
    private String accessKey;
    @Value("${cloud.aws.credentials.secretKey}")
    private String secretKey;
    @Value("${cloud.aws.region.static}")
    private String region;
    @Value("${modoDesarrollo}")
    private boolean isDev;
    @Value("${correoDev}")
    private String correoDev;
    @Value("${AwsSenderAddress}")
    private String sourceAddress;
    @Value("${CopiaOculta}")
    private String copiaOculta;

    @Override
    public void enviarNotificacion(Usuario model, String asunto, String template) {
        var emailFactory = new EmailGeneratorFactory();
        var emailBody = emailFactory.generateEmail(template, model, urlInnova, urlImagenCorreo);
        String toEmail = isDev ? correoDev : model.getCorreo();

        SendEmailRequest request = new SendEmailRequest()
                .withDestination(new Destination().withToAddresses(toEmail).withBccAddresses(copiaOculta))
                .withMessage(new Message().withBody(new Body()
                        .withHtml(new Content().withCharset("UTF-8").withData(emailBody)))
                        .withSubject(new Content().withCharset("UTF-8").withData(asunto)))
                .withSource(sourceAddress);
        var client = new SimpleMailConfig().amazonSimpleEmailService(accessKey, secretKey, region);
        client.sendEmail(request);
    }
}
