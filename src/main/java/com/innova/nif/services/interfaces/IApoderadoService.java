package com.innova.nif.services.interfaces;

import com.innova.nif.models.HijoApoderado;

import java.util.List;

public interface IApoderadoService {
    List<HijoApoderado> obtenerHijos(int idPersonaPadre, int idPeriodo);
}
