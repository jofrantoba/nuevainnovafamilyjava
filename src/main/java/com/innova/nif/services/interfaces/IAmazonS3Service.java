package com.innova.nif.services.interfaces;

public interface IAmazonS3Service {
    String obtenerUrlArchivoPorTipo(String nombreArchivo);

    String generarUrlArchivo(String nombreArchivo);
}
