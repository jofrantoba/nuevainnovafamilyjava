package com.innova.nif.services.interfaces;

import com.innova.nif.models.Usuario;
import javassist.NotFoundException;

import java.text.ParseException;
import java.util.List;

public interface ILoginService {
    List obtenerUsuarioDm(String correo, int periodo, String tipo);

    Usuario obtenerUsuarioDm(String email) throws NotFoundException;

    Usuario obtenerUsuarioPFDM(String correo) throws NotFoundException;

    Usuario obtenerUsuarioPFNIF(Usuario usuarioDM) throws NotFoundException, ParseException;
}
