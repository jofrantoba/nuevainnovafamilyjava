package com.innova.nif.services.interfaces;

import com.innova.nif.models.*;
import com.innova.nif.models.wrappers.FiltroWrapper;
import javassist.NotFoundException;

import java.util.List;

public interface IContactoService {

    List<MensajeRemitente> listaCorreoRemitente(int idMensaje);

    List<Contacto> equipoDirectivo(FiltroWrapper filtros);

    List<Docente> docentes(FiltroWrapper filtros);

    List<Grado> listaGrados();

    List<DocenteNivelIngles> obtenerNivelIngles(FiltroWrapper filtros);

    List<Docente> docentesPorDNI(String dniSeparadoPorComa);

    List<Docente> obtenerDocentesNivelIngles(FiltroWrapper filtros) throws NotFoundException;

    List<Grado> listaGradosPorSedePeriodo(int idSede, int idPeriodo);

    List<SeccionAlumno> listarSecciones(FiltroWrapper filtros) throws NotFoundException;

    List<Contacto> listarPadresFamilia(FiltroWrapper filtros);

    List<Contacto> listarAlumnos(FiltroWrapper filtros);
}
