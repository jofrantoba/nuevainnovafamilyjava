package com.innova.nif.services.interfaces;

import com.innova.nif.models.MensajeAdjunto;
import com.innova.nif.models.Persona;
import com.innova.nif.models.AlertaUsuario;
import com.innova.nif.models.Usuario;
import javassist.NotFoundException;

import java.text.ParseException;
import java.util.List;

public interface IUsuarioService {
    boolean actualizarContrasenaConHash(Persona persona);

    Usuario obtenerUsuarioPorContrasenaHash(String hash);

    Usuario obtenerContrasenia(int idPersona, String grupo) throws ParseException, NotFoundException;

    boolean correoExiste(String email);

    boolean procesarActualizarCorreo(Usuario usuario);

    Usuario obtenerUsuarioCambioContrasenaPorHash(String hash) throws NotFoundException;

    int insertarAdjunto(MensajeAdjunto adjunto);

    boolean actualizarFoto(Persona persona);

    void actualizarEstadoSinNotificacion(Usuario usuario);

    void actualizarUsuarioNotificacion(Usuario usuario);

    List<AlertaUsuario> listarAlertasPorUsuarioTarea(int idSede, int idPeriodo, String idGrados);

    List<AlertaUsuario> listarAlertasPorUsuarioEvento(int idSede, int idPeriodo, String idGrados);

    int actualizarInicioSesion(Usuario usuario);
}
