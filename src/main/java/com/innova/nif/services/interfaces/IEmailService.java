package com.innova.nif.services.interfaces;

import com.innova.nif.models.Usuario;

public interface IEmailService {
    void enviarNotificacion(Usuario model, String asunto, String template);
}
