package com.innova.nif.services.interfaces;

import com.innova.nif.models.MensajeApp;
import com.innova.nif.models.MensajeDestino;
import com.innova.nif.models.MensajePaginado;
import com.innova.nif.models.wrappers.MensajeFiltro;
import com.innova.nif.models.wrappers.MensajeWrapper;
import javassist.NotFoundException;
import org.apache.http.HttpException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.util.List;

public interface IMensajeService {
    int consultarTotalMensajes(int idPersona, String grupo);

    List<MensajePaginado> consultarMensajesPaginado(MensajeFiltro mensajeFiltro);

    List detalleMensaje(int id);

    List consultarAdjuntos(int idMensaje);

    List consultarHistorial(int idCorreo, int idPersona);

    int actualizarMensaje(MensajeWrapper mensajeWrapper) throws IOException, NotFoundException, HttpException;

    int eliminarMensaje(MensajeWrapper mensajeWrapper) throws IOException, NotFoundException, HttpException;

    MensajeDestino obtenerMensajeDestino(int idMensajeDestino) throws NotFoundException;

    int insertarMensaje(MensajeApp mensajeApp) throws TransformerException, ParserConfigurationException, IOException, HttpException;
}
