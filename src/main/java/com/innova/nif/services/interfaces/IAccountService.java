package com.innova.nif.services.interfaces;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.innova.nif.models.MensajeAdjunto;
import com.innova.nif.models.PeriodoLectivo;
import com.innova.nif.models.Persona;
import com.innova.nif.models.Usuario;
import javassist.NotFoundException;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

public interface IAccountService {
    boolean updateUserPassword(Persona persona);

    Usuario obtenerUsuarioRecuperarContrasena(int idPerson, String profile) throws JsonProcessingException, ParseException, NotFoundException;

    MensajeAdjunto consultarFotoPerfil(int idPersona, String idPefil);

    boolean procesarSolicitudDeCambioPassword(Usuario usuario) throws NoSuchAlgorithmException;

    Usuario obtenerUsuarioDmPeriodo(String email, PeriodoLectivo periodoActual, PeriodoLectivo periodoPosterior) throws NotFoundException, ParseException;
}
