package com.innova.nif.services.interfaces;

import com.innova.nif.models.BackOfficeParametros;

public interface IBackOfficeService {
    int obtenerContador(BackOfficeParametros parametros);

    int obtenerConteoFiltros(BackOfficeParametros filtros);
}
