package com.innova.nif.services.interfaces;

import com.innova.nif.models.PeriodoLectivo;
import javassist.NotFoundException;

import java.util.List;

public interface IPeriodoService {
    PeriodoLectivo getPeriodoActivo() throws NotFoundException;

    List<PeriodoLectivo> obtenerPeriodosPF();
}
