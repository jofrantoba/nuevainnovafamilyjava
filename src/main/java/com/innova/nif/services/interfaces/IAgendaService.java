package com.innova.nif.services.interfaces;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.innova.nif.models.*;
import com.innova.nif.models.wrappers.AgendaFiltro;

import java.util.List;

public interface IAgendaService {
    List<Calendario> listaAgenda(AgendaFiltro filtros);

    EventoGeneral obtenerEventoEDDC(AgendaFiltro filtros);

    TareaGeneral obtenerTarea(AgendaFiltro filtros);

    EventoGeneral obtenerEventoALPF(AgendaFiltro filtros);

    int registrarAgenda(AgendaCalendario agenda);

    LogAgenda editarEvento(AgendaCalendario agenda) throws JsonProcessingException;

    int editarTarea(AgendaCalendario agenda);

    LogAgenda eliminarEvento(int idEvento, int idPersona) throws JsonProcessingException;

    int eliminarAgenda(int idAgenda);

    LogAgenda registrarLogTarea(LogAgenda logTarea);

    int eliminarAdjunto(AgendaAdjunto adjunto);
}