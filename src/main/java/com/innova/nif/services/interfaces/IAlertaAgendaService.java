package com.innova.nif.services.interfaces;

import com.innova.nif.models.AgendaCalendario;

import java.io.IOException;

public interface IAlertaAgendaService {
    void registrar(AgendaCalendario agenda) throws IOException;
}
