package com.innova.nif.services;

import com.innova.nif.models.BackOfficeParametro;
import com.innova.nif.models.BackOfficeParametros;
import com.innova.nif.models.SolicitudContador;
import com.innova.nif.repositories.interfaces.IBackOfficeRepository;
import com.innova.nif.services.backofficebuilder.*;
import com.innova.nif.services.interfaces.IBackOfficeService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BackOfficeService implements IBackOfficeService {
    private final IBackOfficeRepository repository;

    public BackOfficeService(IBackOfficeRepository repository) {
        this.repository = repository;
    }

    @Override
    public int obtenerContador(BackOfficeParametros parametros) {
        int contador = obtenerConteoAlumnosProfesores(parametros);
        List<String> codigoAlumnos = obtenerCodigoAlumnos(parametros);
        SolicitudContador solicitudContador = new SolicitudContador();
        solicitudContador.setCodigosAlumnos(codigoAlumnos);
        solicitudContador.setParametros(parametros);
        contador += obtenerConteoRolesFaltantes(solicitudContador);
        return contador;
    }

    @Override
    public int obtenerConteoFiltros(BackOfficeParametros parametros) {
        List<String> consultasSql = parametros.getRoles().stream().allMatch(r -> r.getParametroId().equals(BackOfficeParametros.ALL_CODE))
                ? obtenerConsultasPorTodosLosRoles(parametros, BaseBackOfficeQueryBuilder.COUNT_QUERY)
                : obtenerConsultas(parametros, BaseBackOfficeQueryBuilder.COUNT_QUERY);
        return obtenerConteoPorConsultasSqlDataManagement(consultasSql);
    }

    private List<String> obtenerConsultasPorTodosLosRoles(BackOfficeParametros parametros, String tipoBuilder) {
        List<String> consultasSql = new ArrayList<>();
        BackOfficeQueryDataManagementDirector consultaDirector = new BackOfficeQueryDataManagementDirector();
        consultasSql.add(consultaDirector.construirConsulta(new AlumnosQueryBuilder(tipoBuilder, parametros.getIdPeriodo(), parametros.getEstadoPeriodo(), null), parametros));
        consultasSql.add(consultaDirector.construirConsulta(new DocentesQueryBuilder(tipoBuilder), parametros));
        consultasSql.add(consultaDirector.construirConsulta(new EquipoDirectivoBuilder(tipoBuilder), parametros));
        consultasSql.add(consultaDirector.construirConsulta(new PadreFamiliaQueryBuilder(tipoBuilder, parametros.getIdPeriodo(), parametros.getEstadoPeriodo()), parametros));
        return consultasSql;
    }

    public List<String> obtenerConsultas(BackOfficeParametros parametros, String tipoBuilder) {
        List<String> consultasSql = new ArrayList<>();
        BackOfficeQueryDataManagementDirector consultaDirector = new BackOfficeQueryDataManagementDirector();
        if (contieneFiltroSeleccionarAlumno(parametros))
            consultasSql.add(consultaDirector.construirConsulta(new AlumnosQueryBuilder(tipoBuilder, parametros.getIdPeriodo(), parametros.getEstadoPeriodo(), null), parametros));
        if (contieneFiltroSeleccionarPPFF(parametros))
            consultasSql.add(consultaDirector.construirConsulta(new PadreFamiliaQueryBuilder(tipoBuilder, parametros.getIdPeriodo(), parametros.getEstadoPeriodo()), parametros));
        if (contieneFiltroSeleccionarDocente(parametros))
            consultasSql.add(consultaDirector.construirConsulta(new DocentesQueryBuilder(tipoBuilder), parametros));
        if (parametros.getRoles().stream().anyMatch(r -> !BackOfficeParametros.rolesPea().contains(r.getParametroId())))
            consultasSql.add(consultaDirector.construirConsulta(new EquipoDirectivoBuilder(tipoBuilder), parametros));
        return consultasSql;
    }

    private int obtenerConteoRolesFaltantes(SolicitudContador solicitudContador) {
        int conteo = 0;
        List<String> rolesNoPendientes = rolesFaltantes();
        List<String> rolesIds = solicitudContador.getParametros().getRoles().stream()
                .map(BackOfficeParametro::getParametroId).collect(Collectors.toList());
        boolean tieneCodigoTodos = rolesIds.stream().anyMatch(r -> r.equals(BackOfficeParametros.ALL_CODE));
        boolean tieneRolesPendientes = rolesIds.stream().anyMatch(r -> !rolesNoPendientes.contains(r));
        if (tieneCodigoTodos || tieneRolesPendientes)
            conteo = obtenerCantidadPorParametrosFaltantes(solicitudContador);
        return conteo;
    }

    private int obtenerCantidadPorParametrosFaltantes(SolicitudContador solicitudContador) {
        List<String> consultasSql = solicitudContador.getParametros().getRoles().stream().allMatch(r -> r.getParametroId().equals(BackOfficeParametros.ALL_CODE))
                ? obtenerConsultasTodosLosRolesFaltantes(solicitudContador) : obtenerConsultasFaltantes(solicitudContador);
        return obtenerConteoPorConsultasSqlDataManagement(consultasSql);
    }

    private List<String> obtenerConsultasTodosLosRolesFaltantes(SolicitudContador solicitudContador) {
        List<String> consultasSql = new ArrayList<>();
        BackOfficeQueryDataManagementDirector consultaDirector = new BackOfficeQueryDataManagementDirector();
        consultasSql.add(consultaDirector.construirConsulta(
                new EquipoDirectivoBuilder(BaseBackOfficeQueryBuilder.COUNT_QUERY), solicitudContador.getParametros())
        );
        if (!solicitudContador.getCodigosAlumnos().isEmpty()) {
            consultasSql.add(consultaDirector.construirConsulta(
                    new PadreFamiliaPorAlumnosQueryBuilder(solicitudContador.getParametros().getIdPeriodo(),
                            solicitudContador.getCodigosAlumnos(), BaseBackOfficeQueryBuilder.COUNT_QUERY, false
                    ), solicitudContador.getParametros()));
            consultasSql.add(consultaDirector.construirConsulta(
                    new AlumnosCodigoQueryBuilder(
                            solicitudContador.getParametros().getIdPeriodo(), solicitudContador.getCodigosAlumnos(), BaseBackOfficeQueryBuilder.COUNT_QUERY, false),
                    solicitudContador.getParametros()
            ));
        }
        return consultasSql;
    }

    private List<String> obtenerConsultasFaltantes(SolicitudContador solicitudContador) {
        List<String> consultasSql = new ArrayList<>();
        BackOfficeQueryDataManagementDirector consultaDirector = new BackOfficeQueryDataManagementDirector();
        boolean contieneFiltroAlumno = contieneFiltroSeleccionarAlumno(solicitudContador.getParametros());
        boolean contieneFiltroPadreFamilia = contieneFiltroSeleccionarPPFF(solicitudContador.getParametros());
        boolean contieneFiltroDocente = contieneFiltroSeleccionarDocente(solicitudContador.getParametros());
        if (contieneFiltroAlumno)
            consultasSql.addAll(consultasRestantesParaAlumnos(consultaDirector, solicitudContador.getParametros(), solicitudContador.getCodigosAlumnos(), BaseBackOfficeQueryBuilder.COUNT_QUERY));
        if (contieneFiltroPadreFamilia)
            consultasSql.addAll(consultasRestantesParaPadres(consultaDirector, solicitudContador.getParametros(), solicitudContador.getCodigosAlumnos(), BaseBackOfficeQueryBuilder.COUNT_QUERY));
        if (contieneFiltroDocente && !solicitudContador.getParametros().getSecciones().isEmpty())
            consultasSql.add(consultaDirector.construirConsulta(new DocentesQueryBuilder(BaseBackOfficeQueryBuilder.COUNT_QUERY), solicitudContador.getParametros()));
        if (solicitudContador.getParametros().getRoles().stream().noneMatch(r -> BackOfficeParametros.rolesPea().contains(r.getParametroId())))
            consultasSql.add(consultaDirector.construirConsulta(new EquipoDirectivoBuilder(BaseBackOfficeQueryBuilder.COUNT_QUERY), solicitudContador.getParametros()));
        return consultasSql;
    }

    private List<String> consultasRestantesParaAlumnos(BackOfficeQueryDataManagementDirector consultaDirector, BackOfficeParametros parametros, List<String> codigosAlumnos, String tipo) {
        List<String> consultasSql = new ArrayList<>();
        boolean contieneSeccion = !parametros.getSecciones().isEmpty();
        if (contieneSeccion)
            consultasSql.add(consultaDirector.construirConsulta(new AlumnosCodigoQueryBuilder(parametros.getIdPeriodo(), codigosAlumnos, tipo, true), parametros));
        if (tipo.equals(BaseBackOfficeQueryBuilder.LIST_QUERY))
            consultasSql.add(consultaDirector.construirConsulta(new AlumnosCodigoQueryBuilder(parametros.getIdPeriodo(), codigosAlumnos, tipo, false), parametros));
        return consultasSql;
    }

    private List<String> consultasRestantesParaPadres(BackOfficeQueryDataManagementDirector consultaDirector, BackOfficeParametros parametros, List<String> codigosAlumnos, String tipo) {
        List<String> consultasSql = new ArrayList<>();
        boolean contieneSeccion = !parametros.getSecciones().isEmpty();
        if (contieneSeccion)
            consultasSql.add(consultaDirector.construirConsulta(new PadreFamiliaPorAlumnosQueryBuilder(parametros.getIdPeriodo(), codigosAlumnos, tipo, true), parametros));
        consultasSql.add(consultaDirector.construirConsulta(new PadreFamiliaPorAlumnosQueryBuilder(parametros.getIdPeriodo(), codigosAlumnos, tipo, false), parametros));
        return consultasSql;
    }

    private List<String> rolesFaltantes() {
        List<String> rolesNoPendientes = new ArrayList<>();
        rolesNoPendientes.add(BackOfficeParametros.ALUMNO_CODE);
        rolesNoPendientes.add(BackOfficeParametros.DOCENTE_CODE);
        return rolesNoPendientes;
    }

    private int obtenerConteoAlumnosProfesores(BackOfficeParametros parametros) {
        int contador;
        List<String> consultasSQL = new ArrayList<>();
        boolean filtroSeleccionarTodos = contieneFiltroSeleccionarTodosPrimerRol(parametros);
        boolean filtroPPFFSeleccionado = contieneFiltroSeleccionarPPFF(parametros);
        boolean filtroDocentesSeleccionado = contieneFiltroSeleccionarDocente(parametros);
        boolean filtroAlumnosSeleccionado = contieneFiltroSeleccionarAlumno(parametros);

        if (filtroSeleccionarTodos)
            consultasSQL = obtenerConsultaConteoTodos(parametros);
        else if (filtroPPFFSeleccionado && !filtroAlumnosSeleccionado && !filtroDocentesSeleccionado)
            return 0;
        else if (filtroDocentesSeleccionado && !filtroAlumnosSeleccionado) {
            BackOfficeQueryPeaDirector consultaDirector = new BackOfficeQueryPeaDirector();
            consultasSQL.add(consultaDirector.construirConsulta(new BackOfficeDocentesQueryBuilder(BaseBackOfficeQueryBuilder.COUNT_QUERY), parametros));
        } else
            consultasSQL = obtenerConsultasConteo(parametros);
        contador = obtenerConteoPorConsultasSql(consultasSQL);
        return contador;
    }

    public boolean contieneFiltroSeleccionarTodosPrimerRol(BackOfficeParametros parametros) {
        final Optional<BackOfficeParametro> parametro = parametros.getRoles().stream().findFirst();
        if (parametro.isPresent())
            return !parametros.getRoles().isEmpty() && parametro.get().getParametroId()
                    .equals(BackOfficeParametros.ALL_CODE);
        throw new NullPointerException("Los parámetros de consulta no tienen roles");
    }

    public boolean contieneFiltroSeleccionarPPFF(BackOfficeParametros parametros) {
        return parametros.getRoles().stream().anyMatch(r -> r.getParametroId().equals(BackOfficeParametros.PADRE_FAMILIA_CODE));
    }

    public boolean contieneFiltroSeleccionarDocente(BackOfficeParametros parametros) {
        return parametros.getRoles().stream().anyMatch(r -> r.getParametroId().equals(BackOfficeParametros.DOCENTE_CODE));
    }

    public boolean contieneFiltroSeleccionarAlumno(BackOfficeParametros parametros) {
        return parametros.getRoles().stream().anyMatch(r -> r.getParametroId().equals(BackOfficeParametros.ALUMNO_CODE));
    }

    private List<String> obtenerCodigoAlumnos(BackOfficeParametros parametros) {
        List<String> codigos = new ArrayList<>();
        if (contieneFiltroSeleccionarTodos(parametros) || contieneFiltroSeleccionarAlumno(parametros) ||
                contieneFiltroSeleccionarPPFF(parametros)) {
            BackOfficeQueryPeaDirector consultaDirector = new BackOfficeQueryPeaDirector();
            String consultaSql = consultaDirector.construirConsulta(new BackOfficeAlumnosListCodeQueryBuilder(), parametros);
            codigos = repository.obtenerCodigoAlumnos(consultaSql);
        }
        return codigos;
    }

    public boolean contieneFiltroSeleccionarTodos(BackOfficeParametros parametros) {
        return parametros.getRoles().stream().anyMatch(r -> r.getParametroId().equals(BackOfficeParametros.ALL_CODE));
    }

    private List<String> obtenerConsultaConteoTodos(BackOfficeParametros parametros) {
        List<String> consultasSQL = new ArrayList<>();
        BackOfficeQueryPeaDirector consultaDirector = new BackOfficeQueryPeaDirector();
        consultasSQL.add(consultaDirector.construirConsulta(new BackOfficeAlumnosCountCodeQueryBuilder(), parametros));
        consultasSQL.add(consultaDirector.construirConsulta(new BackOfficeDocentesQueryBuilder(BaseBackOfficeQueryBuilder.COUNT_QUERY), parametros));
        return consultasSQL;
    }

    private List<String> obtenerConsultasConteo(BackOfficeParametros parametros) {
        List<String> consultasSql = new ArrayList<>();
        BackOfficeQueryPeaDirector consultaDirector = new BackOfficeQueryPeaDirector();
        if (contieneFiltroSeleccionarAlumno(parametros))
            consultasSql.add(consultaDirector.construirConsulta(new BackOfficeAlumnosCountCodeQueryBuilder(), parametros));
        if (contieneFiltroSeleccionarDocente(parametros))
            consultasSql.add(consultaDirector.construirConsulta(new BackOfficeDocentesQueryBuilder(BaseBackOfficeQueryBuilder.COUNT_QUERY), parametros));
        return consultasSql;
    }

    public int obtenerConteoPorConsultasSql(List<String> consultasSQL) {
        return consultasSQL.stream().mapToInt(repository::obtenerConteoPorConsulta).sum();
    }

    private int obtenerConteoPorConsultasSqlDataManagement(List<String> consultasSql) {
        return consultasSql.stream().mapToInt(repository::obtenerConteoPorConsultaDataManagement).sum();
    }
}
