package com.innova.nif.services;

import com.innova.nif.models.PeriodoLectivo;
import com.innova.nif.repositories.interfaces.IPeriodoRepository;
import com.innova.nif.services.interfaces.IPeriodoService;
import javassist.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PeriodoService implements IPeriodoService {
    private final IPeriodoRepository repository;

    public PeriodoService(IPeriodoRepository repository) {
        this.repository = repository;
    }

    @Override
    public PeriodoLectivo getPeriodoActivo() throws NotFoundException {
        var periodo = repository.getPeriodoActivo();
        if (periodo == null)
            throw new NotFoundException("No encontraros periodos");
        return periodo;
    }

    @Override
    public List<PeriodoLectivo> obtenerPeriodosPF() {
        return repository.obtenerPeriodosPF();
    }
}
