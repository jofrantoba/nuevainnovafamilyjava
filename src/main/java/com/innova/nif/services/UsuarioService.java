package com.innova.nif.services;

import com.innova.nif.models.MensajeAdjunto;
import com.innova.nif.models.Persona;
import com.innova.nif.models.AlertaUsuario;
import com.innova.nif.models.Usuario;
import com.innova.nif.repositories.interfaces.IUserRepository;
import com.innova.nif.services.interfaces.IUsuarioService;
import javassist.NotFoundException;
import org.springframework.stereotype.Service;

import javax.persistence.PersistenceException;
import java.text.ParseException;
import java.util.List;

@Service
public class UsuarioService implements IUsuarioService {
    private final IUserRepository repositorio;

    public UsuarioService(IUserRepository repositorio) {
        this.repositorio = repositorio;
    }

    @Override
    public boolean actualizarContrasenaConHash(Persona persona) {
        return repositorio.actualizarContrasenaConHash(persona);
    }

    @Override
    public Usuario obtenerContrasenia(int idPersona, String grupo) throws ParseException, NotFoundException {
        return repositorio.usuarioInformacionRecuperarContrasena(idPersona, grupo);
    }

    @Override
    public Usuario obtenerUsuarioPorContrasenaHash(String hash) {
        return repositorio.obtenerUsuarioPorContrasenaHash(hash);
    }

    @Override
    public boolean correoExiste(String email) {
        return repositorio.correoExiste(email);
    }

    @Override
    public boolean procesarActualizarCorreo(Usuario usuario) {
        return repositorio.actualizarCorreo(usuario);
    }

    @Override
    public Usuario obtenerUsuarioCambioContrasenaPorHash(String hash) throws NotFoundException {
        return repositorio.obtenerUsuarioCambioContrasenaPorHash(hash);
    }

    @Override
    public int insertarAdjunto(MensajeAdjunto adjunto) {
        int idFoto = repositorio.insertarFoto(adjunto);
        if (idFoto < 0) {
            throw new PersistenceException("Ocurrió un error al guardar la foto.");
        }
        return idFoto;
    }

    @Override
    public boolean actualizarFoto(Persona persona) {
        return repositorio.actualizarFoto(persona);
    }

    @Override
    public void actualizarEstadoSinNotificacion(Usuario usuario) {
        repositorio.actualizarEstadoSinNotificacion(usuario);
    }

    @Override
    public void actualizarUsuarioNotificacion(Usuario usuario) {
        repositorio.actualizarUsuarioNotificacion(usuario);
    }

    @Override
    public List<AlertaUsuario> listarAlertasPorUsuarioTarea(int idSede, int idPeriodo, String idGrados) {
        return repositorio.listarAlertasPorUsuarioTarea(idSede, idPeriodo, idGrados);
    }

    @Override
    public List<AlertaUsuario> listarAlertasPorUsuarioEvento(int idSede, int idPeriodo, String idGrados) {
        return repositorio.listarAlertasPorUsuarioEvento(idSede, idPeriodo, idGrados);
    }

    @Override
    public int actualizarInicioSesion(Usuario usuario) {
        return repositorio.actualizarInicioSesion(usuario);
    }
}
