package com.innova.nif.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.innova.nif.models.*;
import com.innova.nif.models.wrappers.AgendaFiltro;
import com.innova.nif.repositories.factories.agenda.IAgendaFactory;
import com.innova.nif.repositories.interfaces.IAgendaRepository;
import com.innova.nif.services.interfaces.IAgendaService;
import com.innova.nif.services.interfaces.IAmazonS3Service;
import com.innova.nif.services.utils.AgendaFechaHoraBuilder;
import com.innova.nif.utils.Constantes;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AgendaService implements IAgendaService {
    private static final int ERROR_AL_GUARDAR = -1;
    private final IAgendaFactory factoryRepository;
    private final IAgendaRepository repository;
    private final IAmazonS3Service s3Service;

    public AgendaService(IAgendaFactory factoryRepository, IAgendaRepository repository, IAmazonS3Service s3Service) {
        this.factoryRepository = factoryRepository;
        this.repository = repository;
        this.s3Service = s3Service;
    }

    @Override
    public List listaAgenda(AgendaFiltro filtros) {
        return factoryRepository.getRepository(filtros.getGrupo()).obtenerDatos(filtros);
    }

    @Override
    public EventoGeneral obtenerEventoEDDC(AgendaFiltro filtros) {
        EventoGeneral evento = repository.obtenerEventoEDDC(filtros.getIdTareaEvento());
        configurarFechaHoraEvento(evento);
        List<AgendaAdjunto> adjuntos = obtenerAdjuntos(filtros);
        evento.setLstAdjunto(adjuntos);
        return evento;
    }

    @Override
    public TareaGeneral obtenerTarea(AgendaFiltro filtros) {
        TareaGeneral tarea = repository.obtenerTarea(filtros);
        Docente docente = repository.obtenerDocente(tarea.getTarea().getIdPersona());
        tarea.setNombreDocente(docente.getNombreCompleto());
        tarea.setInicialesDocente(docente.getNombresIniciales());
        List<AgendaAdjunto> adjuntos = obtenerAdjuntos(filtros);
        tarea.setLstAdjunto(adjuntos);
        return tarea;
    }

    @Override
    public EventoGeneral obtenerEventoALPF(AgendaFiltro filtros) {
        EventoGeneral evento = repository.obtenerEventoALPF(filtros);
        List<AgendaAdjunto> adjuntos = obtenerAdjuntos(filtros);
        evento.setLstAdjunto(adjuntos);
        return evento;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int registrarAgenda(AgendaCalendario agenda) {
        int idAgenda = 0;
        if (agenda.esTarea()) {
            idAgenda = guardarTarea(agenda);
            agenda.getTarea().setIdTarea(idAgenda);
        }
        if (agenda.esEvento()) {
            idAgenda = guardarEvento(agenda);
            agenda.getEvento().setIdEvento(idAgenda);
        }
        boolean tieneAdjuntos = idAgenda != ERROR_AL_GUARDAR && agenda.getListaAdjunto() != null;
        if (tieneAdjuntos) {
            configurarIdTareaEvento(idAgenda, agenda);
            registrarAdjuntos(agenda);
        }
        return idAgenda;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public LogAgenda editarEvento(AgendaCalendario agenda) throws JsonProcessingException {
        AgendaFiltro filtro = configurarFiltrosAgenda(agenda);
        EventoGeneral eventoAEditar = procesarEditarEvento(agenda, filtro);
        EventoGeneral eventoEditado = repository.obtenerEventoEDDC(agenda.getIdEvento());
        LogAgenda logEvento = generarLogEvento(agenda.getEvento().getIdPersona(), eventoAEditar, eventoEditado);
        repository.registrarLogEvento(logEvento);
        return logEvento;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int editarTarea(AgendaCalendario agenda) {
        if (!agenda.esTarea()) return 0;
        repository.editarTarea(agenda.getTarea(), agenda.getListaDetalleTarea().get(0));
        int idTarea = agenda.getTarea().getIdTarea();

        actualizarAdjuntoAgenda(idTarea, agenda);
        return idTarea;
    }

    @Override
    public LogAgenda eliminarEvento(int idEvento, int idPersona) throws JsonProcessingException {
        AgendaFiltro filtros = new AgendaFiltro();
        filtros.setIdTareaEvento(idEvento);
        filtros.setTipoAgenda(Constantes.TIPO_AGENDA_EVENTO);
        EventoGeneral eventoAEliminar = obtenerEventoEDDC(filtros);
        repository.eliminarEvento(idEvento);
        LogAgenda logEvento = generarLogEvento(idPersona, eventoAEliminar, null);
        repository.registrarLogEvento(logEvento);
        return logEvento;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int eliminarAgenda(int idAgenda) {
        return repository.eliminarTarea(idAgenda);
    }

    @Override
    public LogAgenda registrarLogTarea(LogAgenda logTarea) {
        int idLog = repository.registrarLogTarea(logTarea);
        logTarea.setIdLog(idLog);
        return logTarea;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int eliminarAdjunto(AgendaAdjunto adjunto) {
        return repository.eliminarAdjunto(adjunto.getIdArchivo());
    }

    private void configurarFechaHoraEvento(EventoGeneral eventoGeneral) {
        if (eventoGeneral.getEvento().getFechaInicio() != null && eventoGeneral.getEvento().getFechaFin() != null)
            eventoGeneral.setFechaHora(
                    new AgendaFechaHoraBuilder(
                            eventoGeneral.getEvento().getFechaInicio(), eventoGeneral.getEvento().getFechaFin()
                    ).construirFechaHora()
            );
    }

    private List<AgendaAdjunto> obtenerAdjuntos(AgendaFiltro filtros) {
        List<AgendaAdjunto> adjuntos = repository.adjuntosAgenda(filtros);
        adjuntos.forEach(adjunto -> {
            adjunto.setUrlView(s3Service.obtenerUrlArchivoPorTipo(adjunto.getNombreAlterado()));
            adjunto.setUrl(s3Service.generarUrlArchivo(adjunto.getNombreAlterado()));
        });
        return adjuntos;
    }

    private void agregarDetallesEvento(AgendaCalendario agenda) {
        agenda.getListaDetalleEvento().forEach(repository::registrarDetalleEvento);
    }

    private void registrarAdjuntos(AgendaCalendario agenda) {
        agenda.getListaAdjunto().forEach(adjunto -> {
            adjunto.setTipoAgenda(agenda.getTipoAgenda());
            repository.registrarAdjunto(adjunto);
        });
    }

    private LogAgenda generarLogEvento(int idPersona, EventoGeneral eventoAnterior, EventoGeneral nuevoEvento) throws JsonProcessingException {
        LogAgenda logEvento = new LogAgenda();
        ObjectMapper mapper = new ObjectMapper();
        String eventoModificado = nuevoEvento == null ? "" : mapper.writeValueAsString(eventoAnterior);
        logEvento.setOriginal(mapper.writeValueAsString(eventoAnterior));
        logEvento.setModificado(eventoModificado);
        logEvento.setFechaLog(new DateTime());
        logEvento.setIdTarea(eventoAnterior.getEvento().getIdEvento());
        logEvento.setTipo(Constantes.TIPO_AGENDA_EVENTO);
        logEvento.setIdDocente(idPersona);
        return logEvento;
    }

    private int guardarTarea(AgendaCalendario agenda) {
        int idTarea = repository.registrarTarea(agenda.getTarea());
        agenda.getListaDetalleTarea().forEach(detalleTarea -> {
            detalleTarea.setIdTarea(idTarea);
            repository.registrarDetalleTarea(detalleTarea);
        });
        return idTarea;
    }

    private int guardarEvento(AgendaCalendario agenda) {
        int idEvento = repository.registrarEvento(agenda.getEvento());
        agenda.getListaDetalleEvento().forEach(detalleEvento -> detalleEvento.setIdEvento(idEvento));
        agregarDetallesEvento(agenda);
        return idEvento;
    }

    private void configurarIdTareaEvento(int idAgenda, AgendaCalendario agenda) {
        agenda.getListaAdjunto().forEach(adjunto -> adjunto.setIdTareaEvento(idAgenda));
    }

    private AgendaFiltro configurarFiltrosAgenda(AgendaCalendario agenda) {
        AgendaFiltro filtro = new AgendaFiltro();
        filtro.setIdTareaEvento(agenda.getIdEvento());
        filtro.setTipoAgenda(Constantes.TIPO_AGENDA_EVENTO);
        return filtro;
    }

    private EventoGeneral procesarEditarEvento(AgendaCalendario agenda, AgendaFiltro filtro) {
        EventoGeneral eventoAEditar = repository.obtenerEventoEDDC(agenda.getIdEvento());
        configurarFechaHoraEvento(eventoAEditar);
        eventoAEditar.setIdGrados(repository.obtenerGradosEvento(agenda.getIdEvento()));
        eventoAEditar.setLstAdjunto(obtenerAdjuntos(filtro));
        repository.editarEvento(agenda.getEvento());
        repository.eliminarAdjuntos(agenda.idsArchivosEliminar());
        agenda.getListaDetalleEvento().forEach(detalleEvento -> detalleEvento.setIdEvento(agenda.getIdEvento()));
        agregarDetallesEvento(agenda);
        if (agenda.getListaAdjunto() != null)
            registrarAdjuntos(agenda);
        return eventoAEditar;
    }

    private void actualizarAdjuntoAgenda(int idAgenda, AgendaCalendario agenda) {
        boolean tieneAdjuntos = idAgenda != ERROR_AL_GUARDAR && agenda.getListaAdjunto() != null;
        if (tieneAdjuntos)
            agenda.getListaAdjunto().forEach(adjunto -> {
                adjunto.setIdTareaEvento(idAgenda);
                adjunto.setTipoAgenda(agenda.getTipoAgenda());
                repository.registrarAdjunto(adjunto);
            });
    }
}
