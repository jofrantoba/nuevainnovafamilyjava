package com.innova.nif.services.backofficebuilder;

public class EquipoDirectivoBackOfficeQuery extends BaseBackOfficeQuery {
    public static final String BASE_EQUIPO_DIRECTIVO_QUERY =
            "FROM datamanagement.gen_persona_perfil perfil " +
                    "INNER JOIN datamanagement.gen_cargo cargo ON perfil.id_cargo = cargo.id " +
                    "INNER JOIN datamanagement.gen_persona_perfil_sede perfil_sede ON perfil_sede.id_persona_perfil = perfil.id " +
                    "INNER JOIN datamanagement.gen_sede sede ON sede.id = perfil_sede.Id_sede " +
                    "INNER JOIN datamanagement.gen_persona persona ON persona.id = perfil.id_persona " +
                    "WHERE perfil.estado = 'ACT' AND cargo.tipo = 'SEDE' ";

    public static final String BASE_EQUIPO_DIRECTIVO_COUNT_QUERY =
            String.format("SELECT COUNT(perfil.id_persona) AS '%s' %s", COUNT, BASE_EQUIPO_DIRECTIVO_QUERY);

    public static final String BASE_EQUIPO_DIRECTIVO_LIST_QUERY =
            String.format("SELECT persona.id as '%s', persona.email_empresa as '%s', 'ED' as '%s' %s", PERSONA_ID, EMAIL, TIPO, BASE_EQUIPO_DIRECTIVO_QUERY);

    public static final String EQUIPO_DIRECTIVO_POR_SEDE_CLAUSE = "AND perfil_sede.Id_sede in (ids) ";
    public static final String EQUIPO_DIRECTIVO_POR_CARGOS_CLAUSE = "AND cargo.codigo in (ids) ";
}
