package com.innova.nif.services.backofficebuilder;

import com.innova.nif.models.BackOfficeParametros;

public class BackOfficeQueryDataManagementDirector {
    public String construirConsulta(IBackOfficeQueryDataManagementBuilder builder, BackOfficeParametros parametros) {
        return ((IBackOfficeQueryDataManagementBuilder) builder.buildCargos(parametros.getRoles()).buildGrados(parametros.getGrados()))
                .buildSeccion(parametros.getSecciones()).buildSedes(parametros.getSedes()).obtenerConsulta();
    }
}
