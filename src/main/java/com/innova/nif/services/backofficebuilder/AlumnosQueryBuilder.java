package com.innova.nif.services.backofficebuilder;

import com.innova.nif.models.BackOfficeParametro;

import java.util.List;
import java.util.stream.Collectors;

public class AlumnosQueryBuilder extends BaseBackOfficeDataManagementQueryBuilder implements IBackOfficeQueryDataManagementBuilder {
    private static final String PERIODO_ACTUAL = "ACT";

    public AlumnosQueryBuilder(String tipoBuilder, int idPeriodo, String estadoPeriodo, List<String> codigos) {
        this.tipoBuilder = tipoBuilder;
        consultaSql = tipoBuilder.equals(COUNT_QUERY) ? AlumnoBackOfficeQuery.BASE_ALUMNOS_COUNT_QUERY : AlumnoBackOfficeQuery.BASE_ALUMNOS_LIST_QUERY;
        consultaSql += idPeriodo + " ";
        if (!estadoPeriodo.equals(PERIODO_ACTUAL))
            consultaSql += String.format("AND alumno.id_periodo_ingreso= %s ", idPeriodo);
        if (codigos != null)
            construirConsultaCodigos(codigos);
    }

    @Override
    public IBackOfficeQueryBuilder buildSeccion(List<BackOfficeParametro> parametros) {
        if (!todosSeleccionados(parametros))
            construirConsulta(parametros, AlumnoBackOfficeQuery.ALUMNO_POR_SECCION_CLAUSE, true);
        return this;
    }

    @Override
    public IBackOfficeQueryBuilder buildCargos(List<BackOfficeParametro> parametros) {
        return this;
    }

    @Override
    public IBackOfficeQueryBuilder buildSedes(List<BackOfficeParametro> parametros) {
        if (!todosSeleccionados(parametros))
            construirConsulta(parametros, AlumnoBackOfficeQuery.ALUMNO_POR_SEDE_CLAUSE, false);
        return this;
    }

    @Override
    public IBackOfficeQueryBuilder buildGrados(List<BackOfficeParametro> parametros) {
        if (!todosSeleccionados(parametros))
            construirConsulta(parametros, AlumnoBackOfficeQuery.ALUMNO_POR_CODIGO_CLAUSE, true);
        return this;
    }

    @Override
    public String obtenerConsulta() {
        return consultaSql;
    }

    private void construirConsultaCodigos(List<String> codigos) {
        if (!codigos.isEmpty()) {
            String codigosConcatenadosConComa = codigos.stream().map(c -> String.format("'%s'", c)).collect(Collectors.joining(","));
            consultaSql = String.format("%s%s", consultaSql, AlumnoBackOfficeQuery.ALUMNO_POR_CODIGO_CLAUSE.replace("ids", codigosConcatenadosConComa));
        }
    }
}
