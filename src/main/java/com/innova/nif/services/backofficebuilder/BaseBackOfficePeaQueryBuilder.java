package com.innova.nif.services.backofficebuilder;

import com.innova.nif.models.BackOfficeParametro;
import com.innova.nif.models.BackOfficeParametros;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class BaseBackOfficePeaQueryBuilder extends BaseBackOfficeQueryBuilder {

    @Override
    boolean todosSeleccionados(List<BackOfficeParametro> parametros) {
        final Optional<BackOfficeParametro> parametro = parametros.stream().findFirst();
        return (parametro.isPresent() && parametro.get().getParametroId().equals(BackOfficeParametros.ALL_CODE))
                    || parametros.isEmpty();
    }

    protected void construirConsultaConDosSentencias(List<BackOfficeParametro> parametrosNivel,
                                                     List<BackOfficeParametro> parametrosSeccion, String firstClause,
                                                     String secondClause) {
        String idsNiveles = concatenarParametroIdConComa(parametrosNivel);
        String idsSecciones = concatenarParametroIdConComa(parametrosSeccion);
        String sentenciaNiveles = reemplazarIdsConValores(firstClause, idsNiveles);
        String sentenciaSecciones = reemplazarIdsConValores(secondClause, idsSecciones);
        String unionConsultaConOr = String.format(BackOfficeQuery.OR_CLAUSE, sentenciaNiveles, sentenciaSecciones);
        consultaSql = String.format("%s%s", consultaSql, unionConsultaConOr);
    }

    private String concatenarParametroIdConComa(List<BackOfficeParametro> parametrosNivel) {
        return parametrosNivel.stream().map(p -> String.format("'%s'", p.getParametroId())).collect(Collectors.joining(","));
    }

    private String reemplazarIdsConValores(String firstClause, String idsNiveles) {
        return firstClause.replace("ids", idsNiveles);
    }
}
