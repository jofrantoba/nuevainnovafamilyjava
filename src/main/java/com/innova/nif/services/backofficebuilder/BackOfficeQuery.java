package com.innova.nif.services.backofficebuilder;

class BackOfficeQuery {
    private BackOfficeQuery() {
    }

    static final String ID_POR_GRADO_CLAUSE = "AND ClassRoom.GradeId IN (ids) ";
    static final String OR_CLAUSE = "AND ( %s OR %s ) ";
    static final String AND_CLAUSE = "AND %s";
    static final String ID_POR_SEDE_CLAUSE = "AND School.SchoolDmCode IN (ids) ";
    static final String ID_POR_SECCION_CLAUSE = "Section.Description IN (ids) ";
    static final String ID_ALUMNO_POR_NIVEL_CLAUSE = "EnglishCodeDM IN (ids)";
    static final String CONTEO_ALUMNO_QUERY_SUFFIX = ") AS students";
    private static final String CONTEO_DOCENTES_PREFIX = "SELECT COUNT(docentes.id_persona) as count FROM ( SELECT  DISTINCT(Teacher.id_persona) as id_persona ";
    private static final String LISTA_DOCENTES_PREFIX = "SELECT DISTINCT(Teacher.id_persona) as PersonaId, Teacher.Email as Email, 'DC' as 'Tipo' ";
    static final String LIST_ID_ALUMNOS_QUERY = "SELECT DISTINCT(Student.StudentNumber) as code " +
            "FROM [dbo].[Student] as Student " +
            "INNER JOIN [dbo].StudentClassRoom as StudentClassRoom ON Student.Id = StudentClassRoom.StudentId " +
            "INNER JOIN [dbo].ClassRoom as ClassRoom ON ClassRoom.Id = StudentClassRoom.ClassRoomId " +
            "INNER JOIN [dbo].Section as Section ON Section.Id = ClassRoom.SectionId " +
            "INNER JOIN [dbo].Grade as Grade ON Grade.Id = ClassRoom.GradeId " +
            "INNER JOIN [dbo].School as School ON School.Id = ClassRoom.SchoolId " +
            "INNER JOIN [dbo].SchoolCalendar as SchoolCalendar ON SchoolCalendar.SchoolId = School.Id " +
            "INNER JOIN [dbo].Calendar as Calendar ON Calendar.Id = SchoolCalendar.CalendarId " +
            "INNER JOIN [dbo].AcademicYear as AcademicYear on AcademicYear.Id = Calendar.AcademicYearId " +
            "WHERE AcademicYear.IsActualYear = 1 AND AcademicYear.Status = 'ACT'";
    private static final String BASE_DOCENTES_QUERY = "FROM [dbo].ClassRoom as ClassRoom " +
            "INNER JOIN [dbo].ClassRoomEnglishLevel as ClassRoomEnglish ON ClassRoom.ClassRoomEnglishLevelId = ClassRoomEnglish.Id " +
            "INNER JOIN [dbo].CourseClassRoom as CourseClassRoom ON CourseClassRoom.ClassRoomId = ClassRoom.Id " +
            "INNER JOIN [dbo].Section as Section ON Section.Id = ClassRoom.SectionId " +
            "INNER JOIN [dbo].School as School ON School.Id = ClassRoom.SchoolId " +
            "INNER JOIN [dbo].TeacherClass as TeacherClass ON TeacherClass.CourseClassRoomId = CourseClassRoom.Id " +
            "INNER JOIN [dbo].Teacher as Teacher on Teacher.Id = TeacherClass.TeacherId " +
            "INNER JOIN [dbo].SchoolCalendar as SchoolCalendar ON SchoolCalendar.SchoolId = School.Id " +
            "INNER JOIN [dbo].Calendar as Calendar ON Calendar.Id = SchoolCalendar.CalendarId " +
            "INNER JOIN [dbo].AcademicYear as AcademicYear on AcademicYear.Id = Calendar.AcademicYearId  " +
            "WHERE AcademicYear.IsActualYear = 1 AND AcademicYear.Status = 'ACT' ";
    static final String CONTEO_DOCENTES_QUERY = CONTEO_DOCENTES_PREFIX + BASE_DOCENTES_QUERY;
    static final String LISTA_DOCENTES_QUERY = LISTA_DOCENTES_PREFIX + BASE_DOCENTES_QUERY;
    static final String ID_DOCENTE_POR_NIVEL_CLAUSE = "ClassRoomEnglish.ClassRoomLevelCode IN (ids)";
    static final String CONTEO_ALUMNOS_QUERY_PREFIX = "SELECT COUNT(students.code) as count FROM (";
    static final String CONTEO_DOCENTES_SUFFIX = ") as docentes";
}
