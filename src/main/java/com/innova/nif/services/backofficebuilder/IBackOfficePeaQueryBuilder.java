package com.innova.nif.services.backofficebuilder;

import com.innova.nif.models.BackOfficeParametro;

import java.util.List;

public interface IBackOfficePeaQueryBuilder extends IBackOfficeQueryBuilder {
    IBackOfficeQueryBuilder buildNivelYSeccion(List<BackOfficeParametro> parametrosNivel, List<BackOfficeParametro> parametrosSeccion);
}
