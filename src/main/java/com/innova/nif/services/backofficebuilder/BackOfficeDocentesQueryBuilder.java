package com.innova.nif.services.backofficebuilder;

import com.innova.nif.models.BackOfficeParametro;

import java.util.List;

public class BackOfficeDocentesQueryBuilder extends BaseBackOfficePeaQueryBuilder implements IBackOfficePeaQueryBuilder {

    public BackOfficeDocentesQueryBuilder(String tipo) {
        tipoBuilder = tipo;
        if (tipo.equals(COUNT_QUERY))
            consultaSql = BackOfficeQuery.CONTEO_DOCENTES_QUERY;
        if (tipo.equals(LIST_QUERY))
            consultaSql = BackOfficeQuery.LISTA_DOCENTES_QUERY;
    }

    @Override
    public IBackOfficePeaQueryBuilder buildSedes(List<BackOfficeParametro> parametros) {
        if (!todosSeleccionados(parametros))
            construirConsulta(parametros, BackOfficeQuery.ID_POR_SEDE_CLAUSE, false);
        return this;
    }

    @Override
    public IBackOfficePeaQueryBuilder buildGrados(List<BackOfficeParametro> parametros) {
        if (!todosSeleccionados(parametros))
            construirConsulta(parametros, BackOfficeQuery.ID_POR_GRADO_CLAUSE, false);
        return this;
    }

    @Override
    public IBackOfficePeaQueryBuilder buildNivelYSeccion(List<BackOfficeParametro> parametrosNivel, List<BackOfficeParametro> parametrosSeccion) {
        if (!todosSeleccionados(parametrosNivel))
            construirConsulta(parametrosNivel, String.format(BackOfficeQuery.AND_CLAUSE, BackOfficeQuery.ID_DOCENTE_POR_NIVEL_CLAUSE), true);
        return this;
    }

    @Override
    public String obtenerConsulta() {
        if (tipoBuilder.equals(COUNT_QUERY))
            return String.format("%s%s", consultaSql, BackOfficeQuery.CONTEO_DOCENTES_SUFFIX);
        return consultaSql;
    }
}
