package com.innova.nif.services.backofficebuilder;

public class AlumnoBackOfficeQuery {
    private AlumnoBackOfficeQuery() {
    }

    private static final String PERSONA_ID = "PersonaId";
    private static final String EMAIL = "Email";
    private static final String TIPO = "Tipo";
    private static final String COUNT = "count";
    private static final String BASE_ALUMNOS_QUERY = "FROM datamanagement.aca_alumno alumno " +
            "INNER JOIN datamanagement.aca_seccion seccion on alumno.id_seccion = seccion.id " +
            "INNER JOIN datamanagement.aca_grado grado on alumno.id_grado = grado.id " +
            "WHERE alumno.estado in ('MAT','MDOC','PMR') " +
            "AND alumno.id_periodo_lectivo=";
    static final String BASE_ALUMNOS_LIST_QUERY = String.format("SELECT DISTINCT alumno.id_persona as '%s', concat(alumno.codigo,'@alumnos.innovaschools.edu.pe') as '%s', 'AL' as '%s' %s",
            PERSONA_ID, EMAIL, TIPO, BASE_ALUMNOS_QUERY);
    static final String BASE_ALUMNOS_COUNT_QUERY = String.format("SELECT COUNT(alumno.id) AS '%s' %s", COUNT, BASE_ALUMNOS_QUERY);
    static final String ALUMNO_POR_CODIGO_CLAUSE = "AND alumno.codigo in (ids) ";
    static final String ALUMNO_POR_NUMERO_CODIGO_CLAUSE = "AND alumno.codigo NOT IN (ids)";
    static final String ALUMNOS_POR_GRADO_CLAUSE = "AND grado.codigo_ps IN (ids) ";
    static final String ALUMNO_POR_SECCION_CLAUSE = "AND seccion.seccion IN (ids) ";
    static final String ALUMNO_POR_SEDE_CLAUSE = "AND alumno.id_sede in (ids) ";
}
