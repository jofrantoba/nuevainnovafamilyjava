package com.innova.nif.services.backofficebuilder;

public class BaseBackOfficeQuery {
    protected BaseBackOfficeQuery() {
    }

    public static final String COUNT = "count";
    public static final String PERSONA_ID = "PersonaId";
    public static final String EMAIL = "Email";
    public static final String TIPO = "Tipo";
}
