package com.innova.nif.services.backofficebuilder;

import com.innova.nif.models.BackOfficeParametro;
import lombok.Getter;

import java.util.List;
import java.util.stream.Collectors;

public class AlumnosCodigoQueryBuilder extends BaseBackOfficeDataManagementQueryBuilder implements IBackOfficeQueryDataManagementBuilder {
    @Getter
    private boolean existeFiltroSeccion;

    public AlumnosCodigoQueryBuilder(int idPeriodo, List<String> codigos, String tipoBuilder, boolean existeFiltroSeccion) {
        this.existeFiltroSeccion = existeFiltroSeccion;
        consultaSql = AlumnoBackOfficeQuery.BASE_ALUMNOS_COUNT_QUERY;
        if (tipoBuilder.equals(LIST_QUERY))
            consultaSql = AlumnoBackOfficeQuery.BASE_ALUMNOS_LIST_QUERY;
        consultaSql += idPeriodo + " ";
        construirCodigos(codigos);
    }

    @Override
    public IBackOfficeQueryBuilder buildSeccion(List<BackOfficeParametro> parametros) {
        if (existeFiltroSeccion && !todosSeleccionados(parametros))
            construirConsulta(parametros, AlumnoBackOfficeQuery.ALUMNO_POR_SECCION_CLAUSE, true);
        return this;
    }

    @Override
    public IBackOfficeQueryBuilder buildCargos(List<BackOfficeParametro> parametros) {
        return this;
    }

    @Override
    public IBackOfficeQueryBuilder buildSedes(List<BackOfficeParametro> parametros) {
        if (existeFiltroSeccion && !todosSeleccionados(parametros))
            construirConsulta(parametros, AlumnoBackOfficeQuery.ALUMNO_POR_SEDE_CLAUSE, false);
        return this;
    }

    @Override
    public IBackOfficeQueryBuilder buildGrados(List<BackOfficeParametro> parametros) {
        if (existeFiltroSeccion && !todosSeleccionados(parametros))
            construirConsulta(parametros, AlumnoBackOfficeQuery.ALUMNOS_POR_GRADO_CLAUSE, true);
        return this;
    }

    @Override
    public String obtenerConsulta() {
        return consultaSql;
    }

    private void construirCodigos(List<String> codigos) {
        if (!codigos.isEmpty()) {
            String sentenciaSql = existeFiltroSeccion ? AlumnoBackOfficeQuery.ALUMNO_POR_NUMERO_CODIGO_CLAUSE :
                    AlumnoBackOfficeQuery.ALUMNO_POR_CODIGO_CLAUSE;
            String codigosConcatenadosPorComa = codigos.stream().map(c -> String.format("'%s'", c)).collect(Collectors.joining(","));
            consultaSql = String.format(consultaSql, sentenciaSql.replace("ids", codigosConcatenadosPorComa));
        }
    }
}
