package com.innova.nif.services.backofficebuilder;

import com.innova.nif.models.BackOfficeParametro;
import com.innova.nif.models.BackOfficeParametros;

import java.util.List;

public class BaseBackOfficeDataManagementQueryBuilder extends BaseBackOfficeQueryBuilder {
    @Override
    boolean todosSeleccionados(List<BackOfficeParametro> parametros) {
        return !parametros.isEmpty() && parametros.stream().allMatch(p -> p.getParametroId().equals(BackOfficeParametros.ALL_CODE));
    }
}
