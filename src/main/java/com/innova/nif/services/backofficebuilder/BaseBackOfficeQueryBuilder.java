package com.innova.nif.services.backofficebuilder;

import com.google.common.base.Strings;
import com.innova.nif.models.BackOfficeParametro;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

public abstract class BaseBackOfficeQueryBuilder {
    public static final String COUNT_QUERY = "CountQuery";
    public static final String LIST_QUERY = "ListQuery";

    @Getter
    protected String tipoBuilder;

    @Getter
    @Setter
    protected String consultaSql;

    abstract boolean todosSeleccionados(List<BackOfficeParametro> parametros);

    protected void construirConsulta(List<BackOfficeParametro> parametros, String clause, boolean isString) {
        if (parametros.isEmpty()) return;

        List<String> datosConsulta = isString
                ? parametros.stream().map(p -> String.format("'%s'", p.getParametroId())).collect(Collectors.toList())
                : parametros.stream().map(BackOfficeParametro::getParametroId).collect(Collectors.toList());

        var idsConcatenados = String.join(",", datosConsulta);
        consultaSql = String.format("%s%s", Strings.isNullOrEmpty(consultaSql) ? "" : consultaSql, clause.replace("ids", idsConcatenados));
    }
}
