package com.innova.nif.services.backofficebuilder;

import com.innova.nif.models.BackOfficeParametro;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

public class PadreFamiliaPorAlumnosQueryBuilder extends BaseBackOfficeDataManagementQueryBuilder
        implements IBackOfficeQueryDataManagementBuilder {

    @Getter
    @Setter
    private boolean existeFiltroSeccion;

    public PadreFamiliaPorAlumnosQueryBuilder(int idPeriodo, List<String> codigosAlumnos, String tipoBuilder,
                                              boolean existeFiltroSeccion) {
        this.existeFiltroSeccion = existeFiltroSeccion;
        this.tipoBuilder = tipoBuilder;
        consultaSql = PadreFamiliaBackOfficeQuery.BASE_PPFF_COUNT_QUERY;
        if (tipoBuilder.equals(LIST_QUERY))
            consultaSql = PadreFamiliaBackOfficeQuery.BASE_PPFF_LIST_QUERY;
        consultaSql += idPeriodo + " ";
        construirCodigosAlumnos(codigosAlumnos);
    }

    private void construirCodigosAlumnos(List<String> codigosAlumnos) {
        if (!codigosAlumnos.isEmpty()) {
            String sentenciaSql = existeFiltroSeccion ?
                    PadreFamiliaBackOfficeQuery.PPFF_EXCEPTO_CODIGOS_ALUMNOS_CLAUSE :
                    PadreFamiliaBackOfficeQuery.PPFF_POR_CODIGO_ALUMNO_CLAUSE;
            String codigosSeparadosPorComa = codigosAlumnos.stream().map(c -> String.format("'%s'", c)).collect(Collectors.joining(","));
            consultaSql = String.format("%s%s", consultaSql, sentenciaSql.replace("ids", codigosSeparadosPorComa));
        }
    }

    @Override
    public IBackOfficeQueryBuilder buildSeccion(List<BackOfficeParametro> parametros) {
        if (existeFiltroSeccion && !todosSeleccionados(parametros))
            construirConsulta(parametros, PadreFamiliaBackOfficeQuery.PPFF_POR_SECCION_CLAUSE, true);
        return this;
    }

    @Override
    public IBackOfficeQueryBuilder buildCargos(List<BackOfficeParametro> parametros) {
        return this;
    }

    @Override
    public IBackOfficeQueryBuilder buildSedes(List<BackOfficeParametro> parametros) {
        if (existeFiltroSeccion && !todosSeleccionados(parametros))
            construirConsulta(parametros, PadreFamiliaBackOfficeQuery.PPFF_POR_SEDE_CLAUSE, false);
        return this;
    }

    @Override
    public IBackOfficeQueryBuilder buildGrados(List<BackOfficeParametro> parametros) {
        if (existeFiltroSeccion && !todosSeleccionados(parametros))
            construirConsulta(parametros, PadreFamiliaBackOfficeQuery.PPFF_POR_GRADO_CLAUSE, true);
        return this;
    }

    @Override
    public String obtenerConsulta() {
        String sufijo = tipoBuilder.equals(COUNT_QUERY) ? PadreFamiliaBackOfficeQuery.BASE_PPFF_QUERY_SUFFIX : "";
        return String.format("%s%s", consultaSql, sufijo);
    }
}
