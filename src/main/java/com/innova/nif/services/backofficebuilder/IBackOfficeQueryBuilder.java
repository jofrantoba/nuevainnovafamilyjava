package com.innova.nif.services.backofficebuilder;

import com.innova.nif.models.BackOfficeParametro;

import java.util.List;

public interface IBackOfficeQueryBuilder {
    IBackOfficeQueryBuilder buildSedes(List<BackOfficeParametro> parametros);

    IBackOfficeQueryBuilder buildGrados(List<BackOfficeParametro> parametros);

    String obtenerConsulta();
}
