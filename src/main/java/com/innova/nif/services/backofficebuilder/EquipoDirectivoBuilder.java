package com.innova.nif.services.backofficebuilder;

import com.innova.nif.models.BackOfficeParametro;
import com.innova.nif.models.BackOfficeParametros;

import java.util.List;
import java.util.stream.Collectors;

public class EquipoDirectivoBuilder extends BaseBackOfficeDataManagementQueryBuilder implements IBackOfficeQueryDataManagementBuilder {
    public EquipoDirectivoBuilder(String type) {
        tipoBuilder = type;
        consultaSql = tipoBuilder.equals(COUNT_QUERY) ? EquipoDirectivoBackOfficeQuery.BASE_EQUIPO_DIRECTIVO_COUNT_QUERY : EquipoDirectivoBackOfficeQuery.BASE_EQUIPO_DIRECTIVO_LIST_QUERY;
    }

    @Override
    public IBackOfficeQueryDataManagementBuilder buildSedes(List<BackOfficeParametro> parametros) {
        if (!todosSeleccionados(parametros))
            construirConsulta(parametros, EquipoDirectivoBackOfficeQuery.EQUIPO_DIRECTIVO_POR_SEDE_CLAUSE, false);
        return this;
    }

    @Override
    public IBackOfficeQueryDataManagementBuilder buildGrados(List<BackOfficeParametro> parametros) {
        return this;
    }

    @Override
    public IBackOfficeQueryBuilder buildSeccion(List<BackOfficeParametro> parametros) {
        return this;
    }

    @Override
    public IBackOfficeQueryBuilder buildCargos(List<BackOfficeParametro> parametros) {
        if (!todosSeleccionados(parametros))
            construirConsulta(
                    parametros.stream().filter(p -> !BackOfficeParametros.rolesPea().contains(p.getParametroId())).collect(Collectors.toList()),
                    EquipoDirectivoBackOfficeQuery.EQUIPO_DIRECTIVO_POR_CARGOS_CLAUSE, true
            );
        return this;
    }

    @Override
    public String obtenerConsulta() {
        return consultaSql;
    }
}
