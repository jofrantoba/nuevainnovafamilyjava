package com.innova.nif.services.backofficebuilder;

public class PadreFamiliaBackOfficeQuery {
    private PadreFamiliaBackOfficeQuery() {

    }

    private static final String COUNT = "count";
    static final String BASE_PPFF_QUERY_SUFFIX = ") AS ppff";
    static final String PPFF_POR_SEDE_CLAUSE = "AND alumno.id_sede in (ids) ";
    static final String PPFF_POR_GRADO_CLAUSE = "AND grado.codigo_ps IN (ids) ";
    static final String PPFF_POR_SECCION_CLAUSE = "AND seccion.seccion IN (ids) ";
    static final String PPFF_EXCEPTO_CODIGOS_ALUMNOS_CLAUSE = "AND alumno.codigo NOT IN (ids) ";
    static final String PPFF_POR_CODIGO_ALUMNO_CLAUSE = "AND alumno.codigo IN (ids) ";
    private static final String PERSONA_ID = "PersonaId";
    private static final String EMAIL = "Email";
    private static final String TIPO = "Tipo";
    private static final String BASE_PPFF_QUERY = "FROM datamanagement.aca_alumno alumno " +
            "INNER JOIN datamanagement.gen_vinculo_familiar vinculo_familiar " +
            "ON vinculo_familiar.id_persona = alumno.id_persona " +
            "LEFT OUTER JOIN datamanagement.aca_seccion seccion on alumno.id_seccion = seccion.id " +
            "INNER JOIN datamanagement.aca_grado grado on alumno.id_grado = grado.id " +
            "INNER JOIN datamanagement.gen_persona persona on persona.id = vinculo_familiar.id_persona_vinculada " +
            "WHERE vinculo_familiar.estado='ACT' " +
            "AND alumno.estado in ('MAT','MDOC','PMR') " +
            "and vinculo_familiar.id_tipo_vinculo in (1,2) " +
            "AND LENGTH(COALESCE(persona.email,''))>0 " +
            "AND alumno.id_periodo_lectivo= ";
    static final String BASE_PPFF_LIST_QUERY = String.format("SELECT distinct persona.id as %s, email as %s, 'PF' as '%s' %s", PERSONA_ID, EMAIL, TIPO, BASE_PPFF_QUERY);
    static final String BASE_PPFF_COUNT_QUERY = String.format("SELECT COUNT(ppff.id_persona_vinculada) AS '%s' FROM (SELECT distinct vinculo_familiar.id_persona_vinculada %s",
            COUNT, BASE_PPFF_QUERY);
}
