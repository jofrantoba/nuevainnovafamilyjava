package com.innova.nif.services.backofficebuilder;

import com.innova.nif.models.BackOfficeParametro;

import java.util.List;

public class PadreFamiliaQueryBuilder extends BaseBackOfficeDataManagementQueryBuilder implements IBackOfficeQueryDataManagementBuilder {
    private static final String ACTUAL = "ACT";

    public PadreFamiliaQueryBuilder(String tipoBuilder, int idPeriodo, String estadoPeriodo) {
        this.tipoBuilder = tipoBuilder;
        consultaSql = tipoBuilder.equals(COUNT_QUERY) ? PadreFamiliaBackOfficeQuery.BASE_PPFF_COUNT_QUERY
                : PadreFamiliaBackOfficeQuery.BASE_PPFF_LIST_QUERY;
        consultaSql += idPeriodo + " ";
        if (!estadoPeriodo.equals(ACTUAL))
            consultaSql += String.format("AND alumno.id_periodo_ingreso= %s ", idPeriodo);
    }

    @Override
    public IBackOfficeQueryBuilder buildSeccion(List<BackOfficeParametro> parametros) {
        if (!todosSeleccionados(parametros))
            construirConsulta(parametros, PadreFamiliaBackOfficeQuery.PPFF_POR_SECCION_CLAUSE, true);
        return this;
    }

    @Override
    public IBackOfficeQueryBuilder buildCargos(List<BackOfficeParametro> parametros) {
        return this;
    }

    @Override
    public IBackOfficeQueryBuilder buildSedes(List<BackOfficeParametro> parametros) {
        if (!todosSeleccionados(parametros))
            construirConsulta(parametros, PadreFamiliaBackOfficeQuery.PPFF_POR_SEDE_CLAUSE, false);
        return this;
    }

    @Override
    public IBackOfficeQueryBuilder buildGrados(List<BackOfficeParametro> parametros) {
        if (!todosSeleccionados(parametros))
            construirConsulta(parametros, PadreFamiliaBackOfficeQuery.PPFF_POR_GRADO_CLAUSE, true);
        return this;
    }

    @Override
    public String obtenerConsulta() {
        String sufijo = tipoBuilder.equals(COUNT_QUERY) ? PadreFamiliaBackOfficeQuery.BASE_PPFF_QUERY_SUFFIX : "";
        return String.format("%s%s", consultaSql, sufijo);
    }
}
