package com.innova.nif.services.backofficebuilder;

import com.innova.nif.models.BackOfficeParametro;

import java.util.List;

public class DocentesQueryBuilder extends BaseBackOfficeDataManagementQueryBuilder implements IBackOfficeQueryDataManagementBuilder {

    public DocentesQueryBuilder(String tipoBuilder) {
        consultaSql = DocenteBackOfficeQuery.BASE_DOCENTES_QUERY;
        this.tipoBuilder = tipoBuilder;
        if (this.tipoBuilder.equals(COUNT_QUERY)) consultaSql = DocenteBackOfficeQuery.BASE_DOCENTES_COUNT_QUERY;
        if (this.tipoBuilder.equals(LIST_QUERY)) consultaSql = DocenteBackOfficeQuery.BASE_DOCENTES_LIST_QUERY;
    }

    @Override
    public IBackOfficeQueryBuilder buildSeccion(List<BackOfficeParametro> parametros) {
        if (!todosSeleccionados(parametros))
            construirConsulta(parametros, DocenteBackOfficeQuery.DOCENTE_POR_SECCION_CLAUSE, true);
        return this;
    }

    @Override
    public IBackOfficeQueryBuilder buildCargos(List<BackOfficeParametro> parametros) {
        return this;
    }

    @Override
    public IBackOfficeQueryBuilder buildSedes(List<BackOfficeParametro> parametros) {
        if (!todosSeleccionados(parametros))
            construirConsulta(parametros, DocenteBackOfficeQuery.DOCENTE_POR_SEDE_CLAUSE, false);
        return this;
    }

    @Override
    public IBackOfficeQueryBuilder buildGrados(List<BackOfficeParametro> parametros) {
        if (!todosSeleccionados(parametros))
            construirConsulta(parametros, DocenteBackOfficeQuery.DOCENTE_POR_GRADO_CLAUSE, true);
        return this;
    }

    @Override
    public String obtenerConsulta() {
        String sufijo = tipoBuilder.equals(COUNT_QUERY) ? DocenteBackOfficeQuery.BASE_DOCENTES_QUERY_SUFIX : "";
        return String.format("%s%s", consultaSql, sufijo);
    }
}
