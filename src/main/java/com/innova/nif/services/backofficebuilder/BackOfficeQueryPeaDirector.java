package com.innova.nif.services.backofficebuilder;

import com.innova.nif.models.BackOfficeParametros;

public class BackOfficeQueryPeaDirector {
    public String construirConsulta(IBackOfficePeaQueryBuilder builder, BackOfficeParametros parametros) {
        return ((IBackOfficePeaQueryBuilder) (builder.buildGrados(parametros.getGrados()).buildSedes(parametros.getSedes())))
                .buildNivelYSeccion(parametros.getNiveles(), parametros.getSecciones()).obtenerConsulta();
    }
}
