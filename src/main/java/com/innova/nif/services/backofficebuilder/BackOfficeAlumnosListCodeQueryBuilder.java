package com.innova.nif.services.backofficebuilder;

import com.innova.nif.models.BackOfficeParametro;

import java.util.List;

public class BackOfficeAlumnosListCodeQueryBuilder extends BaseBackOfficePeaQueryBuilder implements IBackOfficePeaQueryBuilder {
    public BackOfficeAlumnosListCodeQueryBuilder() {
        consultaSql = BackOfficeQuery.LIST_ID_ALUMNOS_QUERY;
    }

    @Override
    public IBackOfficePeaQueryBuilder buildSedes(List<BackOfficeParametro> parametros) {
        if (!todosSeleccionados(parametros))
            construirConsulta(parametros, BackOfficeQuery.ID_POR_SEDE_CLAUSE, false);
        return this;
    }

    @Override
    public IBackOfficePeaQueryBuilder buildGrados(List<BackOfficeParametro> parametros) {
        if (!todosSeleccionados(parametros))
            construirConsulta(parametros, BackOfficeQuery.ID_POR_GRADO_CLAUSE, true);
        return this;
    }

    @Override
    public IBackOfficePeaQueryBuilder buildNivelYSeccion(List<BackOfficeParametro> parametrosNivel, List<BackOfficeParametro> parametrosSeccion) {
        if (todosSeleccionados(parametrosNivel) && todosSeleccionados(parametrosSeccion))
            return this;
        if (todosSeleccionados(parametrosNivel) && !todosSeleccionados(parametrosSeccion))
            construirConsulta(parametrosSeccion, String.format(BackOfficeQuery.AND_CLAUSE, BackOfficeQuery.ID_POR_SECCION_CLAUSE), true);
        else if (todosSeleccionados(parametrosSeccion) && !todosSeleccionados(parametrosNivel))
            construirConsulta(parametrosNivel, String.format(BackOfficeQuery.AND_CLAUSE, BackOfficeQuery.ID_ALUMNO_POR_NIVEL_CLAUSE), true);
        else
            construirConsultaConDosSentencias(parametrosNivel, parametrosSeccion, BackOfficeQuery.ID_ALUMNO_POR_NIVEL_CLAUSE, BackOfficeQuery.ID_POR_SECCION_CLAUSE);
        return this;
    }

    @Override
    public String obtenerConsulta() {
        return consultaSql;
    }
}
