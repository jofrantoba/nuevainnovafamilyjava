package com.innova.nif.services.backofficebuilder;

import com.innova.nif.models.BackOfficeParametro;

import java.util.List;

public interface IBackOfficeQueryDataManagementBuilder extends IBackOfficeQueryBuilder {
    IBackOfficeQueryBuilder buildSeccion(List<BackOfficeParametro> parametros);

    IBackOfficeQueryBuilder buildCargos(List<BackOfficeParametro> parametros);
}
