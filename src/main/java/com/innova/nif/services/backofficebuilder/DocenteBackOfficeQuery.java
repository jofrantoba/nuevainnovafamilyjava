package com.innova.nif.services.backofficebuilder;

public class DocenteBackOfficeQuery {
    private DocenteBackOfficeQuery() {

    }

    private static final String COUNT = "count";
    private static final String PERSONA_ID = "PersonaId";
    private static final String EMAIL = "Email";
    private static final String TIPO = "Tipo";
    static final String BASE_DOCENTES_QUERY = "from aca_periodo_lectivo periodo " +
            "inner join aca_periodo_sede p_sede on periodo.id = p_sede.id_periodo_lectivo " +
            "inner join gen_sede sede on p_sede.id_sede = sede.id " +
            "inner join aca_seccion seccion on seccion.id_periodo_sede = p_sede.id " +
            "inner join aca_grado grado on grado.id = seccion.id_grado " +
            "inner join aca_curso_seccion curso_seccion on seccion.id = curso_seccion.id_seccion " +
            "inner join aca_docente_curso docente_curso on docente_curso.id_curso_seccion = curso_seccion.id " +
            "inner join aca_docente docente on docente_curso.id_docente = docente.id " +
            "inner join gen_persona persona on docente.id_persona = persona.id " +
            "where periodo.estado = 'ACT' ";
    static final String BASE_DOCENTES_COUNT_QUERY = String.format("SELECT COUNT(profesores.id_persona) AS '%s' FROM ( SELECT DISTINCT docente.id_persona %s", COUNT, BASE_DOCENTES_QUERY);
    static final String BASE_DOCENTES_LIST_QUERY = String.format("SELECT DISTINCT persona.id as '%s', persona.email_empresa as '%s', 'DC' as '%s' %s", PERSONA_ID, EMAIL, TIPO, BASE_DOCENTES_QUERY);
    static final String DOCENTE_POR_GRADO_CLAUSE = "AND grado.codigo_ps IN (ids) ";
    static final String DOCENTE_POR_SECCION_CLAUSE = "AND seccion.seccion IN (ids) ";
    static final String DOCENTE_POR_SEDE_CLAUSE = "AND sede.id IN (ids) ";
    static final String BASE_DOCENTES_QUERY_SUFIX = ") AS profesores";
}
