package com.innova.nif.services.async;

import com.innova.nif.models.Usuario;
import com.innova.nif.services.interfaces.IUsuarioService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

@Service
public class ActualizarInicioSesionAsync {
    private final Logger logger;
    private final IUsuarioService service;
    private final TaskExecutor taskExecutor;

    public ActualizarInicioSesionAsync(IUsuarioService service, TaskExecutor taskExecutor) {
        logger = LoggerFactory.getLogger(this.getClass());
        this.service = service;
        this.taskExecutor = taskExecutor;
    }

    public void ejecutar(Usuario usuario) {
        taskExecutor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    int respuesta = service.actualizarInicioSesion(usuario);
                    logger.info(String.format("Se actualizó el inicio de sesión con éxito (código=%s)", respuesta));
                } catch (Exception ex) {
                    logger.error(String.format("Error en el método asíncrono: %s.%s", this.getClass(), "run"), ex);
                }
            }
        });
    }
}
