package com.innova.nif.services.async;

import com.innova.nif.models.AgendaCalendario;
import com.innova.nif.services.AlertaAgendaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class RegistroAlertaAgendaAsync {
    private final Logger logger;
    private final AlertaAgendaService service;
    private final TaskExecutor taskExecutor;

    public RegistroAlertaAgendaAsync(AlertaAgendaService service, TaskExecutor taskExecutor) {
        logger = LoggerFactory.getLogger(this.getClass());
        this.service = service;
        this.taskExecutor = taskExecutor;
    }

    public void ejecutar(AgendaCalendario agenda) {
        taskExecutor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    service.registrar(agenda);
                } catch (IOException ex) {
                    logger.error(String.format("Error en el método asíncrono: %s.%s", this.getClass(), "run"), ex);
                }
            }
        });
    }
}
