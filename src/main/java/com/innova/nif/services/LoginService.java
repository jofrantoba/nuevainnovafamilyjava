package com.innova.nif.services;

import com.innova.nif.models.PeriodoLectivo;
import com.innova.nif.models.Sede;
import com.innova.nif.models.Usuario;
import com.innova.nif.repositories.factories.usuario.ILoginFactory;
import com.innova.nif.repositories.interfaces.IPeriodoRepository;
import com.innova.nif.repositories.interfaces.IUserRepository;
import com.innova.nif.services.interfaces.ILoginService;
import com.innova.nif.utils.Constantes;
import javassist.NotFoundException;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.*;
import java.util.Optional;

@Service
public class LoginService implements ILoginService {
    @Value("${dominioAlumno}")
    private String dominioAlumno;

    private final IPeriodoRepository periodoRepository;
    private final IUserRepository userRepository;
    private final ILoginFactory loginFactory;

    public LoginService(IPeriodoRepository periodoRepository, IUserRepository userRepository,
                        ILoginFactory loginFactory) {
        this.loginFactory = loginFactory;
        this.periodoRepository = periodoRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List obtenerUsuarioDm(String correo, int periodo, String tipo) {
        return loginFactory.getRepository(tipo).obtenerDatos(correo, periodo);
    }

    public Usuario obtenerUsuarioDm(String email) throws NotFoundException {
        Usuario usuarioDM;
        boolean esDominioAlumno = email.split("@")[1].equalsIgnoreCase(dominioAlumno);
        PeriodoLectivo periodoActivo = periodoRepository.getPeriodoActivo();
        String tipoConsulta = obtenerTipoConsulta(esDominioAlumno);
        List<Usuario> usuariosDM = loginFactory.getRepository(tipoConsulta)
                .obtenerDatos(email, periodoActivo.getIdPeriodo());
        if (usuariosDM.isEmpty())
            throw new NotFoundException("No encontramos ninguna cuenta asociada a este correo. Por favor comunícate con tu sede.");
        if (tipoConsulta.equals(Constantes.CONSULTA_INNOVA))
            usuarioDM = obtenerUsuarioInnova(usuariosDM);
        else
            usuarioDM = usuariosDM.get(0);
        if (usuarioDM == null)
            throw new NotFoundException("Usuario inexistente en NIF.");
        usuarioDM.setPeriodoLectivo(periodoActivo);
        return usuarioDM;
    }
    private Usuario obtenerUsuarioInnova(List<Usuario> usuariosDM) {
        Usuario usuarioDM = usuariosDM.get(0);
        usuarioDM.setSedes(obtenerSedesDeUsuario(usuarioDM, usuariosDM));
        usuarioDM.setCargo(generarCargosUsuarios(usuariosDM));
        return usuarioDM;
    }

    public List<Sede> obtenerSedesDeUsuario(Usuario usuario, List<Usuario> usuariosDM) {
        Map<Integer, Sede> diccionarioSedes = new HashMap<>();
        usuariosDM.forEach(u -> {
            Sede sede = new Sede(u.getIdSede(), u.getNombreSede());
            if (sede.getId() == usuario.getIdSede())
                sede.setSelected(true);
            diccionarioSedes.putIfAbsent(sede.getId(), sede);
        });
        return new ArrayList<>(diccionarioSedes.values());
    }

    public String generarCargosUsuarios(List<Usuario> usuariosDM) {
        LinkedHashSet<String> cargos = new LinkedHashSet<>();
        usuariosDM.forEach(f -> cargos.add(f.getCargo()));
        return StringUtils.join(cargos, " | ");
    }

    @Override
    public Usuario obtenerUsuarioPFDM(String correo) throws NotFoundException {
        Usuario usuario;
        List<PeriodoLectivo> periodosPF = periodoRepository.obtenerPeriodosPF();
        PeriodoLectivo periodoActual = obtenerPeriodoPorTipo(periodosPF, Constantes.PERIODO_LECTIVO_ACTUAL);
        if (periodoActual == null)
            throw new NotFoundException("No hay Periodo Lectivo Activo Actual");
        PeriodoLectivo periodoPosterior = obtenerPeriodoPorTipo(periodosPF, Constantes.PERIODO_LECTIVO_POSTERIOR);
        if (periodoPosterior == null) {
            usuario = obtenerPF(
                    loginFactory.getRepository(Constantes.CONSULTA_PADRE_FAMILIA)
                            .obtenerDatos(correo, periodoActual.getIdPeriodo())
            );
            usuario.setPeriodoLectivo(periodoActual);
            return usuario;
        }
        usuario = userRepository.obtenerUsuarioDm(correo, periodoActual.getIdPeriodo(), periodoPosterior.getIdPeriodo());
        usuario.setPeriodoLectivo(usuario.getIdPeriodoLectivo() == periodoActual.getIdPeriodo() ? periodoActual : periodoPosterior);
        return usuario;
    }

    @Override
    public Usuario obtenerUsuarioPFNIF(Usuario usuarioDM) throws NotFoundException, ParseException {
        return userRepository.usuarioInformacionRecuperarContrasena(usuarioDM.getIdPersona(), Constantes.CONSULTA_PADRE_FAMILIA);
    }


    private PeriodoLectivo obtenerPeriodoPorTipo(List<PeriodoLectivo> periodos, String tipo) {
        Optional<PeriodoLectivo> periodo = periodos.stream().filter(f -> f.getIdPeriodo() > 0 && f.getEstado().equals(tipo)).findFirst();
        return periodo.orElse(null);
    }

    private Usuario obtenerPF(List<Usuario> usuarios) throws NotFoundException {
        if (usuarios.isEmpty())
            throw new NotFoundException("No encontramos ninguna cuenta asociada a este correo. Por favor comunícate con tu sede.");
        return usuarios.get(0);
    }

    private String obtenerTipoConsulta(boolean esDominioAlumno) {
        if (esDominioAlumno)
            return Constantes.CONSULTA_ALUMNO;
        return Constantes.CONSULTA_INNOVA;
    }
}
