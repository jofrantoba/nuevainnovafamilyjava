package com.innova.nif.services;

import com.innova.nif.models.HijoApoderado;
import com.innova.nif.repositories.interfaces.IApoderadoRepository;
import com.innova.nif.services.interfaces.IApoderadoService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ApoderadoService implements IApoderadoService {
    private final IApoderadoRepository repository;

    public ApoderadoService(IApoderadoRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<HijoApoderado> obtenerHijos(int idPersonaPadre, int idPeriodo) {
        return repository.obtenerHijos(idPersonaPadre, idPeriodo);
    }
}
