package com.innova.nif.services;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.innova.nif.models.*;
import com.innova.nif.repositories.interfaces.IAlertaAgendaRepository;
import com.innova.nif.services.interfaces.IAlertaAgendaService;
import com.innova.nif.services.interfaces.IUsuarioService;
import com.innova.nif.services.utils.RequestSender;
import com.innova.nif.services.utils.XMLUsuarioAlertaEvento;
import com.innova.nif.services.utils.XMLUsuarioAlertaTarea;
import com.innova.nif.utils.Constantes;
import com.innova.nif.utils.RespuestaApi;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AlertaAgendaService implements IAlertaAgendaService {
    @Value("${urlServiceCalendarioApp}")
    private String urlServiceCalendarioApp;
    @Value("${resourceEventoApp}")
    private String resourceEventoApp;
    @Value("${resourceTareaApp}")
    private String resourceTareaApp;

    protected final Logger logger;
    private final IAlertaAgendaRepository repository;
    private final IUsuarioService usuarioService;

    public AlertaAgendaService(IAlertaAgendaRepository repository, IUsuarioService usuarioService) {
        this.repository = repository;
        this.usuarioService = usuarioService;
        logger = LoggerFactory.getLogger(this.getClass());
    }

    @Override
    public void registrar(AgendaCalendario agenda) throws IOException {
        if (agenda.esTarea())
            procesarAlertaTarea(agenda.getTarea(), agenda.getTipoAgenda(), agenda.getListaDetalleTarea());
        if (agenda.esEvento())
            procesarAlertaEvento(agenda.getEvento(), agenda.getTipoAgenda(), agenda.getListaDetalleEvento());
        enviarNotificacionCalendario(agenda.getTipoAgenda(), agenda.getTarea(), agenda.getEvento());
    }

    private void procesarAlertaTarea(TareaCalendario tarea, String tipoAgenda, List<DetalleTarea> listaDetalleTarea) {
        int idSede = listaDetalleTarea.get(0).getIdSede();
        List<AlertaAgenda> alertas = generarAlertaAgenda(tarea, tipoAgenda, listaDetalleTarea);
        List<AlertaUsuario> userAlertas = usuarioService.listarAlertasPorUsuarioTarea(idSede, tarea.getIdPeriodo(), generarIdGrados(listaDetalleTarea));
        if (userAlertas != null) {
            alertas.forEach(alerta -> alerta.setListaUserAlerta(alertasUsuarioPorGradoSeccion(userAlertas, alerta)));
            registrarAlertaTarea(alertas);
        }
    }

    private void procesarAlertaEvento(EventoCalendario evento, String tipoAgenda, List<DetalleEvento> listaDetalleEvento) {
        int idSede = listaDetalleEvento.get(0).getIdSede();
        AlertaAgenda alerta = generarAlertaAgenda(evento, tipoAgenda, idSede);
        List<AlertaUsuario> userAlertas = usuarioService.listarAlertasPorUsuarioEvento(idSede, evento.getIdPeriodo(),
                generarIdGrados(evento, listaDetalleEvento));
        if (userAlertas != null) {
            alerta.setListaUserAlerta(userAlertas);
            registroAlertaEvento(alerta);
        }
    }

    private void enviarNotificacionCalendario(String tipoAgenda, TareaCalendario tarea, EventoCalendario evento) throws IOException {
        String url = urlServiceCalendarioApp;
        List<NameValuePair> urlParameters = new ArrayList<>();
        if (tipoAgenda.equals(Constantes.TIPO_AGENDA_EVENTO)) {
            url += resourceEventoApp;
            urlParameters.add(new BasicNameValuePair("idEvento", String.valueOf(evento.getIdEvento())));
            urlParameters.add(new BasicNameValuePair("idPeriodo", String.valueOf(evento.getIdPeriodo())));
            urlParameters.add(new BasicNameValuePair("idPersona", String.valueOf(evento.getIdPersona())));
            urlParameters.add(new BasicNameValuePair("grupo", Constantes.PERFIL_EQUIPO_DIRECTIVO));
            urlParameters.add(new BasicNameValuePair("idRegistro", String.valueOf(evento.getIdEvento())));
        } else {
            url += resourceTareaApp;
            urlParameters.add(new BasicNameValuePair("idTarea", String.valueOf(tarea.getIdTarea())));
            urlParameters.add(new BasicNameValuePair("idPeriodo", String.valueOf(tarea.getIdPeriodo())));
            urlParameters.add(new BasicNameValuePair("idPersona", String.valueOf(tarea.getIdPersona())));
            urlParameters.add(new BasicNameValuePair("grupo", Constantes.PERFIL_DOCENTE));
            urlParameters.add(new BasicNameValuePair("idRegistro", String.valueOf(tarea.getIdTarea())));
        }
        HttpResponse respuesta = new RequestSender(url).makePost(urlParameters);
        if (respuesta.getStatusLine().getStatusCode() == HttpStatus.OK.value()) {
            ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            RespuestaApi respuestaApi = objectMapper.readValue(respuesta.getEntity().getContent(), RespuestaApi.class);
            String jsonData = respuestaApi.getData().toString();
            logger.info(jsonData);
        }
    }

    public String generarIdGrados(List<DetalleTarea> listaDetalleTarea) {
        HashSet<Integer> grados = new HashSet<>();
        listaDetalleTarea.forEach(d -> grados.add(d.getIdGrado()));
        StringBuilder builder = new StringBuilder().append(",");
        grados.forEach(id -> builder.append(id).append(","));
        return builder.toString();
    }

    private String generarIdGrados(EventoCalendario evento, List<DetalleEvento> listaDetalleEvento) {
        if (evento.getValorRegistro() != null && !evento.getValorRegistro().isBlank() && !evento.getValorRegistro().isEmpty())
            return "";
        return listaDetalleEvento.stream().map(d -> String.valueOf(d.getIdGrado())).collect(Collectors.joining(","));
    }

    private List<AlertaUsuario> alertasUsuarioPorGradoSeccion(List<AlertaUsuario> userAlertas, AlertaAgenda alertaAgenda) {
        return userAlertas.stream().filter(u -> u.getIdGrado() == alertaAgenda.getIdGrado() &&
                (u.getSeccion().equals(alertaAgenda.getSeccion()) || u.getNivelIngles().equals(alertaAgenda.getSeccion()))
        ).collect(Collectors.toList());
    }

    private void registrarAlertaTarea(List<AlertaAgenda> alertas) {
        alertas.forEach(alerta -> {
            try {
                int idAlerta = repository.registrarAlertaAgenda(alerta);
                String xml = new XMLUsuarioAlertaTarea(alerta.getListaUserAlerta(), idAlerta).convertirAString();
                repository.registrarAlertaAgendaUsuarioTareaXML(xml);
            } catch (TransformerException | ParserConfigurationException ex) {
                logger.error(String.format("Ocurrió un error en el método: %s.%s", this.getClass(), "registrarAlertaTarea"), ex);
            }
        });
    }

    private void registroAlertaEvento(AlertaAgenda alerta) {
        try {
            int idAlerta = repository.registrarAlertaAgenda(alerta);
            String xml = new XMLUsuarioAlertaEvento(alerta.getListaUserAlerta(), idAlerta).convertirAString();
            repository.registrarAlertaAgendaUsuarioEventoXML(xml);
        } catch (TransformerException | ParserConfigurationException ex) {
            logger.error(String.format("Ocurrió un error en el método: %s.%s", this.getClass(), "registroAlertaEvento"), ex);
        }
    }

    private List<AlertaAgenda> generarAlertaAgenda(TareaCalendario tarea, String tipoAgenda, List<DetalleTarea> listaDetalleTarea) {
        List<AlertaAgenda> alertasAgenda = new ArrayList<>();
        listaDetalleTarea.forEach(detalle -> {
            AlertaAgenda alertaAgenda = new AlertaAgenda();
            alertaAgenda.setIdTareaEvento(tarea.getIdTarea());
            alertaAgenda.setIdDetalle(detalle.getIdDetalleTarea());
            alertaAgenda.setTipo(tipoAgenda);
            alertaAgenda.setTitulo(tarea.getTitulo());
            alertaAgenda.setIdPeriodo(tarea.getIdPeriodo());
            alertaAgenda.setIdSede(detalle.getIdSede());
            alertaAgenda.setIdSede(detalle.getIdSede());
            alertaAgenda.setFechaEntrega(tarea.getFechaEntrega());
            alertaAgenda.setColor(tarea.getColor());
            alertaAgenda.setIdGrado(detalle.getIdGrado());
            alertaAgenda.setCodigoGrado(detalle.getCodigoGrado());
            alertaAgenda.setCodigoGrado(detalle.getCodigoGrado());
            alertaAgenda.setSeccion(detalle.getSeccion());
            alertaAgenda.setNombreCurso(detalle.getNombreCurso());
            alertasAgenda.add(alertaAgenda);
        });
        return alertasAgenda;
    }

    private AlertaAgenda generarAlertaAgenda(EventoCalendario evento, String tipoAgenda, int idSede) {
        AlertaAgenda agenda = new AlertaAgenda();
        agenda.setIdTareaEvento(evento.getIdEvento());
        agenda.setTipo(tipoAgenda);
        agenda.setTitulo(evento.getTitulo());
        agenda.setIdPeriodo(evento.getIdPeriodo());
        agenda.setIdSede(idSede);
        agenda.setFechaEntrega(evento.getFechaInicio());
        agenda.setColor(evento.getColor());
        agenda.setUbicacion(evento.getUbicacion());
        return agenda;
    }
}
