package com.innova.nif.services.utils;

import com.google.common.base.Strings;
import com.innova.nif.models.MensajeApp;
import com.innova.nif.models.MensajeDestino;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.util.Map;
import java.util.TreeMap;

public class XMLMensaje {
    MensajeApp mensajeApp;
    private Element elementoPrincipal;
    Document documento;
    DOMSource xml;

    public XMLMensaje(MensajeApp mensajeApp) throws ParserConfigurationException {
        this.mensajeApp = mensajeApp;
        DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
        documentFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
        DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
        documento = documentBuilder.newDocument();
        documento.setXmlStandalone(true);
    }

    public String convertirAString() throws TransformerException {
        generarXML();
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        transformerFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        StringWriter writer = new StringWriter();
        transformer.transform(new DOMSource(documento), new StreamResult(writer));
        return writer.getBuffer().toString();
    }

    private void generarXML() {
        crearElementoPrincipal("BEMensajeDestino");
        TreeMap<String, String> configuracionNodo = new TreeMap<>();
        int secuencia = 1;
        for (MensajeDestino mensaje : mensajeApp.getMensajeDestino()) {
            configuracionNodo.put("IdMensaje", String.valueOf(mensajeApp.getIdMensaje()));
            configuracionNodo.put("IdSecuencia", String.valueOf(secuencia++));
            configuracionNodo.put("IdDestino", String.valueOf(mensaje.getDestinatario().getCodigoPersona()));
            configuracionNodo.put("IdTipoDestino", mensaje.getIdTipoDestino());
            configuracionNodo.put("Leido", String.valueOf(mensaje.isLeido()));
            configuracionNodo.put("EsFavorito", String.valueOf(mensaje.isEsFavorito()));
            configuracionNodo.put("EsEliminado", String.valueOf(mensaje.isEsEliminado()));
            configuracionNodo.put("Grupo", mensaje.getDestinatario().getGrupo());
            configuracionNodo.put("AlertaRevisada", String.valueOf(mensaje.isLeido()));
            configuracionNodo.put("Correo", mensaje.getDestinatario().getEmail());
            configuracionNodo.put("GradoSeccion", mensaje.getGradoSeccion());
            crearNodo("Destinatario", configuracionNodo, "IdMensajeDestino",
                    String.valueOf(mensaje.getIdMensajeDestino()));
        }
        this.xml = new DOMSource(documento);
    }

    private void crearElementoPrincipal(String nombreEtiqueta) {
        elementoPrincipal = documento.createElement(nombreEtiqueta);
        documento.appendChild(elementoPrincipal);
    }

    void crearNodo(String nombreNodo, Map<String, String> elementoValor, String nombreAtributoNodo, String valorAtributoNodo) {
        Element nodo = documento.createElement(nombreNodo);
        if (!Strings.isNullOrEmpty(nombreAtributoNodo) && !Strings.isNullOrEmpty(valorAtributoNodo))
            nodo.setAttribute(nombreAtributoNodo, valorAtributoNodo);
        elementoPrincipal.appendChild(nodo);
        elementoValor.keySet().forEach(k -> crearElemento(k, elementoValor.get(k), nodo));
    }

    private void crearElemento(String nombreEtiqueta, String valor, Element elemento) {
        Element elementoActual = documento.createElement(nombreEtiqueta);
        elementoActual.appendChild(documento.createTextNode(valor));
        elemento.appendChild(elementoActual);
    }
}
