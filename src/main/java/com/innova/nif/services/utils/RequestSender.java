package com.innova.nif.services.utils;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.util.List;

public class RequestSender {
    private String url;
    private CloseableHttpClient client;

    public RequestSender(String url) {
        this.url = url;
        this.client = HttpClientBuilder.create().build();
    }

    public HttpResponse makePost(List<NameValuePair> urlParameters) throws IOException {
        HttpPost httpPost = new HttpPost(url);
        httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
        httpPost.setEntity(new UrlEncodedFormEntity(urlParameters, "UTF-8"));
        return client.execute(httpPost);
    }

    public HttpResponse makePut(List<NameValuePair> urlParameters) throws IOException {
        HttpPut httpPut = new HttpPut(url);
        httpPut.setHeader("Content-Type", "application/x-www-form-urlencoded");
        httpPut.setEntity(new UrlEncodedFormEntity(urlParameters, "UTF-8"));
        return client.execute(httpPut);
    }
}
