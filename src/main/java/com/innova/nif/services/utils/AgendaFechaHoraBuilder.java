package com.innova.nif.services.utils;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.Locale;

public class AgendaFechaHoraBuilder {
    DateTime fechaHoraInicio;
    DateTime fechaHoraFin;
    String horaInicioFormateado;
    String horaFinFormateado;
    boolean horaInicioEsMedianoche;
    boolean horaFinEsMedianoche;

    public AgendaFechaHoraBuilder(DateTime fechaHoraInicio, DateTime fechaHoraFin) {
        this.fechaHoraInicio = fechaHoraInicio;
        this.fechaHoraFin = fechaHoraFin;
        this.horaInicioFormateado = DateTimeFormat.forPattern("HH:mm").print(fechaHoraInicio);
        this.horaFinFormateado = DateTimeFormat.forPattern("HH:mm").print(fechaHoraFin);
        this.horaInicioEsMedianoche = fechaHoraInicio.getHourOfDay() == 0 && fechaHoraInicio.getMinuteOfDay() == 0 &&
                fechaHoraInicio.getSecondOfDay() == 0;
        this.horaFinEsMedianoche = fechaHoraFin.getHourOfDay() == 0 && fechaHoraFin.getMinuteOfDay() == 0 &&
                fechaHoraFin.getSecondOfDay() == 0;
    }

    public String construirFechaHora() {
        return new StringBuilder().append(construirFecha()).append(construirHora()).toString();
    }

    public String construirFecha() {
        if (fechaHoraInicio.toLocalDate().equals(fechaHoraFin.toLocalDate()))
            return formatearFecha("EEEEE d MMMM", fechaHoraInicio, true);
        return new StringBuilder().append("Del ")
                .append(formatearFecha("dd/MM", fechaHoraInicio, false)).append(" al ")
                .append(formatearFecha("dd/MM", fechaHoraFin, false)).toString();
    }

    public String construirHora() {
        if (horaInicioEsMedianoche && horaFinEsMedianoche)
            return "";
        if (!horaInicioEsMedianoche && horaFinEsMedianoche || horaInicioFormateado.equals(horaFinFormateado))
            return new StringBuilder().append(", a partir de la(s) ").append(horaInicioFormateado).toString();
        return !horaInicioEsMedianoche ? new StringBuilder().append(", de ").append(horaInicioFormateado).append(" a ")
                .append(horaFinFormateado).toString() : "";
    }

    private String formatearFecha(String patron, DateTime fecha, boolean aplicarEsPeFormato) {
        return aplicarEsPeFormato ?
                DateTimeFormat.forPattern(patron).withLocale(new Locale("es", "pe")).print(fecha) :
                DateTimeFormat.forPattern(patron).print(fecha);
    }
}
