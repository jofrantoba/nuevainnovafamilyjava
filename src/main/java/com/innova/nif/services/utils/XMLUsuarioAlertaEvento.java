package com.innova.nif.services.utils;

import com.innova.nif.models.AlertaUsuario;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.dom.DOMSource;
import java.util.List;
import java.util.TreeMap;

public class XMLUsuarioAlertaEvento extends XMLUsuarioAlerta {
    public XMLUsuarioAlertaEvento(List<AlertaUsuario> alertaUsuarios, int idAlerta) throws ParserConfigurationException {
        super(alertaUsuarios, idAlerta);
    }

    @Override
    public void generarXML() {
        crearElementoPrincipal("BEUserAlerta");
        TreeMap<String, String> configuracionNodo = new TreeMap<>();
        alertaUsuarios.forEach(userAlerta -> {
            configuracionNodo.put("IdAlertaAgenda", String.valueOf(idAlerta));
            configuracionNodo.put("IdPersona", String.valueOf(userAlerta.getIdPersona()));
            configuracionNodo.put("Perfil", userAlerta.getPerfil());
            crearNodo("Usuario", configuracionNodo);
        });
        this.xml = new DOMSource(documento);
    }
}
