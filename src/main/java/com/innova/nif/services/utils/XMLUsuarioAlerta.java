package com.innova.nif.services.utils;

import com.innova.nif.models.AlertaUsuario;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;

public abstract class XMLUsuarioAlerta {
    List<AlertaUsuario> alertaUsuarios;
    int idAlerta;
    private Element elementoPrincipal;
    Document documento;
    DOMSource xml;

    XMLUsuarioAlerta(List<AlertaUsuario> alertaUsuarios, int idAlerta) throws ParserConfigurationException {
        this.alertaUsuarios = alertaUsuarios;
        this.idAlerta = idAlerta;
        DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
        documentFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
        DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
        documento = documentBuilder.newDocument();
        documento.setXmlStandalone(true);
    }

    public abstract void generarXML();

    public String convertirAString() throws TransformerException {
        generarXML();
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        transformerFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        StringWriter writer = new StringWriter();
        transformer.transform(new DOMSource(documento), new StreamResult(writer));
        return writer.getBuffer().toString();
    }

    void crearElementoPrincipal(String nombreEtiqueta) {
        elementoPrincipal = documento.createElement(nombreEtiqueta);
        documento.appendChild(elementoPrincipal);
    }

    void crearNodo(String nombreNodo, Map<String, String> elementoValor) {
        Element usuario = documento.createElement(nombreNodo);
        elementoPrincipal.appendChild(usuario);
        elementoValor.keySet().forEach(k -> crearElemento(k, elementoValor.get(k), usuario));
    }

    private void crearElemento(String nombreEtiqueta, String valor, Element elemento) {
        Element elementoActual = documento.createElement(nombreEtiqueta);
        elementoActual.appendChild(documento.createTextNode(valor));
        elemento.appendChild(elementoActual);
    }
}
