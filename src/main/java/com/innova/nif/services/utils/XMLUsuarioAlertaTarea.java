package com.innova.nif.services.utils;

import com.innova.nif.models.AlertaUsuario;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.dom.DOMSource;
import java.util.List;
import java.util.TreeMap;

public class XMLUsuarioAlertaTarea extends XMLUsuarioAlerta {
    public XMLUsuarioAlertaTarea(List<AlertaUsuario> alertaUsuarios, int idAlerta) throws ParserConfigurationException {
        super(alertaUsuarios, idAlerta);
    }

    @Override
    public void generarXML() {
        crearElementoPrincipal("BEUserAlerta");
        TreeMap<String, String> configuracionNodo = new TreeMap<>();
        alertaUsuarios.forEach(userAlerta -> {
            configuracionNodo.put("IdAlertaAgenda", String.valueOf(idAlerta));
            configuracionNodo.put("IdPersona", String.valueOf(userAlerta.getIdPersona()));
            configuracionNodo.put("Perfil", userAlerta.getPerfil());
            configuracionNodo.put("IdPersonaAlumno", String.valueOf(userAlerta.getIdPersonaAlumno()));
            configuracionNodo.put("InicialesAlumno", userAlerta.getInicialesAlumno());
            crearNodo("Usuario", configuracionNodo);
        });
        this.xml = new DOMSource(documento);
    }
}
